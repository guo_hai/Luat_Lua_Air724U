## 一、介绍
CORE固件是C语言编写的Lua虚拟机运行环境，基于官方的Lua5.1版本，增加了大量符合蜂窝通信模组的新特性。
## 二、如何获取CORE固件
### 2.1，使用官方发布的固件

[参考Luat版本发布说明](https://doc.openluat.com/article/1334)


#### 发布说明:

见同级目录下的:
**Release_Notes_LuatOS-Air_ASR8910_VXXXX.xls 文件**
    

### 2.2，根据功能要求，在线定制固件
在线定制固件是根据`CORE`已经支持的功能列表，按照客户产品功能需求进行在线固件定制，在线定制最大限度的保持了lua运行的空间，理论上和官方发布的固件一样稳定，而且和官方固件一样支持更新和FOTA升级，具体介绍和使用参考[可选编译使用说明。](https://doc.openluat.com/article/2728)

## 三、下载固件
下载固件通过Luatools工具，选择不同途径获取的文件，解压后选择`.pac`文件，再选择对应的lua脚本进行下载。[具体步骤，参考Luatools使用手册](https://doc.openluat.com/wiki/3?wiki_page_id=701)