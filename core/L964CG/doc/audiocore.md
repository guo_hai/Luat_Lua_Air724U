@[TOC]

# audiocore
audio支持库

<font color='red'>注意：8910和1603差异点：</font>

|      | 平台 | 播放            | 流播放          | 录音            | 流录音          |
| ---- | ---- | --------------- | --------------- | --------------- | --------------- |
| 8910 | RDA  | amr,mp3,pcm,wav | amr,mp3,pcm,wav | amr,pcm,wav     | amr,pcm,wav     |
| 1603 | ASR  | amr,mp3,wav     | amr,mp3,wav     | amr,mp3,wav,pcm | amr,mp3,wav,pcm |



## audiocore.play(audioFilePath)	

播放音频文件,音频文件格式支持：mp3、amr

**参数**

|参数|类型|释义|取值|
|-|-|-|-|
|audioFilePath|string|音频文件的完整路径|如果是Luatools烧录的音频文件xxx.mp3,则文件完整路径为"lua/xxx.mp3"如果是sd卡中根目录下的音频文件yyy.mp3,则文件完整路径为"sdcard0/yyy.mp3"其余路径根据实际值传入即可|

**返回值**

|返回值|类型|释义|取值|
|-|-|-|-|
|result|bool|result,返回同步播放结果|false表示不允许播放true表示允许播放,并且已经去执行播放动作;异步播放结果通过消息rtos.MSG_AUDIO通知到Lua脚本,消息携带参数msg.play_end_ind，true表示播放成功结束,其余值表示播放失败|

**例子**

```lua
--local function audioMsg(msg)  
    log.info("audio.MSG_AUDIO",msg.play_end_ind)  
    sys.publish("LIB_AUDIO_PLAY_IND","RESULT",msg.play_end_ind)  
end  

--注册core上报的rtos.MSG_AUDIO消息的处理函数  
rtos.on(rtos.MSG_AUDIO,audioMsg)  
audiocore.play("/lua/call.mp3")  

```

---



## audiocore.playdata(audioData,audioFormat,[audioLoop])

功能：播放音频数据音频数据格式支持：mp3、amr

**参数**

|参数|类型|释义|取值|
|-|-|-|-|
|audioData|string|音频数据内容|   |
|audioFormat|number|音频数据格式|audiocore.MP3<br/>audiocore.WAV<br/>audiocore.AMR<br/>audiocore.PCM<br/>audiocore.SPX|
|audioLoop|bool|是否循环播放|可选参数,默认不循环,true表示循环播放false或者nil表示仅播放一次,不循环播放|

**返回值**

|返回值|类型|释义|取值|
|-|-|-|-|
|result|bool|返回同步播放结果|false表示不允许播放<br/>true表示允许播放，并且已经去执行播放动作；异步播放结果通过消息rtos.MSG_AUDIO通知到Lua脚本，消息携带参数msg.play_end_ind，true表示播放成功结束，其余值表示播放失败|

**例子**

```lua
--local function audioMsg(msg)  
    log.info("audio.MSG_AUDIO",msg.play_end_ind)  
    sys.publish("LIB_AUDIO_PLAY_IND","RESULT",msg.play_end_ind)  
end  
--注册core上报的rtos.MSG_AUDIO消息的处理函数  
rtos.on(rtos.MSG_AUDIO,audioMsg)  
audiocore.play(io.readFile("/lua/call.mp3"),audiocore.MP3)  

```
---

## audiocore.streamplay(audioFormat,audioData[,audiotype])

功能：流式播放音频数据<br/>音频数据格式支持：mp3、amr

**参数**

|参数|类型|释义|取值|
|-|-|-|-|
|audioFormat|number|音频数据格式|audiocore.MP3<br/>audiocore.WAV<br/>audiocore.AMR<br/>audiocore.PCM<br/>audiocore.SPX<br/>|
|audioData|string|音频数据内容|   |
|audiotype|number|音频播放类型|audiocore.PLAY_LOCAL<br/>audiocore.PLAY_VOLTE|

**返回值**

|返回值|类型|释义|取值|
|-|-|-|-|
|acceptedAudioDataLen|number|接受的音频数据长度|    |

**例子**

```lua
[[注意事项：流式播放音频数据时，在core中有一个4K字节的缓冲区，用来存放音频数据，调用audiocore.streamplay接口时，
音频数据被填充到这个缓冲区内，被填充的最大长度为缓冲区的剩余字节数；例如缓冲区还剩1000字节可以填充，
如果此时调用audiocore.streamplay填充3000字节数据，则实际只能将这3000字节数据的前1000字节填充到缓冲区，
返回值acceptedAudioDataLen的值为1000，表示填充的字节数，剩余的2000字节被丢弃]]
local tBuffer = {}  
local tStreamType  

local function consumer()  
    sys.taskInit(function()  
        audio.setVolume(7)  
        while true do  
            while #tBuffer==0 do  
                sys.waitUntil("DATA_STREAM_IND")  
            end  
            local data = table.remove(tBuffer,1)  
            --log.info("testAudioStream.consumer remove",data:len())  
            local procLen = audiocore.streamplay(tStreamType,data)  
            if procLen<data:len() then  
                --log.warn("produce fast")  
                table.insert(tBuffer,1,data:sub(procLen+1,-1))  
                sys.wait(5)  
            end  
        end  
    end)  
end  

local function producer(streamType)  
    sys.taskInit(function()  
        while true do  
            tStreamType = streamType  
            local tAudioFile =  
            {  
                [audiocore.AMR] = "tip.amr",  
                [audiocore.SPX] = "record.spx",  
                [audiocore.PCM] = "alarm_door.pcm",  
            }  
            local fileHandle = io.open("/lua/"..tAudioFile[streamType],"rb")  
            if not fileHandle then  
                log.error("testAudioStream.producer open file error")  
                return  
            end  
            while true do  
                local data = fileHandle:read(streamType==audiocore.SPX and 1200 or 1024)  
                if not data then fileHandle:close() return end  
                table.insert(tBuffer,data)  
                if #tBuffer==1 then sys.publish("DATA_STREAM_IND") end  
                --log.info("testAudioStream.producer",data:len())  
                sys.wait(10)  
            end   
        end  
    end)  
end  

sys.timerStart(function()  
    --producer(audiocore.AMR)  
    --producer(audiocore.SPX)  
    producer(audiocore.PCM)  
    consumer()  
end,3000)  

```


---

## 以下接口不支持

## audiocore.setpa(audioClass)
## audiocore.getpa()
## audiocore.head_plug(type)
## audiocore.headsetinit(auto)
## audiocore.rtmpopen(url)
## audiocore.rtmpclose()
---
# 其他接口见:
https://doc.openluat.com/wiki/21?wiki_page_id=2243

