module(..., package.seeall)



local function create_style(v)
    local s = lvgl.style_t()
    lvgl.style_init(s)
    if not v then return s end
    -- 设置背景
    if v.bg then s = set_bg(s, v.bg) end
    -- 设置边框
    if v.border then s = set_border(s, v.border) end
    -- 设置阴影
    if v.shadow then s = set_shadow(s, v.shadow) end
    -- 设置文字样式
    if v.text then s = set_text(s, v.text) end
    return s
end

function create(p, v)
    local l = lvgl.label_create(p, nil)
    -- 设置 Label 的内容
    lvgl.label_set_text(l, v.text or "")

    if v.W and v.H then lvgl.obj_set_size(l, v.W , v.H) end

    -- 重新着色
    lvgl.label_set_recolor(l, v.recolor or true)

    -- 文本的行可以使用 lvgl.label_set_align(l, lvgl.LABEL_ALIGN_LEFT/RIGHT/CENTER) 
    -- 左右对齐。请注意，它将仅对齐线，而不对齐标签对象本身。
    lvgl.label_set_align(l, v.label_align or lvgl.LABEL_ALIGN_CENTER)

    if v.click then lvgl.obj_set_click(l, v.click) end
    if v.event then lvgl.obj_set_event_cb(l, v.event) end

    -- 添加样式
    lvgl.obj_add_style(l, lvgl.LABEL_PART_MAIN, create_style(v.style))

    -- 设置 Label 显示模式
    -- lvgl.LABEL_LONG_BREAK – 保持对象宽度，断开（换行）过长的线条并扩大对象高度
    -- lvgl.LABEL_LONG_DOT – 保持对象大小，打断文本并在最后一行写点（使用 lvgl.label_set_static_text 时不支持）
    -- lvgl.LABEL_LONG_SROLL – 保持大小并来回滚动标签
    -- lvgl.LABEL_LONG_SROLL_CIRC – 保持大小并循环滚动标签  
    -- lvgl.LABEL_LONG_CROP – 保持大小并裁剪文本    
    -- lvgl.LABEL_LONG_EXPAND – 将对象大小扩展为文本大小（默认）
    lvgl.label_set_long_mode(l, v.mode or lvgl.LABEL_LONG_CROP)
    -- 设置 Label 的显示长度(若设置了显示模式，则需要在其后设置才会有效)

    -- 设置 Label 的位置
    lvgl.obj_align(l, v.align_to or nil, v.align or lvgl.ALIGN_CENTER,
                   v.align_x or 0, v.align_y or 0)

    return l
end
create(lvgl.scr_act(), {
    text = "#ff0000 hh# #00f0f0 hhh# \n #f0ff00  hhh# #0f0ff0 h ",
    align = lvgl.ALIGN_CENTER,
    style = {
        border = {color = 0x0ff00f, width = 10, opa = 200},
        shadow = {spread = 10, width = 10, color = 0xff00f0},
        line = {color = 0xafccfc, width = 30, opa = 255, rounded = false},
        outline = {color = 0xff0000, opa = 100, width = 20},
        bg = {
            radius = 50,
            color = 0x0f0ff0,
            opa = 250,
            grad = {color = 0x0f0f0f, main = 60, grad = 300}
        },
        text = {
            font = style.font68,
            color = 0xff0000,
            letter_space = 20,
            line_space = 20
        }
    }
})
