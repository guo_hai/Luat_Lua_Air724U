module(..., package.seeall)
local function create_style(v)
    local s = lvgl.style_t()
    lvgl.style_init(s)
    if not v then return s end
    -- 设置背景
    if v.bg then s = set_bg(s, v.bg) end
    -- 设置边框
    if v.border then s = set_border(s, v.border) end
    -- 设置阴影
    if v.shadow then s = set_shadow(s, v.shadow) end
    -- 设置外框线
    if v.outline then s = set_outline(s, v.outline) end
    -- -- 设置文字样式
    -- if v.text then s = set_text(s, v.text) end
    return s
end

function create(p, v)
    local obj = lvgl.page_create(p, nil)
    if not v then return obj end
    if not v.click then lvgl.obj_set_click(obj, false) end
    lvgl.obj_set_size(obj, v.W or 300, v.H or 200)
    lvgl.obj_align(obj, v.align_to, v.align or lvgl.ALIGN_CENTER,
                   v.align_x or 0, v.align_y or 0)

    -- 可以根据以下四个策略显示滚动条：
    -- lvgl.SCRLBAR_MODE_OFF 一直都不显示滚动条
    -- lvgl.SCRLBAR_MODE_ON 一直都显示滚动条
    -- lvgl.SCRLBAR_MODE_DRAG 拖动页面时显示滚动条
    -- lvgl.SCRLBAR_MODE_AUTO 当可滚动容器的大小足以滚动时显示滚动条
    -- lvgl.SCRLBAR_MODE_HIDE 暂时隐藏滚动条
    -- lvgl.SCRLBAR_MODE_UNHIDE 取消隐藏以前隐藏的滚动条。也恢复原始模式
    -- 可以通过以下方式更改滚动条显示策略： lvgl.page_set_scrlbar_mode(page, SB_MODE) 。
    -- 默认值为 lvgl.SCRLBAR_MODE_AUTO 。
    if v.scrollbar_mode then
        lvgl.page_set_scrollbar_mode(obj, v.scrollbar_mode)
    end

    -- 页面的主要部分称为 lvgl.PAGE_PART_BG ，它是页面的背景。
    -- 它使用所有典型的背景样式属性。使用填充会增加侧面的空间。
    if v.bg then
        lvgl.obj_add_style(obj, lvgl.PAGE_PART_BG, create_style(v.bg))
    end

    -- lvgl.LIST_PART_SCROLLBAR 是绘制滚动条的背景的虚拟部分。
    -- 使用所有典型的背景样式属性，使用size设置滚动条的宽度，并使用pad_right和pad_bottom设置间距。

    if v.scrollbar then
        lvgl.obj_add_style(obj, lvgl.LIST_PART_SCROLLBAR,
                           create_style(v.scrollbar))
    end

    -- lvgl.LIST_PART_EDGE_FLASH 还是背景的虚拟部分，当无法进一步沿该方向滚动列表时，
    -- 将在侧面绘制半圆。使用所有典型的背景属性。
    if v.edge_flash then
        lvgl.obj_add_style(obj, lvgl.LIST_PART_EDGE_FLASH,
                           create_style(v.edge_flash))
    end
    -- 可以通过 lvgl.PAGE_PART_SCRL 部分引用可滚动对象。
    -- 它还使用所有典型的背景样式属性和填充来增加侧面的空间。
    if v.scrl then
        lvgl.obj_add_style(obj, lvgl.PAGE_PART_SCRL, create_style(v.scrl))
    end

    return obj
end

-- create(lvgl.scr_act(), {
--     bg = {bg = {color = 0xff9999}},
--     scrl = {bg = {color = 0xffffcc}},
--     edge_flash = {bg = {color = 0xccccff}},
--     scrollbar = {bg = {color = 0xccffcc}}
-- })
