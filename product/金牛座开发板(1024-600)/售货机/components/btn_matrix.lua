module(..., package.seeall)
-- 默认回调
local function event_handler(obj, event)
    if (event == lvgl.EVENT_VALUE_CHANGED) then
        txt = lvgl.btnmatrix_get_active_btn_text(obj)
        -- hhh = lvgl.btnmatrix_get_active_btn(obj)
        local temp = lvgl.btnmatrix_get_active_btn(obj)
        print("矩阵键盘默认回调")
        print(temp)
        print(txt)
    end
end

function create_style_btn(v)
    local s = lvgl.style_t()
    lvgl.style_init(s)
    if not v then return s end
    -- 设置背景
    if v.bg then s = set_bg(s, v.bg) end
    -- 设置边框
    if v.border then s = set_border(s, v.border) end
    -- 设置阴影
    if v.shadow then s = set_shadow(s, v.shadow) end

    -- 设置文字样式
    if v.text then s = set_text(s, v.text) end

    return s
end

function create_style_bg(v)

    local s = lvgl.style_t()
    lvgl.style_init(s)
    if not v then return s end
    -- 设置背景
    if v.bg then s = set_bg(s, v.bg) end
    -- 设置边框
    if v.border then s = set_border(s, v.border) end
    -- 设置阴影
    if v.shadow then s = set_shadow(s, v.shadow) end

    return s
end

function create(p, v)
    local btnMatrix = lvgl.btnmatrix_create(p, nil)
    lvgl.obj_set_size(btnMatrix, v.W or 400, v.H or 200)
    lvgl.btnmatrix_set_recolor(btnMatrix, v.recolor or true)
    lvgl.btnmatrix_set_map(btnMatrix, v.btnArray or {"。", "。"})
    -- 设置btnMatrix的第九个按键所占宽度的比例
    -- lvgl.btnmatrix_set_btn_width(btnMatrix, 9, 3);
    -- lvgl.btnmatrix_set_btn_ctrl(btnMatrix, 9, lvgl.BTNMATRIX_CTRL_CHECKABLE);
    -- lvgl.btnmatrix_set_btn_ctrl(btnMatrix, 11, lvgl.BTNMATRIX_CTRL_CHECK_STATE);
    lvgl.obj_align(btnMatrix, v.align_to or nil, v.align or lvgl.ALIGN_CENTER,
                   v.align_x or 0, v.align_y or 0)
    lvgl.obj_set_event_cb(btnMatrix, v.cb or event_handler)

    lvgl.obj_add_style(btnMatrix, lvgl.BTNMATRIX_PART_BTN,
                       create_style_btn(v.btn_style))
    lvgl.obj_add_style(btnMatrix, lvgl.BTNMATRIX_PART_BG,
                       create_style_bg(v.bg_style))
    return btnMatrix
end

-- create(lvgl.scr_act(), {
--     btnArray = {
--         "1", "2", "3", "\n", "4", "5", "6", "\n", "7", "8", "9", "\n", "0",
--         "#FF0000 删除", ""
--     },
--     W = LCD_W,
--     H = LCD_H / 2,
--     recolor = true,
--     align = lvgl.ALIGN_IN_BOTTOM_MID,
--     align_y = -20,
--     -- cb = event_handler,
--     btn_style = {
--         bg = {
--             radius = 10,
--             color = 0x0f0ff0,
--             opa = 150,
--             grad = {color = 0x0f0f0f}
--         }
--     },
--     bg_style = {
--         bg = {
--             radius = 10,
--             color = 0x0f0ff0,
--             opa = 150,
--             grad = {color = 0x0f0f0f}
--         }
--     }
-- })
