module(...)

password = '1111'

all_goods = {
    {name = "百事可乐", price = "3.00", src = "baishi"},
    {name = "可口可乐", price = "3.00", src = "kele"},
    {name = "爆米花", price = "13.00", src = "a-baomihualingshi"},
    {name = "碧根果", price = "23.00", src = "bigenguo"},
    {name = "鸡腿", price = "5.50", src = "jitui"},
    {name = "柠檬干", price = "5.00", src = "ningmenggan"},
    {name = "铜锣烧", price = "3.00", src = "tongluoshao"},
    {name = "小方蛋糕", price = "8.00", src = "xiaofangdangao"},
    {name = "小鱼干", price = "10.00", src = "xiaoyugan"},
    {name = "薯条", price = "15.00", src = "shutiao"},
    {name = "甜甜圈", price = "3.00", src = "tiantianquan"},
    {name = "蔓越莓饼干", price = "3.00", src = "manyuemeibinggan"},
    {name = "开心果", price = "15.00", src = "kaixinguo"},
    {name = "猪肉脯", price = "20.00", src = "zhuroupu"}
}

all_drinks = {
    {name = "百事可乐", price = "3.00", src = "baishi"},
    {name = "可口可乐", price = "3.00", src = "kele"}
}
all_foods = {
    {name = "爆米花", price = "13.00", src = "a-baomihualingshi"},
    {name = "碧根果", price = "23.00", src = "bigenguo"},
    {name = "鸡腿", price = "5.50", src = "jitui"},
    {name = "铜锣烧", price = "3.00", src = "tongluoshao"},
    {name = "小方蛋糕", price = "8.00", src = "xiaofangdangao"},
    {name = "小鱼干", price = "10.00", src = "xiaoyugan"},
    {name = "薯条", price = "15.00", src = "shutiao"},
    {name = "甜甜圈", price = "3.00", src = "tiantianquan"},
    {name = "蔓越莓饼干", price = "3.00", src = "manyuemeibinggan"},
    {name = "开心果", price = "15.00", src = "kaixinguo"},
    {name = "猪肉脯", price = "20.00", src = "zhuroupu"}
}

other_goods = {{name = "柠檬干", price = "5.00", src = "ningmenggan"}}
