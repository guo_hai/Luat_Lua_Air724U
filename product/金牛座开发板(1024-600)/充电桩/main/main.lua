PROJECT = "chongdianzhuang"
VERSION = "9.9.9"

PRODUCT_KEY = "PA916VPSUsSQ4TcSCfK11sZGaBzgQ3tM"

require "log"
require "sys"
require "http"
require "misc"
require "pins"
--require "mipi_rgb"
require "mipi"
-- require "mipi_lcd_GC9503"
require "tp"
require "UIconfig"

require "service_time"
require "service_sim"
require "fragment_Screensaver"

spi.setup(spi.SPI_1,1,1,8,45000000,1)
font24=lvgl.font_load(spi.SPI_1,24,4,110)
font32=lvgl.font_load(spi.SPI_1,32,4,150)
font36=lvgl.font_load(spi.SPI_1,36,4,156)

lvgl.init(function()
end, tp.input)
-- lvgl.disp_set_rotation(nil, lvgl.DISP_ROT_270)


require "style"
require "activity"

--activity.init()
fragment_Screensaver.init()

require "ProjectSwitch"

sys.init(0, 0)
sys.run()
