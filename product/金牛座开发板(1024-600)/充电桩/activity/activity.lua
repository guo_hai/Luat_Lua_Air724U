module(..., package.seeall)
---------------------------


-- local gunTab=UIconfig.UI.gun

local gun=UIconfig.UI.gun
local getMsg=UIconfig.getStatus

function makeBody()

    body = lvgl.cont_create(lvgl.scr_act(), nil)
    lvgl.obj_set_size(body,1024, 600)
    lvgl.obj_add_style(body, lvgl.CONT_PART_MAIN, style.style_body)
    -- lvgl.obj_set_auto_realign(body, true)
    lvgl.obj_align(body, nil, lvgl.ALIGN_CENTER, 0, 0)
end

function makestatusCont(cont)
    statusCont=lvgl.cont_create(cont)
    lvgl.obj_add_style(statusCont, lvgl.CONT_PART_MAIN, style.style_divBox)
    lvgl.obj_set_size(statusCont,700, 500)
    lvgl.obj_align(statusCont, cont, lvgl.ALIGN_IN_BOTTOM_RIGHT, 0, 0)
end

-- function makeTimeLabel(cont)
--     timeLabel = lvgl.label_create(cont, nil)
--     lvgl.label_set_recolor(timeLabel, true)
--     lvgl.obj_set_style_local_text_font(timeLabel, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font32)
--     lvgl.label_set_text(timeLabel, " ")
--     lvgl.label_set_align(timeLabel, lvgl.LABEL_ALIGN_CENTER)
--     lvgl.obj_align(timeLabel, nil, lvgl.ALIGN_IN_TOP_RIGHT, -30, 30)

--     sys.subscribe("SYSTEM_CHANGE_TIME",function(date)
--         lvgl.label_set_text(timeLabel,"#FFFFFF "..date)
--         lvgl.obj_align(timeLabel, nil, lvgl.ALIGN_IN_TOP_MID, 0, 20)
--         end)
-- end

function makeTop(cont)
    local top=lvgl.cont_create(cont)
    lvgl.obj_set_size(top,lvgl.obj_get_width(cont),60)
    lvgl.obj_align(top, nil, lvgl.ALIGN_IN_TOP_MID, 0, 0)
    lvgl.obj_add_style(top, lvgl.CONT_PART_MAIN, style.style_divBox)


-------------------------------------------
    --4G 图标

    local signImg=lvgl.img_create(top, nil)
    lvgl.img_set_src(signImg,"/lua/nosin_icon.png")
    lvgl.obj_align(signImg,top, lvgl.ALIGN_IN_LEFT_MID, 20, 0)

    sys.subscribe("RSSI_CHANGE",function(img)
        lvgl.img_set_src(signImg,img)
    end)
------------------------------------------------紧急图标
emergencyIcon=lvgl.img_create(top, nil)
lvgl.img_set_src(emergencyIcon,"/lua/eme.png")
lvgl.obj_align(emergencyIcon,signImg, lvgl.ALIGN_OUT_RIGHT_MID, 20, 0)

------------------------------------------------油枪图标

function gunHandler(obj, event)
    if event == lvgl.EVENT_CLICKED then
        if lvgl.obj_get_id(obj)==lvgl.obj_get_id(leftGunIcon) or lvgl.obj_get_id(obj)==lvgl.obj_get_id(img_left) then
            print("左油枪按下")
            lvgl.obj_set_style_local_image_opa(leftGunIcon, lvgl.IMG_PART_MAIN, lvgl.STATE_DEFAULT, 128)
            statusFragmentInit(statusCont,gun.left)
        else
            print("右油枪按下")
            lvgl.obj_set_style_local_image_opa(rightGunIcon, lvgl.IMG_PART_MAIN, lvgl.STATE_DEFAULT, 128)
            statusFragmentInit(statusCont,gun.right)
        end
    end
end

leftGunIcon=lvgl.img_create(top, nil)
lvgl.img_set_src(leftGunIcon,"/lua/gun.png")
lvgl.obj_align(leftGunIcon,emergencyIcon, lvgl.ALIGN_OUT_RIGHT_MID, 40, 0)
lvgl.obj_set_click(leftGunIcon,true)
lvgl.obj_set_event_cb(leftGunIcon,gunHandler)

rightGunIcon=lvgl.img_create(top, nil)
lvgl.img_set_src(rightGunIcon,"/lua/gun.png")
lvgl.obj_align(rightGunIcon,leftGunIcon, lvgl.ALIGN_OUT_RIGHT_MID, 40, 0)
lvgl.obj_set_click(rightGunIcon,true)
lvgl.obj_set_event_cb(rightGunIcon,gunHandler)
-------------------------------------------------------------------
    timeLabel = lvgl.label_create(top, nil)
    lvgl.label_set_recolor(timeLabel, true)
    lvgl.obj_set_style_local_text_font(timeLabel, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font32)
    lvgl.label_set_text(timeLabel, " ")
    lvgl.label_set_align(timeLabel, lvgl.LABEL_ALIGN_CENTER)
    lvgl.obj_align(timeLabel, nil, lvgl.ALIGN_IN_RIGHT_MID, -50, 0)

    sys.subscribe("SYSTEM_CHANGE_TIME",function(date)
        lvgl.label_set_text(timeLabel,"#FFFFFF "..date)
        lvgl.obj_align(timeLabel, nil, lvgl.ALIGN_IN_RIGHT_MID, -50, 0)
        sys.publish("SCREENSAVER_UNINIT")
        end)
end

function makeGunIcon(cont)

    local m_img,m_txt=getMsg(gun.left)

    img_left = lvgl.img_create(cont, nil)
    -- 设置图片显示的图像
    lvgl.img_set_src(img_left, m_img)
    -- 图片居中
    lvgl.obj_align(img_left, nil, lvgl.ALIGN_IN_LEFT_MID, 100,-50)
    lvgl.obj_set_click(img_left,true)
    lvgl.obj_set_event_cb(img_left,gunHandler)

    gun_label_left_txt = lvgl.label_create(cont, nil)
    lvgl.label_set_recolor(gun_label_left_txt, true)
    lvgl.obj_set_style_local_text_font(gun_label_left_txt, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font24)
    lvgl.label_set_text(gun_label_left_txt, "#FFFFFF 左枪")
    lvgl.obj_align(gun_label_left_txt, img_left, lvgl.ALIGN_OUT_TOP_MID, 0, 0)

    gun_label_left_status = lvgl.label_create(cont, nil)
    lvgl.label_set_recolor(gun_label_left_status, true)
    lvgl.obj_set_style_local_text_font(gun_label_left_status, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font36)
    lvgl.label_set_text(gun_label_left_status, m_txt)
    lvgl.obj_align(gun_label_left_status, img_left, lvgl.ALIGN_OUT_BOTTOM_MID, 0, 0)

--------

    m_img,m_txt=getMsg(gun.right)

    img_right = lvgl.img_create(cont, nil)
    -- 设置图片显示的图像
    lvgl.img_set_src(img_right, m_img)
    -- 图片居中
    lvgl.obj_align(img_right, nil, lvgl.ALIGN_IN_RIGHT_MID,-100, -50)
    lvgl.obj_set_click(img_right,true)
    lvgl.obj_set_event_cb(img_right,gunHandler)

    gun_label_right_txt = lvgl.label_create(cont, nil)
    lvgl.label_set_recolor(gun_label_right_txt, true)
    lvgl.obj_set_style_local_text_font(gun_label_right_txt, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font24)
    lvgl.label_set_text(gun_label_right_txt, "#FFFFFF 右枪")
    lvgl.obj_align(gun_label_right_txt, img_right, lvgl.ALIGN_OUT_TOP_MID, 0, 0)

    gun_label_right_status = lvgl.label_create(cont, nil)
    lvgl.label_set_recolor(gun_label_right_status, true)
    lvgl.obj_set_style_local_text_font(gun_label_right_status, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font36)
    lvgl.label_set_text(gun_label_right_status, m_txt)
    lvgl.obj_align(gun_label_right_status, img_right, lvgl.ALIGN_OUT_BOTTOM_MID, 0, 0)
end

function makeQR(cont)
    c = lvgl.cont_create(cont, nil)
    lvgl.obj_add_style(c, lvgl.CONT_PART_MAIN, style.style_QR)
    lvgl.obj_set_size(c,230, 270)
    lvgl.obj_align(c, nil, lvgl.ALIGN_IN_LEFT_MID, 50, 0)

    qrcode=lvgl.qrcode_create(c,nil)
    lvgl.qrcode_set_txt(qrcode,UIconfig.UI.QR)
    lvgl.obj_set_size(qrcode,200,200)
    lvgl.obj_align(qrcode, nil, lvgl.ALIGN_IN_TOP_MID, 0, 15)

    while misc.getImei()=="" do
        sys.wait(5)
    end
    local label = lvgl.label_create(lvgl.scr_act(), nil)
    lvgl.obj_set_style_local_text_font(label, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font32)
    lvgl.label_set_text(label, misc.getImei())
    lvgl.obj_align(label,qrcode, lvgl.ALIGN_OUT_BOTTOM_MID, 0, 10)
end

function changeStatus(msg)
    if msg=="GUN_LEFT"then
        print("左改变")
        lvgl.img_set_src(img_left, gunTab.left.img)
        lvgl.label_set_text(gun_label_left_status, gunTab.left.status)
        lvgl.obj_align(gun_label_left_status, img_left, lvgl.ALIGN_OUT_BOTTOM_MID, 0, 0)
    elseif msg=="GUN_RIGHT" then
        lvgl.img_set_src(img_right, gunTab.right.img)
        lvgl.label_set_text(gun_label_right_status, gunTab.right.status)
        lvgl.obj_align(gun_label_right_status, img_right, lvgl.ALIGN_OUT_BOTTOM_MID, 0, 0)
        print("右改变")
    elseif msg=="QR_CODE" then
        lvgl.obj_del(qrcode)
        qrcode=lvgl.qrcode_create(c,nil)
        lvgl.qrcode_set_txt(qrcode,UIconfig.UI.QR)
        lvgl.obj_set_size(qrcode,200,200)
        lvgl.obj_align(qrcode, nil, lvgl.ALIGN_CENTER, 0, 0)
        print("QR")
    end
end

sys.subscribe("UI_CHANGE_FLAG",changeStatus)

function statusFragmentInit(cont,t)
    if ststusFragment~=nil then return end
    ststusFragment=lvgl.cont_create(cont, nil)
    lvgl.obj_add_style(ststusFragment, lvgl.CONT_PART_MAIN, style.style_body)
    lvgl.obj_set_size(ststusFragment,lvgl.obj_get_width(cont),lvgl.obj_get_height(cont))
    lvgl.cont_set_layout(ststusFragment, lvgl.LAYOUT_COLUMN_MID)
    lvgl.obj_align(ststusFragment, nil, lvgl.ALIGN_IN_LEFT_MID, 0, 0)

    local ctrCnt=lvgl.cont_create(ststusFragment, nil)
    lvgl.obj_add_style(ctrCnt, lvgl.CONT_PART_MAIN, style.style_body)
    lvgl.obj_set_size(ctrCnt,lvgl.obj_get_width(ststusFragment),40)

    function backHandler(obj, event)
        if event == lvgl.EVENT_CLICKED then
            print("返回按钮按下")
            statusFragmentUninit()
        end
    end

    local save=lvgl.img_create(ctrCnt, nil)
    lvgl.obj_set_click(save,true)
    lvgl.img_set_src(save,"/lua/back.png")
    lvgl.obj_align(save,ctrCnt, lvgl.ALIGN_IN_LEFT_MID, 0, 0)
    lvgl.obj_set_event_cb(save,backHandler)
----------------------------------------------------------
    local status=t.para
    local function makeItem(cont,text)
        local label = lvgl.label_create(cont, nil)
        lvgl.obj_add_style(label, lvgl.LABEL_PART_MAIN, style.style_text)
        lvgl.obj_set_style_local_text_font(label, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font36)
        lvgl.label_set_text(label, text)
    end

    local statusCont=lvgl.cont_create(ststusFragment, nil)
    lvgl.cont_set_fit(statusCont, lvgl.FIT_TIGHT)
    lvgl.cont_set_layout(statusCont, lvgl.LAYOUT_COLUMN_LEFT)
    lvgl.obj_add_style(statusCont, lvgl.CONT_PART_MAIN, style.style_body)

    makeItem(statusCont,string.format("电压 : %s (V)",status.V))
    makeItem(statusCont,string.format("电流 : %s (A)",status.A))
    makeItem(statusCont,string.format("功率 : %s (kW)",status.P))
    makeItem(statusCont,string.format("已用电量 : %s (kWh)",status.USEDPOWER))
    makeItem(statusCont,string.format("已充时间 : %s (Min)",status.USEDTIME))
    makeItem(statusCont,string.format("已用金额 : %s (元)",status.USEDMONEY))


end

function statusFragmentUninit()
    lvgl.obj_del(ststusFragment)
    lvgl.obj_set_style_local_image_opa(rightGunIcon, lvgl.IMG_PART_MAIN, lvgl.STATE_DEFAULT, 255)
    lvgl.obj_set_style_local_image_opa(leftGunIcon, lvgl.IMG_PART_MAIN, lvgl.STATE_DEFAULT, 255)
    ststusFragment=nil
end

function init()

    makeBody()
    makeTop(body)
    makestatusCont(body)
    -- makeTimeLabel(body)
    makeGunIcon(statusCont)
    sys.taskInit(makeQR,body)
    -----------

end



