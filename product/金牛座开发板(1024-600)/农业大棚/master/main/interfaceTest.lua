module(..., package.seeall)

----------------------------------------------
--按键点击接口
local labelClick=function(msg)

    sys.publish("SCREENSAVER_INIT",500)--唤醒屏保 第二个参数为间隔时间，默认1000，单位毫秒

    if msg=="SYSTEM_RUNNING" then
        print("运行标签按下")
    elseif msg=="SYSTEM_MOTO" then
        print("电机标签按下")
    elseif msg=="SYSTEM_PRESSURE" then
        print("压力标签按下")
    elseif msg=="SYSTEM_WATER" then
        print("水质标签按下")
    elseif msg=="SYSTEM_FILTER" then
        print("滤芯标签按下")
    elseif msg=="SYSTEM_OIL" then
        print("泵油标签按下")
        
    end
end

sys.subscribe("SYSTEM_LABEL_CLICK", labelClick)

-----------------------------------------------
--参数变化接口
local paraChange=function(msg)
    print(msg.."参数发生改变")
end

sys.subscribe("PARASETTING_CHANGE", paraChange)

--------------------------------------------------
--支路开关变化接口

local switchChange=function(msg,status)
    print("开关",msg,status and "打开" or "关闭")
end

sys.subscribe("CTL_SWITCH", switchChange)


--------------------------------------------------------

local timeCrl=function(msg)
    print(msg,"时控参数改变")
end

sys.subscribe("TIMECONTROL_CHANGE",timeCrl)