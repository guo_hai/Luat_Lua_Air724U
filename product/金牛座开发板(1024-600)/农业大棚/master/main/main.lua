PROJECT = "nongyedapeng"
VERSION = "9.9.9"

PRODUCT_KEY = "PA916VPSUsSQ4TcSCfK11sZGaBzgQ3tM"

require "log"
require "sys"
require "http"
require "misc"
require "pins"
--require "mipi_rgb"
-- require "mipi_lcd_GC9503"
require "mipi"
require "tp"
require "service_sim"
require "service_time"
-- require "interfaceTest"
require "UIConfig"

-- 测试需要打开 "test"
-- require "test"


-- lvgl.obj_set_style_local_text_font=function (...)
--     return
-- end

_G.BEEP=function() pio.pin.plus(13,250,500,20) end

spi.setup(spi.SPI_1,1,1,8,45000000,1)
font18=lvgl.font_load(spi.SPI_1,18,4,110)
font20=lvgl.font_load(spi.SPI_1,20,4,110)
font24=lvgl.font_load(spi.SPI_1,24,4,110)
font28=lvgl.font_load(spi.SPI_1,28,4,110)
font32=lvgl.font_load(spi.SPI_1,32,4,150)
font36=lvgl.font_load(spi.SPI_1,36,4,156)
font40=lvgl.font_load(spi.SPI_1,40,4,156)
font42=lvgl.font_load(spi.SPI_1,42,4,156)
font46=lvgl.font_load(spi.SPI_1,46,4,156)
-- font24=lvgl.font_load(spi.SPI_1,16,2,40)
-- font32=lvgl.font_load(spi.SPI_1,16,2,40)
-- font36=lvgl.font_load(spi.SPI_1,16,2,40)

lvgl.init(function()
end, tp.input)
-- lvgl.disp_set_rotation(nil, lvgl.DISP_ROT_90)


require "style"
require "inputBox"
require "activity_status"
require "interfaceTest"



--正式调试打开activity_status.activityInit()
activity_status.activityInit()

-- sys.publish("BACKLIGHT_STATUS",1)



--------------------------------------------------------------------------------------------------------------------
--[[
--该区域代码为演示支路数页面代码，正式使用需删除，仅需要activity_status.activityInit()即可
-- 按键回调函数
event_handler = function(obj, event)
    if event == lvgl.EVENT_CLICKED then
        -- if lvgl.obj_get_id(obj)==lvgl.obj_get_id(btn1) then
        --     runningStatus.RunningSatusTable={runningStatus.RunningSatusTable[1]}
        -- end
        -- lvgl.obj_del(btn1)
        -- lvgl.obj_del(btn2)
        -- activity_status.activityInit()
        pio.pin.plus(13,250,500,20)
    end
end

-- 按键1
btn1 = lvgl.btn_create(lvgl.scr_act(), nil)
lvgl.obj_set_event_cb(btn1, event_handler)
lvgl.obj_align(btn1, nil, lvgl.ALIGN_CENTER, -100, 0)
-- 按键1 的文字
local label1 = lvgl.label_create(btn1, nil)
lvgl.label_set_text(label1, "单支路演示")

btn2 = lvgl.btn_create(lvgl.scr_act(), nil)
lvgl.obj_set_event_cb(btn2, event_handler)
lvgl.obj_align(btn2, nil, lvgl.ALIGN_CENTER, 100, 0)
-- 按键1 的文字
local label2 = lvgl.label_create(btn2, nil)
lvgl.label_set_text(label2, "多支路演示")

--]]

---------------

-- function settingFnc(obj,event)
--     if event == lvgl.EVENT_CLICKED then
--         print("设置按下")
--     end
-- end
-- --设置
-- _G.setting=lvgl.img_create(lvgl.scr_act(), nil)
-- lvgl.img_set_src(setting,"/lua/setting_icon_big.png")
-- lvgl.obj_align(setting,switch , lvgl.ALIGN_IN_TOP_LEFT, 0, 0)
-- lvgl.obj_set_click(setting,true)
-- lvgl.obj_set_event_cb(setting,settingFnc)


--------------


------
--矩阵键盘
-- local map={"1", "2", "3", "\n",
--             "4","5","6","\n",
--             "7", "8", "9", "\n",
--             "-","0", ".","\n",
--             "D","C","Y",""}

-- local btnm1=lvgl.btnmatrix_create(lvgl.scr_act(),nil)
-- lvgl.obj_add_style(btnm1,lvgl.BTNMATRIX_PART_BTN, style.style_temp)
-- lvgl.obj_set_style_local_text_font(btnm1, lvgl.BTNMATRIX_PART_BTN, lvgl.STATE_DEFAULT, font32)
-- lvgl.obj_set_size(line, 560, 1)
-- lvgl.btnmatrix_set_map(btnm1, map)
-- lvgl.btnmatrix_set_btn_width(btnm1, 11, 2)

-------

require "ProjectSwitch"

sys.init(0, 0)
sys.run()
