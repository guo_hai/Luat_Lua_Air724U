module(..., package.seeall)

_G.fragmentQRcode=false

local topicTable={}
local function topicTableAdd(id,callback)
    topicTable[id]=callback
    sys.subscribe(id,callback)
end

function init(cont)
    lvgl.obj_clean(cont)
    qrcode=lvgl.qrcode_create(cont,nil)
    lvgl.qrcode_set_txt(qrcode,UIConfig.UIParams.qrcode)
    lvgl.obj_set_size(qrcode,250,250)
    lvgl.obj_align(qrcode, nil, lvgl.ALIGN_CENTER, 0, 0)
    function changeQR()
        lvgl.qrcode_set_txt(qrcode,UIConfig.UIParams.qrcode)
    end
    topicTableAdd("CHANGE_QRCODE",changeQR)
    _G.fragmentQRcode=true
end

function uninit()
    -- print("取消订阅")
    for id, callback in pairs(topicTable) do
        sys.unsubscribe(id, callback)
    end
    _G.fragmentQRcode=false
end
