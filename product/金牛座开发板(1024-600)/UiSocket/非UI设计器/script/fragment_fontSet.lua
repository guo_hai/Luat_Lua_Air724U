module(..., package.seeall)

_G.fontSetCont=nil

event_handler = function(obj, event)
        if (event == lvgl.EVENT_VALUE_CHANGED) then
            local opt = lvgl.dropdown_get_selected(obj)
            log.info("Option:", lvgl.dropdown_get_selected(obj))
            _G.BEEP()
            if opt == 1 then
                _G.Font="vector"
            else
                _G.Font="default"
            end
            nvm.set("Font",_G.Font)
            sys.publish("Font_CHANGE")
        end
    end

function makeDropdown()
        local itemCont=lvgl.cont_create(_G.fontSetCont, nil)
        lvgl.obj_set_click(itemCont,false)
        lvgl.obj_set_size(itemCont, 210,60)
        lvgl.obj_add_style(itemCont, lvgl.CONT_PART_MAIN, style.style_divBox)

        local label=lvgl.label_create(itemCont, nil)
        if _G.Font=="vector"then
            lvgl.obj_set_style_local_text_font(label, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font28)
        end
        lvgl.label_set_text(label, "字库:")
        lvgl.obj_align(label, itemCont, lvgl.ALIGN_IN_LEFT_MID, 0, 0)

        local dd = lvgl.dropdown_create(itemCont, nil)
        if _G.Font=="vector"then
            lvgl.obj_set_style_local_text_font(dd, lvgl.DROPDOWN_PART_SELECTED,lvgl.STATE_DEFAULT, font28)
            lvgl.obj_set_style_local_text_font(dd, lvgl.DROPDOWN_PART_LIST,lvgl.STATE_DEFAULT, font28)
            lvgl.obj_set_style_local_text_font(dd, lvgl.DROPDOWN_PART_MAIN,lvgl.STATE_DEFAULT, font28)
        end
        lvgl.dropdown_set_options(dd, [[默认字库
矢量字库]])
        lvgl.obj_set_event_cb(dd, event_handler)
        lvgl.obj_set_size(dd, 150,40)
        lvgl.obj_align(dd, itemCont, lvgl.ALIGN_IN_LEFT_MID, 60, 0)
end

function makeFontSetBox(cont)
    _G.fontSetCont=lvgl.cont_create(cont, nil)
    lvgl.obj_set_click(_G.fontSetCont,false)
    lvgl.obj_set_size(_G.fontSetCont, 260, 100)
    lvgl.obj_add_style(_G.fontSetCont, lvgl.CONT_PART_MAIN, style.style_labelBgSnowWhite)
    lvgl.obj_align(_G.fontSetCont, cont, lvgl.ALIGN_IN_TOP_RIGHT,-20, 0)
    lvgl.cont_set_layout(_G.fontSetCont,lvgl.LAYOUT_CENTER)
    makeDropdown()
    return _G.fontSetCont
end

function init(cont)
    makeFontSetBox(cont)
end
