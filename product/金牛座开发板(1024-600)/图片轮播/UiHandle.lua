
--This function name and notes cannot be modified
--@@funCfg: <joinUs, noexist>
function joinUs()
------------USER CODE DATA--------------
lvgl.style_set_bg_color(Style_LvglButton2_1, lvgl.STATE_DEFAULT, lvgl.color_hex(0xFF0000))
lvgl.style_set_value_str(Style_LvglButton2_1, lvgl.STATE_DEFAULT, "逗你的，还是手机扫码加群一起玩耍吧")
lvgl.style_set_value_color(Style_LvglButton2_1, lvgl.STATE_DEFAULT, lvgl.color_hex(0x1A1A1A))
lvgl.obj_add_style(ScreenA.LvglButton2.self, lvgl.BTN_PART_MAIN, Style_LvglButton2_1)
------------USER CODE DATA--------------
end


--This function name and notes cannot be modified
--@@funCfg: <reset, noexist>
function reset()
------------USER CODE DATA--------------
lvgl.style_set_bg_color(Style_LvglButton2_1, lvgl.STATE_DEFAULT, lvgl.color_hex(0x0088FF))
lvgl.style_set_value_str(Style_LvglButton2_1, lvgl.STATE_DEFAULT, "Hello LuatOS UI")
lvgl.obj_add_style(ScreenA.LvglButton2.self, lvgl.BTN_PART_MAIN, Style_LvglButton2_1)
lvgl.style_set_value_color(Style_LvglButton2_1, lvgl.STATE_DEFAULT, lvgl.color_hex(0xFFFFFF))
------------USER CODE DATA--------------
end

require "http"
require "audio"
require "pins"

pins.setup(pio.P0_7,1)

local t={
    {
        img="/lua/2.jpg",
        music="/lua/A.mp3",
    },
    {
        img="/lua/1.jpg",
        music="/lua/B.mp3",
    }
}

local u={}
local m={}

local i=0


function padd()
    sys.taskInit(function()
        while true do
            i=i+1
            -- 设置图片显示的图像
            lvgl.img_set_src(ScreenA.LvglImg2.self, t[i].img)
            --playMusic(t[i].music)
            sys.wait(2000)
            if i>=2 then
                i=0
            end
        end
    end)
end

function playMusic(src)
    audio.play(0,"FILE",src,1,function ()
        log.info("audio result")
    end)
end

function simulation()
    sys.taskInit(function()
        while true do
            i=i+1
            lvgl.img_set_src(ScreenA.LvglImg2.self,t[i].img)
            sys.wait(2000)
            if i >= 2 then
                i = 0
            end
        end
    end)
end

local Payload
sys.subscribe("URL_RPT_RESULT",function(payload)
    if payload then
        Payload=payload
        log.info("payload",Payload)
        local picName = string.sub(Payload,-9)
        local lastName = string.sub(Payload,-4)
        if lastName == ".jpg" or lastName == ".png" then
            sys.taskInit(function()
                http.request("GET",
                    Payload,
                    nil,
                    nil,
                    nil,
                    30000,
                    function(result,statusCode,head,filePath)
                        log.info("uploadRpt.rsp",result,statusCode,head,filePath)
                        sys.publish("UPLOAD_RPT_RSP",result,statusCode,head,filePath)
                    end,
                    "/lua/"..picName)
                local _,result,statusCode,head,filePath = sys.waitUntil("UPLOAD_RPT_RSP")
                log.info("文件路径",io.exists("/lua/"..picName))
                if result and statusCode=="200" and filePath and filePath~="" then
                    if io.exists("/lua/"..picName) then
                        table.insert(u,"/lua/"..picName)
                        sys.publish("PICTRUE_CHANGE_INT")
                    end
                end
            end)
        elseif lastName == ".mp3" or lastName == ".amr" then
            sys.taskInit(function()
                sys.wait(5000)
                --挂载SD卡,返回值0表示失败，1表示成功
                --io.mount(io.SDCARD)
                log.info("IO",io.mount(io.SDCARD))
                http.request("GET",
                    Payload,
                    nil,
                    nil,
                    nil,
                    30000,
                    function(result,statusCode,head,filePath)
                        log.info("uploadRpt.rsp",result,statusCode,head,filePath)
                        sys.publish("UPLOAD_RPT_RSP",result,statusCode,head,filePath)
                    end,
                    "/sdcard0/"..picName)
                local _,result,statusCode,head,filePath = sys.waitUntil("UPLOAD_RPT_RSP")
                log.info("文件路径",io.exists("/sdcard0/"..picName))
                if result and statusCode=="200" and filePath and filePath~="" then
                    if io.exists("/sdcard0/"..picName) then
                        table.insert(m,"/sdcard0/"..picName)
                        sys.publish("MUSIC_CHANGE_INT")
                    end
                end
            end)
        end
    end
end)

--This function name and notes cannot be modified
--@@funCfg: <change, exist>
function change()
------------USER CODE DATA--------------
if not lvgl.indev_get_emu_touch then
    padd()
elseif lvgl.indev_get_emu_touch then
    simulation()
end
------------USER CODE DATA--------------
end

sys.subscribe("PICTRUE_CHANGE_INT", function()
    lvgl.img_set_src(ScreenA.LvglImg1.self,u[1])
    table.remove(u)
end)

sys.subscribe("MUSIC_CHANGE_INT",function()
    if not lvgl.indev_get_emu_touch then
        log.info("music name",m[1])
        playMusic(m[1])
        table.remove(m)
    elseif lvgl.indev_get_emu_touch then
        log.info("audioplay result")
    end
end)
