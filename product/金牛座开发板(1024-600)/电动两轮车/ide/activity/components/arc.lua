module(..., package.seeall)

function create_bg_style(v)
    if not v then v = {} end

    local s = lvgl.style_t()

    lvgl.style_init(s)

    lvgl.style_set_bg_color(s, lvgl.STATE_DEFAULT,
                            lvgl.color_hex(v.bg_color or 0xFFFFFF))
    lvgl.style_set_bg_opa(s, lvgl.STATE_DEFAULT, v.bg_opa or 255)

    lvgl.style_set_line_color(s, lvgl.STATE_DEFAULT,
                              lvgl.color_hex(v.line_color or 0xFFFFFF))
    lvgl.style_set_line_width(s, lvgl.STATE_DEFAULT, v.line_width or 5)

    lvgl.style_set_border_color(s, lvgl.STATE_DEFAULT,
                                lvgl.color_hex(v.border_color or 0xFFFFFF))

    lvgl.style_set_border_width(s, lvgl.STATE_DEFAULT, v.border_width or 1)
    lvgl.style_set_border_opa(s, lvgl.STATE_DEFAULT, v.border_opa or 255)


    -- 外边距
    lvgl.style_set_margin_left(s, lvgl.STATE_DEFAULT, v.margin_left or 0)
    lvgl.style_set_margin_right(s, lvgl.STATE_DEFAULT, v.margin_right or 0)
    lvgl.style_set_margin_top(s, lvgl.STATE_DEFAULT, v.margin_top or 0)
    lvgl.style_set_margin_bottom(s, lvgl.STATE_DEFAULT, v.margin_bottom or 0)

    return s
end

function create_indic_style(v)
    if not v then v = {} end

    local s = lvgl.style_t()

    lvgl.style_init(s)
    -- Write style state: lvgl.STATE_DEFAULT for s
    lvgl.style_set_line_color(s, lvgl.STATE_DEFAULT,
                              lvgl.color_hex(v.line_color or 0xFFFFFF))
    lvgl.style_set_line_width(s, lvgl.STATE_DEFAULT, v.line_width or 5)
    return s
end


function create(p, v)
    local a = lvgl.arc_create(p, nil)
    lvgl.obj_set_size(a, v.R or 110, v.R or 110)

    -- 偏移角度，默认开始角度在三点钟方向，通过这个值修改弧线的起点
    lvgl.arc_set_rotation(a, v.rotation or 0)

    -- 背景角度   注意：要先绘制背景！！！
    lvgl.arc_set_bg_angles(a, 0, v.bg_end or 360)
    -- 前景角度
    lvgl.arc_set_angles(a, 0, v.arc_end or 50)

    lvgl.arc_set_range(a, 0, 100)
    lvgl.arc_set_value(a, 80)

    lvgl.arc_set_adjustable(a, false)

    lvgl.obj_align(a, v.align_to, v.align or lvgl.ALIGN_IN_TOP_MID,
                   v.align_x or 0, v.align_y or 0)

    -- lvgl.obj_set_style_local_line_rounded(a, lvgl.ARC_PART_INDIC, lvgl.STATE_DEFAULT, 0)

    lvgl.obj_add_style(a, lvgl.ARC_PART_BG, create_bg_style(v.style.bg))

    lvgl.obj_add_style(a, lvgl.ARC_PART_INDIC, create_indic_style(v.style.indic))
    return a
end
