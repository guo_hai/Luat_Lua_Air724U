module(..., package.seeall)

function create(p, v)
    local obj = lvgl.led_create(p, nil)
    if v.click then lvgl.obj_set_click(obj, true) end
    lvgl.obj_set_size(obj, v.W or 200, v.H or 200)

    -- 可以使用 lvgl.led_set_bright(led, bright) 设置它们的亮度。亮度应介于0（最暗）和255（最亮）之间。
    if v.bright then lvgl.led_set_bright(obj, v.bright) end

    -- 使用 lvgl.led_on(led) 和 lvgl.led_off(led) 将亮度设置为预定义的ON或OFF值。
    -- lvgl.led_toggle(led) 在ON和OFF状态之间切换。
    -- lvgl.led_off(obj)

    -- LED只有一个主要部分，称为 lvgl.LED_PART_MAIN ，它使用所有典型的背景样式属性。
    lvgl.obj_add_style(obj, lvgl.LED_PART_MAIN, create_style(v.style))

    lvgl.obj_align(obj, v.align_to or nil, v.align or lvgl.ALIGN_CENTER,
                   v.align_x or 0, v.align_y or 0)

    return obj
end

local led_data = {
    W = 200,
    H = 200,
    style = {
        -- border = {color = 0x0f0ff0, opa = 150, width = 10},
        -- outline= {color = 0x0f0ff0, opa = 150, width = 10},
        bg = {
            radius = 100,
            color = 0xff0000,
            opa = 255,
            grad = {color = 0x0f0f0f}
        }
    }
}
-- create(lvgl.scr_act(), led_data)
