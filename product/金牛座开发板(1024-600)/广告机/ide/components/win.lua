module(..., package.seeall)

function create(p, v)
    local obj = lvgl.win_create(p, nil)
    if not v then return obj end
    if not v.click then lvgl.obj_set_click(obj, false) end
    lvgl.obj_set_size(obj, v.W or 300, v.H or 200)

    -- 窗口上有一个标题，可以通过以下方式修改 lvgl.win_set_title(obj, "New title")
    lvgl.win_set_title(obj, "New title")

    -- 可以使用以下命令将控制按钮添加到窗口标题的右侧： lvgl.win_add_btn_right(obj, lvgl.SYMBOL_CLOSE) ，
    -- 要在窗口标题的左侧添加按钮，请使用lvgl.win_add_btn_left(win，lvgl.SYMBOL_CLOSE)。
    -- 第二个参数是图像源，因此它可以是符号，指向lvgl.img_dsc_t变量的指针或文件的路径。
    -- 可以使用 lvgl.win_add_btn_left(obj, lvgl.SYMBOL_CLOSE) 设置按钮的宽度。如果 w == 0 ，则按钮将为正方形。
    -- lvgl.win_close_event_cb 可以用作关闭窗口的事件回调。

    lvgl.win_add_btn_right(obj, SYMBOL_CLOSE)
    lvgl.win_add_btn_left(obj, SYMBOL_BATTERY_EMPTY)

    -- 可以根据以下四个策略显示滚动条：
    -- lvgl.SCRLBAR_MODE_OFF 一直都不显示滚动条
    -- lvgl.SCRLBAR_MODE_ON 一直都显示滚动条
    -- lvgl.SCRLBAR_MODE_DRAG 拖动页面时显示滚动条
    -- lvgl.SCRLBAR_MODE_AUTO 当可滚动容器的大小足以滚动时显示滚动条
    -- lvgl.SCRLBAR_MODE_HIDE 暂时隐藏滚动条
    -- lvgl.SCRLBAR_MODE_UNHIDE 取消隐藏以前隐藏的滚动条。也恢复原始模式
    -- 可以通过以下方式更改滚动条显示策略： lvgl.page_set_scrollbar_mode(page, SB_MODE) 。
    -- 默认值为 lvgl.SCRLBAR_MODE_AUTO 。
    if v.mode then lvgl.page_set_scrollbar_mode(obj, v.mode) end

    -- 要直接滚动窗口，可以使用 lvgl.win_scroll_hor(obj, dist_px) 或 lvgl.win_scroll_ver(obj, dist_px) 。
    -- 要使窗口在其上显示对象，请使用 lvgl.win_focus(obj, child, lvgl.ANIM_ON/OFF) 。
    -- 滚动和焦点动画的时间可以使用 lvgl.win_set_anim_time(obj, anim_time_ms) 进行调整

    -- 要设置内容的布局，请使用 lvgl.win_set_layout(obj, lvgl.LAYOUT_...) 。有关详细信息，请参见 容器(lvgl.cont) 。

    -- 主要部分是 lvgl.WIN_PART_BG ，它包含另外两个实际部分：
    -- lvgl.WIN_PART_HEADER 顶部的标题容器，带有标题和控制按钮
    -- lvgl.WIN_PART_CONTENT_SCRL 页眉下方内容的页面可滚动部分。
    -- 除此之外， lvgl.WIN_PART_CONTENT_SCRL 还有一个滚动条，称为 lvgl.WIN_PART_CONTENT_SCRL 。
    -- 阅读 页面(lvgl.page) 的文档以获取有关滚动条的更多详细信息。
    -- 所有部分均支持典型的背景属性。标题使用标题部分的Text属性。
    -- 控制按钮的高度为：标头高度-标头padding_top-标头padding_bottom。

    if v.style then
        lvgl.obj_add_style(obj, lvgl.WIN_PART_BG, create_style(v.style.bg))
        lvgl.obj_add_style(obj, lvgl.WIN_PART_CONTENT_SCROLLABLE,
                           create_style(v.style.scrollable))
        lvgl.obj_add_style(obj, lvgl.WIN_PART_HEADER,
                           create_style(v.style.header))
        lvgl.obj_add_style(obj, lvgl.WIN_PART_SCROLLBAR,
                           create_style(v.style.scrollbar))
    end

    lvgl.obj_align(obj, v.align_to, v.align or lvgl.ALIGN_CENTER,
                   v.align_x or 0, v.align_y or 0)
    return obj
end

-- local win_obj = create(lvgl.scr_act(), {
--     style = {
--         bg = {bg = {radius = 10, color = 0x00ffff, opa = 255}},
--         scrollable = {bg = {radius = 10, color = 0x0000ff, opa = 255}},
--         header = {
--             bg = {radius = 10, color = 0x00ff00, opa = 255},
--             text = {font = style.font48}
--         },
--         scrollbar = {bg = {radius = 10, color = 0xffff00, opa = 255}}
--     }
-- })

-- cont.create(win_obj, {
--     W = 300,
--     H = 300,
--     click = false,
--     style = {
--         border = {color = 0x0f0f0f, width = 2, opa = 30},
--         shadow = {spread = 30, color = 0xff00f0},
--         bg = {
--             radius = 10,
--             color = 0x0f0ff0,
--             opa = 150,
--             grad = {color = 0x0f0f0f}
--         }
--     }
-- })

