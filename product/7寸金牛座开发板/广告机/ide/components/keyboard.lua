module(..., package.seeall)

function create(p, v)
    local obj = lvgl.keyboard_create(p, nil)
    if v.click then lvgl.obj_set_click(obj, true) end
    lvgl.obj_set_size(obj, v.W or 400, v.H or 200)
    

    -- lvgl.KEYBOARD_MODE_TEXT_LOWER – 显示小写字母
    -- lvgl.KEYBOARD_MODE_TEXT_UPPER – 显示大写字母
    -- lvgl.KEYBOARD_MODE_TEXT_SPECIAL – 显示特殊字符
    -- lvgl.KEYBOARD_MODE_NUM – 显示数字，+ /-号和小数点。
    -- 默认更多是 lvgl.KEYBOARD_MODE_TEXT_UPPER 。
    lvgl.keyboard_set_mode(obj, v.mode or lvgl.KEYBOARD_MODE_NUM)

    -- lvgl.keyboard_set_ctrl_map(obj)
    -- lvgl.keyboard_set_cursor_manage(obj)
    -- lvgl.keyboard_set_map(obj)
    -- lvgl.keyboard_set_textarea(obj)

    -- lvgl.KEYBOARD_PART_BG 这是主要部分，并使用了所有典型的背景属性
    -- lvgl.KEYBOARD_PART_BTN 这是按钮的虚拟部分。它还使用所有典型的背景属性和文本属性。

    if v.style then
        lvgl.obj_add_style(obj, lvgl.KEYBOARD_PART_BG, create_style(v.style.bg))
        lvgl.obj_add_style(obj, lvgl.KEYBOARD_PART_BTN,
                           create_style(v.style.btn))
    end
    lvgl.obj_align(obj, v.align_to or nil, v.align or lvgl.ALIGN_CENTER,
    v.align_x or 0, v.align_y or 0)
end
local keyboard_data = {
    mode = lvgl.KEYBOARD_MODE_TEXT_UPPER,
    style = {
        bg = {
            bg = {
                radius = 10,
                color = 0x0f0ff0,
                opa = 150,
                grad = {color = 0x0f0f0f}
            }
        },
        btn = {
            bg = {
                radius = 10,
                color = 0x0f0ff0,
                opa = 150,
                grad = {color = 0x0f0f0f}
            },
            text = {font = style.font24}
        }
    }

}
-- create(lvgl.scr_act(), keyboard_data)
