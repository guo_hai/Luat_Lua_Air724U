module(..., package.seeall)

function create(p, v)
    obj = lvgl.cont_create(p, nil)
    lvgl.cont_set_fit(obj, v.fit or lvgl.FIT_NONE)
    if v.click then lvgl.obj_set_click(obj, true) end
    if v.W and v.H then lvgl.obj_set_size(obj, v.W, v.H) end
    -- LAYOUT_CENTER
    -- LAYOUT_COLUMN_LEFT
    -- LAYOUT_COLUMN_MID
    -- LAYOUT_COLUMN_RIGHT
    -- LAYOUT_GRID
    -- LAYOUT_OFF
    -- LAYOUT_PRETTY_BOTTOM
    -- LAYOUT_PRETTY_MID
    -- LAYOUT_PRETTY_TOP
    -- LAYOUT_ROW_BOTTOM
    -- LAYOUT_ROW_MID
    -- LAYOUT_ROW_TOP
    if v.layout then lvgl.cont_set_layout(obj, v.layout) end
    -- lvgl.obj_set_auto_realign(Titlecont, true)                   
    lvgl.obj_align(obj, v.align_to, v.align or lvgl.ALIGN_CENTER,
                   v.align_x or 0, v.align_y or 0)
    if v.event then lvgl.obj_set_event_cb(obj, v.event) end
    lvgl.obj_add_style(obj, lvgl.CONT_PART_MAIN, create_style(v.style))
    return obj
end
local cont_data = {
    W = 1024,
    H = 600,
    click = false,
    style = {
        border = {color = 0x0f0f0f, width = 2, opa = 30},
        shadow = {spread = 30, color = 0xff00f0},
        bg = {
            radius = 10,
            color = 0xff00ff,
            opa = 250,
            grad = {color = 0x0f0f0f}
        }
    }
}
-- local cont_obj = create(lvgl.scr_act(), cont_data)
