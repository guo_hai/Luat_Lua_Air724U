--This function name and notes cannot be modified
--@@funCfg: <joinUs, exist>
function joinUs()
------------USER CODE DATA--------------

------------USER CODE DATA--------------
end

--更新时间
sys.subscribe("SYSTEM_CHANGE_TIME",function(date)
    lvgl.label_set_text(ScreenA.LvglLabel2.self, date)
    end)

--更新4G信号
sys.subscribe("RSSI_CHANGE",function(img)
    lvgl.img_set_src(ScreenA.LvglImg6.self,img)
end)

--字体设置
event_handler = function(obj, event)
    log.info("event_handler",event,lvgl.EVENT_VALUE_CHANGED,lvgl.EVENT_CANCEL,lvgl.EVENT_DEFOCUSED)
    if (event == lvgl.EVENT_VALUE_CHANGED) then
        local opt = lvgl.dropdown_get_selected(obj)
        log.info("Option:", lvgl.dropdown_get_selected(obj))
        _G.BEEP()
        if opt == 1 then
            _G.Font="vector"
        else
            _G.Font="default"
        end
        lvgl.obj_del(itemCont)
        nvm.set("Font",_G.Font)
        sys.restart('设置字体重启')
    end

end

--This function name and notes cannot be modified
--@@funCfg: <Font_Set, exist>
function Font_Set()
------------USER CODE DATA--------------
itemCont=lvgl.cont_create(lvgl.scr_act(), nil)
lvgl.obj_set_click(itemCont,false)
lvgl.obj_set_size(itemCont, 220,60)
lvgl.obj_align(itemCont, ScreenA.LvglButton2.self, lvgl.ALIGN_OUT_BOTTOM_MID, 0, 0)

local label=lvgl.label_create(itemCont, nil)

lvgl.label_set_text(label, "字库:")
lvgl.obj_align(label, itemCont, lvgl.ALIGN_IN_LEFT_MID, 5, 0)

local dd = lvgl.dropdown_create(itemCont, nil)

lvgl.dropdown_set_options(dd, [[默认字库
矢量字库]])
lvgl.obj_set_event_cb(dd, event_handler)
lvgl.obj_set_size(dd, 150,40)
lvgl.obj_align(dd, itemCont, lvgl.ALIGN_IN_LEFT_MID, 60, 0)
------------USER CODE DATA--------------
end

_G.KEYBOARDTEMPTEXT = ""

--地址输入
function Addr_keyCb(obj, e)
    -- 默认处理事件
    lvgl.keyboard_def_event_cb(keyBoard, e)
    if(e == lvgl.EVENT_CANCEL) or (e ==lvgl.EVENT_APPLY)  or (e ==lvgl.EVENT_DELETE) then
        _G.BEEP()
        lvgl.keyboard_set_textarea(keyBoard, nil)
        local text =lvgl.textarea_get_text(ScreenA.LvglTextarea1.self)
        if text == "" then
            lvgl.textarea_set_text(ScreenA.LvglTextarea1.self,_G.KEYBOARDTEMPTEXT)
            text = _G.KEYBOARDTEMPTEXT
        end
        _G.ipAddress = text
        --删除 KeyBoard
        if e ~=lvgl.EVENT_DELETE then
            lvgl.obj_del(keyBoard)
            keyBoard = nil
        end
    end
end

--This function name and notes cannot be modified
--@@funCfg: <Addr_CLICKED, exist>
function Addr_CLICKED()
------------USER CODE DATA--------------
        --删除 KeyBoard
        if _G.keyBoard~=nil then
            lvgl.obj_del(keyBoard)
            keyBoard = nil
        end
        --创建一个 KeyBoard
        keyBoard = lvgl.keyboard_create(lvgl.scr_act(), nil)
        --设置 KeyBoard 尺寸
        lvgl.obj_set_size(keyBoard, 300,160)
        --设置对齐方式
        lvgl.obj_align(keyBoard,ScreenA.LvglTextarea1.self, lvgl.ALIGN_OUT_RIGHT_BOTTOM, 0, 0)
        --获取并设置文本
        _G.KEYBOARDTEMPTEXT=lvgl.textarea_get_text(ScreenA.LvglTextarea1.self)
        lvgl.textarea_set_text(ScreenA.LvglTextarea1.self,"")
        --设置 KeyBoard 的光标是否显示
        lvgl.keyboard_set_cursor_manage(keyBoard, true)
        --为 KeyBoard 设置一个文本区域
        lvgl.keyboard_set_textarea(keyBoard, ScreenA.LvglTextarea1.self)
        lvgl.obj_set_event_cb(keyBoard, Addr_keyCb)
------------USER CODE DATA--------------
end

--端口输入
function Port_keyCb(obj, e)
    -- 默认处理事件
    lvgl.keyboard_def_event_cb(keyBoard, e)
    if(e == lvgl.EVENT_CANCEL) or (e ==lvgl.EVENT_APPLY) or (e ==lvgl.EVENT_DELETE) then
        _G.BEEP()
        lvgl.keyboard_set_textarea(keyBoard, nil)
        local text =lvgl.textarea_get_text(ScreenA.LvglTextarea2.self)
        if text == "" then
            lvgl.textarea_set_text(ScreenA.LvglTextarea2.self,_G.KEYBOARDTEMPTEXT)
            text = _G.KEYBOARDTEMPTEXT
        end
        _G.ipPort = text
        --删除 KeyBoard
        if e ~=lvgl.EVENT_DELETE then
            lvgl.obj_del(keyBoard)
            keyBoard = nil
        end
    end
end

--This function name and notes cannot be modified
--@@funCfg: <Port_CLICKED, exist>
function Port_CLICKED()
------------USER CODE DATA--------------
    --删除 KeyBoard
    if _G.keyBoard~=nil then
        lvgl.obj_del(keyBoard)
        keyBoard = nil
    end
    --创建一个 KeyBoard
    keyBoard = lvgl.keyboard_create(lvgl.scr_act(), nil)
    --设置 KeyBoard 尺寸
    lvgl.obj_set_size(keyBoard, 300,160)
    --设置对齐方式
    lvgl.obj_align(keyBoard,ScreenA.LvglTextarea2.self, lvgl.ALIGN_OUT_RIGHT_BOTTOM, 0, 0)
    --获取并设置文本
    _G.KEYBOARDTEMPTEXT=lvgl.textarea_get_text(ScreenA.LvglTextarea2.self)
    lvgl.textarea_set_text(ScreenA.LvglTextarea2.self,"")
    --设置 KeyBoard 的光标是否显示
    lvgl.keyboard_set_cursor_manage(keyBoard, true)
    --为 KeyBoard 设置一个文本区域
    lvgl.keyboard_set_textarea(keyBoard, ScreenA.LvglTextarea2.self)
    lvgl.obj_set_event_cb(keyBoard, Port_keyCb)
------------USER CODE DATA--------------
end


--发送数据输入
function Send_keyCb(obj, e)
    -- 默认处理事件
    lvgl.keyboard_def_event_cb(keyBoard, e)
    if(e == lvgl.EVENT_CANCEL) or (e ==lvgl.EVENT_APPLY) or (e ==lvgl.EVENT_DELETE) then
        _G.BEEP()
        lvgl.keyboard_set_textarea(keyBoard, nil)
        local text =lvgl.textarea_get_text(ScreenA.LvglTextarea3.self)
        if text == "" then
            lvgl.textarea_set_text(ScreenA.LvglTextarea3.self,_G.KEYBOARDTEMPTEXT)
            text = _G.KEYBOARDTEMPTEXT
        end
        _G.sendData = text
        --删除 KeyBoard
        if e ~=lvgl.EVENT_DELETE then
            lvgl.obj_del(keyBoard)
            keyBoard = nil
        end
    end
end

--This function name and notes cannot be modified
--@@funCfg: <SendData_CLICKED, exist>
function SendData_CLICKED()
    ------------USER CODE DATA--------------
    --删除 KeyBoard
    if _G.keyBoard~=nil then
        lvgl.obj_del(keyBoard)
        keyBoard = nil
    end
     --创建一个 KeyBoard
    keyBoard = lvgl.keyboard_create(lvgl.scr_act(), nil)
    --设置 KeyBoard 尺寸
    lvgl.obj_set_size(keyBoard, 300,160)
    --设置对齐方式
    lvgl.obj_align(keyBoard,ScreenA.LvglTextarea3.self, lvgl.ALIGN_OUT_RIGHT_BOTTOM, 0, 0)
    --获取并设置文本
    _G.KEYBOARDTEMPTEXT=lvgl.textarea_get_text(ScreenA.LvglTextarea3.self)
    lvgl.textarea_set_text(ScreenA.LvglTextarea3.self,"")
    --设置 KeyBoard 的光标是否显示
    lvgl.keyboard_set_cursor_manage(keyBoard, true)
    --为 KeyBoard 设置一个文本区域
    lvgl.keyboard_set_textarea(keyBoard, ScreenA.LvglTextarea3.self)
    lvgl.obj_set_event_cb(keyBoard, Send_keyCb)
    ------------USER CODE DATA--------------
    end


--更改协议
--This function name and notes cannot be modified
--@@funCfg: <Prot_Dropdown, exist>
function Prot_Dropdown()
------------USER CODE DATA--------------
    local opt = lvgl.dropdown_get_selected(ScreenA.LvglDropdown1.self)
    log.info("Option:", opt)
    _G.BEEP()
    if opt == 1 then
        _G.ipProtocol = "udp"
    else
        _G.ipProtocol = "tcp"
    end
------------USER CODE DATA--------------
end

_G.fragmentConnect=true

--连接服务器
--This function name and notes cannot be modified
--@@funCfg: <Connect_CLICKED, exist>
function Connect_CLICKED()
------------USER CODE DATA--------------
    _G.BEEP()
    log.info("Connect_CLICKED",_G.fragmentConnect)
    if _G.fragmentConnect then
        lvgl.label_set_text(vLabel_LvglButton3, "断开连接")
        _G.fragmentConnect = false
        sys.publish("CONNECT_IND")
    else
        lvgl.label_set_text(vLabel_LvglButton3, "连接")
        _G.fragmentConnect=true
        sys.publish("SOCKET_DISCONNECT_IND")
        socketOutMsg.sndTest("disconnect")
    end
------------USER CODE DATA--------------
end

--发送数据
--This function name and notes cannot be modified
--@@funCfg: <Send_CLICKED, exist>
function Send_CLICKED()
------------USER CODE DATA--------------
    _G.BEEP()
    socketOutMsg.sndTest(_G.sendData)
    log.info("UI makeSendBtn",_G.fragmentSend)
    if _G.fragmentSend then
        _G.fragmentSend = false
    else
        _G.fragmentSend=true
    end
------------USER CODE DATA--------------
end

--接收到数据
function recvDataInd()
    --if RecvLabel then lvgl.label_set_text(RecvLabel, _G.recvData) end
    --if RecvLabel then lvgl.label_set_text(RecvLenLabel, "已接收"..#_G.recvData.."个字节")end
    log.info("recvDataInd",_G.recvData,#_G.recvData)
    lvgl.label_set_text(ScreenA.LvglLabel10.self, _G.recvData)
    lvgl.label_set_text(vLabel_LvglButton5, "已接收"..#_G.recvData.."个字节")
end

sys.subscribe("RECV_DATA_IND",recvDataInd)

