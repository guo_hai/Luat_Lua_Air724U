module(..., package.seeall)
--------------
require "sim"

require "net"

function RSSIchange()
    local Rssi=0
    sys.taskInit(function()
        while true do
            local s=net.getRssi()
            if s~=Rssi then
                Rssi=s
                sys.publish("RSSI_CHANGE","/lua/sin_icon_"..math.ceil((5/31)*Rssi)..".png")   
            end
            sys.wait(1000)
        end
    end)
end

sys.subscribe("NET_STATE_REGISTERED",RSSIchange)
