module(..., package.seeall)
_G.fragmentSend = true
_G.ipAddress="112.125.89.8"
_G.ipPort="37391"
sendData="socket send test"
_G.recvData="socket recv test"
rvCont = nil

event_handler_send = function(obj, event)
    if event == lvgl.EVENT_CLICKED then
        _G.BEEP()
        socketOutMsg.sndTest(sendData)
        log.info("UI makeSendBtn",_G.fragmentSend)
        if _G.fragmentSend then
            _G.fragmentSend = false
        else
            _G.fragmentSend=true
        end
    end
end

function sendInit(sysCont)

    function makeSendBtn()
        local btn = lvgl.cont_create(sysCont, nil)
        lvgl.obj_set_size(btn, 200, 50)
        lvgl.obj_add_style(btn, lvgl.BTN_PART_MAIN, style.style_QRbtnBg)
        lvgl.obj_set_event_cb(btn, event_handler_send)
        local Label=lvgl.label_create(btn, nil)
        if _G.Font=="vector"then
            lvgl.obj_set_style_local_text_font(Label, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font24)       
        end 
        lvgl.label_set_recolor(Label, true) 
        lvgl.label_set_text(Label, "#FFFFFE 发送")
        lvgl.obj_align(Label, btn, lvgl.ALIGN_CENTER, 0, 0)
    end

    function makeLable(text,x,y)
        local label=lvgl.label_create(sysCont, nil)
        if _G.Font=="vector"then
            lvgl.obj_set_style_local_text_font(label, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font24)       
        end 
        lvgl.label_set_text(label, text)
        lvgl.obj_align(label, sysCont, lvgl.ALIGN_IN_TOP_MID, x, y)
    end
    
    event_handler = function(obj, event)
        if (event == lvgl.EVENT_VALUE_CHANGED) then
            print("Option:", lvgl.dropdown_get_selected(obj))
        end
    end
      
        --创建limit输入框
    local function makeLimitBox(cont,alignObj,alignX,alignY,p1,p2)
        local limitCont=lvgl.cont_create(cont, nil)
        lvgl.obj_set_click(limitCont,false)
        lvgl.obj_set_size(limitCont, 202,280) 
        lvgl.obj_add_style(limitCont, lvgl.CONT_PART_MAIN, style.style_divBox)
        lvgl.obj_align(limitCont,cont, lvgl.ALIGN_IN_TOP_MID, 5, 2)

        --输入框回调
        local function paraChange(obj)
            sendData = lvgl.textarea_get_text(obj)
            log.info("UI sendData",sendData)
        end

        local inputLeft=inputBox.init(limitCont,200,278,lvgl.ALIGN_CENTER,5, 0,p1,paraChange)

        return limitCont,inputLeft,inputRight
    end

    makeLable("发送数据",0,0)
    
    
    --发送内容输入框
    local itemCont=lvgl.cont_create(sysCont, nil)
    lvgl.obj_set_click(itemCont,false)
    lvgl.obj_set_size(itemCont, 210,260) 
    lvgl.obj_add_style(itemCont, lvgl.CONT_PART_MAIN, style.style_divBox)
     
    makeLimitBox(itemCont,sysCont,10,0,"socket send test","09")
    
    --发送按钮
    makeSendBtn()
    
end

local RecvLabel
local RecvLenLabel

function recvInit(recvCont)

    function makeRecvBtn()
        local btn = lvgl.cont_create(recvCont, nil)
        lvgl.obj_set_size(btn, 200, 50)
        lvgl.obj_add_style(btn, lvgl.BTN_PART_MAIN, style.style_QRbtnBg)
        --lvgl.obj_set_event_cb(btn, event_handler_send)
        RecvLenLabel=lvgl.label_create(btn, nil)
        if _G.Font=="vector"then
            lvgl.obj_set_style_local_text_font(RecvLenLabel, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font24)       
        end 
        lvgl.label_set_recolor(RecvLenLabel, true) 
        lvgl.label_set_text(RecvLenLabel, "#FFFFFE 已接收"..#_G.recvData.."个字节")
        lvgl.obj_align(RecvLenLabel, btn, lvgl.ALIGN_CENTER, 0, 0)
    end
    
    --数据接收
    local label=lvgl.label_create(recvCont, nil)
    if _G.Font=="vector"then
        lvgl.obj_set_style_local_text_font(label, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font24)       
    end     
    lvgl.label_set_text(label, "数据接收")
    lvgl.obj_align(label, recvCont, lvgl.ALIGN_IN_TOP_MID, 0, 0)
      
    local itemCont=lvgl.cont_create(recvCont, nil)
    lvgl.obj_set_click(itemCont,false)
    lvgl.obj_set_size(itemCont, 210,260) 
    lvgl.obj_add_style(itemCont, lvgl.CONT_PART_MAIN, style.style_msgBg)
    
    RecvLabel=lvgl.label_create(itemCont, nil)
    if _G.Font=="vector"then
        lvgl.obj_set_style_local_text_font(RecvLabel, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font24)       
    end 
    lvgl.label_set_long_mode(RecvLabel,lvgl.LABEL_LONG_BREAK)
    lvgl.obj_set_width(RecvLabel, 200)
    lvgl.label_set_text(RecvLabel, _G.recvData)
    lvgl.obj_align(RecvLabel, itemCont, lvgl.ALIGN_IN_TOP_LEFT, 0, 0) 

    makeRecvBtn()    
end

function recvDataInd()
    if RecvLabel then lvgl.label_set_text(RecvLabel, _G.recvData) end  
    if RecvLabel then lvgl.label_set_text(RecvLenLabel, "#FFFFFE 已接收"..#_G.recvData.."个字节")end
end

function init(sysCont,recvCont)
    sendInit(sysCont)
    recvInit(recvCont)
    rvCont = recvCont
end

sys.subscribe("RECV_DATA_IND",recvDataInd)
