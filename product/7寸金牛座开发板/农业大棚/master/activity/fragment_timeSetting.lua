module(..., package.seeall)


local topicTable={}
local function topicTableAdd(id,callback)
    topicTable[id]=callback
    sys.subscribe(id,callback)
end

_G.fragmentTimeSetting=false

local tempBranchTable
local branchTable

local function cloneTab(tb)
    local ret = {}
    for k,v in pairs(tb) do
        ret[k] = type(v) == "table" and cloneTab(v) or v
    end
    return ret
end

local function test(t1,t2)
    if #t1 ~=#t2 then return false end
    for _,t in ipairs(t1) do
        for i,v in pairs(t) do
            if t1[_][i]~=t2[_][i] then return false end
        end
    end
    return true
end

function save()
    for id, callback in pairs(topicTable) do
        sys.unsubscribe(id, callback)
    end
    if test(branchTable.timeControl,tempBranchTable.timeControl) then return end
    -- print("TIMECONTROL_CHANGE",branchTable.path)
    sys.publish("TIMECONTROL_CHANGE",branchTable.path)
end

function makeBG(cont)
    local BGcont=lvgl.cont_create(cont, nil)
    lvgl.obj_set_click(BGcont,false)
    lvgl.obj_set_size(BGcont, 790, 400) 
    lvgl.obj_add_style(BGcont, lvgl.CONT_PART_MAIN, style.style_contentBox)
    lvgl.obj_align(BGcont, cont, lvgl.ALIGN_IN_TOP_RIGHT, -5, 0)

    local settingBox=lvgl.cont_create(BGcont, nil)
    lvgl.obj_set_click(settingBox,false)
    lvgl.obj_set_size(settingBox, 770, 380) 
    lvgl.obj_add_style(settingBox, lvgl.CONT_PART_MAIN, style.style_msgBg)
    lvgl.obj_align(settingBox,BGcont, lvgl.ALIGN_CENTER, 0, 0)

    return settingBox
    
end

function paddiingBox(cont)
--------------------------------------------------------------
    local labelBox=lvgl.cont_create(cont, nil)
    lvgl.obj_set_click(labelBox,false)
    lvgl.obj_set_size(labelBox, 814,50) 
    lvgl.obj_add_style(labelBox, lvgl.CONT_PART_MAIN, style.style_divBox)
    lvgl.obj_align(labelBox,cont, lvgl.ALIGN_IN_TOP_MID, 0, 0)

    local label=lvgl.label_create(labelBox, nil)
    lvgl.label_set_recolor(label, true) 
    lvgl.obj_set_style_local_text_font(label, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font32)
    lvgl.label_set_text(label, "#E67517 分段补光设置")
    lvgl.obj_align(label, labelBox, lvgl.ALIGN_IN_LEFT_MID,20,0)

    --下拉框
    local RunningSatusTable=UIConfig.getBranchTable()
    local option=""
    for i=1,#RunningSatusTable do
        local v=RunningSatusTable[i]
        option=option..v.path
        if i<#RunningSatusTable then option=option.."\n" end
    end

    local event_handler = function(obj, event)
        if event == lvgl.EVENT_CLICKED then
            _G.BEEP()
        elseif event == lvgl.EVENT_VALUE_CHANGED then
            _G.BEEP()
            save()
            local index=lvgl.dropdown_get_selected(obj)
            branchTable=RunningSatusTable[index+1]
            paddingContral(settingCont,branchTable)
            -- paddingContral(settingCont,RunningSatusTable[index+1])
            log.info("UI","TODO:",index)
            -- changeMode()
        end
    end
    local dd = lvgl.dropdown_create(labelBox, nil)
    lvgl.obj_add_style(dd, lvgl.DROPDOWN_PART_SELECTED, style.style_labelBgBlue_noRadius)
    lvgl.obj_add_style(dd, lvgl.DROPDOWN_PART_LIST, style.style_list_BG_font)
    lvgl.obj_add_style(dd, lvgl.DROPDOWN_PART_MAIN, style.style_list_BG)
    lvgl.obj_set_style_local_text_font(dd, lvgl.DROPDOWN_PART_SELECTED,lvgl.STATE_DEFAULT, font24)
    lvgl.obj_set_style_local_text_font(dd, lvgl.DROPDOWN_PART_LIST,lvgl.STATE_DEFAULT, font24)
    lvgl.dropdown_set_options(dd, option)
    -- 设置对齐
    lvgl.obj_align(dd, labelBox, lvgl.ALIGN_IN_RIGHT_MID, -30, 0)
    lvgl.obj_set_event_cb(dd, event_handler)

    local line=lvgl.cont_create(labelBox, nil)
    lvgl.obj_set_click(line,false)
    lvgl.obj_set_size(line, 814, 1) 
    lvgl.obj_add_style(line, lvgl.CONT_PART_MAIN, style.style_line)
    lvgl.obj_align(line, labelBox, lvgl.ALIGN_IN_BOTTOM_MID, 0, 0)
--------------------------------------------------------------
    local titleBox=lvgl.cont_create(cont, nil)
    lvgl.obj_set_click(titleBox,false)
    lvgl.obj_set_size(titleBox, 814,40) 
    lvgl.obj_add_style(titleBox, lvgl.CONT_PART_MAIN, style.style_divBox)
    lvgl.obj_align(titleBox,labelBox, lvgl.ALIGN_OUT_BOTTOM_MID, 0, 0)

    local dateLabel=lvgl.label_create(titleBox, nil)
    lvgl.label_set_recolor(dateLabel, true) 
    lvgl.obj_set_style_local_text_font(dateLabel, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font32)
    lvgl.label_set_text(dateLabel, "日期")
    lvgl.obj_align(dateLabel, nil, lvgl.ALIGN_IN_LEFT_MID,220,0)

    local powerLabel=lvgl.label_create(titleBox, nil)
    lvgl.label_set_recolor(powerLabel, true) 
    lvgl.obj_set_style_local_text_font(powerLabel, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font32)
    lvgl.label_set_text(powerLabel, "补光时间")
    lvgl.obj_align(powerLabel, dateLabel, lvgl.ALIGN_OUT_RIGHT_MID,140,0)

    local offLabel=lvgl.label_create(titleBox, nil)
    lvgl.label_set_recolor(offLabel, true) 
    lvgl.obj_set_style_local_text_font(offLabel, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font32)
    lvgl.label_set_text(offLabel, "日照时间")
    lvgl.obj_align(offLabel,powerLabel, lvgl.ALIGN_OUT_RIGHT_MID,10,0)

    local switchLabel=lvgl.label_create(titleBox, nil)
    lvgl.label_set_recolor(switchLabel, true) 
    lvgl.obj_set_style_local_text_font(switchLabel, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font32)
    lvgl.label_set_text(switchLabel, "开关")
    lvgl.obj_align(switchLabel,offLabel, lvgl.ALIGN_OUT_RIGHT_MID,25,0)
--------------------------------------------------------------
    -- local settingCont=lvgl.cont_create(cont, nil)
    -- lvgl.obj_set_click(settingCont,false)
    -- lvgl.obj_set_size(settingCont, 814,280) 
    -- lvgl.obj_add_style(settingCont, lvgl.CONT_PART_MAIN, style.style_divBox)
    -- lvgl.cont_set_layout(settingCont, lvgl.LAYOUT_COLUMN_LEFT)
    -- lvgl.obj_align(settingCont,cont, lvgl.ALIGN_IN_BOTTOM_MID, 0, 0)

    settingCont = lvgl.page_create(cont, nil)
    lvgl.obj_set_size(settingCont, 814,290)
    lvgl.obj_set_click(settingCont,false)
    lvgl.obj_add_style(settingCont, lvgl.PAGE_PART_MAIN,style.style_divBox)
    lvgl.page_set_scrl_layout(settingCont,lvgl.LAYOUT_COLUMN_LEFT)
    lvgl.obj_align(settingCont,cont,lvgl.ALIGN_IN_BOTTOM_MID, 0, 0)
    -- lvgl.page_set_scrollbar_mode(settingCont, lvgl.SCRLBAR_MODE_DRAG)

--------------------------------------------填充具体时间段

local function makeItem(cont,t,parent)

    local itemCont=lvgl.cont_create(cont, nil)
    lvgl.obj_set_click(itemCont,false)
    lvgl.obj_set_size(itemCont, 814,60) 
    lvgl.obj_add_style(itemCont, lvgl.CONT_PART_MAIN, style.style_divBox)
    
    local tierLabel=lvgl.label_create(itemCont, nil)
    lvgl.obj_set_style_local_text_font(tierLabel, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font32)
    lvgl.label_set_text(tierLabel, t.seg..":")
    lvgl.obj_align(tierLabel,itemCont, lvgl.ALIGN_IN_LEFT_MID, 30, 0)


    --时间段
    local dateCont=lvgl.cont_create(itemCont, nil)
    lvgl.obj_set_click(dateCont,false)
    lvgl.obj_set_size(dateCont, 285,50) 
    lvgl.obj_add_style(dateCont, lvgl.CONT_PART_MAIN, style.style_dataBGWhite)
    lvgl.cont_set_layout(dateCont, lvgl.LAYOUT_ROW_MID)
    lvgl.obj_align(dateCont,tierLabel, lvgl.ALIGN_OUT_RIGHT_MID, 20, 0)


    -- local map={"一","二","三","四","五","六","日"}
    -- local englishMap={"monday","tuesday","wednesday","thursday","friday","saturday","sunday"}

    local map={  
        {"一","monday",btn=nil},
        {"二","tuesday",btn=nil},
        {"三","wednesday",btn=nil},
        {"四","thursday",btn=nil},
        {"五","friday",btn=nil},
        {"六","saturday",btn=nil},
        {"日","sunday",btn=nil}
            }

            local function makeBtn(index)
                local text=map[index][1]
                -- 按键回调函数
                event_handler = function(obj, event)
                    if event == lvgl.EVENT_CLICKED then
                        _G.BEEP()
                        local btnStatus=lvgl.btn_get_state(obj)
                        if btnStatus==lvgl.BTN_STATE_CHECKED_RELEASED then
                            log.info("UI",t.seg..text,"按下")
                            t[map[index][2]]=1
                            -- gg(text,1)
                            -- setAndGetStatus(text,1)
                        elseif btnStatus==lvgl.BTN_STATE_RELEASED then
                            log.info("UI",t.seg..text,"放开")
                            t[map[index][2]]=0
                            -- setAndGetStatus(text,0)
                            -- gg(text,0)
                            -- status=0
                        end
                        -- log.info("UI","Clicked 当前按钮状态:",lvgl.btn_get_state(obj),text)
                    end
                end
                local btn = lvgl.btn_create(dateCont, nil)
                map[index].btn=btn
                local status=t[map[index][2]]
                -- local status=setAndGetStatus(text)
                if status==1 then
                    lvgl.btn_set_state(btn,lvgl.BTN_STATE_CHECKED_RELEASED)
                end
                lvgl.obj_set_size(btn, 35,35) 
                lvgl.obj_add_style(btn, lvgl.BTN_PART_MAIN, style.style_btnWhite)
                lvgl.btn_set_checkable(btn, true)
                lvgl.obj_set_event_cb(btn, event_handler)
                -- 按键 的文字
                local label = lvgl.label_create(btn, nil)
                -- lvgl.obj_set_style_local_bg_color(btn,lvgl.BTN_PART_MAIN,lvgl.BTN_STATE_PRESSED,lvgl.color_hex(0x217bda))
                lvgl.obj_set_style_local_text_font(label, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font32)
                lvgl.label_set_text(label, text)
                lvgl.obj_align(label,btn, lvgl.ALIGN_CENTER, 0, 0)
            end
    
    -- local function makeBtn(index)
    --     local text=map[index]
    --     -- 按键回调函数
    --     event_handler = function(obj, event)
    --         if event == lvgl.EVENT_CLICKED then
    --             _G.BEEP()
    --             local btnStatus=lvgl.btn_get_state(obj)
    --             if btnStatus==lvgl.BTN_STATE_CHECKED_RELEASED then
    --                 log.info("UI",t.seg..text,"按下")
    --                 t[englishMap[index]]=1
    --                 -- gg(text,1)
    --                 -- setAndGetStatus(text,1)
    --             elseif btnStatus==lvgl.BTN_STATE_RELEASED then
    --                 log.info("UI",t.seg..text,"放开")
    --                 t[englishMap[index]]=0
    --                 -- setAndGetStatus(text,0)
    --                 -- gg(text,0)
    --                 -- status=0
    --             end
    --             -- log.info("UI","Clicked 当前按钮状态:",lvgl.btn_get_state(obj),text)
    --         end
    --     end
    --     local btn = lvgl.btn_create(dateCont, nil)
    --     local status=t[englishMap[index]]
    --     -- local status=setAndGetStatus(text)
    --     if status==1 then
    --         lvgl.btn_set_state(btn,lvgl.BTN_STATE_CHECKED_RELEASED)
    --     end
    --     lvgl.obj_set_size(btn, 35,35) 
    --     lvgl.obj_add_style(btn, lvgl.BTN_PART_MAIN, style.style_btnWhite)
    --     lvgl.btn_set_checkable(btn, true)
    --     lvgl.obj_set_event_cb(btn, event_handler)
    --     -- 按键 的文字
    --     local label = lvgl.label_create(btn, nil)
    --     -- lvgl.obj_set_style_local_bg_color(btn,lvgl.BTN_PART_MAIN,lvgl.BTN_STATE_PRESSED,lvgl.color_hex(0x217bda))
    --     lvgl.obj_set_style_local_text_font(label, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font32)
    --     lvgl.label_set_text(label, text)
    --     lvgl.obj_align(label,btn, lvgl.ALIGN_CENTER, 0, 0)
    -- end

    for index, value in ipairs(map) do
        -- makeBtn(value,t[englishMap[index]])
        makeBtn(index)
    end


    local powerOnTimeCont,powerOnStart,powerOnEnd
    local powerOffTimeCont,powerOffStart,powerOffEnd

    local function paraChange(obj)--输入框回调
        local function test(j)
            return lvgl.obj_get_id(obj)==lvgl.obj_get_id(j)
        end
        if test(powerOnStart) then
            log.info("UI",t.seg,"开机时间start",lvgl.textarea_get_text(obj))
            t.ontimeHour=tonumber(lvgl.textarea_get_text(obj))
        elseif test(powerOnEnd) then
            log.info("UI",t.seg,"开机时间end",lvgl.textarea_get_text(obj))
            t.ontimeMinite=tonumber(lvgl.textarea_get_text(obj))
        elseif test(powerOffStart) then
            log.info("UI",t.seg,"关机时间end",lvgl.textarea_get_text(obj))
            t.offtimeHour=tonumber(lvgl.textarea_get_text(obj))
        elseif test(powerOffEnd) then
            log.info("UI",t.seg,"关机时间end",lvgl.textarea_get_text(obj))
            t.offtimeMinite=tonumber(lvgl.textarea_get_text(obj))
        end
    end

    --创建limit输入框
    local function makeLimitBox(cont,alignObj,alignX,alignY,p1,p2)
        local limitCont=lvgl.cont_create(cont, nil)
        lvgl.obj_set_click(limitCont,false)
        lvgl.obj_set_size(limitCont, 120,50) 
        lvgl.obj_add_style(limitCont, lvgl.CONT_PART_MAIN, style.style_dataBGWhite)
        lvgl.obj_align(limitCont,alignObj, lvgl.ALIGN_OUT_RIGHT_MID, alignX, alignY)

        local dot=lvgl.label_create(limitCont, nil)
        lvgl.label_set_recolor(dot, true) 
        lvgl.obj_set_style_local_text_font(dot, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font32)
        lvgl.label_set_text(dot, "#E67517 :")
        lvgl.obj_align(dot,limitCont, lvgl.ALIGN_CENTER, 0, 0)

        local inputLeft=inputBox.init(limitCont,50,40,lvgl.ALIGN_IN_LEFT_MID,5, 0,p1,paraChange)
        local inputRight=inputBox.init(limitCont,50,40,lvgl.ALIGN_IN_RIGHT_MID,-5, 0,p2,paraChange)

        return limitCont,inputLeft,inputRight
    end

    --开机时间段

    local ontimeHour=string.format("%02d",t.ontimeHour)
    local ontimeMinite=string.format("%02d",t.ontimeMinite)
    powerOnTimeCont,powerOnStart,powerOnEnd=makeLimitBox(itemCont,dateCont,20,0,ontimeHour,ontimeMinite)

    --关机时间段
    local offtimeHour=string.format("%02d",t.offtimeHour)
    local offtimeMinite=string.format("%02d",t.offtimeMinite)
    powerOffTimeCont,powerOffStart,powerOffEnd=makeLimitBox(itemCont,powerOnTimeCont,20,0,offtimeHour,offtimeMinite)

    --开关
    local function switch_event(obj, event)
        if event == lvgl.EVENT_CLICKED then
            _G.BEEP()
            if lvgl.switch_get_state(obj)then
                t.switch=1
            else
                t.switch=0
            end
            log.info("UI",t.seg,lvgl.switch_get_state(obj))
        end
    end
    local switch = lvgl.switch_create(itemCont, nil)
    if t.switch==1 then 
        lvgl.switch_on(switch,lvgl.ANIM_OFF)
    end
    lvgl.obj_set_size(switch ,80,40)
    lvgl.obj_add_style(switch, lvgl.SWITCH_PART_INDIC, style.style_switch)
    lvgl.obj_add_style(switch, lvgl.SWITCH_PART_KNOB, style.style_switch_KNOB)
    lvgl.obj_add_style(switch, lvgl.SWITCH_PART_BG, style.style_switch_BG)
    lvgl.obj_align(switch ,powerOffTimeCont, lvgl.ALIGN_OUT_RIGHT_MID, 20, 0)
    lvgl.obj_set_event_cb(switch, switch_event)


    local function timeCrlChange()
        -- print("时控改变",branchTable.path,t.seg)
        tempBranchTable=cloneTab(parent)
        lvgl.textarea_set_text(powerOnStart,string.format("%02d",t.ontimeHour))
        lvgl.textarea_set_text(powerOnEnd,string.format("%02d",t.ontimeMinite))
        lvgl.textarea_set_text(powerOffStart,string.format("%02d",t.offtimeHour))
        lvgl.textarea_set_text(powerOffEnd,string.format("%02d",t.offtimeMinite))
        if t.switch==1 then 
            lvgl.switch_on(switch,lvgl.ANIM_OFF)
        else
            lvgl.switch_off(switch,lvgl.ANIM_OFF)
        end

        for k, v in pairs(map) do
            local btn=v.btn
            local status=t[v[2]]
            if status==1 then
                lvgl.btn_set_state(btn,lvgl.BTN_STATE_CHECKED_RELEASED)
            elseif status==0 then
                lvgl.btn_set_state(btn,lvgl.BTN_STATE_RELEASED)
            end
        end
    end

    topicTableAdd(branchTable.path..t.seg,timeCrlChange)

end

    function paddingContral(cont,t)
        lvgl.page_clean(cont)
        tempBranchTable=cloneTab(t)
        for _, v in ipairs(t.timeControl) do
            makeItem(cont,v,t)
        end
    end

    branchTable=RunningSatusTable[1]

    paddingContral(settingCont,branchTable)

    -- makeItem(settingCont,RunningSatusTable[1].timeControl[1])
    -- makeItem(settingCont,RunningSatusTable[1].timeControl[2])

end


function init(cont)
    lvgl.obj_clean(cont)
    -- log.info("UI","分段时间设置")
    local settingBox=makeBG(cont)
    paddiingBox(settingBox)
    _G.fragmentTimeSetting=true
end

function uninit()
    _G.fragmentTimeSetting=false
end