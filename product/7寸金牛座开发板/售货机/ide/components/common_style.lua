module(..., package.seeall)
_G.judge_create = function(s)
    if not s then
        s = lvgl.style_t()
        lvgl.style_init(s)
    end
    return s
end

_G.create_style = function(v)
    local s = lvgl.style_t()
    lvgl.style_init(s)
    if not v then return s end
    -- 设置背景
    if v.bg then s = set_bg(s, v.bg) end
    -- 设置边框
    if v.border then s = set_border(s, v.border) end
    -- 设置阴影
    if v.shadow then s = set_shadow(s, v.shadow) end
    -- 设置外框线
    if v.outline then s = set_outline(s, v.outline) end
    -- 内边距
    if v.pad then s = set_pad(s, v.pad) end
    -- 外边距
    if v.margin then s = set_margin(s, v.margin) end
    if v.value then s = set_value(s, v.value) end
    if v.text then s = set_text(s, v.text) end

    -- 为true可以将溢出的内容剪切到圆角(半径> 0)上。默认值：false。
    lvgl.style_set_clip_corner(s, lvgl.STATE_DEFAULT, true)

    return s
end
_G.set_value = function(s, v)
    if v.align then
        lvgl.style_set_value_align(s, lvgl.STATE_DEFAULT, v.align)
    end
    if v.blend_mode then
        lvgl.style_set_value_blend_mode(s, lvgl.STATE_DEFAULT, v.blend_mode)
    end
    if v.color then
        lvgl.style_set_value_color(s, lvgl.STATE_DEFAULT,
                                   lvgl.color_hex(v.color))
    end
    if v.font then lvgl.style_set_value_font(s, lvgl.STATE_DEFAULT, v.font) end
    if v.line_space then
        lvgl.style_set_value_line_space(s, lvgl.STATE_DEFAULT, v.line_space)
    end
    if v.ofs_x then
        lvgl.style_set_value_ofs_x(s, lvgl.STATE_DEFAULT, v.ofs_x)
    end
    if v.ofs_y then
        lvgl.style_set_value_ofs_y(s, lvgl.STATE_DEFAULT, v.ofs_y)
    end
    if v.opa then lvgl.style_set_value_opa(s, lvgl.STATE_DEFAULT, v.opa) end
    print(v.str)
    local str = v.str
    if v.str then lvgl.style_set_value_str(s, lvgl.STATE_DEFAULT, str) end
    return s
end

_G.set_text = function(s, v)
    -- 设置文本选择的背景色。默认值：lvgl.COLOR_BLACK
    if v.sel_color then
        lvgl.style_set_text_sel_color(s, lvgl.STATE_DEFAULT,
                                      lvgl.color_hex(v.sel_color))
    end

    -- 设置文字颜色
    if v.color then
        lvgl.style_set_text_color(s, lvgl.STATE_DEFAULT, lvgl.color_hex(v.color))
    end
    -- 设置文字透明度
    if v.opa then lvgl.style_set_text_opa(s, lvgl.STATE_DEFAULT, v.opa) end

    -- 文本的字母空间。默认值：0 
    if v.letter_space then
        lvgl.style_set_text_letter_space(s, lvgl.STATE_DEFAULT, v.letter_space)
    end
    -- 文本的行距。默认值：0 
    if v.line_space then
        lvgl.style_set_text_line_space(s, lvgl.STATE_DEFAULT, v.line_space)
    end
    -- 添加文字修饰。可以是lvgl.TEXT_DECOR_NONE/UNDERLINE/STRIKETHROUGH 
    -- 默认值：lvgl.TEXT_DECOR_NONE
    if v.decor then lvgl.style_set_text_decor(s, lvgl.STATE_DEFAULT, v.decor) end
    -- 设置字体
    if v.font then lvgl.style_set_text_font(s, lvgl.STATE_DEFAULT, v.font) end
    -- 设置文本的混合模式。
    -- 可以lvgl.BLEND_MODE_NORMAL/ADDITIVE/SUBTRACTIVE 
    -- 默认值：lvgl.BLEND_MODE_NORMAL。
    -- text_blend_mode (lvgl.blend_mode_t )
    return s
end
_G.set_shadow = function(s, v)
    -- 设置轮廓的宽度(模糊大小)。默认值：0。
    lvgl.style_set_shadow_width(s, lvgl.STATE_DEFAULT, v.width or 0)
    -- 指定阴影的颜色。默认值： lvgl.COLOR_BLACK。
    lvgl.style_set_shadow_color(s, lvgl.STATE_DEFAULT,
                                lvgl.color_hex(v.color or 0))
    -- 在每个方向上使阴影大于背景的值达到此值。默认值：0。
    lvgl.style_set_shadow_spread(s, lvgl.STATE_DEFAULT, v.spread or 0)
    -- 设置阴影的X偏移量。默认值：0。
    lvgl.style_set_shadow_ofs_x(s, lvgl.STATE_DEFAULT, v.ofs_x or 0)
    -- 设置阴影的Y偏移量。默认值：0。
    lvgl.style_set_shadow_ofs_y(s, lvgl.STATE_DEFAULT, v.ofs_x or 0)
    -- 指定阴影的不透明度。默认值：lvgl.OPA_TRANSP。
    lvgl.style_set_shadow_opa(s, lvgl.STATE_DEFAULT, v.opa or 80)
    -- 设置阴影的混合模式。可以是 lvgl.BLEND_MODE_NORMAL/ADDITIVE:叠加/SUBTRACTIVE:相减.默认值：lvgl.BLEND_MODE_NOR
    -- lvgl.style_set_shadow_blend_mode(s, lvgl.STATE_DEFAULT, lvgl.SUBTRACTIVE)
    return s
end

_G.set_bg_grad = function(s, v)
    lvgl.style_set_bg_grad_color(s, lvgl.STATE_DEFAULT,
                                 lvgl.color_hex(v.bg_grad_color or 0xFFFFFF))
    lvgl.style_set_bg_grad_dir(s, lvgl.STATE_DEFAULT,
                               v.bg_grad_dir or lvgl.GRAD_DIR_VER)
    --    /*Shift the gradient to the bottom*/
    if v.main then lvgl.style_set_bg_main_stop(s, lvgl.STATE_DEFAULT, v.main) end
    if v.grad then lvgl.style_set_bg_grad_stop(s, lvgl.STATE_DEFAULT, v.grad) end
    return s
end

_G.set_bg = function(s, v)
    local s = judge_create(s)
    lvgl.style_set_size(s, lvgl.STATE_DEFAULT, v.W or 100, v.H or 100)

    -- 背景颜色
    if v.color then
        lvgl.style_set_bg_color(s, lvgl.STATE_DEFAULT, lvgl.color_hex(v.color))
    end
    -- 背景透明度
    if v.opa then lvgl.style_set_bg_opa(s, lvgl.STATE_DEFAULT, v.opa) end
    if v.grad then s = set_bg_grad(s, v.grad) end
    -- 设置圆角
    if v.radius then lvgl.style_set_radius(s, lvgl.STATE_DEFAULT, v.radius) end
    -- lvgl.style_set_transform_angle(s, lvgl.STATE_DEFAULT, v.angle or 450)
    -- lvgl.style_set_transform_height(s, lvgl.STATE_DEFAULT, v.height or 45)
    -- lvgl.style_set_transform_width(s, lvgl.STATE_DEFAULT, v.width or 45)
    -- lvgl.style_set_transform_zoom(s, lvgl.STATE_DEFAULT, v.zoom or 1024)

    return s
end

_G.set_pad = function(s, v)
    if v.all then
        lvgl.style_set_pad_all(s, lvgl.STATE_DEFAULT, v.all)
        return s
    end
    -- 内边距
    if v.left then lvgl.style_set_pad_left(s, lvgl.STATE_DEFAULT, v.left) end
    if v.right then lvgl.style_set_pad_right(s, lvgl.STATE_DEFAULT, v.right) end
    if v.top then lvgl.style_set_pad_top(s, lvgl.STATE_DEFAULT, v.top) end
    if v.bottom then
        lvgl.style_set_pad_bottom(s, lvgl.STATE_DEFAULT, v.bottom)
    end
    if v.inner then lvgl.style_set_pad_inner(s, lvgl.STATE_DEFAULT, v.inner) end
    return s
end

_G.set_margin = function(s, v)
    -- 外边距
    if v.all then
        lvgl.style_set_margin_all(s, lvgl.STATE_DEFAULT, v.all)
        return s
    end
    lvgl.style_set_margin_left(s, lvgl.STATE_DEFAULT, v.left or 0)
    lvgl.style_set_margin_right(s, lvgl.STATE_DEFAULT, v.right or 0)
    lvgl.style_set_margin_top(s, lvgl.STATE_DEFAULT, v.top or 0)
    lvgl.style_set_margin_bottom(s, lvgl.STATE_DEFAULT, v.bottom or 0)
    return s
end

_G.set_border = function(s, v)
    -- 边框颜色
    lvgl.style_set_border_color(s, lvgl.STATE_DEFAULT,
                                lvgl.color_hex(v.color or 0))
    -- 边框宽度
    lvgl.style_set_border_width(s, lvgl.STATE_DEFAULT, v.width or 0)
    -- 边框透明度
    lvgl.style_set_border_opa(s, lvgl.STATE_DEFAULT, v.opa or 255)
    -- 指定要绘制边框的哪一侧。可以是 lvgl.BORDER_SIDE_NONE/LEFT/RIGHT/TOP/BOTTOM/FULL 
    -- 。ORed值也是可能的。默认值： lvgl.BORDER_SIDE_FULL 。
    lvgl.style_set_border_side(s, lvgl.STATE_DEFAULT,
                               v.side or lvgl.BORDER_SIDE_FULL)
    -- 如果true在绘制完所有子级之后绘制边框。默认值：false
    lvgl.style_set_border_post(s, lvgl.STATE_DEFAULT, v.post or false)
    return s
end

_G.set_outline = function(s, v)
    -- /*Add outline*/
    lvgl.style_set_outline_width(s, lvgl.STATE_DEFAULT, v.width or 2)
    lvgl.style_set_outline_opa(s, lvgl.STATE_DEFAULT, v.opa or 255)
    lvgl.style_set_outline_color(s, lvgl.STATE_DEFAULT,
                                 lvgl.color_hex(v.color or 0xFF0000))
    lvgl.style_set_outline_pad(s, lvgl.STATE_DEFAULT, v.pad or 8)
    return s
end

_G.set_line = function(s, v)

    -- style_set_line_blend_mode
    -- style_set_line_color
    -- style_set_line_dash_gap
    -- style_set_line_dash_width
    -- style_set_line_opa
    -- style_set_line_rounded
    -- style_set_line_width

    lvgl.style_set_line_color(s, lvgl.STATE_DEFAULT,
                              lvgl.color_hex(v.color or 0xFFFFFF))
    lvgl.style_set_line_width(s, lvgl.STATE_DEFAULT, v.width or 5)

    lvgl.style_set_line_opa(s, lvgl.STATE_DEFAULT, v.opa or 255)
    -- 端点圆角
    lvgl.style_set_line_rounded(s, lvgl.STATE_DEFAULT, v.rounded)
    return s
end
