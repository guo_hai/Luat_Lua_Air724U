module(..., package.seeall)

function create_style(v)
    local s = lvgl.style_t()
    lvgl.style_init(s)
    if not v then return s end
    -- 设置背景
    if v.bg then s = set_bg(s, v.bg) end
    -- 设置边框
    if v.border then s = set_border(s, v.border) end
    -- 设置阴影
    if v.shadow then s = set_shadow(s, v.shadow) end
    -- 设置文字样式
    if v.text then s = set_text(s, v.text) end
    return s
end

function create(p, v)
    obj = lvgl.switch_create(p, nil)
    lvgl.obj_set_click(obj, v.click or true)
    lvgl.obj_set_size(obj, v.W or 100, v.H or 100)
    -- lvgl.obj_set_auto_realign(Titlecont, true)                   
    lvgl.obj_align(obj, v.align_to, v.align or lvgl.ALIGN_CENTER,
                   v.align_x or 0, v.align_y or 0)

    if v.event then lvgl.obj_set_event_cb(obj, v.event) end

    lvgl.obj_add_style(obj, lvgl.SWITCH_PART_BG, create_style(v.bg))
    return obj
end
