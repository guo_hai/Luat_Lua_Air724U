module(..., package.seeall)

local function create_style(v)
    local s = lvgl.style_t()
    lvgl.style_init(s)
    if not v then return s end
    -- 设置背景
    if v.bg then s = set_bg(s, v.bg) end
    -- 设置边框
    if v.border then s = set_border(s, v.border) end
    -- 设置阴影
    if v.shadow then s = set_shadow(s, v.shadow) end
    -- 设置外框线
    if v.outline then s = set_outline(s, v.outline) end
    return s
end

function create(p, v)
    local obj = lvgl.qrcode_create(p, nil)
    lvgl.obj_set_click(obj, v.click or true)
    lvgl.obj_set_size(obj, v.W or 200, v.H or 200)
    lvgl.qrcode_set_txt(obj, v.text or "https://doc.openluat.com/home")

    lvgl.obj_align(obj, v.align_to, v.align or lvgl.ALIGN_CENTER,
                   v.align_x or 0, v.align_y or 0)
    -- lvgl.obj_add_style(obj, lvgl.QRCODE_PART_MAIN, create_style(v.style))
    return obj
end

