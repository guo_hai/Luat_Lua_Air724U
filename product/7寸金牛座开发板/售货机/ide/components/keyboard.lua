module(..., package.seeall)

function create_style(v)
    local s = lvgl.style_t()
    lvgl.style_init(s)
    if not v then return s end
    -- 设置背景
    if v.bg then s = set_bg(s, v.bg) end
    -- 设置边框
    if v.border then s = set_border(s, v.border) end
    -- 设置阴影
    if v.shadow then s = set_shadow(s, v.shadow) end
    -- 设置外框线
    if v.outline then s = set_outline(s, v.outline) end
    -- 设置文字样式
    if v.text then s = set_text(s, v.text) end

    return s
end

function create(p, v)
    local obj = lvgl.keyboard_create(p, nil)
    lvgl.obj_set_click(obj, v.click or true)
    lvgl.obj_set_size(obj, v.W or 400, v.H or 200)
    -- lvgl.obj_set_auto_realign(Titlecont, true)                   
    lvgl.obj_align(obj, v.align_to or nil, v.align or lvgl.ALIGN_CENTER,
                   v.align_x or 0, v.align_y or 0)

    -- lvgl.KEYBOARD_MODE_TEXT_LOWER – 显示小写字母
    -- lvgl.KEYBOARD_MODE_TEXT_UPPER – 显示大写字母
    -- lvgl.KEYBOARD_MODE_TEXT_SPECIAL – 显示特殊字符
    -- lvgl.KEYBOARD_MODE_NUM – 显示数字，+ /-号和小数点。
    -- 默认更多是 lvgl.KEYBOARD_MODE_TEXT_UPPER 。
    lvgl.keyboard_set_mode(obj, v.mode or lvgl.KEYBOARD_MODE_NUM)

    -- lvgl.keyboard_set_ctrl_map(obj)
    -- lvgl.keyboard_set_cursor_manage(obj)
    -- lvgl.keyboard_set_map(obj)
    -- lvgl.keyboard_set_textarea(obj)

    -- lvgl.KEYBOARD_PART_BG 这是主要部分，并使用了所有典型的背景属性
    -- lvgl.KEYBOARD_PART_BTN 这是按钮的虚拟部分。它还使用所有典型的背景属性和文本属性。

    lvgl.obj_add_style(obj, lvgl.KEYBOARD_PART_BG, create_style(v.bg))
    lvgl.obj_add_style(obj, lvgl.KEYBOARD_PART_BTN, create_style(v.btn))
end

-- create(lvgl.scr_act(), {
--     mode = lvgl.KEYBOARD_MODE_TEXT_UPPER,
--     bg = {bg = {radius = 0, color = 0xffcccc}, border = {width = 0}},
--     btn = {
--         bg = {radius = 10, color = 0xccccFF},
--         border = {width = 0},
--         text = {
--             -- font = style.font36,--不可用，会丢失图标
--             color = 0,
--             letter_space = 2,
--             decor = lvgl.STRIKETHROUGH
--         }
--     }

-- })
