module(..., package.seeall)

function create_style(v)
    if not v then v = {} end

    local s = lvgl.style_t()

    lvgl.style_init(s)

    -- 设置背景
    if v.bg then s = set_bg(s, v.bg) end
    -- 设置边框
    if v.border then s = set_border(s, v.border) end
    -- 设置阴影
    if v.shadow then s = set_shadow(s, v.shadow) end

    -- image_blend_mode ( lvgl.blend_mode_t )：设置图像的混合模式。
    -- 可以是 lvgl.BLEND_MODE_NORMAL/ADDITIVE/SUBTRACTIVE。
    -- 默认值：lvgl.BLEND_MODE_NORMAL。
    -- lvgl.style_set_image_blend_mode(s, lvgl.STATE_DEFAULT, lvgl.BLEND_MODE_NORMAL)
    if v.opa then lvgl.style_set_image_opa(s, lvgl.STATE_DEFAULT, v.opa) end
    if v.recolor then
        lvgl.style_set_image_recolor_opa(s, lvgl.STATE_DEFAULT,
                                         v.recolor_opa or 255)
        lvgl.style_set_image_recolor(s, lvgl.STATE_DEFAULT,
                                     lvgl.color_hex(v.recolor))
    end
    return s
end

function create(p, v)
    local obj = lvgl.img_create(p, nil)
    if v.H and v.W then lvgl.obj_set_size(obj, v.W, v.H) end
    -- 缩放不可用
    if v.zoom then lvgl.img_set_zoom(obj, v.zoom) end
    lvgl.img_set_src(obj, v.src)

    if v.W and v.H then lvgl.obj_set_size(obj, v.W, v.H) end

    if v.pivot_x and v.pivot_y then
        lvgl.img_set_pivot(obj, v.pivot_x, v.pivot_y)
    end

    if v.angle then lvgl.img_set_angle(obj, v.angle) end

    if v.click then lvgl.obj_set_click(obj, v.click) end
    if v.event then lvgl.obj_set_event_cb(obj, v.event) end

    lvgl.obj_align(obj, v.align_to or p, v.align or lvgl.ALIGN_CENTER,
                   v.align_x or 0, v.align_y or 0)

    lvgl.obj_add_style(obj, lvgl.IMG_PART_MAIN, create_style(v.style))

    return obj
end
