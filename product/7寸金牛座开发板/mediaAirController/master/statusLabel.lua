module(..., package.seeall)

_G.HOME=false
_G.MEDIA=false
_G.AIRCTR=false
_G.TEST=false
_G.UPDATA=false
local g={_G.HOME,_G.MEDIA,_G.AIRCTR,_G.TEST,_G.UPDATA}

_G.statusBox=lvgl.cont_create(lvgl.scr_act(), nil)
lvgl.obj_set_click(statusBox,false)
lvgl.obj_set_size(statusBox, 854, 45)  
-- lvgl.obj_set_auto_realign(Titlecont, true)                   
lvgl.obj_align(statusBox, nil, lvgl.ALIGN_IN_TOP_MID, 0, 0) 
lvgl.obj_add_style(statusBox, lvgl.CONT_PART_MAIN, stytle.stytle_statusBox)

local sinIcon=lvgl.img_create(statusBox, nil)
lvgl.img_set_src(sinIcon, "/lua/waitSin.bin")
lvgl.obj_align(sinIcon,statusBox, lvgl.ALIGN_IN_RIGHT_MID, -35, 0)

local label = lvgl.label_create(statusBox, nil)
lvgl.label_set_recolor(label, true)   
lvgl.label_set_text(label, "")
lvgl.obj_align(label,sinIcon, lvgl.ALIGN_OUT_LEFT_MID, -28, 0)
service_sim.signalService(label,sinIcon)


_G.backIcon=lvgl.cont_create(lvgl.scr_act(), nil)
lvgl.obj_set_click(backIcon,false)
lvgl.obj_set_size(backIcon, 300, 45)
lvgl.obj_add_style(backIcon, lvgl.CONT_PART_MAIN, stytle.stytle_divBox)

_G.BACKFNC=nil

function backEvent(obj, event)
    if event == lvgl.EVENT_CLICKED then
        -- for k, v in pairs(g) do
        --     v=false
        -- end
        -- print(g,#g)
        if _G.BACKFNC ~=nil then _G.BACKFNC() _G.BACKFNC=nil end
        activity_HOME.init()
        -- sys.taskInit(function()
        --     -- lvgl.obj_clean(contentBox)
        --     -- lvgl.obj_del(qrimg)
        --     -- print("clean")
        --     activity_HOME.init()
        -- end)
    end
end

_G.backimg=lvgl.imgbtn_create(backIcon, nil)
-- lvgl.img_set_src(backimg, "/lua/backIcon_ept.bin")
lvgl.imgbtn_set_src(backimg, lvgl.BTN_STATE_RELEASED, "/lua/backIcon.png")
lvgl.imgbtn_set_src(backimg, lvgl.BTN_STATE_PRESSED, "/lua/backIcon.png")
lvgl.obj_set_event_cb(backimg, backEvent)
lvgl.obj_align(backimg,backIcon, lvgl.ALIGN_IN_TOP_LEFT, 0, 0)

_G.winTitle = lvgl.label_create(backIcon, nil)
lvgl.label_set_recolor(winTitle, true)
lvgl.label_set_text(winTitle, "")
lvgl.obj_align(winTitle,backIcon, lvgl.ALIGN_IN_LEFT_MID, 70, 0) 

function setWinTitle(text)
    lvgl.label_set_text(winTitle, "#ffffff "..text)
end
-- function setBackImg(path)
--     lvgl.imgbtn_set_src(backimg, lvgl.BTN_STATE_RELEASED, path)
-- end