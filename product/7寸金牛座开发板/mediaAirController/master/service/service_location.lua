module(..., package.seeall)
--------------

require"lbsLoc"

_G.locationTable={
    tencent={},
    LBS_LOC={
        result=nil,
        lat=nil,
        lng=nil
    }
}

local function cbFnc(result,prompt,head,body)
    if result and body then
        -- log.info("testHttp.cbFnc","body="..body)
        _G.locationTable.tencent,result,errinfo=json.decode(body)
        log.info("腾讯位置转换结果:",_G.locationTable.tencent.status==0 and "成功" or "失败状态码:".._G.locationTable.tencent.status)
    end
end

function getLocation()
    return _G.locationTable.LBS_LOC
end

function addrService(label)
    sys.taskInit(function()
        while _G.HOME and _G.locationTable.tencent.status==nil do
            sys.wait(500)
        end
        if _G.HOME and label==_G.locationLabel then
            lvgl.label_set_text(label, "#FFFFFF ".._G.locationTable.tencent.result.address)
        end
    end)
end

sys.taskInit(function()

            while true do
                    --请求基站定位
                lbsLoc.request(function(result,lat,lng)
                    --log.info("testLbsLoc.getLocCb",result,lat,lng)
                    sys.publish("LBS_LOC_IND",result,lat,lng)
                end,nil,1000*60)
                
                --阻塞等待基站定位结果
                local _,result,lat,lng = sys.waitUntil("LBS_LOC_IND")
                _G.locationTable.LBS_LOC.result=result
                _G.locationTable.LBS_LOC.lat=lat
                _G.locationTable.LBS_LOC.lng=lng
                --打印基站wifi定位结果
                log.info("显示基站定位",result,lat,lng)
                if result==0 then 
                    http.request("GET","https://apis.map.qq.com/ws/geocoder/v1/?location="..lat..","..lng.."&key=QQ3BZ-4QT6X-PCW4K-7JUIT-LCM2T-SDF66&get_poi=0",nil,nil,nil,nil,cbFnc)
                    break
                end
            end
end)
