module(..., package.seeall)

    style_body = lvgl.style_t()
    lvgl.style_init(style_body)
    lvgl.style_set_bg_color(style_body, lvgl.CONT_PART_MAIN,lvgl.color_hex(0x0d132e))
    lvgl.style_set_radius(style_body, lvgl.CONT_PART_MAIN, 0)
    lvgl.style_set_border_width(style_body, lvgl.CONT_PART_MAIN, 0)
    --透明
    -- lvgl.style_set_bg_opa(stytle_statusBox, lvgl.CONT_PART_MAIN,0)
---------------------------------------------------------------------------
    style_div = lvgl.style_t()
    lvgl.style_init(style_div)
    lvgl.style_set_radius(style_div, lvgl.CONT_PART_MAIN, 0)
    lvgl.style_set_border_width(style_div, lvgl.CONT_PART_MAIN, 0)
    lvgl.style_set_bg_opa(style_div, lvgl.CONT_PART_MAIN,0)
    lvgl.style_set_pad_inner(style_div, lvgl.CONT_PART_MAIN,0)

    -----------------------

    style_div_blue = lvgl.style_t()
    lvgl.style_init(style_div_blue)
    lvgl.style_set_radius(style_div_blue, lvgl.CONT_PART_MAIN, 0)
    lvgl.style_set_border_width(style_div_blue, lvgl.CONT_PART_MAIN, 0)
    lvgl.style_set_bg_color(style_div_blue, lvgl.CONT_PART_MAIN,lvgl.color_hex(0x082f52))

-------------------------------------------------
style_div_blue_lite = lvgl.style_t()
lvgl.style_init(style_div_blue_lite)
lvgl.style_set_radius(style_div_blue_lite, lvgl.CONT_PART_MAIN, 0)
lvgl.style_set_border_width(style_div_blue_lite, lvgl.CONT_PART_MAIN, 0)
lvgl.style_set_bg_color(style_div_blue_lite, lvgl.CONT_PART_MAIN,lvgl.color_hex(0x072342))

------------------------------------------------------------------------------------------------------

    style_temp = lvgl.style_t()
    lvgl.style_init(style_temp)
    lvgl.style_set_bg_color(style_temp, lvgl.CONT_PART_MAIN,lvgl.color_hex(0xff0000))
    lvgl.style_set_radius(style_temp, lvgl.CONT_PART_MAIN, 0)
    lvgl.style_set_border_width(style_temp, lvgl.CONT_PART_MAIN, 0)
    lvgl.style_set_bg_opa(style_temp, lvgl.CONT_PART_MAIN,255)

    lvgl.style_set_pad_left(style_temp, lvgl.CONT_PART_MAIN,0)
    lvgl.style_set_pad_right(style_temp, lvgl.CONT_PART_MAIN,0)
    lvgl.style_set_pad_top(style_temp, lvgl.CONT_PART_MAIN,0)
    lvgl.style_set_pad_bottom(style_temp, lvgl.CONT_PART_MAIN,0)
    lvgl.style_set_pad_inner(style_temp, lvgl.CONT_PART_MAIN,0)

    lvgl.style_set_margin_top(style_temp, lvgl.CONT_PART_MAIN,0)
    lvgl.style_set_margin_bottom(style_temp, lvgl.CONT_PART_MAIN,0)

------------------------------------------------------------------------------------------------------
    style_statusLabel = lvgl.style_t()
    lvgl.style_init(style_statusLabel)
    lvgl.style_set_radius(style_statusLabel, lvgl.CONT_PART_MAIN, 0)
    lvgl.style_set_border_width(style_statusLabel, lvgl.CONT_PART_MAIN, 0)
    lvgl.style_set_text_color(style_statusLabel,lvgl.CONT_PART_MAIN,lvgl.color_hex(0xFFFFFF))

------------------------------------------------------------
style_leve = lvgl.style_t()
lvgl.style_init(style_leve)
-- lvgl.style_set_border_color(style_leve,lvgl.STATE_DEFAULT,lvgl.color_hex(0xFF0000))
-- lvgl.style_set_radius(style_leve, lvgl.CONT_PART_MAIN, 0)
-- lvgl.style_set_border_width(style_leve, lvgl.CONT_PART_MAIN, 0)
-- lvgl.style_set_radius(style_leve, lvgl.STATE_FOCUSED, 20)
lvgl.style_set_border_width(style_leve,lvgl.STATE_FOCUSED, 0)
lvgl.style_set_text_color(style_leve,lvgl.CONT_PART_MAIN,lvgl.color_hex(0xFFFFFF))


--------------------------------------------------------------
style_arc_bg_0 = lvgl.style_t()
lvgl.style_init(style_arc_bg_0)
lvgl.style_set_line_opa(style_arc_bg_0, lvgl.STATE_DEFAULT, 0)
------------------------------------------------------------------------------------------------------
style_arc_front1 = lvgl.style_t()
lvgl.style_init(style_arc_front1)
lvgl.style_set_line_width(style_arc_front1, lvgl.STATE_DEFAULT,3)
lvgl.style_set_line_color(style_arc_front1, lvgl.STATE_DEFAULT,lvgl.color_hex(0xfa6a64))
----------------------------------------------------------------
style_arc_front2 = lvgl.style_t()
lvgl.style_init(style_arc_front2)
lvgl.style_set_line_width(style_arc_front2, lvgl.STATE_DEFAULT,3)
lvgl.style_set_line_color(style_arc_front2, lvgl.STATE_DEFAULT,lvgl.color_hex(0x0cd3db))
----------------------------------------------------------------
style_arc_front3 = lvgl.style_t()
lvgl.style_init(style_arc_front3)
lvgl.style_set_line_width(style_arc_front3, lvgl.STATE_DEFAULT,3)
lvgl.style_set_line_color(style_arc_front3, lvgl.STATE_DEFAULT,lvgl.color_hex(0x8292fb))

----------------------------------
style_arc_urgent = lvgl.style_t()
lvgl.style_init(style_arc_urgent)
lvgl.style_set_line_width(style_arc_urgent, lvgl.STATE_DEFAULT,10)
lvgl.style_set_line_rounded(style_arc_urgent,lvgl.STATE_DEFAULT,false)
lvgl.style_set_line_color(style_arc_urgent, lvgl.STATE_DEFAULT,lvgl.color_hex(0xf43b42))
lvgl.style_set_bg_color(style_arc_urgent, lvgl.CONT_PART_MAIN,lvgl.color_hex(0xf43b42))
lvgl.style_set_radius(style_arc_urgent, lvgl.CONT_PART_MAIN, 10)
lvgl.style_set_border_width(style_arc_urgent, lvgl.CONT_PART_MAIN, 0)

----------------------------------
style_arc_error = lvgl.style_t()
lvgl.style_init(style_arc_error)
lvgl.style_set_line_width(style_arc_error, lvgl.STATE_DEFAULT,10)
lvgl.style_set_line_rounded(style_arc_error,lvgl.STATE_DEFAULT,false)
lvgl.style_set_line_color(style_arc_error, lvgl.STATE_DEFAULT,lvgl.color_hex(0xfb8357))
lvgl.style_set_bg_color(style_arc_error, lvgl.CONT_PART_MAIN,lvgl.color_hex(0xfb8357))
lvgl.style_set_radius(style_arc_error, lvgl.CONT_PART_MAIN, 10)
lvgl.style_set_border_width(style_arc_error, lvgl.CONT_PART_MAIN, 0)

----------------------------------
style_arc_warn = lvgl.style_t()
lvgl.style_init(style_arc_warn)
lvgl.style_set_line_width(style_arc_warn, lvgl.STATE_DEFAULT,10)
lvgl.style_set_line_rounded(style_arc_warn,lvgl.STATE_DEFAULT,false)
lvgl.style_set_line_color(style_arc_warn, lvgl.STATE_DEFAULT,lvgl.color_hex(0xf8c900))
lvgl.style_set_bg_color(style_arc_warn, lvgl.CONT_PART_MAIN,lvgl.color_hex(0xf8c900))
lvgl.style_set_radius(style_arc_warn, lvgl.CONT_PART_MAIN, 10)
lvgl.style_set_border_width(style_arc_warn, lvgl.CONT_PART_MAIN, 0)

----------------------------------
style_arc_nomal = lvgl.style_t()
lvgl.style_init(style_arc_nomal)
lvgl.style_set_line_width(style_arc_nomal, lvgl.STATE_DEFAULT,10)
lvgl.style_set_line_rounded(style_arc_nomal,lvgl.STATE_DEFAULT,false)
lvgl.style_set_line_color(style_arc_nomal, lvgl.STATE_DEFAULT,lvgl.color_hex(0x75bc52))
lvgl.style_set_bg_color(style_arc_nomal, lvgl.CONT_PART_MAIN,lvgl.color_hex(0x75bc52))
lvgl.style_set_radius(style_arc_nomal, lvgl.CONT_PART_MAIN, 10)
lvgl.style_set_border_width(style_arc_nomal, lvgl.CONT_PART_MAIN, 0)

----------------------------------
style_arc_info = lvgl.style_t()
lvgl.style_init(style_arc_info)
lvgl.style_set_line_width(style_arc_info, lvgl.STATE_DEFAULT,10)
lvgl.style_set_line_rounded(style_arc_info,lvgl.STATE_DEFAULT,false)
lvgl.style_set_line_color(style_arc_info, lvgl.STATE_DEFAULT,lvgl.color_hex(0x4fb9db))
lvgl.style_set_bg_color(style_arc_info, lvgl.CONT_PART_MAIN,lvgl.color_hex(0x4fb9db))
lvgl.style_set_radius(style_arc_info, lvgl.CONT_PART_MAIN, 10)
lvgl.style_set_border_width(style_arc_info, lvgl.CONT_PART_MAIN, 0)


------------------------------------

style_w_cont = lvgl.style_t()
lvgl.style_init(style_w_cont)
lvgl.style_set_bg_color(style_w_cont, lvgl.CONT_PART_MAIN,lvgl.color_hex(0xf43b42))
lvgl.style_set_radius(style_w_cont, lvgl.CONT_PART_MAIN, 9)
lvgl.style_set_border_width(style_w_cont, lvgl.CONT_PART_MAIN, 0)
--------------------------------------------------------------------

style_bg_boder = lvgl.style_t()
lvgl.style_init(style_bg_boder)
lvgl.style_set_bg_color(style_bg_boder, lvgl.CONT_PART_MAIN,lvgl.color_hex(0x0d132e))
lvgl.style_set_border_color(style_bg_boder,lvgl.CONT_PART_MAIN,lvgl.color_hex(0x116193))
lvgl.style_set_border_color(style_bg_boder,lvgl.STATE_FOCUSED,lvgl.color_hex(0x116193))
lvgl.style_set_radius(style_bg_boder, lvgl.CONT_PART_MAIN, 10)
lvgl.style_set_border_width(style_bg_boder, lvgl.CONT_PART_MAIN, 2)
lvgl.style_set_bg_opa(style_bg_boder, lvgl.CONT_PART_MAIN,255)

lvgl.style_set_pad_left(style_bg_boder, lvgl.CONT_PART_MAIN,0)
lvgl.style_set_pad_right(style_bg_boder, lvgl.CONT_PART_MAIN,0)
lvgl.style_set_pad_top(style_bg_boder, lvgl.CONT_PART_MAIN,0)
lvgl.style_set_pad_bottom(style_bg_boder, lvgl.CONT_PART_MAIN,0)
lvgl.style_set_pad_inner(style_bg_boder, lvgl.CONT_PART_MAIN,0)

lvgl.style_set_margin_top(style_bg_boder, lvgl.CONT_PART_MAIN,0)
lvgl.style_set_margin_bottom(style_bg_boder, lvgl.CONT_PART_MAIN,0)


----------------------

style_bg_bg = lvgl.style_t()
lvgl.style_init(style_bg_bg)
-- lvgl.style_set_bg_color(style_bg_bg, lvgl.CONT_PART_MAIN,lvgl.color_hex(0xff0000))
lvgl.style_set_bg_opa(style_bg_bg, lvgl.CONT_PART_MAIN,0)
lvgl.style_set_radius(style_bg_bg, lvgl.CONT_PART_MAIN, 0)
lvgl.style_set_border_width(style_bg_bg, lvgl.CONT_PART_MAIN, 0)

lvgl.style_set_pad_left(style_bg_bg, lvgl.CONT_PART_MAIN,20)
lvgl.style_set_pad_right(style_bg_bg, lvgl.CONT_PART_MAIN,20)
lvgl.style_set_pad_top(style_bg_bg, lvgl.CONT_PART_MAIN,0)
lvgl.style_set_pad_bottom(style_bg_bg, lvgl.CONT_PART_MAIN,0)
lvgl.style_set_pad_inner(style_bg_bg, lvgl.CONT_PART_MAIN,0)

lvgl.style_set_margin_top(style_bg_bg, lvgl.CONT_PART_MAIN,0)
lvgl.style_set_margin_bottom(style_bg_bg, lvgl.CONT_PART_MAIN,0)

-----------------

style_bg_page = lvgl.style_t()
lvgl.style_init(style_bg_page)
-- lvgl.style_set_bg_color(style_bg_page, lvgl.CONT_PART_MAIN,lvgl.color_hex(0xff0000))
lvgl.style_set_radius(style_bg_page, lvgl.CONT_PART_MAIN, 0)
lvgl.style_set_border_width(style_bg_page, lvgl.CONT_PART_MAIN, 0)
lvgl.style_set_bg_opa(style_bg_page, lvgl.CONT_PART_MAIN,0)

lvgl.style_set_pad_left(style_bg_page, lvgl.CONT_PART_MAIN,0)
lvgl.style_set_pad_right(style_bg_page, lvgl.CONT_PART_MAIN,0)
lvgl.style_set_pad_top(style_bg_page, lvgl.CONT_PART_MAIN,0)
lvgl.style_set_pad_bottom(style_bg_page, lvgl.CONT_PART_MAIN,0)
lvgl.style_set_pad_inner(style_bg_page, lvgl.CONT_PART_MAIN,0)

lvgl.style_set_margin_top(style_bg_page, lvgl.CONT_PART_MAIN,0)
lvgl.style_set_margin_bottom(style_bg_page, lvgl.CONT_PART_MAIN,0)


-----------------------

style_bg_UL = lvgl.style_t()
lvgl.style_init(style_bg_UL)
lvgl.style_set_bg_color(style_bg_UL, lvgl.CONT_PART_MAIN,lvgl.color_hex(0xff0000))
lvgl.style_set_radius(style_bg_UL, lvgl.CONT_PART_MAIN, 0)
lvgl.style_set_border_width(style_bg_UL, lvgl.CONT_PART_MAIN, 0)
lvgl.style_set_bg_opa(style_bg_UL, lvgl.CONT_PART_MAIN,0)

lvgl.style_set_pad_left(style_bg_UL, lvgl.CONT_PART_MAIN,0)
lvgl.style_set_pad_right(style_bg_UL, lvgl.CONT_PART_MAIN,0)
lvgl.style_set_pad_top(style_bg_UL, lvgl.CONT_PART_MAIN,0)
lvgl.style_set_pad_bottom(style_bg_UL, lvgl.CONT_PART_MAIN,0)
lvgl.style_set_pad_inner(style_bg_UL, lvgl.CONT_PART_MAIN,0)

lvgl.style_set_margin_top(style_bg_UL, lvgl.CONT_PART_MAIN,0)
lvgl.style_set_margin_bottom(style_bg_UL, lvgl.CONT_PART_MAIN,1)

--------------
style_labelBgBlue_noRadius = lvgl.style_t()
lvgl.style_init(style_labelBgBlue_noRadius)
lvgl.style_set_bg_color(style_labelBgBlue_noRadius, lvgl.CONT_PART_MAIN,lvgl.color_hex(0xE67517))
lvgl.style_set_border_width(style_labelBgBlue_noRadius, lvgl.CONT_PART_MAIN, 0)
lvgl.style_set_radius(style_labelBgBlue_noRadius, lvgl.CONT_PART_MAIN, 0)
-- lvgl.style_set_text_color(style_labelBgBlue_noRadius,lvgl.color_hex(0xffffff))

lvgl.style_set_pad_left(style_labelBgBlue_noRadius, lvgl.CONT_PART_MAIN,0)
lvgl.style_set_pad_right(style_labelBgBlue_noRadius, lvgl.CONT_PART_MAIN,0)
lvgl.style_set_pad_top(style_labelBgBlue_noRadius, lvgl.CONT_PART_MAIN,0)
lvgl.style_set_pad_bottom(style_labelBgBlue_noRadius, lvgl.CONT_PART_MAIN,0)
lvgl.style_set_pad_inner(style_labelBgBlue_noRadius, lvgl.CONT_PART_MAIN,0)
