module(..., package.seeall)

function create(p, v)
    local signal_cont = cont.create(p or lvgl.scr_act(), v.main)

    color_ = 0x6cff8f
    for i = 1, #v.signal_data do
        if i > v.signal_value then color_ = 0xcccccc end
        v.signal_pointer[i] = cont.create(signal_cont, {
            W = v.signal_data[i].W or 4,
            H = v.signal_data[i].H,
            align = v.signal_data[i].align or lvgl.ALIGN_OUT_RIGHT_BOTTOM,
            align_x = v.signal_data[i].align_x or 3,
            align_to = v.signal_pointer[i - 1],
            click = false,
            style = {border = {width = 0}, bg = {radius = 0, color = color_}}
        })
    end
    return {pointer = v.signal_pointer, cont = signal_cont}
end

function set_signal(pointer, signal_value)
    local valid_signal_style = set_bg(nil, {radius = 0, color = 0x6cff8f})
    local invalid_signal_style = set_bg(nil, {radius = 0, color = 0xcccccc})
    for i = 1, #pointer do
        if i > signal_value then
            valid_signal_style = invalid_signal_style
        end
        lvgl.obj_add_style(pointer[i], lvgl.CONT_PART_MAIN, valid_signal_style)

    end
end

-- v = {
--     main = {
--         W = 30,
--         H = 30,
--         align = lvgl.ALIGN_IN_TOP_RIGHT,
--         align_x = -10,
--         align_y = 10,
--         click = false,
--         style = {border = {width = 0}, bg = {radius = 0, color = 0x4e93fe}}
--     },
--     signal_data = {
--         {W = 4, H = 4, align = lvgl.ALIGN_IN_BOTTOM_LEFT, align_x = 2},
--         {H = 11}, {H = 18}, {H = 25}
--     },
--     signal_value = 3,
--     signal_pointer = {}
-- }
-- set_signal(create(lvgl.scr_act(), v), 2)


-- signal_cont = signal.create(lvgl.scr_act(), {
--     main = {
--         W = 30,
--         H = 30,
--         align = lvgl.ALIGN_IN_TOP_RIGHT,
--         align_x = -10,
--         align_y = 10,
--         click = false,
--         style = {border = {width = 0}, bg = {radius = 0, opa = 0}}
--     },
--     signal_data = {
--         {W = 4, H = 4, align = lvgl.ALIGN_IN_BOTTOM_LEFT, align_x = 2},
--         {H = 11}, {H = 18}, {H = 25}
--     },
--     signal_value = 3,
--     signal_pointer = {}
-- })

-- -- 可使用此接口修改信号值范围1-4
-- --  signal.set_signal(signal_cont.pointer, 2)