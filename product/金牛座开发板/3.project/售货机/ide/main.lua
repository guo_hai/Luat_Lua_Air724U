PROJECT = "vending_machine"
VERSION = "1.0.2"

PRODUCT_KEY = "PA916VPSUsSQ4TcSCfK11sZGaBzgQ3tM"

require "log"
require "sys"
require "http"
require "misc"
require "pins"
require "mipi_lcd_GC9503"
require "tp"

require "config"
require "nvm"
require "symbol"
require "ProjectSwitch"

nvm.init("config.lua")

lvgl.init(function()
    if not lvgl.indev_get_emu_touch then
        lvgl.disp_set_rotation(nil, lvgl.DISP_ROT_270)
    end
    spi.setup(spi.SPI_1, 1, 1, 8, 5000000, 1)
    -- _G.LCD_W, _G.LCD_H = lvgl.disp_get_lcd_info()
    if lvgl.indev_get_emu_touch then
        _G.LCD_W, _G.LCD_H = lvgl.disp_get_lcd_info()
    else
        _G.LCD_H, _G.LCD_W = lvgl.disp_get_lcd_info()
    end
end, tp.input)

require "components"
require "home"

require "update"
update.request()

-- require "test_page"

sys.init(0, 0)
sys.run()
