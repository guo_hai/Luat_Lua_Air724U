module(..., package.seeall)

function get_goods_event()
    local text = lvgl.textarea_get_text(first_text_area)
    data = {}
    if text == "" then
        data = {
            src = "cuowu",
            state_label = "未输入取货码",
            tips_label = "请核实您的取货码,并通过屏幕输入"
        }
    elseif text == nvm.get("password") then
        data = {
            src = "chenggong",
            state_label = "出货成功",
            tips_label = "请立即从下方出货口取走您的商品"
        }
    else
        data = {
            src = "shibai",
            state_label = "取货码有误",
            tips_label = "请核实您的取货码,并通过屏幕输入"
        }
    end
    custom_msg.create({content_fun = home.create_msg, msg = data})
end

function create()
    lvgl.obj_clean(right_cont)
    back_cont = cont.create(right_cont, {
        W = LCD_W * 6 / 54,
        H = LCD_W * 6 / 108,
        align = lvgl.ALIGN_IN_TOP_LEFT,
        align_x = 20,
        align_y = 20,
        click = true,
        event = back_home,
        style = {
            border = {width = 0},
            shadow = {
                width = 10,
                spread = 1,
                opa = 60,
                color = 0,
                ofs_y = 2,
                ofs_x = 2
            },
            bg = {radius = 20, color = 0xffd264}
        }
    })

    back_img = img.create(back_cont, {
        align = lvgl.ALIGN_IN_TOP_MID,
        src = "/lua/back.bin",
        style = {recolor = 0xffffff}
    })

    clock_img = img.create(back_cont, {
        align = lvgl.ALIGN_IN_BOTTOM_LEFT,
        align_x = 15,
        src = "/lua/clock.bin",
        style = {recolor = 0xffffff}
    })

    auto_back_time_label = label.create(back_cont, {
        text = "113秒",
        align = lvgl.ALIGN_OUT_RIGHT_MID,
        align_to = clock_img,
        align_x = 5,
        style = {text = {font = style.font14, color = 0xffffff}}
    })

    title_label = label.create(right_cont, {
        text = "取货码取货",
        align = lvgl.ALIGN_IN_TOP_MID,
        align_y = 20,
        style = {text = {font = style.font54, color = 0x3FC6C9}}
    })

    input_cont = cont.create(right_cont, {
        W = LCD_W * 2 / 6,
        H = LCD_H * 3 / 24,
        align = lvgl.ALIGN_OUT_BOTTOM_MID,
        align_to = title_label,
        align_y = 15,
        click = false,
        style = {
            shadow = {width = 10, spread = 1, opa = 60, color = 0, ofs_y = 2},
            border = {width = 0},
            bg = {color = 0x3FC6C9, radius = 15, opa = 255}
        }
    })

    pg_code_label = label.create(input_cont, {
        text = "请输入正确的取货码",
        style = {
            text = {font = style.font24, color = 0xffffff, letter_space = 2}
        }
    })

    first_text_area = text_area.create(input_cont, {
        W = LCD_W * 2 / 6,
        H = LCD_H * 3 / 24,
        text_align = lvgl.LABEL_ALIGN_CENTER,
        -- event = text_area_cb,
        one_line = true,
        -- align_y = -10,
        -- cursor_hidden = true,
        style = {
            text = {font = style.font48, color = 0, letter_space = 2},
            bg = {opa = 0, radius = 10},
            border = {width = 0}
        }
    })
    function btn_matrix_event(obj, event)
        if (event == lvgl.EVENT_VALUE_CHANGED) then
            local txt = lvgl.btnmatrix_get_active_btn_text(obj)
            local temp = lvgl.btnmatrix_get_active_btn(obj)
            if temp == 11 then
                lvgl.textarea_del_char(first_text_area)
            elseif temp == 10 then
                lvgl.textarea_add_text(first_text_area, 0)
            elseif temp == 9 then
                lvgl.textarea_set_text(first_text_area, "")
            else
                lvgl.textarea_add_text(first_text_area, temp + 1)
            end

            if lvgl.textarea_get_text(first_text_area) == "" then
                lvgl.obj_set_hidden(pg_code_label, false)
            else
                lvgl.obj_set_hidden(pg_code_label, true)
            end
            print(temp)
            print(txt)
        end
    end

    btn_matrix_ = btn_matrix.create(right_cont, {
        btnArray = {
            "1", "2", "3", "\n", "4", "5", "6", "\n", "7", "8", "9", "\n",
            "重输", "0", "删除", ""
        },
        W = LCD_W * 1 / 3,
        H = LCD_H * 1 / 2,
        recolor = true,
        align = lvgl.ALIGN_OUT_BOTTOM_MID,
        align_to = input_cont,
        align_y = 10,
        cb = btn_matrix_event,
        btn_style = {
            shadow = {
                width = 10,
                spread = 1,
                opa = 60,
                color = 0,
                ofs_x = 2,
                ofs_y = 2
            },
            bg = {radius = 50, color = 0x3FC6C9, opa = 250},
            border = {width = 0},
            text = {font = style.font24}
        },
        bg_style = {border = {width = 0}, bg = {opa = 155}}
    })
    get_btn = btn.create(right_cont, {
        W = LCD_W * 2 / 6,
        H = LCD_H * 3 / 24,
        align = lvgl.ALIGN_OUT_BOTTOM_MID,
        align_to = btn_matrix_,
        event = function(obj, event)
            if event == lvgl.EVENT_CLICKED then get_goods_event() end
        end,
        label = {
            style = {
                text = {
                    font = style.font54,
                    color = 0xffffff,
                    letter_space = 20
                }
            },
            text = "领取"
        },
        style = {
            shadow = {
                width = 10,
                spread = 1,
                opa = 60,
                color = 0,
                ofs_x = 2,
                ofs_y = 2
            },
            border = {width = 0},
            bg = {radius = 40, color = 0xEFE15B}
        }
    })
end
