module(..., package.seeall)

function create()
    lvgl.obj_clean(right_cont)
    right_cont_head = cont.create(right_cont, {
        W = LCD_W * 2 / 3,
        H = LCD_H * 14 / 24,
        align = lvgl.ALIGN_IN_TOP_RIGHT,
        click = false,
        style = {border = {width = 0}, bg = {color = 0xffffff}}
    })
    img.create(right_cont_head, {
        align_y = 10,
        src = "/lua/help.jpg"
    })
    qrcode_cont = cont.create(right_cont, {
        W = LCD_H * 16 / 48,
        H = LCD_H * 16 / 48,
        align = lvgl.ALIGN_OUT_BOTTOM_MID,
        align_to = right_cont_head,
        click = false,
        style = {border = {width = 3, color = 0xccffcc}, bg = {radius = 5}}
    })

    service_qrcode = qrcode.create(qrcode_cont, {
        W = (LCD_H * 16 / 48) - 5,
        H = (LCD_H * 16 / 48) - 5,
        text = "请打钱"
    })

    label.create(right_cont, {
        text = "扫码关注在线客服",
        align = lvgl.ALIGN_OUT_BOTTOM_MID,
        align_to = service_qrcode,
        align_y = 5,
        style = {text = {font = style.font24}}
    })

    back_cont = cont.create(right_cont, {
        W = LCD_W * 6 / 54,
        H = LCD_W * 6 / 108,
        align = lvgl.ALIGN_IN_BOTTOM_RIGHT,
        align_x = -5,
        align_y = -5,
        click = true,
        event = back_home,
        style = {
            border = {width = 0},
            shadow = {
                width = 10,
                spread = 1,
                opa = 60,
                color = 0,
                ofs_y = 2,
                ofs_x = 2
            },
            bg = {radius = 20, color = 0xffd264}
        }
    })

    back_img = img.create(back_cont, {
        align = lvgl.ALIGN_IN_TOP_MID,
        src = "/lua/back.bin",
        style = {recolor = 0xffffff}
    })

    clock_img = img.create(back_cont, {
        align = lvgl.ALIGN_IN_BOTTOM_LEFT,
        align_x = 15,
        src = "/lua/clock.bin",
        style = {recolor = 0xffffff}
    })

    auto_back_time_label = label.create(back_cont, {
        text = "7秒",
        align = lvgl.ALIGN_OUT_RIGHT_MID,
        align_to = clock_img,
        align_x = 5,
        style = {text = {font = style.font14, color = 0xffffff}}
    })

end
