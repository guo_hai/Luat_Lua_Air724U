----------------------------------------------------------------------------
-- 1. This file automatically generates code for the LuatOS's UI designer
-- 2. In case of accident, modification is strictly prohibited
----------------------------------------------------------------------------

--Import event file
require "UiHandle"

local function objectHide()
	local o = {}
	local tSelf = {}
	setmetatable(o, tSelf)
	tSelf.__index = tSelf
	tSelf.__tostring = function(j)
		return j.self
	end
	tSelf.__tocall = function(j)
		return j.cb
	end
	tSelf.self = nil
	tSelf.cb = function(e) end
	return o
end

ScreenA = 
{
	create = nil, 
	free = nil,
	contFather_ScreenA = nil,
	LvglButton1 = objectHide(),
	LvglImg1 = objectHide(),
	LvglLabel1 = objectHide(),
	LvglImg2 = objectHide(),
	LvglLabel2 = objectHide(),
	LvglLabel3 = objectHide(),
	LvglLabel4 = objectHide(),
	LvglLabel5 = objectHide(),
	LvglLabel6 = objectHide(),
	LvglLabel7 = objectHide(),
	LvglLabel8 = objectHide(),
	LvglLabel9 = objectHide(),
	LvglLabel10 = objectHide(),
	LvglLabel11 = objectHide(),
	LvglLabel12 = objectHide(),
	LvglLabel13 = objectHide(),
	LvglLabel14 = objectHide(),
	LvglLabel15 = objectHide(),
	LvglLabel16 = objectHide(),
	LvglLabel17 = objectHide(),
	LvglLabel18 = objectHide(),
	LvglLabel19 = objectHide(),
	LvglLabel20 = objectHide(),
	LvglLabel21 = objectHide(),
}
----------------------------------------------------------------------------
--The following is the content of screen: ScreenA
---------------------------------------------------------------------------
ScreenA.create = function()
	ScreenA.contFather_ScreenA = lvgl.cont_create(lvgl.scr_act(), nil)
	lvgl.obj_set_size(ScreenA.contFather_ScreenA, 854, 480)
	lvgl.obj_align(ScreenA.contFather_ScreenA, nil, lvgl.ALIGN_IN_TOP_LEFT, 0, 0)

	--This is the BTN_PART_MAIN's style of ScreenA.LvglButton1
	Style_LvglButton1_1=lvgl.style_t()
	lvgl.style_init(Style_LvglButton1_1)
	lvgl.style_set_radius(Style_LvglButton1_1, lvgl.STATE_DEFAULT, 4)
	lvgl.style_set_bg_color(Style_LvglButton1_1, lvgl.STATE_DEFAULT, lvgl.color_hex(0x0088FF))
	lvgl.style_set_value_str(Style_LvglButton1_1, lvgl.STATE_DEFAULT, "点击加入")
	lvgl.style_set_value_color(Style_LvglButton1_1, lvgl.STATE_DEFAULT, lvgl.color_hex(0xFFFFFF))
	lvgl.style_set_value_letter_space(Style_LvglButton1_1, lvgl.STATE_DEFAULT, 4)

	--This is the base code of ScreenA.LvglButton1
	ScreenA.LvglButton1.self = lvgl.btn_create(ScreenA.contFather_ScreenA, nil)
	lvgl.obj_set_size(ScreenA.LvglButton1.self, 362, 79)
	lvgl.obj_set_click(ScreenA.LvglButton1.self, true)
	lvgl.obj_align(ScreenA.LvglButton1.self, ScreenA.contFather_ScreenA, lvgl.ALIGN_IN_TOP_LEFT, 65, 608)
	lvgl.obj_add_style(ScreenA.LvglButton1.self, lvgl.BTN_PART_MAIN, Style_LvglButton1_1)
	--This is to add callback function for ScreenA.LvglButton1
	--This is callBack function of ScreenA.LvglButton1
	local handleLvglButton1 = function(obj, e)
		ScreenA.LvglButton1.cb(e)
		ScreenA.LvglButton1.cb = function(e)
			if (e == lvgl.EVENT_CLICKED)then
				joinUs()
			end
		end
	end
	lvgl.obj_set_event_cb(ScreenA.LvglButton1.self, handleLvglButton1)


	--This is the IMG_PART_MAIN's style of ScreenA.LvglImg1
	Style_LvglImg1_1=lvgl.style_t()
	lvgl.style_init(Style_LvglImg1_1)

	--This is the base code of ScreenA.LvglImg1
	ScreenA.LvglImg1.self = lvgl.img_create(ScreenA.contFather_ScreenA, nil)
	lvgl.obj_set_click(ScreenA.LvglImg1.self, true)
	lvgl.img_set_src(ScreenA.LvglImg1.self, "/lua/bg.png")
	lvgl.img_set_zoom(ScreenA.LvglImg1.self, 287)
	lvgl.img_set_pivot(ScreenA.LvglImg1.self, 0, 0)
	lvgl.obj_align(ScreenA.LvglImg1.self, ScreenA.contFather_ScreenA, lvgl.ALIGN_IN_TOP_LEFT, -2, -9)
	lvgl.obj_add_style(ScreenA.LvglImg1.self, lvgl.IMG_PART_MAIN, Style_LvglImg1_1)


	--This is the LABEL_PART_MAIN's style of ScreenA.LvglLabel1
	Style_LvglLabel1_1=lvgl.style_t()
	lvgl.style_init(Style_LvglLabel1_1)
	lvgl.style_set_text_color(Style_LvglLabel1_1, lvgl.STATE_DEFAULT, lvgl.color_hex(0xFFFFFF))
	lvgl.style_set_text_font(Style_LvglLabel1_1, lvgl.STATE_DEFAULT, lvgl.font_load("normal", 36))

	--This is the base code of ScreenA.LvglLabel1
	ScreenA.LvglLabel1.self = lvgl.label_create(ScreenA.contFather_ScreenA, nil)
	lvgl.obj_set_width(ScreenA.LvglLabel1.self, 180)
	lvgl.obj_set_click(ScreenA.LvglLabel1.self, true)
	lvgl.label_set_long_mode(ScreenA.LvglLabel1.self, lvgl.LABEL_LONG_EXPAND)
	lvgl.label_set_align(ScreenA.LvglLabel1.self, lvgl.LABEL_ALIGN_LEFT)
	lvgl.label_set_text(ScreenA.LvglLabel1.self, "电力集中器")
	lvgl.obj_align(ScreenA.LvglLabel1.self, ScreenA.contFather_ScreenA, lvgl.ALIGN_IN_TOP_LEFT, 8, 5)
	lvgl.obj_add_style(ScreenA.LvglLabel1.self, lvgl.LABEL_PART_MAIN, Style_LvglLabel1_1)


	--This is the IMG_PART_MAIN's style of ScreenA.LvglImg2
	Style_LvglImg2_1=lvgl.style_t()
	lvgl.style_init(Style_LvglImg2_1)

	--This is the base code of ScreenA.LvglImg2
	ScreenA.LvglImg2.self = lvgl.img_create(ScreenA.contFather_ScreenA, nil)
	lvgl.obj_set_click(ScreenA.LvglImg2.self, true)
	lvgl.img_set_src(ScreenA.LvglImg2.self, "/lua/nosin_icon.png")
	lvgl.img_set_zoom(ScreenA.LvglImg2.self, 256)
	lvgl.img_set_pivot(ScreenA.LvglImg2.self, 0, 0)
	lvgl.obj_align(ScreenA.LvglImg2.self, ScreenA.contFather_ScreenA, lvgl.ALIGN_IN_TOP_LEFT, 783, 12)
	lvgl.obj_add_style(ScreenA.LvglImg2.self, lvgl.IMG_PART_MAIN, Style_LvglImg2_1)


	--This is the LABEL_PART_MAIN's style of ScreenA.LvglLabel2
	Style_LvglLabel2_1=lvgl.style_t()
	lvgl.style_init(Style_LvglLabel2_1)
	lvgl.style_set_text_color(Style_LvglLabel2_1, lvgl.STATE_DEFAULT, lvgl.color_hex(0xFFFFFF))
	lvgl.style_set_text_font(Style_LvglLabel2_1, lvgl.STATE_DEFAULT, lvgl.font_load("normal", 28))

	--This is the base code of ScreenA.LvglLabel2
	ScreenA.LvglLabel2.self = lvgl.label_create(ScreenA.contFather_ScreenA, nil)
	lvgl.obj_set_width(ScreenA.LvglLabel2.self, 180)
	lvgl.obj_set_click(ScreenA.LvglLabel2.self, true)
	lvgl.label_set_long_mode(ScreenA.LvglLabel2.self, lvgl.LABEL_LONG_EXPAND)
	lvgl.label_set_align(ScreenA.LvglLabel2.self, lvgl.LABEL_ALIGN_LEFT)
	lvgl.label_set_text(ScreenA.LvglLabel2.self, "2022-4-28 周四 11:06:28")
	lvgl.obj_align(ScreenA.LvglLabel2.self, ScreenA.contFather_ScreenA, lvgl.ALIGN_IN_TOP_LEFT, 474, 10)
	lvgl.obj_add_style(ScreenA.LvglLabel2.self, lvgl.LABEL_PART_MAIN, Style_LvglLabel2_1)


	--This is the LABEL_PART_MAIN's style of ScreenA.LvglLabel3
	Style_LvglLabel3_1=lvgl.style_t()
	lvgl.style_init(Style_LvglLabel3_1)
	lvgl.style_set_text_color(Style_LvglLabel3_1, lvgl.STATE_DEFAULT, lvgl.color_hex(0xFFFFFF))
	lvgl.style_set_text_font(Style_LvglLabel3_1, lvgl.STATE_DEFAULT, lvgl.font_load("normal", 22))

	--This is the base code of ScreenA.LvglLabel3
	ScreenA.LvglLabel3.self = lvgl.label_create(ScreenA.contFather_ScreenA, nil)
	lvgl.obj_set_width(ScreenA.LvglLabel3.self, 180)
	lvgl.obj_set_click(ScreenA.LvglLabel3.self, true)
	lvgl.label_set_long_mode(ScreenA.LvglLabel3.self, lvgl.LABEL_LONG_EXPAND)
	lvgl.label_set_align(ScreenA.LvglLabel3.self, lvgl.LABEL_ALIGN_LEFT)
	lvgl.label_set_text(ScreenA.LvglLabel3.self, "闸状态")
	lvgl.obj_align(ScreenA.LvglLabel3.self, ScreenA.contFather_ScreenA, lvgl.ALIGN_IN_TOP_LEFT, 19, 137)
	lvgl.obj_add_style(ScreenA.LvglLabel3.self, lvgl.LABEL_PART_MAIN, Style_LvglLabel3_1)


	--This is the LABEL_PART_MAIN's style of ScreenA.LvglLabel4
	Style_LvglLabel4_1=lvgl.style_t()
	lvgl.style_init(Style_LvglLabel4_1)
	lvgl.style_set_text_color(Style_LvglLabel4_1, lvgl.STATE_DEFAULT, lvgl.color_hex(0xFFFFFF))
	lvgl.style_set_text_font(Style_LvglLabel4_1, lvgl.STATE_DEFAULT, lvgl.font_load("normal", 30))

	--This is the base code of ScreenA.LvglLabel4
	ScreenA.LvglLabel4.self = lvgl.label_create(ScreenA.contFather_ScreenA, nil)
	lvgl.obj_set_width(ScreenA.LvglLabel4.self, 180)
	lvgl.obj_set_click(ScreenA.LvglLabel4.self, true)
	lvgl.label_set_long_mode(ScreenA.LvglLabel4.self, lvgl.LABEL_LONG_EXPAND)
	lvgl.label_set_align(ScreenA.LvglLabel4.self, lvgl.LABEL_ALIGN_LEFT)
	lvgl.label_set_text(ScreenA.LvglLabel4.self, "合")
	lvgl.obj_align(ScreenA.LvglLabel4.self, ScreenA.contFather_ScreenA, lvgl.ALIGN_IN_TOP_LEFT, 105, 93)
	lvgl.obj_add_style(ScreenA.LvglLabel4.self, lvgl.LABEL_PART_MAIN, Style_LvglLabel4_1)


	--This is the LABEL_PART_MAIN's style of ScreenA.LvglLabel5
	Style_LvglLabel5_1=lvgl.style_t()
	lvgl.style_init(Style_LvglLabel5_1)
	lvgl.style_set_text_color(Style_LvglLabel5_1, lvgl.STATE_DEFAULT, lvgl.color_hex(0xFFFFFF))
	lvgl.style_set_text_font(Style_LvglLabel5_1, lvgl.STATE_DEFAULT, lvgl.font_load("normal", 22))

	--This is the base code of ScreenA.LvglLabel5
	ScreenA.LvglLabel5.self = lvgl.label_create(ScreenA.contFather_ScreenA, nil)
	lvgl.obj_set_width(ScreenA.LvglLabel5.self, 180)
	lvgl.obj_set_click(ScreenA.LvglLabel5.self, true)
	lvgl.label_set_long_mode(ScreenA.LvglLabel5.self, lvgl.LABEL_LONG_EXPAND)
	lvgl.label_set_align(ScreenA.LvglLabel5.self, lvgl.LABEL_ALIGN_LEFT)
	lvgl.label_set_text(ScreenA.LvglLabel5.self, "有功电能")
	lvgl.obj_align(ScreenA.LvglLabel5.self, ScreenA.contFather_ScreenA, lvgl.ALIGN_IN_TOP_LEFT, 19, 288)
	lvgl.obj_add_style(ScreenA.LvglLabel5.self, lvgl.LABEL_PART_MAIN, Style_LvglLabel5_1)


	--This is the LABEL_PART_MAIN's style of ScreenA.LvglLabel6
	Style_LvglLabel6_1=lvgl.style_t()
	lvgl.style_init(Style_LvglLabel6_1)
	lvgl.style_set_text_color(Style_LvglLabel6_1, lvgl.STATE_DEFAULT, lvgl.color_hex(0xFFFFFF))
	lvgl.style_set_text_font(Style_LvglLabel6_1, lvgl.STATE_DEFAULT, lvgl.font_load("normal", 30))

	--This is the base code of ScreenA.LvglLabel6
	ScreenA.LvglLabel6.self = lvgl.label_create(ScreenA.contFather_ScreenA, nil)
	lvgl.obj_set_width(ScreenA.LvglLabel6.self, 180)
	lvgl.obj_set_click(ScreenA.LvglLabel6.self, true)
	lvgl.label_set_long_mode(ScreenA.LvglLabel6.self, lvgl.LABEL_LONG_EXPAND)
	lvgl.label_set_align(ScreenA.LvglLabel6.self, lvgl.LABEL_ALIGN_LEFT)
	lvgl.label_set_text(ScreenA.LvglLabel6.self, "10.1kW")
	lvgl.obj_align(ScreenA.LvglLabel6.self, ScreenA.contFather_ScreenA, lvgl.ALIGN_IN_TOP_LEFT, 69, 236)
	lvgl.obj_add_style(ScreenA.LvglLabel6.self, lvgl.LABEL_PART_MAIN, Style_LvglLabel6_1)


	--This is the LABEL_PART_MAIN's style of ScreenA.LvglLabel7
	Style_LvglLabel7_1=lvgl.style_t()
	lvgl.style_init(Style_LvglLabel7_1)
	lvgl.style_set_text_color(Style_LvglLabel7_1, lvgl.STATE_DEFAULT, lvgl.color_hex(0xFFFFFF))
	lvgl.style_set_text_font(Style_LvglLabel7_1, lvgl.STATE_DEFAULT, lvgl.font_load("normal", 22))

	--This is the base code of ScreenA.LvglLabel7
	ScreenA.LvglLabel7.self = lvgl.label_create(ScreenA.contFather_ScreenA, nil)
	lvgl.obj_set_width(ScreenA.LvglLabel7.self, 180)
	lvgl.obj_set_click(ScreenA.LvglLabel7.self, true)
	lvgl.label_set_long_mode(ScreenA.LvglLabel7.self, lvgl.LABEL_LONG_EXPAND)
	lvgl.label_set_align(ScreenA.LvglLabel7.self, lvgl.LABEL_ALIGN_LEFT)
	lvgl.label_set_text(ScreenA.LvglLabel7.self, "无功电能")
	lvgl.obj_align(ScreenA.LvglLabel7.self, ScreenA.contFather_ScreenA, lvgl.ALIGN_IN_TOP_LEFT, 19, 415)
	lvgl.obj_add_style(ScreenA.LvglLabel7.self, lvgl.LABEL_PART_MAIN, Style_LvglLabel7_1)


	--This is the LABEL_PART_MAIN's style of ScreenA.LvglLabel8
	Style_LvglLabel8_1=lvgl.style_t()
	lvgl.style_init(Style_LvglLabel8_1)
	lvgl.style_set_text_color(Style_LvglLabel8_1, lvgl.STATE_DEFAULT, lvgl.color_hex(0xFFFFFF))
	lvgl.style_set_text_font(Style_LvglLabel8_1, lvgl.STATE_DEFAULT, lvgl.font_load("normal", 30))

	--This is the base code of ScreenA.LvglLabel8
	ScreenA.LvglLabel8.self = lvgl.label_create(ScreenA.contFather_ScreenA, nil)
	lvgl.obj_set_width(ScreenA.LvglLabel8.self, 180)
	lvgl.obj_set_click(ScreenA.LvglLabel8.self, true)
	lvgl.label_set_long_mode(ScreenA.LvglLabel8.self, lvgl.LABEL_LONG_EXPAND)
	lvgl.label_set_align(ScreenA.LvglLabel8.self, lvgl.LABEL_ALIGN_LEFT)
	lvgl.label_set_text(ScreenA.LvglLabel8.self, "10.1kW")
	lvgl.obj_align(ScreenA.LvglLabel8.self, ScreenA.contFather_ScreenA, lvgl.ALIGN_IN_TOP_LEFT, 69, 371)
	lvgl.obj_add_style(ScreenA.LvglLabel8.self, lvgl.LABEL_PART_MAIN, Style_LvglLabel8_1)


	--This is the LABEL_PART_MAIN's style of ScreenA.LvglLabel9
	Style_LvglLabel9_1=lvgl.style_t()
	lvgl.style_init(Style_LvglLabel9_1)
	lvgl.style_set_text_color(Style_LvglLabel9_1, lvgl.STATE_DEFAULT, lvgl.color_hex(0xFFFFFF))
	lvgl.style_set_text_font(Style_LvglLabel9_1, lvgl.STATE_DEFAULT, lvgl.font_load("normal", 22))

	--This is the base code of ScreenA.LvglLabel9
	ScreenA.LvglLabel9.self = lvgl.label_create(ScreenA.contFather_ScreenA, nil)
	lvgl.obj_set_width(ScreenA.LvglLabel9.self, 180)
	lvgl.obj_set_click(ScreenA.LvglLabel9.self, true)
	lvgl.label_set_long_mode(ScreenA.LvglLabel9.self, lvgl.LABEL_LONG_EXPAND)
	lvgl.label_set_align(ScreenA.LvglLabel9.self, lvgl.LABEL_ALIGN_LEFT)
	lvgl.label_set_text(ScreenA.LvglLabel9.self, "当前电路运行正常")
	lvgl.obj_align(ScreenA.LvglLabel9.self, ScreenA.contFather_ScreenA, lvgl.ALIGN_IN_TOP_LEFT, 329, 68)
	lvgl.obj_add_style(ScreenA.LvglLabel9.self, lvgl.LABEL_PART_MAIN, Style_LvglLabel9_1)


	--This is the LABEL_PART_MAIN's style of ScreenA.LvglLabel10
	Style_LvglLabel10_1=lvgl.style_t()
	lvgl.style_init(Style_LvglLabel10_1)
	lvgl.style_set_text_color(Style_LvglLabel10_1, lvgl.STATE_DEFAULT, lvgl.color_hex(0xFFFFFF))
	lvgl.style_set_text_font(Style_LvglLabel10_1, lvgl.STATE_DEFAULT, lvgl.font_load("normal", 22))

	--This is the base code of ScreenA.LvglLabel10
	ScreenA.LvglLabel10.self = lvgl.label_create(ScreenA.contFather_ScreenA, nil)
	lvgl.obj_set_width(ScreenA.LvglLabel10.self, 180)
	lvgl.obj_set_click(ScreenA.LvglLabel10.self, true)
	lvgl.label_set_long_mode(ScreenA.LvglLabel10.self, lvgl.LABEL_LONG_EXPAND)
	lvgl.label_set_align(ScreenA.LvglLabel10.self, lvgl.LABEL_ALIGN_LEFT)
	lvgl.label_set_text(ScreenA.LvglLabel10.self, "线路温度")
	lvgl.obj_align(ScreenA.LvglLabel10.self, ScreenA.contFather_ScreenA, lvgl.ALIGN_IN_TOP_LEFT, 220, 224)
	lvgl.obj_add_style(ScreenA.LvglLabel10.self, lvgl.LABEL_PART_MAIN, Style_LvglLabel10_1)


	--This is the LABEL_PART_MAIN's style of ScreenA.LvglLabel11
	Style_LvglLabel11_1=lvgl.style_t()
	lvgl.style_init(Style_LvglLabel11_1)
	lvgl.style_set_text_color(Style_LvglLabel11_1, lvgl.STATE_DEFAULT, lvgl.color_hex(0xFFFFFF))
	lvgl.style_set_text_font(Style_LvglLabel11_1, lvgl.STATE_DEFAULT, lvgl.font_load("normal", 30))

	--This is the base code of ScreenA.LvglLabel11
	ScreenA.LvglLabel11.self = lvgl.label_create(ScreenA.contFather_ScreenA, nil)
	lvgl.obj_set_width(ScreenA.LvglLabel11.self, 180)
	lvgl.obj_set_click(ScreenA.LvglLabel11.self, true)
	lvgl.label_set_long_mode(ScreenA.LvglLabel11.self, lvgl.LABEL_LONG_EXPAND)
	lvgl.label_set_align(ScreenA.LvglLabel11.self, lvgl.LABEL_ALIGN_LEFT)
	lvgl.label_set_text(ScreenA.LvglLabel11.self, "36.2")
	lvgl.obj_align(ScreenA.LvglLabel11.self, ScreenA.contFather_ScreenA, lvgl.ALIGN_IN_TOP_LEFT, 385, 172)
	lvgl.obj_add_style(ScreenA.LvglLabel11.self, lvgl.LABEL_PART_MAIN, Style_LvglLabel11_1)


	--This is the LABEL_PART_MAIN's style of ScreenA.LvglLabel12
	Style_LvglLabel12_1=lvgl.style_t()
	lvgl.style_init(Style_LvglLabel12_1)
	lvgl.style_set_text_color(Style_LvglLabel12_1, lvgl.STATE_DEFAULT, lvgl.color_hex(0xFFFFFF))
	lvgl.style_set_text_font(Style_LvglLabel12_1, lvgl.STATE_DEFAULT, lvgl.font_load("normal", 22))

	--This is the base code of ScreenA.LvglLabel12
	ScreenA.LvglLabel12.self = lvgl.label_create(ScreenA.contFather_ScreenA, nil)
	lvgl.obj_set_width(ScreenA.LvglLabel12.self, 180)
	lvgl.obj_set_click(ScreenA.LvglLabel12.self, true)
	lvgl.label_set_long_mode(ScreenA.LvglLabel12.self, lvgl.LABEL_LONG_EXPAND)
	lvgl.label_set_align(ScreenA.LvglLabel12.self, lvgl.LABEL_ALIGN_LEFT)
	lvgl.label_set_text(ScreenA.LvglLabel12.self, "有功功率")
	lvgl.obj_align(ScreenA.LvglLabel12.self, ScreenA.contFather_ScreenA, lvgl.ALIGN_IN_TOP_LEFT, 220, 417)
	lvgl.obj_add_style(ScreenA.LvglLabel12.self, lvgl.LABEL_PART_MAIN, Style_LvglLabel12_1)


	--This is the LABEL_PART_MAIN's style of ScreenA.LvglLabel13
	Style_LvglLabel13_1=lvgl.style_t()
	lvgl.style_init(Style_LvglLabel13_1)
	lvgl.style_set_text_color(Style_LvglLabel13_1, lvgl.STATE_DEFAULT, lvgl.color_hex(0xFFFFFF))
	lvgl.style_set_text_font(Style_LvglLabel13_1, lvgl.STATE_DEFAULT, lvgl.font_load("normal", 22))

	--This is the base code of ScreenA.LvglLabel13
	ScreenA.LvglLabel13.self = lvgl.label_create(ScreenA.contFather_ScreenA, nil)
	lvgl.obj_set_width(ScreenA.LvglLabel13.self, 180)
	lvgl.obj_set_click(ScreenA.LvglLabel13.self, true)
	lvgl.label_set_long_mode(ScreenA.LvglLabel13.self, lvgl.LABEL_LONG_EXPAND)
	lvgl.label_set_align(ScreenA.LvglLabel13.self, lvgl.LABEL_ALIGN_LEFT)
	lvgl.label_set_text(ScreenA.LvglLabel13.self, "无功功率")
	lvgl.obj_align(ScreenA.LvglLabel13.self, ScreenA.contFather_ScreenA, lvgl.ALIGN_IN_TOP_LEFT, 455, 419)
	lvgl.obj_add_style(ScreenA.LvglLabel13.self, lvgl.LABEL_PART_MAIN, Style_LvglLabel13_1)


	--This is the LABEL_PART_MAIN's style of ScreenA.LvglLabel14
	Style_LvglLabel14_1=lvgl.style_t()
	lvgl.style_init(Style_LvglLabel14_1)
	lvgl.style_set_text_color(Style_LvglLabel14_1, lvgl.STATE_DEFAULT, lvgl.color_hex(0xFFFFFF))
	lvgl.style_set_text_font(Style_LvglLabel14_1, lvgl.STATE_DEFAULT, lvgl.font_load("normal", 30))

	--This is the base code of ScreenA.LvglLabel14
	ScreenA.LvglLabel14.self = lvgl.label_create(ScreenA.contFather_ScreenA, nil)
	lvgl.obj_set_width(ScreenA.LvglLabel14.self, 180)
	lvgl.obj_set_click(ScreenA.LvglLabel14.self, true)
	lvgl.label_set_long_mode(ScreenA.LvglLabel14.self, lvgl.LABEL_LONG_EXPAND)
	lvgl.label_set_align(ScreenA.LvglLabel14.self, lvgl.LABEL_ALIGN_LEFT)
	lvgl.label_set_text(ScreenA.LvglLabel14.self, "10.1kW")
	lvgl.obj_align(ScreenA.LvglLabel14.self, ScreenA.contFather_ScreenA, lvgl.ALIGN_IN_TOP_LEFT, 284, 374)
	lvgl.obj_add_style(ScreenA.LvglLabel14.self, lvgl.LABEL_PART_MAIN, Style_LvglLabel14_1)


	--This is the LABEL_PART_MAIN's style of ScreenA.LvglLabel15
	Style_LvglLabel15_1=lvgl.style_t()
	lvgl.style_init(Style_LvglLabel15_1)
	lvgl.style_set_text_color(Style_LvglLabel15_1, lvgl.STATE_DEFAULT, lvgl.color_hex(0xFFFFFF))
	lvgl.style_set_text_font(Style_LvglLabel15_1, lvgl.STATE_DEFAULT, lvgl.font_load("normal", 30))

	--This is the base code of ScreenA.LvglLabel15
	ScreenA.LvglLabel15.self = lvgl.label_create(ScreenA.contFather_ScreenA, nil)
	lvgl.obj_set_width(ScreenA.LvglLabel15.self, 180)
	lvgl.obj_set_click(ScreenA.LvglLabel15.self, true)
	lvgl.label_set_long_mode(ScreenA.LvglLabel15.self, lvgl.LABEL_LONG_EXPAND)
	lvgl.label_set_align(ScreenA.LvglLabel15.self, lvgl.LABEL_ALIGN_LEFT)
	lvgl.label_set_text(ScreenA.LvglLabel15.self, "10.1kW")
	lvgl.obj_align(ScreenA.LvglLabel15.self, ScreenA.contFather_ScreenA, lvgl.ALIGN_IN_TOP_LEFT, 513, 372)
	lvgl.obj_add_style(ScreenA.LvglLabel15.self, lvgl.LABEL_PART_MAIN, Style_LvglLabel15_1)


	--This is the LABEL_PART_MAIN's style of ScreenA.LvglLabel16
	Style_LvglLabel16_1=lvgl.style_t()
	lvgl.style_init(Style_LvglLabel16_1)
	lvgl.style_set_text_color(Style_LvglLabel16_1, lvgl.STATE_DEFAULT, lvgl.color_hex(0xFFFFFF))
	lvgl.style_set_text_font(Style_LvglLabel16_1, lvgl.STATE_DEFAULT, lvgl.font_load("normal", 22))

	--This is the base code of ScreenA.LvglLabel16
	ScreenA.LvglLabel16.self = lvgl.label_create(ScreenA.contFather_ScreenA, nil)
	lvgl.obj_set_width(ScreenA.LvglLabel16.self, 180)
	lvgl.obj_set_click(ScreenA.LvglLabel16.self, true)
	lvgl.label_set_long_mode(ScreenA.LvglLabel16.self, lvgl.LABEL_LONG_EXPAND)
	lvgl.label_set_align(ScreenA.LvglLabel16.self, lvgl.LABEL_ALIGN_LEFT)
	lvgl.label_set_text(ScreenA.LvglLabel16.self, "当前电压")
	lvgl.obj_align(ScreenA.LvglLabel16.self, ScreenA.contFather_ScreenA, lvgl.ALIGN_IN_TOP_LEFT, 656, 109)
	lvgl.obj_add_style(ScreenA.LvglLabel16.self, lvgl.LABEL_PART_MAIN, Style_LvglLabel16_1)


	--This is the LABEL_PART_MAIN's style of ScreenA.LvglLabel17
	Style_LvglLabel17_1=lvgl.style_t()
	lvgl.style_init(Style_LvglLabel17_1)
	lvgl.style_set_text_color(Style_LvglLabel17_1, lvgl.STATE_DEFAULT, lvgl.color_hex(0xFFFFFF))
	lvgl.style_set_text_font(Style_LvglLabel17_1, lvgl.STATE_DEFAULT, lvgl.font_load("normal", 22))

	--This is the base code of ScreenA.LvglLabel17
	ScreenA.LvglLabel17.self = lvgl.label_create(ScreenA.contFather_ScreenA, nil)
	lvgl.obj_set_width(ScreenA.LvglLabel17.self, 180)
	lvgl.obj_set_click(ScreenA.LvglLabel17.self, true)
	lvgl.label_set_long_mode(ScreenA.LvglLabel17.self, lvgl.LABEL_LONG_EXPAND)
	lvgl.label_set_align(ScreenA.LvglLabel17.self, lvgl.LABEL_ALIGN_LEFT)
	lvgl.label_set_text(ScreenA.LvglLabel17.self, "当前电流")
	lvgl.obj_align(ScreenA.LvglLabel17.self, ScreenA.contFather_ScreenA, lvgl.ALIGN_IN_TOP_LEFT, 653, 284)
	lvgl.obj_add_style(ScreenA.LvglLabel17.self, lvgl.LABEL_PART_MAIN, Style_LvglLabel17_1)


	--This is the LABEL_PART_MAIN's style of ScreenA.LvglLabel18
	Style_LvglLabel18_1=lvgl.style_t()
	lvgl.style_init(Style_LvglLabel18_1)
	lvgl.style_set_text_color(Style_LvglLabel18_1, lvgl.STATE_DEFAULT, lvgl.color_hex(0xFFFFFF))
	lvgl.style_set_text_font(Style_LvglLabel18_1, lvgl.STATE_DEFAULT, lvgl.font_load("normal", 22))

	--This is the base code of ScreenA.LvglLabel18
	ScreenA.LvglLabel18.self = lvgl.label_create(ScreenA.contFather_ScreenA, nil)
	lvgl.obj_set_width(ScreenA.LvglLabel18.self, 180)
	lvgl.obj_set_click(ScreenA.LvglLabel18.self, true)
	lvgl.label_set_long_mode(ScreenA.LvglLabel18.self, lvgl.LABEL_LONG_EXPAND)
	lvgl.label_set_align(ScreenA.LvglLabel18.self, lvgl.LABEL_ALIGN_LEFT)
	lvgl.label_set_text(ScreenA.LvglLabel18.self, "剩余电流")
	lvgl.obj_align(ScreenA.LvglLabel18.self, ScreenA.contFather_ScreenA, lvgl.ALIGN_IN_TOP_LEFT, 653, 421)
	lvgl.obj_add_style(ScreenA.LvglLabel18.self, lvgl.LABEL_PART_MAIN, Style_LvglLabel18_1)


	--This is the LABEL_PART_MAIN's style of ScreenA.LvglLabel19
	Style_LvglLabel19_1=lvgl.style_t()
	lvgl.style_init(Style_LvglLabel19_1)
	lvgl.style_set_text_color(Style_LvglLabel19_1, lvgl.STATE_DEFAULT, lvgl.color_hex(0xFFFFFF))
	lvgl.style_set_text_font(Style_LvglLabel19_1, lvgl.STATE_DEFAULT, lvgl.font_load("normal", 30))

	--This is the base code of ScreenA.LvglLabel19
	ScreenA.LvglLabel19.self = lvgl.label_create(ScreenA.contFather_ScreenA, nil)
	lvgl.obj_set_width(ScreenA.LvglLabel19.self, 180)
	lvgl.obj_set_click(ScreenA.LvglLabel19.self, true)
	lvgl.label_set_long_mode(ScreenA.LvglLabel19.self, lvgl.LABEL_LONG_EXPAND)
	lvgl.label_set_align(ScreenA.LvglLabel19.self, lvgl.LABEL_ALIGN_LEFT)
	lvgl.label_set_text(ScreenA.LvglLabel19.self, "220V")
	lvgl.obj_align(ScreenA.LvglLabel19.self, ScreenA.contFather_ScreenA, lvgl.ALIGN_IN_TOP_LEFT, 733, 73)
	lvgl.obj_add_style(ScreenA.LvglLabel19.self, lvgl.LABEL_PART_MAIN, Style_LvglLabel19_1)


	--This is the LABEL_PART_MAIN's style of ScreenA.LvglLabel20
	Style_LvglLabel20_1=lvgl.style_t()
	lvgl.style_init(Style_LvglLabel20_1)
	lvgl.style_set_text_color(Style_LvglLabel20_1, lvgl.STATE_DEFAULT, lvgl.color_hex(0xFFFFFF))
	lvgl.style_set_text_font(Style_LvglLabel20_1, lvgl.STATE_DEFAULT, lvgl.font_load("normal", 30))

	--This is the base code of ScreenA.LvglLabel20
	ScreenA.LvglLabel20.self = lvgl.label_create(ScreenA.contFather_ScreenA, nil)
	lvgl.obj_set_width(ScreenA.LvglLabel20.self, 180)
	lvgl.obj_set_click(ScreenA.LvglLabel20.self, true)
	lvgl.label_set_long_mode(ScreenA.LvglLabel20.self, lvgl.LABEL_LONG_EXPAND)
	lvgl.label_set_align(ScreenA.LvglLabel20.self, lvgl.LABEL_ALIGN_LEFT)
	lvgl.label_set_text(ScreenA.LvglLabel20.self, "10.1A")
	lvgl.obj_align(ScreenA.LvglLabel20.self, ScreenA.contFather_ScreenA, lvgl.ALIGN_IN_TOP_LEFT, 731, 244)
	lvgl.obj_add_style(ScreenA.LvglLabel20.self, lvgl.LABEL_PART_MAIN, Style_LvglLabel20_1)


	--This is the LABEL_PART_MAIN's style of ScreenA.LvglLabel21
	Style_LvglLabel21_1=lvgl.style_t()
	lvgl.style_init(Style_LvglLabel21_1)
	lvgl.style_set_text_color(Style_LvglLabel21_1, lvgl.STATE_DEFAULT, lvgl.color_hex(0xFFFFFF))
	lvgl.style_set_text_font(Style_LvglLabel21_1, lvgl.STATE_DEFAULT, lvgl.font_load("normal", 30))

	--This is the base code of ScreenA.LvglLabel21
	ScreenA.LvglLabel21.self = lvgl.label_create(ScreenA.contFather_ScreenA, nil)
	lvgl.obj_set_click(ScreenA.LvglLabel21.self, true)
	lvgl.label_set_long_mode(ScreenA.LvglLabel21.self, lvgl.LABEL_LONG_EXPAND)
	lvgl.label_set_align(ScreenA.LvglLabel21.self, lvgl.LABEL_ALIGN_LEFT)
	lvgl.label_set_text(ScreenA.LvglLabel21.self, "11mA")
	lvgl.obj_align(ScreenA.LvglLabel21.self, ScreenA.contFather_ScreenA, lvgl.ALIGN_IN_TOP_LEFT, 733, 379)
	lvgl.obj_add_style(ScreenA.LvglLabel21.self, lvgl.LABEL_PART_MAIN, Style_LvglLabel21_1)
end
ScreenA.free = function()
	lvgl.obj_del(ScreenA.contFather_ScreenA)
	ScreenA.contFather_ScreenA = nil
end


----------------------------------------------------------------------------
-----------------------This is the Initial of lvglGUI-----------------------
----------------------------------------------------------------------------
function lvglUiInitial()
	ScreenA.create()
end
