module(..., package.seeall)
function create(p, v)
    obj = lvgl.spinner_create(p, nil)
     if v.click then
        lvgl.obj_set_click(obj, true)
    else
        lvgl.obj_set_click(obj, false)
    end
    if v.W and v.H then lvgl.obj_set_size(obj, v.W, v.H) end

    -- 弧长
    -- 圆弧的长度可以通过 lvgl.spinner_set_arc_length(obj,deg) 进行调整。
    lvgl.spinner_set_arc_length(obj, v.deg)

    -- 旋转速度
    -- 旋转速度可以通过 lvgl.spinner_set_spin_time(obj, time_ms) 进行调整。
    lvgl.spinner_set_spin_time(obj, v.time)

    -- 旋转方向
    -- 旋转方向可以通过 lvgl.spinner_set_dir(obj, lvgl.SPINNER_DIR_FORWARD/BACKWARD) 进行更改。
    lvgl.spinner_set_dir(obj, lvgl.SPINNER_DIR_FORWARD)

    -- 旋转类型
    -- 支持以下旋转类型
    -- lvgl.SPINNER_TYPE_SPINNING_ARC 旋转弧线，在顶部减速
    -- lvgl.SPINNER_TYPE_FILLSPIN_ARC 旋转弧线，在顶部放慢速度，但也伸展弧线
    -- lvgl.SPINNER_TYPE_CONSTANT_ARC 以恒定速度旋转
    -- 使用 lvgl.spinner_set_type(obj, lvgl.SPINNER_TYPE_...) 进行设置
    lvgl.spinner_set_type(obj, lvgl.SPINNER_TYPE_SPINNING_ARC)

    lvgl.obj_align(obj, v.align_to, v.align or lvgl.ALIGN_CENTER,
                   v.align_x or 0, v.align_y or 0)
    if v.event then lvgl.obj_set_event_cb(obj, v.event) end

    --     旋转器包括一下部分：

    -- lvgl.SPINNER_PART_BG: 主要部分
    -- lvgl.SPINNER_PART_INDIC: 旋转弧（虚拟部分）
    -- 零件和样式的作用与 弧(lvgl.arc) 情况相同。
    if v.style then
        lvgl.obj_add_style(obj, lvgl.SPINNER_PART_BG, create_style(v.style.bg))
        lvgl.obj_add_style(obj, lvgl.SPINNER_PART_INDIC,
                           create_style(v.style.indic))
    end

end
local spinner_data = {
    W = LCD_H,
    H = LCD_H,
    deg = 60,
    time = 3000,
    style = {
        bg = {
            border = {color = 0xff0f0f, width = 10, opa = 30},
            bg = {radius = 50, color = 0, opa = 200, grad = {color = 0x0f0fff}},
            line = {color = 0x00ff00, width = 30, opa = 200, rounded = false}
        },
        indic = {
            bg = {radius = 10, color = 0x00ff00, grad = {color = 0x0f0f0f}},
            line = {color = 0xff0000, width = 30, opa = 200, rounded = true}
        }
    }
}

-- create(lvgl.scr_act(), spinner_data)
