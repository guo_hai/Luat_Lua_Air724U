module(..., package.seeall)

local event_handler = function(obj, event)
    if event == lvgl.EVENT_CLICKED then
        print("按键默认回调")
    end
end

function create(p, v)
    obj = lvgl.list_create(p, nil)
    -- if v.click then
    --     lvgl.obj_set_click(obj, true)
    -- else
    --     lvgl.obj_set_click(obj, false)
    -- end
    lvgl.obj_set_size(obj, v.W or 400, v.H or 200)
    local btn_list = {}
    if v.btns then
        local icon
        for i = 1, #v.btns do
            icon = v.icon[i] or icon
            btn_list[i] = lvgl.list_add_btn(obj, icon, v.btns[i])
            lvgl.label_set_anim_speed(btn_list[i], 5)
            lvgl.obj_set_event_cb(btn_list[i], function(o, e)
                if e == lvgl.EVENT_CLICKED then
                    print(v.btns[i])
                    if v.event then
                        v.event(v.btns[i])
                    end
                end
            end)
            lvgl.obj_add_style(btn_list[i], lvgl.BTN_PART_MAIN, create_style(v.style.btn))
        end
    end

    -- 列表与 页面(Page) 具有相同的部分
    -- lvgl.LIST_PART_BG 
    -- lvgl.LIST_PART_EDGE_FLASH
    -- lvgl.LIST_PART_SCROLLABLE 
    -- lvgl.LIST_PART_SCROLLBAR 
    -- 有关详细信息，请参见 页面(Page) 部分。
    -- 列表上的按钮被视为普通按钮，它们只有一个主要部分，称为 lvgl.BTN_PART_MAIN 。
    -- 列表上的按钮可以有边缘，称为 lvgl.BTN_PART_EDGE 。

    if v.style then
        lvgl.obj_add_style(obj, lvgl.LIST_PART_BG, create_style(v.style.bg))
        lvgl.obj_add_style(obj, lvgl.LIST_PART_SCROLLBAR, create_style(v.style.bar))
        lvgl.obj_add_style(obj, lvgl.LIST_PART_EDGE_FLASH, create_style(v.style.edge_flash))
        lvgl.obj_add_style(obj, lvgl.PAGE_PART_SCROLLABLE, create_style(v.style.lable))

    end

    lvgl.obj_align(obj, v.align_to or nil, v.align or lvgl.ALIGN_CENTER, v.align_x or 0, v.align_y or 0)
    return {
        obj = obj,
        btn_list = btn_list
    }
end
local list_data = {
    W = LCD_W,
    H = LCD_H,
    icon = {SYMBOL_NEW_LINE, SYMBOL_DIRECTORY, SYMBOL_CLOSE, SYMBOL_EDIT, SYMBOL_USB, SYMBOL_GPS, SYMBOL_STOP,
            SYMBOL_VIDEO},
    btns = {"星期一星期一星期一星期一", "Open", "Delete", "Edit", "USB", "GPS", "Stop", "Video", "Stop",
            "Video", "Stop", "Video", "Stop", SYMBOL_STOP .. "Video"},
    style = {
        bg = {
            bg = {
                color = 0xff0000,
                radius = 0
            },
            border = {
                width = 0
            }
        },
        btn = {
            bg = {
                color = 0x00ffff
            },
            text = {
                color = 0xff0000
            }
        },
        lable = {
            bg = {
                color = 0x0000ff
            }
        },
        edge_flash = {
            bg = {
                color = 0x0000ff
            }
        },
        bar = {
            bg = {
                color = 0xcccccc
            }
        }
    }
}
-- list_obj = create(lvgl.scr_act(), list_data)

