module(..., package.seeall)

function create(p, v)
    obj = lvgl.checkbox_create(p, nil)
     if v.click then
        lvgl.obj_set_click(obj, true)
    else
        lvgl.obj_set_click(obj, false)
    end
    if v.W and v.H then lvgl.obj_set_size(obj, v.W, v.H) end

    -- 可以通过 lvgl.checkbox_set_text(cb, "New text") 函数修改文本。它将动态分配文本。
    -- 要设置静态文本，请使用 lvgl.checkbox_set_static_text(cb, txt) 。
    -- 这样，将仅存储 txt 指针，并且在存在复选框时不应释放该指针。
    if v.text then lvgl.checkbox_set_text(obj, v.text) end

    -- 可以通过 lvgl.checkbox_set_checked(cb, true/false) 手动选中/取消选中复选框。
    -- 设置为 true 将选中该复选框，而设置为 false 将取消选中该复选框。
    if v.checked then lvgl.checkbox_set_checked(obj, true) end

    -- 要禁用复选框，调用 lvgl.checkbox_set_disabled(cb, true) .
    if v.disabled then lvgl.checkbox_set_disabled(obj, true) end

    if v.align_to or v.align or v.align_x or v.align_y then
        lvgl.obj_align(obj, v.align_to, v.align or lvgl.ALIGN_CENTER,
                       v.align_x or 0, v.align_y or 0)
    end

    -- 该复选框的主要部分称为 lvgl.CHECKBOX_PART_BG 。
    -- 它是“项目符号”及其旁边的文本的容器。背景使用所有典型的背景样式属性。

    -- 项目符号是真正的 基础对象(lvgl.obj) ，可以用 lvgl.CHECKBOX_PART_BULLET 引用。
    -- 项目符号会自动继承背景状态。因此，背景被按下时，项目符号也会进入按下状态。
    -- 项目符号还使用所有典型的背景样式属性。

    -- 标签没有专用部分。因为文本样式属性始终是继承的，所以可以在背景样式中设置其样式。
    lvgl.obj_add_style(obj, lvgl.CHECKBOX_PART_BG,
                       create_style(v.style.bg))
    lvgl.obj_add_style(obj, lvgl.CHECKBOX_PART_BULLET,
                       create_style(v.style.bullet))
    lvgl.obj_align(obj, v.align_to, v.align or lvgl.ALIGN_CENTER,
                   v.align_x or 0, v.align_y or 0)
end
checkbox_data = {
    text = "测试多选框",
    style = {
        bg = {
            outline = {width = 0},
            border = {color = 0x0f0f0f, width = 0, opa = 30},
            shadow = {spread = 30, color = 0xff00f0},
            text = {font = style.font24, color = 0x00ff},
            pad = {all = 30},
            bg = {
                radius = 10,
                color = 0xff0ff0,
                opa = 150,
                grad = {color = 0x0f0f0f}
            }
        },
        bullet = {
            border = {color = 0x0f0f0f, width = 2, opa = 30},
            shadow = {spread = 30, color = 0xff00f0},
            margin = {right = 200},
            bg = {
                grad = {color = 0x0f0f0f},
                radius = 10,
                color = 0x0f0ff0,
                opa = 150

            }
        }
    }
}
-- create(lvgl.scr_act(), checkbox_data)
