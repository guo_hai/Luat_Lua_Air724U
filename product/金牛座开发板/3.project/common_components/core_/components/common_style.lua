module(..., package.seeall)

create_s = function(v, s)
    if not v then return s end
    -- for key, value in pairs(v) do print(key, value) end
    local state = v.state or lvgl.STATE_DEFAULT
    -- state = 0
    -- 背景
    if v.bg then s = set_bg(s, v.bg, state) end
    -- 边框
    if v.border then s = set_border(s, v.border, state) end
    -- 阴影
    if v.shadow then s = set_shadow(s, v.shadow, state) end
    -- 外框线
    if v.outline then s = set_outline(s, v.outline, state) end
    -- 内边距
    if v.pad then s = set_pad(s, v.pad, state) end
    -- 外边距
    if v.margin then s = set_margin(s, v.margin, state) end
    -- 主要对应在按键的文字样式，文字内容需要变量储存否则显示乱码
    if v.value then s = set_value(s, v.value, state) end
    -- 字体样式
    if v.text then s = set_text(s, v.text, state) end
    if v.line then s = set_line(s, v.line, state) end
    if v.img then s = set_img(s, v.img, state) end
    if v.transform then s = set_transform(s, v.transform, state) end
    return s
end


_G.create_style = function(v)
    -- 初始化样式
    local s = lvgl.style_t()
    lvgl.style_init(s)
    -- 参数为空，则使用默认样式
    if not v then return s end
    -- 参数包含已经创建好的样式，则直接返回
    if v.style then return v.style end
    -- 判断参数是数组还是表，表的话对应默认状态样式，数组的话对应不同状态的样式
    if v[1] == nil then
        return create_s(v, s)
    else
        for i = 1, #v do s = create_s(v[i], s) end
    end
    return s
end

_G.set_img = function(s, v, state)
    -- image_blend_mode ( lvgl.blend_mode_t )：设置图像的混合模式。
    -- 可以是 lvgl.BLEND_MODE_NORMAL/ADDITIVE/SUBTRACTIVE。
    -- 默认值：lvgl.BLEND_MODE_NORMAL。
    -- lvgl.style_set_image_blend_mode(s, state, lvgl.BLEND_MODE_NORMAL)
    if v.opa then lvgl.style_set_image_opa(s, state, v.opa) end
    if v.recolor then
        lvgl.style_set_image_recolor_opa(s, state, v.recolor_opa or 255)
        lvgl.style_set_image_recolor(s, state, lvgl.color_hex(v.recolor))
    end
    return s
end

_G.set_value = function(s, v, state)
    if v.align then lvgl.style_set_value_align(s, state, v.align) end
    if v.blend_mode then
        lvgl.style_set_value_blend_mode(s, state, v.blend_mode)
    end
    if v.color then
        lvgl.style_set_value_color(s, state, lvgl.color_hex(v.color))
    end
    if v.font then lvgl.style_set_value_font(s, state, v.font) end
    if v.line_space then
        lvgl.style_set_value_line_space(s, state, v.line_space)
    end
    if v.ofs_x then lvgl.style_set_value_ofs_x(s, state, v.ofs_x) end
    if v.ofs_y then lvgl.style_set_value_ofs_y(s, state, v.ofs_y) end
    if v.opa then lvgl.style_set_value_opa(s, state, v.opa) end
    local str = v.str
    if v.str then lvgl.style_set_value_str(s, state, str) end
    return s
end

_G.set_text = function(s, v, state)
    -- 设置文本选择的背景色。默认值：lvgl.COLOR_BLACK
    if v.sel_color then
        lvgl.style_set_text_sel_color(s, state, lvgl.color_hex(v.sel_color))
    end
    -- 设置文字颜色
    if v.color then
        lvgl.style_set_text_color(s, state, lvgl.color_hex(v.color))
    end
    -- 设置文字透明度
    if v.opa then lvgl.style_set_text_opa(s, state, v.opa) end

    -- 文本的字母空间。默认值：0 
    if v.letter_space then
        lvgl.style_set_text_letter_space(s, state, v.letter_space)
    end
    -- 文本的行距。默认值：0 
    if v.line_space then
        lvgl.style_set_text_line_space(s, state, v.line_space)
    end
    -- 添加文字修饰。可以是lvgl.TEXT_DECOR_NONE/UNDERLINE/STRIKETHROUGH 
    -- 默认值：lvgl.TEXT_DECOR_NONE
    if v.decor then lvgl.style_set_text_decor(s, state, v.decor) end
    -- 设置字体
    if v.font then lvgl.style_set_text_font(s, state, v.font) end
    -- 设置文本的混合模式。
    -- 可以lvgl.BLEND_MODE_NORMAL/ADDITIVE/SUBTRACTIVE 
    -- 默认值：lvgl.BLEND_MODE_NORMAL。
    -- text_blend_mode (lvgl.blend_mode_t )
    return s
end
_G.set_shadow = function(s, v, state)
    -- 设置轮廓的宽度(模糊大小)。默认值：0。
    lvgl.style_set_shadow_width(s, state, v.width or 0)
    -- 指定阴影的颜色。默认值： lvgl.COLOR_BLACK。
    lvgl.style_set_shadow_color(s, state, lvgl.color_hex(v.color or 0))
    -- 在每个方向上使阴影大于背景的值达到此值。默认值：0。
    lvgl.style_set_shadow_spread(s, state, v.spread or 0)
    -- 设置阴影的X偏移量。默认值：0。
    lvgl.style_set_shadow_ofs_x(s, state, v.ofs_x or 0)
    -- 设置阴影的Y偏移量。默认值：0。
    lvgl.style_set_shadow_ofs_y(s, state, v.ofs_x or 0)
    -- 指定阴影的不透明度。默认值：lvgl.OPA_TRANSP。
    lvgl.style_set_shadow_opa(s, state, v.opa or 80)
    -- 设置阴影的混合模式。可以是 lvgl.BLEND_MODE_NORMAL/ADDITIVE:叠加/SUBTRACTIVE:相减.默认值：lvgl.BLEND_MODE_NOR
    -- lvgl.style_set_shadow_blend_mode(s, state, lvgl.SUBTRACTIVE)
    return s
end

_G.set_bg_grad = function(s, v, state)
    lvgl.style_set_bg_grad_color(s, state,
                                 lvgl.color_hex(v.bg_grad_color or 0xFFFFFF))
    lvgl.style_set_bg_grad_dir(s, state, v.bg_grad_dir or lvgl.GRAD_DIR_VER)
    --    /*Shift the gradient to the bottom*/
    if v.main then lvgl.style_set_bg_main_stop(s, state, v.main) end
    if v.grad then lvgl.style_set_bg_grad_stop(s, state, v.grad) end
    return s
end

_G.set_bg = function(s, v, state)

    if v.W and v.H then lvgl.style_set_size(s, state, v.W, v.H) end
    -- 背景颜色
    if v.color then
        lvgl.style_set_bg_color(s, state, lvgl.color_hex(v.color))
    end
    -- 背景透明度
    if v.opa then lvgl.style_set_bg_opa(s, state, v.opa) end
    if v.grad then s = set_bg_grad(s, v.grad, state) end
    -- 设置圆角
    if v.radius then lvgl.style_set_radius(s, state, v.radius) end

    -- 为true可以将溢出的内容剪切到圆角(半径> 0)上。默认值：false。
    lvgl.style_set_clip_corner(s, state, true)

    return s
end

_G.set_transform = function(s, v, state)

    if v.angle then lvgl.style_set_transform_angle(s, state, v.angle) end
    if v.height then lvgl.style_set_transform_height(s, state, v.height) end
    if v.width then lvgl.style_set_transform_width(s, state, v.width) end
    if v.zoom then lvgl.style_set_transform_zoom(s, state, v.zoom) end
    return s
end

_G.set_pad = function(s, v, state)
    if v.all then
        lvgl.style_set_pad_all(s, state, v.all)
        return s
    end
    -- 内边距
    if v.left then lvgl.style_set_pad_left(s, state, v.left) end
    if v.right then lvgl.style_set_pad_right(s, state, v.right) end
    if v.top then lvgl.style_set_pad_top(s, state, v.top) end
    if v.bottom then lvgl.style_set_pad_bottom(s, state, v.bottom) end
    if v.inner then lvgl.style_set_pad_inner(s, state, v.inner) end
    return s
end

_G.set_margin = function(s, v, state)
    -- 外边距
    if v.all then
        lvgl.style_set_margin_all(s, state, v.all)
        return s
    end
    lvgl.style_set_margin_left(s, state, v.left or 0)
    lvgl.style_set_margin_right(s, state, v.right or 0)
    lvgl.style_set_margin_top(s, state, v.top or 0)
    lvgl.style_set_margin_bottom(s, state, v.bottom or 0)
    return s
end

_G.set_border = function(s, v, state)
    -- 边框颜色
    lvgl.style_set_border_color(s, state, lvgl.color_hex(v.color or 0))
    -- 边框宽度
    lvgl.style_set_border_width(s, state, v.width or 0)
    -- 边框透明度
    lvgl.style_set_border_opa(s, state, v.opa or 255)
    -- 指定要绘制边框的哪一侧。可以是 lvgl.BORDER_SIDE_NONE/LEFT/RIGHT/TOP/BOTTOM/FULL 
    -- 。ORed值也是可能的。默认值： lvgl.BORDER_SIDE_FULL 。
    lvgl.style_set_border_side(s, state, v.side or lvgl.BORDER_SIDE_FULL)
    -- 如果true在绘制完所有子级之后绘制边框。默认值：false
    lvgl.style_set_border_post(s, state, v.post or false)
    return s
end

_G.set_outline = function(s, v, state)
    -- /*Add outline*/
    lvgl.style_set_outline_width(s, state, v.width or 2)
    lvgl.style_set_outline_opa(s, state, v.opa or 255)
    lvgl.style_set_outline_color(s, state, lvgl.color_hex(v.color or 0xFF0000))
    lvgl.style_set_outline_pad(s, state, v.pad or 8)
    return s
end

_G.set_line = function(s, v, state)
    -- line_color ( lvgl.color_t )：线条的颜色。默认值：lvgl.COLOR_BLACK
    -- line_opa ( lvgl.opa_t )：直线的不透明度。默认值：lvgl.OPA_COVER
    -- line_width ( lvgl.style_int_t )：线的宽度。默认值：0。
    -- line_dash_width ( lvgl.style_int_t )：破折号的宽度。仅对水平或垂直线绘制虚线。0：禁用破折号。默认值：0。
    -- line_dash_gap ( lvgl.style_int_t )：两条虚线之间的间隙。仅对水平或垂直线绘制虚线。0：禁用破折号。默认值：0。
    -- line_rounded ( bool )：true：绘制圆角的线尾。默认值：false。
    -- line_blend_mode ( lvgl.blend_mode_t )：设置线条的混合模式。可以是 lvgl.BLEND_MODE_NORMAL/ADDITIVE/SUBTRACTIVE。默认值：lvgl.BLEND_MODE_NORMAL。

    lvgl.style_set_line_dash_gap(s, state, v.gap or 0)

    lvgl.style_set_line_color(s, state, lvgl.color_hex(v.color or 0xFFFFFF))
    lvgl.style_set_line_width(s, state, v.width or 5)

    lvgl.style_set_line_opa(s, state, v.opa or 255)
    -- 端点圆角
    lvgl.style_set_line_rounded(s, state, v.rounded)
    return s
end
