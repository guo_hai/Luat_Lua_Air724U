module(..., package.seeall)

function tab_view_cb(obj, event)
    print(obj, event)
    if (event == lvgl.KEY_RIGHT) then log.info("table", event) end
end

function create(p, v)
    -- 创建一个 TabView
    obj = lvgl.tabview_create(p, nil)
    -- 设置 TabView 的显示大小
    lvgl.obj_set_size(obj, v.W or 440, v.H or 200)

    -- 默认情况下，选项卡选择器按钮位于“选项卡”视图的顶部。
    -- 可以使用 lvgl.tabview_set_btns_pos(tabview, lvgl.TABVIEW_TAB_POS_TOP/BOTTOM/LEFT/RIGHT/NONE) 进行更改
    -- lvgl.TABVIEW_TAB_POS_NONE 将隐藏选项卡。
    lvgl.tabview_set_btns_pos(obj, v.btn_pos or lvgl.TABVIEW_TAB_POS_BOTTOM)
    -- 动画时间由 lvgl.tabview_set_anim_time(tabview, anim_time_ms) 调整。加载新选项卡时使用。
    if v.time then lvgl.tabview_set_anim_time(obj, v.time) end
    lvgl.obj_set_event_cb(obj, v.cb or tab_view_cb)
    -- 设置 TabView 的位置
    lvgl.obj_align(obj, v.align_to, v.align or lvgl.ALIGN_CENTER,
                   v.align_x or 0, v.align_y or 0)

    -- 添加样式
    if v.style then
        -- Tab视图对象包含几个部分。主要是 lvgl.TABVIEW_PART_BG 。
        -- 它是一个矩形容器，用于容纳Tab视图的其他部分。
        -- 在背景上创建了2个重要的实际部分：
        -- lvgl.TABVIEW_PART_BG_SCRL 这是 页面(lvgl.page) 的可滚动部分。它使选项卡的内容彼此相邻。
        -- 页面的背景始终是透明的，不能从外部访问。
        -- lvgl.TABVIEW_PART_TAB_BG 选项卡按钮是一个 按钮矩阵(lvgl.btnmatrix) 。
        -- 单击按钮将 lvgl.TABVIEW_PART_BG_SCRL 滚动到相关选项卡的内容。
        -- 可以通过 lvgl.TABVIEW_PART_TAB_BTN 访问选项卡按钮。选择选项卡时，按钮处于选中状态，
        -- 可以使用 lvgl.STATE_CHECKED 设置样式。
        -- 选项卡的按钮矩阵的高度是根据字体高度加上背景和按钮样式的填充来计算的。
        -- 列出的所有部分均支持典型的背景样式属性和填充。
        -- lvgl.TABVIEW_PART_TAB_BG 还有一个实际部分，即一个指标，称为 lvgl.TABVIEW_PART_INDIC 。
        -- 它是当前选定选项卡下的一个类似矩形的细对象。当选项卡视图是动画到其它选项卡中的指示器也将被动画。
        -- 它可以是使用典型背景样式属性的样式。 size样式属性将设置其厚度。
        -- 添加新选项卡后，将在 lvgl.TABVIEW_PART_BG_SCRL 上为其创建一个页面，
        -- 并将新按钮添加到lvgl.TABVIEW_PART_TAB_BG 按钮矩阵。
        -- 创建的页面可以用作普通页面，它们具有通常的页面部分。
        lvgl.obj_add_style(obj, lvgl.TABVIEW_PART_BG, create_style(v.style.bg))
        lvgl.obj_add_style(obj, lvgl.TABVIEW_PART_BG_SCROLLABLE,
                           create_style(v.style.scrollable))
        lvgl.obj_add_style(obj, lvgl.TABVIEW_PART_INDIC,
                           create_style(v.style.indic))
        lvgl.obj_add_style(obj, lvgl.TABVIEW_PART_TAB_BG,
                           create_style(v.style.tab))
        lvgl.obj_add_style(obj, lvgl.TABVIEW_PART_TAB_BTN,
                           create_style(v.style.btn))
    end

    -- 添加页面
    local page_list = {}
    if v.page_list then
        for i = 1, #v.page_list do
            page_list[i] = lvgl.tabview_add_tab(obj, v.page_list[i])
            if v.style.page then
                lvgl.obj_add_style(page_list[i], lvgl.PAGE_PART_BG,
                                   create_style(v.style.page.bg))
                lvgl.obj_add_style(page_list[i], lvgl.PAGE_PART_EDGE_FLASH,
                                   create_style(v.style.page.edge_flash))
                lvgl.obj_add_style(page_list[i], lvgl.PAGE_PART_SCROLLABLE,
                                   create_style(v.style.page.lable))
                lvgl.obj_add_style(page_list[i], lvgl.PAGE_PART_SCROLLBAR,
                                   create_style(v.style.page.bar))
            end
        end
    end
    return {obj = obj, page_list = page_list}
end

local tab_view_data = {
    W = LCD_W,
    H = LCD_H,
    time = 100,
    btn_pos = lvgl.TABVIEW_TAB_POS_NONE,
    page_list = {"电影", "电视剧", "电视剧", "电视剧", "电视剧"},
    style = {
        bg = {bg = {radius = 0, color = 0x00ffff, opa = 150}},
        scrollable = {bg = {radius = 10, color = 0x0f0ff0, opa = 255}},
        indic = {bg = {radius = 10, color = 0x00ff00, opa = 255}},
        tab = {bg = {radius = 10, color = 0x0f0ff0, opa = 255}, pad = {all = 0}},
        page = {
            bg = {bg = {color = 0xff0000}, pad = {all = 0}},
            lable = {bg = {color = 0x00ff00}},
            edge_flash = {bg = {color = 0x0000ff}},
            bar = {bg = {W = 10, H = 10, color = 0}}

        },

        btn = {
            pad = {all = 3}, -- 按钮的内边距
            text = {font = style.font24}, -- 按钮的文本样式
            bg = {radius = 10, opa = 255, color = 0xff0000} -- 按钮的背景样式
        }
    }
}
-- local tab_view_obj = create(lvgl.scr_act(), tab_view_data)

-- local tab_view_obj = tab_view.create(lvgl.scr_act(), tab_view_data)

-- sys.taskInit(function()
--     local oo = 0
--     while true do
--         sys.wait(1000)
--         oo = oo + 1
--         lvgl.tabview_set_tab_act(obj, (oo%5), lvgl.ANIM_ON)
--     end
-- end)
