module(..., package.seeall)

function create(p, v)
    local obj = lvgl.page_create(p, nil)
    if not v then return obj end
    if not v.click then lvgl.obj_set_click(obj, false) end
    lvgl.obj_set_size(obj, v.W or 300, v.H or 200)

    -- 可以根据以下四个策略显示滚动条：
    -- lvgl.SCROLLBAR_MODE_OFF 一直都不显示滚动条
    -- lvgl.SCROLLBAR_MODE_ON 一直都显示滚动条
    -- lvgl.SCROLLBAR_MODE_DRAG 拖动页面时显示滚动条
    -- lvgl.SCROLLBAR_MODE_AUTO 当可滚动容器的大小足以滚动时显示滚动条
    -- lvgl.SCROLLBAR_MODE_HIDE 暂时隐藏滚动条
    -- lvgl.SCROLLBAR_MODE_UNHIDE 取消隐藏以前隐藏的滚动条。也恢复原始模式

    -- 可以通过以下方式更改滚动条显示策略： lvgl.page_set_scrollbar_mode(page, SB_MODE) 。
    -- 默认值为 lvgl.SCROLLBAR_MODE_AUTO 。
    if v.mode then lvgl.page_set_scrollbar_mode(obj, v.mode) end

    -- 页面的主要部分称为 lvgl.PAGE_PART_BG ，它是页面的背景。
    -- 它使用所有典型的背景样式属性。使用填充会增加侧面的空间。
    -- lvgl.LIST_PART_SCROLLBAR 是绘制滚动条的背景的虚拟部分。
    -- 使用所有典型的背景样式属性，使用size设置滚动条的宽度，并使用pad_right和pad_bottom设置间距。
    -- lvgl.LIST_PART_EDGE_FLASH 还是背景的虚拟部分，当无法进一步沿该方向滚动列表时，
    -- 将在侧面绘制半圆。使用所有典型的背景属性。
    -- 可以通过 lvgl.PAGE_PART_SCROLL 部分引用可滚动对象。
    -- 它还使用所有典型的背景样式属性和填充来增加侧面的空间。

    -- PAGE_PART_BG
    -- PAGE_PART_EDGE_FLASH
    -- PAGE_PART_SCROLLABLE
    -- PAGE_PART_SCROLLBAR

    if v.style then
        lvgl.obj_add_style(obj, lvgl.PAGE_PART_BG, create_style(v.style.bg))
        lvgl.obj_add_style(obj, lvgl.PAGE_PART_EDGE_FLASH,
                           create_style(v.style.edge_flash))
        lvgl.obj_add_style(obj, lvgl.PAGE_PART_SCROLLABLE,
                           create_style(v.style.lable))
        lvgl.obj_add_style(obj, lvgl.PAGE_PART_SCROLLBAR,
                           create_style(v.style.bar))
    end
    lvgl.obj_align(obj, v.align_to, v.align or lvgl.ALIGN_CENTER,
                   v.align_x or 0, v.align_y or 0)
    return obj
end
local page_data = {
    W = LCD_W,
    H = LCD_H,
    mode = lvgl.SCROLLBAR_MODE_OFF,
    page_glue = true,
    style = {
        bg = {bg = {color = 0xff0000}, pad = {all = 0}},
        lable = {bg = {color = 0x00ff00}},
        edge_flash = {bg = {color = 0x0000ff}},
        bar = {bg = {W = 10, H = 10, color = 0}}
    }
}

-- local test_page = create(lvgl.scr_act(), page_data)

local cont_data = {
    W = LCD_W,
    H = LCD_H,
    click = true,
    page_glue = true,
    align = lvgl.ALIGN_IN_TOP_LEFT,
    style = {
        border = {color = 0x0f0f0f, width = 0, opa = 30},
        shadow = {spread = 30, color = 0xff00f0},
        bg = {radius = 0, color = 0, opa = 250},
        margin = {all = 0}
    }
}

-- local cont_obj = cont.create(test_page, cont_data)

local tab_view_data = {
    page_list = {"电影", "电视剧", "电视剧", "电视剧", "电视剧"},
    style = {
        bg = {bg = {radius = 10, color = 0x00ffff, opa = 150}},
        scrollable = {bg = {radius = 10, color = 0x0f0ff0, opa = 255}},
        indic = {bg = {radius = 10, color = 0x00ff00, opa = 255}},
        tab = {bg = {radius = 10, color = 0x0f0ff0, opa = 255}, pad = {all = 0}},

        btn = {
            pad = {all = 3}, -- 按钮的内边距
            text = {font = style.font48}, -- 按钮的文本样式
            bg = {radius = 10, opa = 255, color = 0xff0000} -- 按钮的背景样式
        }
    }
}
-- tab_view.create(test_page, tab_view_data)

