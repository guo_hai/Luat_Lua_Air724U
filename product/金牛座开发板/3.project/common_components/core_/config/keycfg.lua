module(..., package.seeall)

local keyMap = {{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}}
keyMap[0] = {}
keyMap[255] = {}
keyMap[255][255] = "PWR"
keyMap[2][2] = "OK"
keyMap[3][2] = "UP"
keyMap[5][2] = "DOWN"
keyMap[4][2] = "LEFT"
keyMap[1][2] = "RIGHT"
keyMap[3][0] = "A"
keyMap[4][1] = "B"
keyMap[3][1] = "C"
keyMap[4][0] = "D"
keyMap[2][0] = "1"
keyMap[2][1] = "2"
keyMap[1][0] = "3"
keyMap[1][1] = "4"
keyMap[0][6] = "5"

local pwrTimer

-- 注册按键消息的处理函数
rtos.on(rtos.MSG_KEYPAD, function(msg)
    -- log.info("keyMsg",msg.key_matrix_row,msg.key_matrix_col,msg.pressed)
    -- print(key_matrix_row, key_matrix_col, pressed)
    -- print(keyMap[msg.key_matrix_row][msg.key_matrix_col], msg.pressed)
    sys.publish("KEY_EVENT", keyMap[msg.key_matrix_row][msg.key_matrix_col],
                msg.pressed)

end)
-- 初始化键盘阵列
rtos.init_module(rtos.MOD_KEYPAD, 0, 0x7F, 0x7F)

function getC()
    pmd.ldoset(7, pmd.LDO_VBACKLIGHT_R)
    pmd.ldoset(0, pmd.LDO_VBACKLIGHT_R)
    local _, d = adc.read(2)
    return d < 2450
end
