module(..., package.seeall)

font16 = lvgl.font_load(spi.SPI_1, 16, 2)
font24 = lvgl.font_load(spi.SPI_1, 24, 2)
font48 = lvgl.font_load(spi.SPI_1, 48, 2)
font68 = lvgl.font_load(spi.SPI_1, 68, 2)
font96 = lvgl.font_load(spi.SPI_1, 96, 1)

-- font8 = lvgl.font_load("/sdcard0/OPPOSans-H-8.bin")
-- font16 = lvgl.font_load("/sdcard0/OPPOSans-H-16.bin")
-- font20 = lvgl.font_load("/lua/opposans_b_20.bin")
-- font48 = lvgl.font_load("/sdcard0/OPPOSans-H-24.bin")
-- font50 = lvgl.font_load("/lua/opposans_b_50.bin")
-- font96 = lvgl.font_load("/sdcard0/OPPOSans-H-24.bin")

