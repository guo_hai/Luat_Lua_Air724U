module(..., package.seeall)

local songs = {}
local function cbFnc(result, prompt, head, body)
    log.info("testHttp.cbFnc result", result)
    log.info("testHttp.cbFnc prompt", prompt)
    if result and head then
        for k, v in pairs(head) do
            log.info("testHttp.cbFnc head", k .. ": " .. v)
        end
    end
    if result and body then
        print(body)
        s = json.decode(body)
        if s.code == 200 then
            songs = s.songs
            sys.publish("MUSIC_LIST")
        else
            playList.create("512381617")
        end

    end
end

-- 生成歌曲列表
function create(id)
    sys.taskInit(function()
        loadPage.create()
        print("...............")
        http.request("GET", URL .. "/playlist/track/all?id=" .. id, nil, nil,
                     nil, nil, cbFnc)
        local result = sys.waitUntil("MUSIC_LIST")
        if not result then return end
        lvgl.obj_clean(PARENT_CONT)
        create_s()

        local cont_data = {
            W = LCD_W,
            H = LCD_H/8,
            click = true,
            event = function(obj, event)
                if event == lvgl.EVENT_CLICKED then
                    print("按键默认回调")
                    playList.create("512381617")
                end
            end,
            style = {
                border = {color = 0x0f0f0f, width = 2, opa = 30},
                shadow = {spread = 30, color = 0xff00f0},
                bg = {
                    radius = 10,
                    color = 0xff00ff,
                    opa = 250,
                    grad = {color = 0x0f0f0f}
                }
            }
        }
        local cont_obj = cont.create(lvgl.scr_act(), cont_data)

    end)
end

local list_data = {
    W = LCD_W,
    H = LCD_H * 0.8,
    icon = {
        lvgl.SYMBOL_NEW_LINE, lvgl.SYMBOL_DIRECTORY, lvgl.SYMBOL_CLOSE,
        lvgl.SYMBOL_EDIT, lvgl.SYMBOL_USB, lvgl.SYMBOL_GPS, lvgl.SYMBOL_STOP,
        lvgl.SYMBOL_VIDEO
    },

    style = {
        bg = {bg = {color = 0xff0000}},
        lable = {bg = {color = 0x0000ff}},
        edge_flash = {bg = {color = 0x0000ff}},
        bar = {bg = {color = 0xcccccc}}
    }
}
btn = {bg = {color = 0xff}, text = {font = style.font24, color = 0xff0000}}

function create_s()
    if songs then
        local songs_list = list.create(PARENT_CONT, list_data)
        local btn_list = {}
        for k, v in pairs(songs) do

            btn_list[k] = lvgl.list_add_btn(songs_list.obj, nil, v.name)
            lvgl.obj_set_event_cb(btn_list[k], function(o, e)
                if e == lvgl.EVENT_CLICKED then print(v.name) end
            end)
            -- btn.bg.color = btn.bg.color + 0x50
            lvgl.obj_add_style(btn_list[k], lvgl.BTN_PART_MAIN,
                               create_style(btn))

        end
    end
end
