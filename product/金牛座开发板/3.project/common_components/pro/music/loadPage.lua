module(..., package.seeall)
local spinner_data = {
    W = LCD_H,
    H = LCD_H,
    deg = 60,
    time = 3000,
    style = {
        bg = {
            border = {color = 0xff0f0f, width = 10, opa = 30},
            bg = {radius = 50, color = 0, opa = 200, grad = {color = 0x0f0fff}},
            line = {color = 0x00ff00, width = 30, opa = 200, rounded = false}
        },
        indic = {
            bg = {radius = 10, color = 0x00ff00, grad = {color = 0x0f0f0f}},
            line = {color = 0xff0000, width = 30, opa = 200, rounded = true}
        }
    }
}

function create()
    lvgl.obj_clean(PARENT_CONT)
    spinner.create(PARENT_CONT, spinner_data)
end
