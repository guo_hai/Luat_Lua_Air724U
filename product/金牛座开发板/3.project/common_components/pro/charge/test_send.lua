module(..., package.seeall)

data_connect = function(data)
    return string.char(0xFE, 0xFE, 0xFE, 0xFE, 0x68) .. data.gun_id .. string.char(0x68) .. data.control ..
               string.char(data.data_len) .. data.identifier .. data.data .. testUart1.checkSum(data) ..
               string.char(0x16)
end
-- 枪列表
gun_list = {
    A = string.char(0x01, 0x01, 0x27, 0x10, 0x10, 0x22),
    B = string.char(0x01, 0x01, 0x27, 0x10, 0x10, 0x22)
}

get_data = function(t_l)
    local data = t_l.data
    data = string.sub33(data)
    data = tonumber(data:toHex() or 0) / 10
    print("数值", data, t_l.gun_id:toHex())
    print("类型", t_l.identifier:toHex())
    sys.publish("UART_DATA_ACK", {
        data = data,
        gun = t_l.gun_id
    })
end

-- 控制码列表
ask_list = {
    read = {
        data = string.char(0x11),
        list = {
            vol = string.char(0x33, 0x34, 0x34, 0x35), -- 电压
            electric = string.char(0x33, 0x34, 0x35, 0x35), -- 电流
            power = string.char(0x33, 0x33, 0x33, 0x33) -- 功率
        }
    },
    control = {
        data = string.char(0x1C),
        list = {
            close = string.char(0x35, 0x89, 0x67, 0x45, 0x77, 0x66, 0x55, 0x44, 0x4D, 0x34, 0x8C, 0x8C, 0x56, 0x64,
                0x45, 0xCC, 0x44), -- 断开
            open = string.char(0x35, 0x89, 0x67, 0x45, 0x77, 0x66, 0x55, 0x44, 0x4F, 0x34, 0x8C, 0x8C, 0x56, 0x64, 0x45,
                0xCC, 0x44) -- 合闸
        }
    }, -- 请求
    find = {
        data = string.char(0x13),
        list = {}
    } -- 查找
}

function send_data(values)
    local t_l = {
        control = "", -- 控制类型
        data_len = "", -- 数据长度
        identifier = "" -- 数据标识编码
    }
    t_l.gun_id = gun_list[values.id or "A"]

    for k, v in pairs(ask_list) do
        for k1, v1 in pairs(v.list) do
            if k1 == values.index then
                t_l.control = v.data
                t_l.data_len = #v1
                t_l.identifier = v1
                break
            end
        end
    end

    testUart1.loop_print(t_l)

    -- local temp = control_ask_list[v.control or "read"]
    -- t_l.control = temp.data
    -- for k, v in pairs(temp.list) do
    --     for k1, v1 in pairs(v) do
    --         if v.index == k1 then
    --             t_l.identifier = v1
    --             t_l.data_len = #v1
    --             break
    --         end
    --     end
    -- end
    if t_l.identifier then
        func(t_l)
    end
end

function func(v)
    v = v or {}
    local data = {
        gun_id = v.gun_id or "", -- 枪ID
        control = v.control or "", -- 控制类型
        data_len = v.data_len or 0, -- 数据长度
        identifier = v.identifier or "", -- 数据标识编码
        data = v.data or "", -- 数据
        check_sum = v.check_sum or "" -- 校验和
    }
    local ss = data_connect(data)
    print(ss:toHex())
    uart.write(1, ss)
end

sys.subscribe("UART_PARSE_DATA", function(data)
    testUart1.loop_print(data)
    get_data(data)
    print("UART_PARSE_DATA")
end)

-- sys.taskInit(function()
--     index_list = {"vol", "electric", "power"}
--     while true do
--         print("read_data")
--         for k, v in pairs(index_list) do
--             print("read_data")
--             read_data({
--                 id = "A",
--                 index = v
--             })
--             sys.wait(500)
--         end
--         sys.wait(5000)

--     end
-- end)
