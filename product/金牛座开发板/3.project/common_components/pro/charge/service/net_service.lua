module(..., package.seeall)

require "socketTask"
require "misc"

serverConnect=function()
    --连接服务器
    log.info("连接服务器")
    while not socketTask.isReady() do
        sys.wait(5)
    end
    return true
end

updateAtr=function()
    local imei=misc.getImei()
    local csq=net.getRssi()
    log.info("上传IMEI CSQ",imei,csq)
    socketOutMsg.send(imei.."_"..csq)
    return true
end

getData=function()
    --获取时间费率
    local time=0
    local money =0
    socketOutMsg.send("请求时间费率")
    local topic="SERVER_TIME_MONEY"
    
    _,d=sys.waitUntil(topic,120000)
    if _ then
        print("获取的时间费率",d)
    end
    log.info("获取时间费率",time,money)
    return time,money
end

charge=function()
--TODO 具体的两把枪
--以下是模拟
    local gun=gun_service.gun
    gun.A.power=true
    gun.B.power=true
end
sys.subscribe("CHARGE",charge)

function SERVER(data) --刷卡或者服务器下发指令
    if data=="TimeAndMoney" then
        sys.publish("SERVER_TIME_MONEY",data)
    elseif data=="charge"  then
        sys.publish("CHARGE",data)
    else
        print("收到未定义消息",data)
    end
    
end

sys.subscribe("SERVER_DOWNLOAD",SERVER)