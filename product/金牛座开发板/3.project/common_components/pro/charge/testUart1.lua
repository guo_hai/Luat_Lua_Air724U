--- 模块功能：串口1功能测试
-- @author openLuat
-- @module uart.testUart
-- @license MIT
-- @copyright openLuat
-- @release 2018.03.27
module(..., package.seeall)

require "utils"
require "pm"

-- 帧起始符 = 0x68
local FRAME_START = 0x68


sys.subscribe("UART_DATA_ACK", function(data)

end)





-- 数据标识编码列表
identifier_ask_list = {
    close = string.char(0x35, 0x89, 0x67, 0x45, 0x77, 0x66, 0x55, 0x44, 0x4D, 0x34, 0x8C, 0x8C, 0x56, 0x64, 0x45, 0xCC,
        0x44), -- 断开
    open = string.char(0x35, 0x89, 0x67, 0x45, 0x77, 0x66, 0x55, 0x44, 0x4F, 0x34, 0x8C, 0x8C, 0x56, 0x64, 0x45, 0xCC,
        0x44) -- 合闸
}

control_ack_list = {
    read = string.char(0x91),
    control = string.char(0x9C), -- 应答
    find = string.char(0x99) -- 查找
}

-- 串口ID,1对应uart1
-- 如果要修改为uart2，把UART_ID赋值为2即可
local UART_ID = 1
-- 串口读到的数据缓冲区
local rdbuf = ""

-- FE FE FE FE 68 01 00 00 00 00 00 68 91 08 33 33 33 33 33 33 33 33 74 16

sys.subscribe("uart_rx", function(data)

    local t_l = {
        gun_id = "", -- 枪ID
        control = "", -- 控制类型
        data_len = "", -- 数据长度
        identifier = "", -- 数据标识编码
        data = "", -- 数据
        check_sum = "" -- 校验和
    }

    -- 去除帧头
    local index = string.find(data, string.char(FRAME_START))
    if index then
        -- +1除去帧头  -2除去帧尾
        data = string.sub(data, index + 1, -2)
    else
        return
    end

    index = string.find(data, string.char(FRAME_START))
    t_l.gun_id = string.sub(data, 1, index - 1)
    data = string.sub(data, index + 1)
    t_l.control = string.sub(data, 1, 1)
    t_l.data_len = string.byte(string.sub(data, 2, 2))
    if t_l.data_len ~= 0 then
        -- 数据长度不为0
        t_l.identifier = string.sub(data, 3, 6)
        if t_l.data_len > 4 then
            t_l.data = string.sub(data, 7, t_l.data_len + 7 - 4 - 1) -- -4是标识符，-1是数组尾前移
        end
        t_l.check_sum = string.sub(data, #data)
    end

    -- loop_print(t_l)
    -- 判断校验和，如果不对，则不处理
    if checkSum(t_l) ~= t_l.check_sum then
        return
    end
    sys.publish("UART_PARSE_DATA", t_l)

end)

function string.sub33(str)
    return str:gsub('.', function(c)
        return string.char(string.byte(c) - 0x33)
    end)
end

-- 检查控制码
function check_type(data)
    local index = false
    for k, v in pairs(control_ack_list) do
        print(k, v:toHex())
        if v == data.control then
            index = true
            break
        end
    end
    return index
end

-- 计算字符串累加和，溢出范围为0x00-0xFF
string_sum = function(str, range)
    local sum = 0
    for i = 1, #str do
        sum = sum + string.byte(str, i)
        if range then
            sum = bit.band(sum, range)
        end
    end
    return sum
end

-- 校验累加和
function checkSum(data)
    local sum = string_sum(data.gun_id, 0xFF) + string.byte(data.control) + data.data_len +
                    string_sum(data.identifier, 0xFF) + string_sum(data.data, 0xFF) + FRAME_START * 2
    sum = bit.band(sum, 0xff)
    return string.char(sum)
end
-- 循环打印解析后的数据
function loop_print(data)
    for k, v in pairs(data) do
        if v then
            if k == "data_len" then
                print(k, v)
            else
                print(k, v:toHex())
            end
        end
    end
end

--[[
函数名：parse
功能  ：按照帧结构解析处理一条完整的帧数据
参数  ：
        data：所有未处理的数据
返回值：第一个返回值是一条完整帧报文的处理结果，第二个返回值是未处理的数据
]]
local function parse(data)
    if not data then
        return
    end

    local tail = string.find(data, string.char(0x16))
    if not tail then
        return false, data
    end
    sys.publish("uart_rx", rdbuf)
    return true, string.sub(data, tail + 1, -1)
end

--[[
函数名：proc
功能  ：处理从串口读到的数据
参数  ：
        data：当前一次从串口读到的数据
返回值：无
]]
local function proc(data)
    if not data or string.len(data) == 0 then
        return
    end
    -- 追加到缓冲区
    rdbuf = rdbuf .. data

    local result, unproc
    unproc = rdbuf
    -- 根据帧结构循环解析未处理过的数据
    while true do
        result, unproc = parse(unproc)
        if not unproc or unproc == "" or not result then
            break
        end
    end

    rdbuf = unproc or ""
end

--[[
函数名：read
功能  ：读取串口接收到的数据
参数  ：无
返回值：无
]]
local function read()
    local data = ""
    -- 底层core中，串口收到数据时：
    -- 如果接收缓冲区为空，则会以中断方式通知Lua脚本收到了新数据；
    -- 如果接收缓冲器不为空，则不会通知Lua脚本
    -- 所以Lua脚本中收到中断读串口数据时，每次都要把接收缓冲区中的数据全部读出，这样才能保证底层core中的新数据中断上来，此read函数中的while语句中就保证了这一点
    while true do
        data = uart.read(UART_ID, "*l")
        if not data or string.len(data) == 0 then
            break
        end
        -- -- 打开下面的打印会耗时
        -- log.info("testUart.read bin", data)
        -- log.info("testUart.read hex", data:toHex())
        proc(data)
    end
end

--[[
函数名：write
功能  ：通过串口发送数据
参数  ：
        s：要发送的数据
返回值：无
]]
function write(s)
    log.info("testUart.write", s)
    uart.write(UART_ID, s .. "\r\n")
end

local function writeOk()
    log.info("testUart.writeOk")
end

-- 保持系统处于唤醒状态，此处只是为了测试需要，所以此模块没有地方调用pm.sleep("testUart")休眠，不会进入低功耗休眠状态
-- 在开发“要求功耗低”的项目时，一定要想办法保证pm.wake("testUart")后，在不需要串口时调用pm.sleep("testUart")
pm.wake("testUart")
-- 注册串口的数据接收函数，串口收到数据后，会以中断方式，调用read接口读取数据
uart.on(UART_ID, "receive", read)
-- 注册串口的数据发送通知函数
uart.on(UART_ID, "sent", writeOk)

-- 配置并且打开串口
-- uart.setup(UART_ID, 115200, 8, uart.PAR_NONE, uart.STOP_1, nil, 1)
uart.setup(UART_ID, 2400, 8, uart.PAR_EVEN, uart.STOP_1, nil, 1)
-- 如果需要打开“串口发送数据完成后，通过异步消息通知”的功能，则使用下面的这行setup，注释掉上面的一行setup
-- uart.setup(UART_ID,115200,8,uart.PAR_NONE,uart.STOP_1,nil,1)
uart.set_rs485_oe(UART_ID, pio.P0_18)
