module(..., package.seeall)

home_cont = cont.create(lvgl.scr_act(), {
    W = LCD_W,
    H = LCD_H,
    click = false,
    style = {border = {width = 0}, bg = {color = 0xffcccc}}
})
local gauge_data = {
    R = 240,
    -- align = lvgl.ALIGN_CENTER,
    align_x = 0,
    align_y = 0,
    critical_value = 100,
    style = {
        bg = {
            pad = {all = 50},
            border = {color = 0x00ff0f, width = 2, opa = 200},
            shadow = {spread = 30, color = 0xff00f0},
            bg = {
                radius = 10,
                color = 0x0f0ff0,
                opa = 150,
                grad = {color = 0x0f0f0f}
            }
        },
        major = {
            pad = {all = 200},
            border = {color = 0x00ff0f, width = 2, opa = 200},
            shadow = {spread = 30, color = 0xff00f0},
            bg = {
                radius = 10,
                color = 0x0f0ff0,
                opa = 150,
                grad = {color = 0x0f0f0f}
            },
            line = {color = 0xafccfc, width = 3, opa = 100, rounded = false},
            text = {
                font = style.font24,
                color = 0xff0000,
                opa = 100,
                letter_space = 2,
                line_space = 0
            }
        },
        needle = {
            pad = {inner = 100},
            line = {color = 0xafccfc, width = 3, opa = 100, rounded = false},
            border = {color = 0x00ff0f, width = 2, opa = 200},
            shadow = {spread = 30, color = 0xff00f0},
            bg = {
                radius = 10,
                color = 0x0f0ff0,
                opa = 150,
                grad = {color = 0x0f0f0f}
            }
        }
    }
}
-- gauge.create(home_cont, gauge_data)

local img_data = {
    src = "nosin_icon.png",
    -- align = lvgl.ALIGN_IN_TOP_LEFT,
    zoom = 600,
    style = {border = {width = 2}}
}

img_obj = img.create(home_cont, img_data)

sys.taskInit(function()
    index = 0
    sys.wait(3000)
    while true do
        lvgl.img_set_src(img_obj, "/lua/sin_icon_" .. (index % 5 + 1) .. ".png")
        index = index + 1
        sys.wait(1000)
    end
end)
