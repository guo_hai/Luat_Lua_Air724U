module(..., package.seeall)
local cont_data = {
    W = LCD_W,
    H = LCD_H,
    click = true,
    page_glue = true,
    -- align = lvgl.ALIGN_IN_TOP_LEFT,
    style = {
        border = {
            color = 0x0f0f0f,
            width = 0,
            opa = 30
        },
        shadow = {
            spread = 30,
            color = 0xff00f0
        },
        bg = {
            radius = 0,
            color = 0,
            opa = 250,
            margin = {
                all = 300
            }
        }
    }
}
LCD_W, LCD_H = 265, 265

p_cont = cont.create(lvgl.scr_act(), cont_data)

local tab_view_data = {
    W = LCD_W,
    H = LCD_H,
    time = 100,
    btn_pos = lvgl.TABVIEW_TAB_POS_NONE,
    page_list = {"电影", "电视剧", "电视剧"},
    style = {
        bg = {
            bg = {
                radius = 0,
                opa = 0
            },
            pad = {
                all = 0
            }
        },
        -- scrollable = {bg = {radius = 0, color = 0x00ff00, opa = 255}},
        -- indic = {bg = {radius = 10, color = 0x00ff00, opa = 255}},
        -- tab = {bg = {radius = 10, color = 0x0f0ff0, opa = 255}},
        page = {
            bg = {
                bg = {
                    color = 0xff0000
                },
                pad = {
                    all = 0
                }
            },
            lable = {
                bg = {
                    color = 0x00ffcc
                }
            },
            edge_flash = {
                bg = {
                    color = 0x0000ff
                }
            },
            bar = {
                bg = {
                    color = 0
                }
            }
        }
    }
}
local tab_view_obj = tab_view.create(p_cont, tab_view_data)
page_list_obj = tab_view_obj.page_list

local cont_data = {
    W = LCD_W - 25,
    H = LCD_H - 25,
    click = true,
    page_glue = true,
    layout = lvgl.LAYOUT_PRETTY_MID,
    -- align = lvgl.ALIGN_IN_TOP_LEFT,
    style = {
        border = {
            width = 0
        },
        bg = {
            radius = 0,
            color = 0,
            opa = 250
        },
        pad = {
            all = 8
        }
    }
}
local cont_list = {}
for i = 1, #page_list_obj do
    lvgl.page_set_scrollbar_mode(page_list_obj[i], lvgl.SCROLLBAR_MODE_OFF)
    cont_list[i] = cont.create(page_list_obj[i], cont_data)
end

home_cont = cont_list[1]
lvgl.cont_set_layout(home_cont, lvgl.LAYOUT_OFF)

img.create(home_cont, {
    src = "bg.jpg",
    page_glue = true
})

home_time_label = label.create(home_cont, {
    text = "09:00",
    align_x = 20,
    align_y = 50,
    align = lvgl.ALIGN_IN_TOP_LEFT,
    style = {
        text = {
            font = style.font50,
            color = 0xffffff
        }
    }
})

home_period_label = label.create(home_cont, {
    text = "上午",
    align_x = 10,
    align = lvgl.ALIGN_OUT_RIGHT_BOTTOM,
    align_to = home_time_label,
    style = {
        text = {
            font = style.font20,
            color = 0xffffff
        }
    }
})

home_date_label = label.create(home_cont, {
    text = "06月12日  星期日",
    -- align_x = 20,
    align_y = 10,
    align = lvgl.ALIGN_OUT_BOTTOM_LEFT,
    align_to = home_time_label,
    style = {
        text = {
            font = style.font20,
            color = 0xffffff
        }
    }
})

app_data = {{{
    img_src = "biji",
    text = "笔记"
}, {
    img_src = "fenlei",
    text = "分类"
}, {
    img_src = "gengduo",
    text = "更多"
}, {
    img_src = "gerenzhongxin",
    text = "个人中心"
}}, {{
    img_src = "jiemi",
    text = "解密"
}, {
    img_src = "mima",
    text = "密码"
}, {
    img_src = "paizhao",
    text = "拍照"
}, {
    img_src = "shijian",
    text = "时间"
}}}

local app_cont_data = {
    W = (LCD_W - 55) / 2,
    H = (LCD_W - 55) / 2,
    click = true,
    page_glue = true,
    style = {
        border = {
            width = 0
        },
        bg = {
            radius = 30,
            color = 0x00ff00,
            opa = 100
        }
    }
}

function add_app(p, v)
    local app_cont_ = cont.create(p, app_cont_data)
    img.create(app_cont_, {
        src = v.img_src .. ".png",
        align = lvgl.ALIGN_IN_TOP_MID,
        align_y = 15
    })
    label.create(app_cont_, {
        text = v.text,
        align = lvgl.ALIGN_IN_BOTTOM_MID,
        align_y = -10,
        style = {
            text = {
                font = style.font20,
                color = 0
            }
        }
    })
end

for i = 1, #app_data do
    for j = 1, #app_data[i] do
        add_app(cont_list[i + 1], app_data[i][j])
    end
end
local current_page = 0
sys.subscribe("KEY_EVENT", function(key, state)
    print(key)
    print(state)
    if not state then
        if key == "RIGHT" then
            if current_page < #cont_list - 1 then
                current_page = current_page + 1
                lvgl.tabview_set_tab_act(tab_view_obj.obj, current_page, lvgl.ANIM_ON)
            end
        elseif key == "LEFT" then
            if current_page > 0 then
                current_page = current_page - 1
                lvgl.tabview_set_tab_act(tab_view_obj.obj, current_page, lvgl.ANIM_ON)
            end
        end
    end
end)

-- --bTimeSyned ：时间是否已经成功同步过
local bTimeSyned = false

sys.timerLoopStart(function()
    if bTimeSyned and current_page == 0 then
        local weekTable = {"日", "一", "二", "三", "四", "五", "六"}
        local tClock = os.date("*t")
        -- local t=string.format("#FFFFFF %02d:%02d\n#FFFFFF %04d年%02d月%02d日",tClock.hour,tClock.min,tClock.year,tClock.month,tClock.day)
        local time = string.format("%02d:%02d", tClock.hour, tClock.min)
        lvgl.label_set_text(home_time_label, time)
        local date = string.format("%02d月%02d日  星期%s", tClock.month, tClock.day, weekTable[tClock.wday])
        lvgl.label_set_text(home_date_label, date)
    else
        -- 注册基站时间同步的URC消息处理函数
        ril.regUrc("+NITZ", function()
            log.info("cell.timeSync")
            bTimeSyned = true
        end)
    end

end, 1000)
