module(..., package.seeall)
-- 默认回调
function event_handler(obj, event) print("矩阵键盘默认回调") end

function create_style_btn(v)
    local style_btnMatrix = lvgl.style_t()
    lvgl.style_init(style_btnMatrix)

    lvgl.style_set_bg_color(style_btnMatrix, lvgl.STATE_DEFAULT,
                            lvgl.color_hex(v.color_hex or 0x0000FF))
    lvgl.style_set_bg_grad_color(style_btnMatrix, lvgl.STATE_DEFAULT,
                                 lvgl.color_hex(v.grad_color_hex or 0xFFFFFF))
    lvgl.style_set_bg_grad_dir(style_btnMatrix, lvgl.STATE_DEFAULT,
                               v.grad_dir or lvgl.GRAD_DIR_VER)

    lvgl.style_set_radius(style_btnMatrix, lvgl.STATE_DEFAULT, v.radius or 5)
    lvgl.style_set_border_width(style_btnMatrix, lvgl.STATE_DEFAULT,
                                v.border_width or 1)

    -- 透明度
    lvgl.style_set_bg_opa(style_btnMatrix, lvgl.STATE_DEFAULT, v.opa or 0)

    lvgl.style_set_border_color(style_btnMatrix, lvgl.STATE_DEFAULT,
                                lvgl.color_hex(v.border_color_hex or 0x0000FF))

    lvgl.style_set_border_opa(style_btnMatrix, lvgl.STATE_DEFAULT,
                              v.border_opa or 255)

    lvgl.style_set_text_font(style_btnMatrix, lvgl.STATE_DEFAULT,
                             v.font or style.font24)

    return style_btnMatrix
end

function create_style_bg(v)
    local style_btnMatrix = lvgl.style_t()
    lvgl.style_init(style_btnMatrix)

    -- 透明度
    lvgl.style_set_bg_opa(style_btnMatrix, lvgl.STATE_DEFAULT, v.opa or 0)

    lvgl.style_set_bg_color(style_btnMatrix, lvgl.STATE_DEFAULT,
                            lvgl.color_hex(v.color_hex or 0x0000FF))
    lvgl.style_set_bg_grad_color(style_btnMatrix, lvgl.STATE_DEFAULT,
                                 lvgl.color_hex(v.grad_color_hex or 0xFFFFFF))
    lvgl.style_set_bg_grad_dir(style_btnMatrix, lvgl.STATE_DEFAULT,
                               v.grad_dir or lvgl.GRAD_DIR_VER)

    -- 透明度
    lvgl.style_set_bg_opa(style_btnMatrix, lvgl.STATE_DEFAULT, v.opa or 0)

    lvgl.style_set_radius(style_btnMatrix, lvgl.STATE_DEFAULT, v.radius or 5)
    lvgl.style_set_border_width(style_btnMatrix, lvgl.STATE_DEFAULT,
                                v.border_width or 1)

    lvgl.style_set_border_color(style_btnMatrix, lvgl.STATE_DEFAULT,
                                lvgl.color_hex(v.border_color_hex or 0xFFFFFF))

    lvgl.style_set_border_opa(style_btnMatrix, lvgl.STATE_DEFAULT,
                              v.border_opa or 255)
    return style_btnMatrix
end

function create(p, v)
    local btnMatrix = lvgl.btnmatrix_create(p, nil)
    lvgl.obj_set_size(btnMatrix, v.W or 400, v.H or 200)
    lvgl.btnmatrix_set_recolor(btnMatrix, v.recolor or true)
    lvgl.btnmatrix_set_map(btnMatrix, v.btnArray or {"。", "。"})
    -- 设置btnMatrix的第九个按键所占宽度的比例
    -- lvgl.btnmatrix_set_btn_width(btnMatrix, 9, 3);
    -- lvgl.btnmatrix_set_btn_ctrl(btnMatrix, 9, lvgl.BTNMATRIX_CTRL_CHECKABLE);
    -- lvgl.btnmatrix_set_btn_ctrl(btnMatrix, 11, lvgl.BTNMATRIX_CTRL_CHECK_STATE);
    lvgl.obj_align(btnMatrix, v.align_to or nil, v.align or lvgl.ALIGN_CENTER,
                   v.align_x or 0, v.align_y or 0)
    lvgl.obj_set_event_cb(btnMatrix, v.cb or event_handler)
    lvgl.obj_add_style(btnMatrix, lvgl.BTNMATRIX_PART_BTN,
                       create_style_btn(v.btn_style))
    lvgl.obj_add_style(btnMatrix, lvgl.BTNMATRIX_PART_BG,
                       create_style_bg(v.bg_style))
    return btnMatrix
end
