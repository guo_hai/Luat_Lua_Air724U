module(..., package.seeall)

    style_body = lvgl.style_t()
    lvgl.style_init(style_body)
    lvgl.style_set_bg_color(style_body, lvgl.CONT_PART_MAIN,lvgl.color_hex(0x000518))
    lvgl.style_set_radius(style_body, lvgl.CONT_PART_MAIN, 0)
    lvgl.style_set_border_width(style_body, lvgl.CONT_PART_MAIN, 0)
    --透明
    -- lvgl.style_set_bg_opa(stytle_statusBox, lvgl.CONT_PART_MAIN,0)
    
-------------------------------
style_text=lvgl.style_t()
lvgl.style_init(style_text)
lvgl.style_set_text_color(style_text,lvgl.CONT_PART_MAIN,lvgl.color_hex(0xFF0000))
-- lvgl.style_set_radius(style_text, lvgl.CONT_PART_MAIN, 0)
-- lvgl.style_set_border_width(style_text, lvgl.CONT_PART_MAIN, 0)
-- lvgl.style_set_pad_left(style_text, lvgl.CONT_PART_MAIN,0)
-- lvgl.style_set_pad_right(style_text, lvgl.CONT_PART_MAIN,0)
-- lvgl.style_set_pad_top(style_text, lvgl.CONT_PART_MAIN,0)
-- lvgl.style_set_pad_bottom(style_text, lvgl.CONT_PART_MAIN,0)
-- lvgl.style_set_pad_inner(style_text, lvgl.CONT_PART_MAIN,0)
--------------------------------------------------------------------------------------------------

    style_temp = lvgl.style_t()
    lvgl.style_init(style_temp)
    lvgl.style_set_bg_color(style_temp, lvgl.CONT_PART_MAIN,lvgl.color_hex(0xFF0000))
    lvgl.style_set_radius(style_temp, lvgl.CONT_PART_MAIN, 0)
    lvgl.style_set_border_width(style_temp, lvgl.CONT_PART_MAIN, 0)

    ---------------------
    style_icon = lvgl.style_t()
    lvgl.style_init(style_icon)
    lvgl.style_set_radius(style_icon, lvgl.IMG_PART_MAIN, 20)
    lvgl.style_set_border_width(style_icon, lvgl.IMG_PART_MAIN, 5)
    lvgl.style_set_border_color(style_icon,lvgl.IMG_PART_MAIN,lvgl.color_hex(0xFF0000))
    --透明
    -- lvgl.style_set_bg_opa(stytle_statusBox, lvgl.CONT_PART_MAIN,0)

-------
style_QR = lvgl.style_t()
lvgl.style_init(style_QR)
lvgl.style_set_bg_color(style_QR, lvgl.CONT_PART_MAIN,lvgl.color_hex(0xFFFFFF))
------------
------------------------------------------------------------------------------------------------------
style_divBox = lvgl.style_t()
lvgl.style_init(style_divBox)
lvgl.style_set_bg_opa(style_divBox, lvgl.CONT_PART_MAIN,0)
lvgl.style_set_radius(style_divBox, lvgl.CONT_PART_MAIN, 0)
lvgl.style_set_border_width(style_divBox, lvgl.CONT_PART_MAIN, 0)
lvgl.style_set_pad_left(style_divBox, lvgl.CONT_PART_MAIN,0)
lvgl.style_set_pad_right(style_divBox, lvgl.CONT_PART_MAIN,0)
lvgl.style_set_pad_top(style_divBox, lvgl.CONT_PART_MAIN,0)
lvgl.style_set_pad_bottom(style_divBox, lvgl.CONT_PART_MAIN,0)
lvgl.style_set_pad_inner(style_divBox, lvgl.CONT_PART_MAIN,0)
