module(..., package.seeall)

function init()
    wallImg=lvgl.img_create(lvgl.scr_act(), nil)
    lvgl.obj_set_size(wallImg, 854, 480)
    lvgl.obj_align(wallImg, nil, lvgl.ALIGN_CENTER, 0, 0)
    lvgl.img_set_src(wallImg, "/lua/wall.jpg")
end

function uninit()
    if wallImg~=nil then
        lvgl.obj_del(wallImg)
        wallImg=nil
        activity.init()
    end
end

sys.subscribe("SCREENSAVER_INIT",init)
sys.subscribe("SYSTEM_CHANGE_TIME",uninit)