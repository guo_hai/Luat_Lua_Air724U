module(..., package.seeall)

function create(p, v)
    local obj = lvgl.label_create(p, nil)
    -- 设置 Label 的内容
    lvgl.label_set_text(obj, v.text or "")

    if v.W and v.H then lvgl.obj_set_size(obj, v.W, v.H) end

    -- 重新着色
    lvgl.label_set_recolor(obj, v.recolor or true)

    -- 文本的行可以使用 lvgl.label_set_align(obj, lvgl.LABEL_ALIGN_LEFT/RIGHT/CENTER) 
    -- 左右对齐。请注意，它将仅对齐线，而不对齐标签对象本身。
    lvgl.label_set_align(obj, v.label_align or lvgl.LABEL_ALIGN_CENTER)

    if v.click then lvgl.obj_set_click(obj, v.click) end
    if v.event then lvgl.obj_set_event_cb(obj, v.event) end

    -- 添加样式
    lvgl.obj_add_style(obj, lvgl.LABEL_PART_MAIN, create_style(v.style))

    -- 设置 Label 显示模式
    -- lvgl.LABEL_LONG_BREAK – 保持对象宽度，断开（换行）过长的线条并扩大对象高度
    -- lvgl.LABEL_LONG_DOT – 保持对象大小，打断文本并在最后一行写点（使用 lvgl.label_set_static_text 时不支持）
    -- lvgl.LABEL_LONG_SROLL – 保持大小并来回滚动标签
    -- lvgl.LABEL_LONG_SROLL_CIRC – 保持大小并循环滚动标签  
    -- lvgl.LABEL_LONG_CROP – 保持大小并裁剪文本    
    -- lvgl.LABEL_LONG_EXPAND – 将对象大小扩展为文本大小（默认）
    lvgl.label_set_long_mode(obj, v.mode or lvgl.LABEL_LONG_CROP)
    -- 设置 Label 的显示长度(若设置了显示模式，则需要在其后设置才会有效)
    -- if v.W and v.H then lvgl.obj_set_size(obj, v.W, v.H) end

    
    -- 设置 Label 的高度
    -- lvgl.obj_set_height(obj, v.H or 20)
    -- 设置 Label 的位置

    
    lvgl.obj_set_width(obj, v.W or 100)

    
    lvgl.obj_align(obj, v.align_to, v.align or lvgl.ALIGN_CENTER,
                   v.align_x or 0, v.align_y or 0)

    return obj
end

label_data = {
    W = 200,
    H = 100,
    text = "123456789012345678901234567890",
    mode = lvgl.LABEL_LONG_SROLL,
    align = lvgl.ALIGN_CENTER,
    style = {
        pad = {all = 0},
        border = {color = 0xff0000, width = 40, opa = 255},
        shadow = {spread = 100, width = 100, color = 0x00ff00},
        outline = {color = 0x0000ff, opa = 255, width = 40},
        bg = {
            radius = 0,
            color = 0xccffcc,
            opa = 250,
            grad = {color = 0xffffff, main = 60, grad = 300}
        },
        text = {
            font = style.font96,
            color = 0,
            letter_space = 0,
            line_space = 20
        }
    }
}

-- create(lvgl.scr_act(), label_data)

