module(..., package.seeall)
function create(p, v)
    local obj = lvgl.spinbox_create(p, nil)
    if v.click then lvgl.obj_set_click(obj, true) end
    if v.W and v.H then lvgl.obj_set_size(obj, v.W, v.H) end

    -- lvgl.spinbox_set_range(spinbox, min, max) 设置Spinbox的范围。
    lvgl.spinbox_set_range(obj, v.min, v.max)

    -- lvgl.spinbox_set_value(spinbox, num) 手动设置Spinbox的值。
    lvgl.spinbox_set_value(obj, v.num)

    -- lvgl.spinbox_increment(spinbox) 和 lvgl.spinbox_decrement(spinbox) 递增/递减Spinbox的值。

    -- lvgl.spinbox_set_step(spinbox, step) 设置增量减量。
    lvgl.spinbox_set_step(obj, v.step)

    lvgl.obj_align(obj, v.align_to, v.align or lvgl.ALIGN_CENTER,
                   v.align_x or 0, v.align_y or 0)
    if v.event then lvgl.obj_set_event_cb(obj, v.event) end
    -- 数字调整框的主要部分称为 lvgl.SPINBOX_PART_BG ，它是使用所有典型背景样式属性的矩形背景。
    -- 它还使用其文本样式属性描述标签的样式。
    -- lvgl.SPINBOX_PART_CURSOR 是描述光标的虚拟部分。阅读文本区域文档以获取详细说明。

    lvgl.obj_add_style(obj, lvgl.SPINBOX_PART_BG, create_style(v.style.bg))
    lvgl.obj_add_style(obj, lvgl.SPINBOX_PART_CURSOR,
                       create_style(v.style.cursor))

    return obj
end
local spinbox_data = {
    W = 200,
    H = 100,
    max = 100,
    min = -100,
    step = 1,
    num = 1.10,
    click = false,
    style = {
        bg = {
            bg = {
                radius = 10,
                color = 0x0f0ff0,
                opa = 150,
                grad = {color = 0x00ffff}
            },
            text = {font = style.font48}
        },
        cursor = {bg = {color = 0xffff00}}
    }
}
-- create(lvgl.scr_act(), spinbox_data)
