module(..., package.seeall)

require "home_fragment"
require "error_fragment"
require "driver_fragment"
require "setting_fragment"

function makeStatusLable()
    _G.statusLabel=lvgl.cont_create(lvgl.scr_act(), nil)
    lvgl.obj_set_size(_G.statusLabel,854, 40)
    lvgl.obj_align(_G.statusLabel, nil, lvgl.ALIGN_IN_TOP_MID, 0, 0)
    lvgl.obj_add_style(_G.statusLabel, lvgl.CONT_PART_MAIN, style.style_statusLabel)

    local statusImg = lvgl.img_create(_G.statusLabel, nil)
    lvgl.img_set_src(statusImg, "/lua/status.png")

    local statusLabel = lvgl.label_create(_G.statusLabel, nil)
    lvgl.obj_set_style_local_text_font(statusLabel, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font24)
    lvgl.label_set_text(statusLabel,"数据中心监控系统")
    -- lvgl.label_set_align(label, lvgl.LABEL_ALIGN_CENTER)
    lvgl.obj_align(statusLabel, nil, lvgl.ALIGN_CENTER, 0, 0)
end

function makeBody()
    _G.body=lvgl.cont_create(lvgl.scr_act(), nil)
    lvgl.obj_set_size(_G.body,854, 440)
    lvgl.obj_add_style(_G.body, lvgl.CONT_PART_MAIN, style.style_body)
    lvgl.obj_align(_G.body, nil, lvgl.ALIGN_IN_BOTTOM_MID, 0, 0)
end

function activityInit()
    makeStatusLable()
    makeBody()
    home_fragment.Init()
    -- error_fragment.Init()
    -- driver_fragment.Init()
    -- setting_fragment.Init()
end
