module(..., package.seeall)

local topicTable={}
local function topicTableAdd(id,callback)
    topicTable[id]=callback
    sys.subscribe(id,callback)
end

function aa(cont)
-- 创建图片控件
local img = lvgl.img_create(cont, nil)
-- 设置图片显示的图像
lvgl.img_set_src(img, "/lua/t1.png")
-- 图片居中
lvgl.obj_align(img, nil, lvgl.ALIGN_IN_TOP_LEFT, 20, 15)

local label=lvgl.label_create(img, nil)
    lvgl.label_set_recolor(label, true)
    lvgl.obj_set_style_local_text_font(label, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font22)
    lvgl.label_set_text(label,"#FFFFFF 首页")
    lvgl.obj_align(label, nil, lvgl.ALIGN_IN_LEFT_MID, 3, 0)

    local label=lvgl.label_create(img, nil)
    lvgl.label_set_recolor(label, true)
    lvgl.obj_set_style_local_text_font(label, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font22)
    lvgl.label_set_text(label,"#FFFFFF 设备")
    lvgl.obj_align(label, nil, lvgl.ALIGN_IN_RIGHT_MID, -50, 0)

    local function event_handler(obj, event)
        if event == lvgl.EVENT_CLICKED then
            BEEP()
            unInit()
            home_fragment.Init()
            print("左按下")
        end
    end
    
    
        local home=lvgl.cont_create(img, nil)
        lvgl.obj_set_size(home,70, 40)
        lvgl.obj_add_style(home, 0, style.style_div)
        lvgl.obj_set_click(home,true)
        lvgl.obj_set_event_cb(home, event_handler)
end

----------

local function makedriver (cont)
    driverCnt=lvgl.page_create(cont, nil)
    if not lvgl.indev_get_emu_touch then lvgl.page_set_scrollbar_mode(driverCnt, lvgl.SCRLBAR_MODE_OFF) end
    lvgl.obj_set_size(driverCnt,854, 370)
    lvgl.page_set_scrl_layout(driverCnt,lvgl.LAYOUT_COLUMN_MID)
    lvgl.obj_align(driverCnt, nil, lvgl.ALIGN_IN_BOTTOM_MID, 0, 0)
    lvgl.obj_add_style(driverCnt, lvgl.CONT_PART_MAIN, style.style_bg_page)

    local function makeUL(cont,title,t)

        local cc=lvgl.cont_create(cont, nil)
        lvgl.obj_set_size(cc,854, 150)
        lvgl.obj_add_style(cc, lvgl.CONT_PART_MAIN, style.style_bg_UL)

        -- 创建图片控件
        local img = lvgl.img_create(cc, nil)
        -- 设置图片显示的图像
        lvgl.img_set_src(img, "/lua/t2.png")
        -- 图片居中
        lvgl.obj_align(img, nil, lvgl.ALIGN_IN_TOP_LEFT, 20, 0)
        
        local label=lvgl.label_create(img, nil)
        lvgl.label_set_recolor(label, true)
        lvgl.obj_set_style_local_text_font(label, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font22)
        lvgl.label_set_text(label,"#FFFFFF "..title)
        lvgl.obj_align(label, nil, lvgl.ALIGN_IN_LEFT_MID, 8, 0)




        local cnt=lvgl.cont_create(cc, nil)
        lvgl.obj_set_size(cnt,854, 100)
        -- lvgl.obj_set_size(cnt,854, 300)
        lvgl.obj_align(cnt, nil, lvgl.ALIGN_IN_BOTTOM_MID, 0, 0)
        lvgl.cont_set_layout(cnt, lvgl.LAYOUT_PRETTY_MID)
        lvgl.obj_add_style(cnt, lvgl.CONT_PART_MAIN, style.style_bg_bg)

        local item=function(cnt,t)
            local cont=lvgl.cont_create(cnt, nil)
            lvgl.obj_set_click(cont,false)
            lvgl.obj_set_size(cont,250, 100)
            lvgl.obj_add_style(cont, lvgl.CONT_PART_MAIN, style.style_bg_boder)
    
            local img = lvgl.img_create(cont, nil)
            -- 设置图片显示的图像
            lvgl.img_set_src(img, "/lua/"..t.img)
            -- 图片居中
            lvgl.obj_align(img, nil, lvgl.ALIGN_IN_LEFT_MID, 5, -10)
    
            local label=lvgl.label_create(cont, nil)
            lvgl.label_set_recolor(label, true)
            lvgl.obj_set_style_local_text_font(label, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font18)
            lvgl.label_set_text(label,"#18aaf2 "..t.text)
            lvgl.obj_align(label, img, lvgl.ALIGN_OUT_BOTTOM_MID, 0, 0)
    
            local l1=lvgl.label_create(cont, nil)
            lvgl.label_set_recolor(l1, true)
            lvgl.obj_set_style_local_text_font(l1, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font18)
            lvgl.label_set_text(l1,"#1470cc "..t.up)
            lvgl.obj_align(l1, nil, lvgl.ALIGN_IN_LEFT_MID, 70, -15)
    
            local l2=lvgl.label_create(cont, nil)
            lvgl.label_set_recolor(l2, true)
            lvgl.obj_set_style_local_text_font(l2, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font18)
            lvgl.label_set_text(l2,"#1470cc "..t.down)
            lvgl.obj_align(l2, nil, lvgl.ALIGN_IN_LEFT_MID, 70, 15)
    
            local dw1=lvgl.label_create(cont, nil)
            lvgl.label_set_recolor(dw1, true)
            lvgl.obj_set_style_local_text_font(dw1, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font18)
            lvgl.label_set_text(dw1,"#1470cc "..t.dw1)
            lvgl.obj_align(dw1, nil, lvgl.ALIGN_IN_RIGHT_MID, -15, -15)
    
            local dw2=lvgl.label_create(cont, nil)
            lvgl.label_set_recolor(dw2, true)
            lvgl.obj_set_style_local_text_font(dw2, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font18)
            lvgl.label_set_text(dw2,"#1470cc "..t.dw2)
            lvgl.obj_align(dw2, nil, lvgl.ALIGN_IN_RIGHT_MID, -15, 15)
    
            local p1=lvgl.label_create(cont, nil)
            lvgl.label_set_recolor(p1, true)
            lvgl.obj_set_style_local_text_font(p1, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font22)
            lvgl.label_set_text(p1,"#75bc4d "..t.p1)
            lvgl.obj_align(p1,dw1, lvgl.ALIGN_OUT_LEFT_MID, -5, 0)
    
            local p2=lvgl.label_create(cont, nil)
            lvgl.label_set_recolor(p2, true)
            lvgl.obj_set_style_local_text_font(p2, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font22)
            lvgl.label_set_text(p2,"#75bc4d "..t.p2)
            lvgl.obj_align(p2,dw2, lvgl.ALIGN_OUT_LEFT_MID, -5, 0)
        end

        for _, v in ipairs(t) do
            item(cnt,v)
        end
    end

    local t1={
        {
            img="battery_jump.png",
            text="电池组",
            up="单体个数",
            down="单体电压",
            p1="16",
            p2="3.33",
            dw1="个",
            dw2="V",
        },
        {
            img="battery_jump.png",
            text="电池组",
            up="单体个数",
            down="单体电压",
            p1="16",
            p2="3.33",
            dw1="个",
            dw2="V",
        },
        {
            img="battery_jump.png",
            text="电池组",
            up="单体个数",
            down="单体电压",
            p1="16",
            p2="3.33",
            dw1="个",
            dw2="V",
        }
    }

    makeUL(driverCnt,"动力设备",t1)
    makeUL(driverCnt,"环境设备",t1)
    -- makeUL(driverCnt,"安防设备",t1)

    -- makeUL(driverCnt,t1)
    -- makeUL(driverCnt,t1)
end

function Init()
    lvgl.obj_clean(_G.body)
    aa(_G.body)
    makedriver(_G.body)
end

function unInit()
    -- print("取消订阅")
    for id, callback in pairs(topicTable) do
        sys.unsubscribe(id, callback)
    end
end