module(..., package.seeall)

require "inputBox"

local topicTable={}
local function topicTableAdd(id,callback)
    topicTable[id]=callback
    sys.subscribe(id,callback)
end
-------------

function aa(cont)
    -- 创建图片控件
    local img = lvgl.img_create(cont, nil)
    -- 设置图片显示的图像
    lvgl.img_set_src(img, "/lua/t1.png")
    -- 图片居中
    lvgl.obj_align(img, nil, lvgl.ALIGN_IN_TOP_LEFT, 20, 15)
    
    local label=lvgl.label_create(img, nil)
        lvgl.label_set_recolor(label, true)
        lvgl.obj_set_style_local_text_font(label, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font22)
        lvgl.label_set_text(label,"#FFFFFF 首页")
        lvgl.obj_align(label, nil, lvgl.ALIGN_IN_LEFT_MID, 3, 0)
    
        local label=lvgl.label_create(img, nil)
        lvgl.label_set_recolor(label, true)
        lvgl.obj_set_style_local_text_font(label, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font22)
        lvgl.label_set_text(label,"#FFFFFF 设置")
        lvgl.obj_align(label, nil, lvgl.ALIGN_IN_RIGHT_MID, -50, 0)
    
        local function event_handler(obj, event)
            if event == lvgl.EVENT_CLICKED then
                BEEP()
                unInit()
                home_fragment.Init()
                print("左按下")
            end
        end
        
        
            local home=lvgl.cont_create(img, nil)
            lvgl.obj_set_size(home,70, 40)
            lvgl.obj_add_style(home, 0, style.style_div)
            lvgl.obj_set_click(home,true)
            lvgl.obj_set_event_cb(home, event_handler)
    end


    --------

    local makeSettingCnt=function(cont)
        SettingCnt=lvgl.cont_create(cont, nil)
        lvgl.obj_set_size(SettingCnt,854, 370)
        lvgl.obj_align(SettingCnt, nil, lvgl.ALIGN_IN_BOTTOM_MID, 0, 0)
        lvgl.obj_add_style(SettingCnt, lvgl.CONT_PART_MAIN, style.style_div)


        local cc=lvgl.cont_create(SettingCnt, nil)
        lvgl.obj_set_size(cc,854, 90)
        lvgl.obj_add_style(cc, lvgl.CONT_PART_MAIN, style.style_div)

        -- 创建图片控件
        local img = lvgl.img_create(cc, nil)
        -- 设置图片显示的图像
        lvgl.img_set_src(img, "/lua/t2.png")
        -- 图片居中
        lvgl.obj_align(img, nil, lvgl.ALIGN_IN_TOP_LEFT, 20, 0)
        
        local label=lvgl.label_create(img, nil)
        lvgl.label_set_recolor(label, true)
        lvgl.obj_set_style_local_text_font(label, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font22)
        lvgl.label_set_text(label,"#FFFFFF 告警等级")
        lvgl.obj_align(label, nil, lvgl.ALIGN_IN_LEFT_MID, 8, 0)

        local cb = lvgl.checkbox_create(cc, nil)
        lvgl.checkbox_set_text(cb, "紧急")
        lvgl.obj_align(cb, img, lvgl.ALIGN_OUT_BOTTOM_LEFT,5, 12)
        lvgl.obj_add_style(cb, lvgl.CHECKBOX_PART_BG, style.style_leve)

        local cb = lvgl.checkbox_create(cc, nil)
        lvgl.checkbox_set_text(cb, "严重")
        lvgl.obj_align(cb, img, lvgl.ALIGN_OUT_BOTTOM_LEFT,95+10, 12)
        lvgl.obj_add_style(cb, lvgl.CHECKBOX_PART_BG, style.style_leve)

        local cb = lvgl.checkbox_create(cc, nil)
        lvgl.checkbox_set_text(cb, "重要")
        lvgl.obj_align(cb, img, lvgl.ALIGN_OUT_BOTTOM_LEFT,185+10, 12)
        lvgl.obj_add_style(cb, lvgl.CHECKBOX_PART_BG, style.style_leve)

        local cb = lvgl.checkbox_create(cc, nil)
        lvgl.checkbox_set_text(cb, "一般")
        lvgl.obj_align(cb, img, lvgl.ALIGN_OUT_BOTTOM_LEFT,275+10, 12)
        lvgl.obj_add_style(cb, lvgl.CHECKBOX_PART_BG, style.style_leve)

        local cb = lvgl.checkbox_create(cc, nil)
        lvgl.checkbox_set_text(cb, "提示")
        lvgl.obj_align(cb, img, lvgl.ALIGN_OUT_BOTTOM_LEFT,365+10, 12)
        -- lvgl.obj_add_style(cb, lvgl.CHECKBOX_PART_BG, style.style_leve)
        lvgl.obj_add_style(cb, lvgl.CONT_PART_MAIN, style.style_leve)


        -------

        local cc1=lvgl.cont_create(SettingCnt, nil)
        lvgl.obj_set_size(cc1,854, 280)
        lvgl.obj_add_style(cc1, lvgl.CONT_PART_MAIN, style.style_div)
        lvgl.obj_align(cc1, cc, lvgl.ALIGN_OUT_BOTTOM_MID,0, 0)

        -- 创建图片控件
        local img = lvgl.img_create(cc1, nil)
        -- 设置图片显示的图像
        lvgl.img_set_src(img, "/lua/t2.png")
        -- 图片居中
        lvgl.obj_align(img, nil, lvgl.ALIGN_IN_TOP_LEFT, 20, 0)

        local label=lvgl.label_create(img, nil)
        lvgl.label_set_recolor(label, true)
        lvgl.obj_set_style_local_text_font(label, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font22)
        lvgl.label_set_text(label,"#FFFFFF 告警阈值")
        lvgl.obj_align(label, nil, lvgl.ALIGN_IN_LEFT_MID, 8, 0)

        local cnt=lvgl.cont_create(cc1, nil)
        lvgl.obj_set_size(cnt,350, 100)
        -- lvgl.obj_align(cnt, nil, lvgl.ALIGN_IN_CENTER, -200, 50)
        lvgl.obj_align(cnt, nil, lvgl.ALIGN_IN_TOP_LEFT, 20, 50)
        lvgl.obj_add_style(cnt, lvgl.CONT_PART_MAIN, style.style_bg_boder)

        local img1 = lvgl.img_create(cnt, nil)
            -- 设置图片显示的图像
            lvgl.img_set_src(img1, "/lua/battery_jump.png")
            -- 图片居中
            lvgl.obj_align(img1, nil, lvgl.ALIGN_IN_LEFT_MID, 15, -15)

        local label=lvgl.label_create(cnt, nil)
        lvgl.label_set_recolor(label, true)
        lvgl.obj_set_style_local_text_font(label, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font16)
        lvgl.label_set_text(label,"#FFFFFF 温度预警")
        lvgl.obj_align(label, img1, lvgl.ALIGN_OUT_BOTTOM_MID, 0, 0)

        local label=lvgl.label_create(cnt, nil)
        lvgl.label_set_recolor(label, true)
        lvgl.obj_set_style_local_text_font(label, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font22)
        lvgl.label_set_text(label,"#FFFFFF 温度上限")
        lvgl.obj_align(label, nil, lvgl.ALIGN_IN_RIGHT_MID, -110, -20)

        local label=lvgl.label_create(cnt, nil)
        lvgl.label_set_recolor(label, true)
        lvgl.obj_set_style_local_text_font(label, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font22)
        lvgl.label_set_text(label,"#FFFFFF 温度下限")
        lvgl.obj_align(label, nil, lvgl.ALIGN_IN_RIGHT_MID, -110, 20)

        local up=inputBox.init(cnt,50,40,lvgl.ALIGN_IN_RIGHT_MID,-50, -20,"80",nil,lvgl.ALIGN_OUT_RIGHT_MID)
        local low=inputBox.init(cnt,50,40,lvgl.ALIGN_IN_RIGHT_MID,-50, 20,"12",nil,lvgl.ALIGN_OUT_RIGHT_MID)

        -------


        local cnt1=lvgl.cont_create(cc1, nil)
        lvgl.obj_set_size(cnt1,350, 100)
        -- lvgl.obj_align(cnt1, nil, lvgl.ALIGN_IN_CENTER, 200, 50)
        lvgl.obj_align(cnt1, cnt, lvgl.ALIGN_OUT_BOTTOM_MID, 0, 15)
        lvgl.obj_add_style(cnt1, lvgl.CONT_PART_MAIN, style.style_bg_boder)

        local img2 = lvgl.img_create(cnt1, nil)
            -- 设置图片显示的图像
            lvgl.img_set_src(img2, "/lua/battery_jump.png")
            -- 图片居中
            lvgl.obj_align(img2, nil, lvgl.ALIGN_IN_LEFT_MID, 15, -15)

        local label=lvgl.label_create(cnt1, nil)
        lvgl.label_set_recolor(label, true)
        lvgl.obj_set_style_local_text_font(label, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font16)
        lvgl.label_set_text(label,"#FFFFFF 负载预警")
        lvgl.obj_align(label, img2, lvgl.ALIGN_OUT_BOTTOM_MID, 0, 0)

        local label=lvgl.label_create(cnt1, nil)
        lvgl.label_set_recolor(label, true)
        lvgl.obj_set_style_local_text_font(label, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font22)
        lvgl.label_set_text(label,"#FFFFFF 负载上限")
        lvgl.obj_align(label, nil, lvgl.ALIGN_IN_RIGHT_MID, -110, -20)

        local label=lvgl.label_create(cnt1, nil)
        lvgl.label_set_recolor(label, true)
        lvgl.obj_set_style_local_text_font(label, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font22)
        lvgl.label_set_text(label,"#FFFFFF 负载下限")
        lvgl.obj_align(label, nil, lvgl.ALIGN_IN_RIGHT_MID, -110, 20)

        local up=inputBox.init(cnt1,50,40,lvgl.ALIGN_IN_RIGHT_MID,-50, -20,"80",nil,lvgl.ALIGN_OUT_RIGHT_MID,0,-20)
        local low=inputBox.init(cnt1,50,40,lvgl.ALIGN_IN_RIGHT_MID,-50, 20,"12",nil,lvgl.ALIGN_OUT_RIGHT_MID,0,-80)

        --------------

        local cnt2=lvgl.cont_create(cc1, nil)
        lvgl.obj_set_size(cnt2,350, 100)
        -- lvgl.obj_align(cnt2, nil, lvgl.ALIGN_IN_CENTER, -200, 50)
        lvgl.obj_align(cnt2, nil, lvgl.ALIGN_IN_TOP_RIGHT, -20, 50)
        lvgl.obj_add_style(cnt2, lvgl.CONT_PART_MAIN, style.style_bg_boder)

        local img1 = lvgl.img_create(cnt2, nil)
            -- 设置图片显示的图像
            lvgl.img_set_src(img1, "/lua/battery_jump.png")
            -- 图片居中
            lvgl.obj_align(img1, nil, lvgl.ALIGN_IN_LEFT_MID, 15, -15)

        local label=lvgl.label_create(cnt2, nil)
        lvgl.label_set_recolor(label, true)
        lvgl.obj_set_style_local_text_font(label, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font16)
        lvgl.label_set_text(label,"#FFFFFF 电压预警")
        lvgl.obj_align(label, img1, lvgl.ALIGN_OUT_BOTTOM_MID, 0, 0)

        local label=lvgl.label_create(cnt2, nil)
        lvgl.label_set_recolor(label, true)
        lvgl.obj_set_style_local_text_font(label, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font22)
        lvgl.label_set_text(label,"#FFFFFF 电压上限")
        lvgl.obj_align(label, nil, lvgl.ALIGN_IN_RIGHT_MID, -110, -20)

        local label=lvgl.label_create(cnt2, nil)
        lvgl.label_set_recolor(label, true)
        lvgl.obj_set_style_local_text_font(label, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font22)
        lvgl.label_set_text(label,"#FFFFFF 电压下限")
        lvgl.obj_align(label, nil, lvgl.ALIGN_IN_RIGHT_MID, -110, 20)

        local up=inputBox.init(cnt2,50,40,lvgl.ALIGN_IN_RIGHT_MID,-50, -20,"80",nil,lvgl.ALIGN_OUT_LEFT_MID)
        local low=inputBox.init(cnt2,50,40,lvgl.ALIGN_IN_RIGHT_MID,-50, 20,"12",nil,lvgl.ALIGN_OUT_LEFT_MID)

        -----------------

        

local cnt3=lvgl.cont_create(cc1, nil)
lvgl.obj_set_size(cnt3,350, 100)
-- lvgl.obj_align(cnt3, nil, lvgl.ALIGN_IN_CENTER, 200, 50)
lvgl.obj_align(cnt3, cnt2, lvgl.ALIGN_OUT_BOTTOM_MID, 0, 15)
lvgl.obj_add_style(cnt3, lvgl.CONT_PART_MAIN, style.style_bg_boder)

local img2 = lvgl.img_create(cnt3, nil)
    -- 设置图片显示的图像
    lvgl.img_set_src(img2, "/lua/battery_jump.png")
    -- 图片居中
    lvgl.obj_align(img2, nil, lvgl.ALIGN_IN_LEFT_MID, 15, -15)

local label=lvgl.label_create(cnt3, nil)
lvgl.label_set_recolor(label, true)
lvgl.obj_set_style_local_text_font(label, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font16)
lvgl.label_set_text(label,"#FFFFFF 电流预警")
lvgl.obj_align(label, img2, lvgl.ALIGN_OUT_BOTTOM_MID, 0, 0)

local label=lvgl.label_create(cnt3, nil)
lvgl.label_set_recolor(label, true)
lvgl.obj_set_style_local_text_font(label, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font22)
lvgl.label_set_text(label,"#FFFFFF 电流上限")
lvgl.obj_align(label, nil, lvgl.ALIGN_IN_RIGHT_MID, -110, -20)

local label=lvgl.label_create(cnt3, nil)
lvgl.label_set_recolor(label, true)
lvgl.obj_set_style_local_text_font(label, lvgl.LABEL_PART_MAIN, lvgl.STATE_DEFAULT, font22)
lvgl.label_set_text(label,"#FFFFFF 电流下限")
lvgl.obj_align(label, nil, lvgl.ALIGN_IN_RIGHT_MID, -110, 20)

local up=inputBox.init(cnt3,50,40,lvgl.ALIGN_IN_RIGHT_MID,-50, -20,"80",nil,lvgl.ALIGN_OUT_LEFT_MID,0,-20)
local low=inputBox.init(cnt3,50,40,lvgl.ALIGN_IN_RIGHT_MID,-50, 20,"12",nil,lvgl.ALIGN_OUT_LEFT_MID,0,-80)


    end
-------
function Init()
    lvgl.obj_clean(_G.body)
    aa(_G.body)
    makeSettingCnt(_G.body)

end

function unInit()
    print("取消订阅")
    for id, callback in pairs(topicTable) do
        print("---",id)
        sys.unsubscribe(id, callback)
    end
end