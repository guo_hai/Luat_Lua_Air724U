--!nonstrict
require 'sys'
local nn = {
	optim = {
		Adam = {};
	};
	activations = {deriv = {};};
	layers = {};
	neuronal_networks = {};
}
local e = 2.71828
function nn.DotProduct(x, y)
	local res = {}
	for i, v in pairs(y) do
		table.insert(res, x[i] * y[i])
	end
	return res
end
function nn.Multiply2dArray(x, y)
	local res = {}
	for i, v in pairs(y) do
		table.insert(res, x[i] * y[i])
	end
	return res
end
function nn.Multiply(x, y)
	--log.debug(x)
	local res = {}
	for i, v in pairs(x) do
		table.insert(res, x[i] * y)
	end
	return res
end
function nn.less2dArray(x, y)
	local res = {}
	for i, v in pairs(y) do
		table.insert(res, x[i] - y[i])
	end
	return res
end
local function check(a, b) return a[b] end
local DotProduct = nn.DotProduct
function nn.compute_loss(x)
	return x^2-2*x+1
end
function nn.grad(x)
	return 2*x-2
end
local function compute_Adam()
	nn.optim.Adam["m_dw"], nn.optim.Adam["v_dw"] = 0, 0
	nn.optim.Adam["m_db"], nn.optim.Adam["v_db"] = 0, 0
	nn.optim.Adam["beta1"] = 0.9
	nn.optim.Adam["beta2"] = 0.999
	nn.optim.Adam["epsilon"] = 1e-8
	nn.optim.Adam["eta"] = 0.01
end compute_Adam()
--//
function nn.optim.Adam.Update(t, weights, bnns, dw, db)
	local self = nn.optim.Adam
	self.m_dw = self.beta1*self.m_dw + (1-self.beta1)*dw
	self.m_db = self.beta1*self.m_db + (1-self.beta1)*db
	self.v_dw = self.beta2*self.v_dw + (1-self.beta2)*(dw^2)
	self.v_db = self.beta2*self.v_db + (1-self.beta2)*(db^2)
	local m_dw_corr = self.m_dw/(1-self.beta1^t)
	local m_db_corr = self.m_db/(1-self.beta1^t)
	local v_dw_corr = self.v_dw/(1-self.beta2^t)
	local v_db_corr = self.v_db/(1-self.beta2^t)
	local w = weights - self.eta*(m_dw_corr/(math.sqrt(v_dw_corr)+self.epsilon))
	local b = bnns - self.eta*(m_db_corr/(math.sqrt(v_db_corr)+self.epsilon))
	return w, b
end
--//
function nn.activations.tanh(x)
	local res = {}
	for i, v in pairs(x) do
		table.insert(res, math.sinh(x[i])/math.cosh(x[i]))
	end
	return res
end
--//
function nn.activations.sigmoid(x)
	local res = {}
	for i, v in pairs(x) do
		table.insert(res, 1/(1 + (-x[i] * e)))
	end
	return res
end
--//
function nn.activations.relu(x)
	local res = {}
	for i, v in pairs(x) do
		table.insert(res, math.max(0, x[i]))
	end
	return res
end
--//
function nn.activations.softmax(x)
	local res = {}
	for i, v in pairs(x) do
		table.insert(res, x[i] * e/(x[i] * e))
	end
	return res
end
--//
function nn.activations.deriv.sigmoid_derivate(x)
	local res = {}
	local originalFunction = nn.activations.sigmoid(x)
	for i, v in pairs(originalFunction) do
		table.insert(res, originalFunction[i]*(1-originalFunction[i]))
	end
	return res
end
--//
function nn.ComputeActivation(Activation)
	if type(Activation) == "function" then
	return Activation else
		return nn.activations[Activation] or nn.activations.deriv[Activation]
	end
end
--//
local folder_of_layers = nn.layers
--//
function nn.layers.Create_Layer(name, ord, Activation)
	local function computeSelf()
		folder_of_layers[name] = {};
		folder_of_layers[name]["Activation"] = nn.ComputeActivation(Activation)
		folder_of_layers[name]["ord"] = ord
	end computeSelf()
	local self = folder_of_layers[name]
	function self.layer_think(inputs, weights, bnns)
		local res = nn.Multiply(inputs, weights)
		for i, v in pairs(res) do
			res[i] = res[i] + bnns
		end
		return self["Activation"](res)
	end
	function self.ChangeActivation(NewActivation)
		self["Activation"] = NewActivation
	end
	return folder_of_layers[name]
end
--//
function nn.UsePreMadeLayers()
	local PREMADE_LAYERS = {
	nn.layers.Create_Layer("FirstLayerPREMADE", 1, nn.activations.relu),
	nn.layers.Create_Layer("FirstHiddenLayerPREMADE", 2, nn.activations.sigmoid),
	nn.layers.Create_Layer("MiddleLayerPREMADE", 3, nn.activations.tanh),
	nn.layers.Create_Layer("SecondHiddenLayerPREMADE", 4, nn.activations.relu),
	nn.layers.Create_Layer("FinalLayerPREMADE", 5, nn.activations.softmax)} return PREMADE_LAYERS
end
--//
function nn.Create_neuronal_network(name, layers)
	nn.neuronal_networks[name] = {} 
	local self = nn.neuronal_networks[name]
	local name = name
	name = name
	local function computeLayers()
		self["weights-1"] = {}
		self["bnns-1"] = {}
		self["TrainComplete"] = {false} 
		self["TrainCompleted"] = {}
		if type(layers) == "string" and layers:lower():find("premade") then
			self["layers"] = nn.UsePreMadeLayers() --自带的预置网络
			return self["layers"]
		else
			self["layers"] = layers
			return self["layers"]
		end
	end local layers = computeLayers()
	--//
	function self.run(input, layerz)
		local array = {}
		if layerz == "general" or layerz == nil then
			for i = 1,#self["layers"] do
				for i, v in pairs(self["layers"]) do
					if v["ord"] == i then
						if i == 1 then
							table.insert(array, v.layer_think(input, self["weights"], self["bnns"]))
						else
							table.insert(array, v.layer_think(array[i-1], self["weights"], self["bnns"]))
						end
					end
				end
			end
			return array[#array-1]
		else
			for i = 1,#self["layers"] do
				for i, v in pairs(self["layers"]) do
					if v["ord"] == i then
						if i == 1 then
							table.insert(array, v.layer_think(input, self["weights"], self["bnns"]))
						else
							table.insert(array, v.layer_think(array[i-1], self["weights"], self["bnns"]))
						end
					end
				end
			end
			return array[#array-1]
		end
	end
	--//
	function self.TrainCompleted:Connect(functionCalled)
			for i,v in pairs(nn.neuronal_networks) do
				if tostring(i) == tostring(name) then
				for i=1, 10000 do
					if unpack(v.TrainComplete) == true then
						functionCalled()
						v.TrainComplete = {false}
					end
					sys.wait(0.004)
				end
				end
			end
	end
	--//
	function self.Train(inputs, outputs, optim, iterations, printq)
		local result = {}
		math.randomseed(os.time())
		if (self["weights"]) then else 
			self["weights"] = math.random()
			self["bnns"] = math.random()
			--print(self["weights"])
			--print(self["bnns"])
		end
		table.insert(self["weights-1"],1, self["weights"]) table.insert(self["bnns-1"],1, self["bnns"])
		for i = 1,iterations do
			result = self.run(inputs, "general")
			if (printq == true) then print(i.."/"..iterations.." completed") print(unpack(result)) end
			local dw, db = nn.grad(self.weights), nn.grad(self.bnns)
			self["weights"], self["bnns"] = optim.Update(i, self.weights, self.bnns, dw, db)
			local weightsAnt = self["weights-1"] local bnnsAnt = self["bnns-1"]
			table.insert(self["weights-1"], self["weights"]) table.insert(self["bnns-1"], self["bnns"])
			sys.wait(0.004)
		end
		self.TrainComplete = {true}
		return result
	end
	return self
end

return nn