PROJECT = 'LuatAI'
VERSION = '0.0.1'
require 'log'
LOG_LEVEL = log.LOGLEVEL_TRACE
require 'sys'

package.path = package.path ..';./?.lua' --添加同级目录下的所有lua文件

local LuatAIgeneralNN = require('NNmodule') --导入generalNN模块

local LuatAIlearnNN = require('learn')--导入learnNN模块


--演示LuatAI的神经网络两个独立模块，generalNN和learnNN的用法
--两者均属于全连接网络
--generalNN只有前向传播方式，learnNN有前向和反向传播两种方式
--learnNN支持张量（矩阵），generalNN仅支持一维数据
--generalNN只有1个模块，可迅速搭建神经网络，但不灵活；learnNN则相反，但目前还不稳定，存在一些问题
--generalNN偏向入门AI神经网络的介绍；learnNN无论从复杂度还是使用方法，更接近于真正意义的神经网络

function learnNNTest()
	--使用方法与pytorch几乎完全相同

	-- XOR训练数据
	--也可以是各种特征数据和标签数据
	--全连接神经元层
	--数据传播流程为：前向传播，反向传播，参数更新
	--提供sigmoid、relu、tanh、softmax
	--softmax函数在实际使用场景并不多
	--一般softmax用在神经网络全连接层操作结束后，如果属于分类任务才需要，也常被全连接层替代
	--基本很少会涉及softmax反向传播求导
	--因此，提供的softmax函数前向是softmax的，反向是relu函数的，不建议使用
	--使用mse作为loss函数


	local train_features = {{0, 0}}
	local train_labels = {{0}}
  
	local n_input = #train_features[1]
	local n_output = #train_labels[1]
  
	local model = learn.model.nnet({modules = {
	  learn.layer.linear({n_input = n_input, n_output = n_input * 3}),
	  learn.transfer.sigmoid({}),
	  learn.layer.linear({n_input = n_input * 3, n_output =  n_output}),
	  learn.transfer.sigmoid({})
	}})

	local epochs = 1
	local learning_rate = 0.5
	local error = model.fit(train_features, train_labels, epochs, learning_rate, true)

	for _, prediction in pairs(model.predict(train_features)) do
		print(table.concat(prediction, ", "))
	end
	


end


function generalNNTest()
	--三个神经元层，激活函数有relu,sigmoid,tanh，softmax
	--layers.Create_Layer()函数创建一个神经元层，三个参数为 该层名称、该层传播序号、该层的激活函数
	local MyLayers = {
		LuatAIgeneralNN.layers.Create_Layer("InputLayer", 1, LuatAIgeneralNN.activations.relu),
		LuatAIgeneralNN.layers.Create_Layer("HiddenLayer", 2, LuatAIgeneralNN.activations.relu),
		LuatAIgeneralNN.layers.Create_Layer("OutLayer", 3, LuatAIgeneralNN.activations.softmax)
	}

	log.info('generalNNconstructed!')
	--Create_neuronal_network()函数创建神经网络骨干，两个参数为 该骨干名称、该骨干包含的神经元层
	local MyNeuralNetwork = LuatAIgeneralNN.Create_neuronal_network("generalnn", MyLayers)

	--生物上，labelsY为1或0，也就是模拟生物神经元，在上一层几个神经元的共同作用下，该神经元是否激活
	--但无论labelsY元素的数量和数值如何，featuresX经神经网络处理后的数据，总能反应featuresX数据间的关系
	--即输入数据0对应激活后的值总相同，输入数据1对应激活后的值也相同，但两者分别的对应值不同，且存在规律
	--从中可以理解，神经网络实质就是编码器
	--生物神经元也是如此
	local featuresX  = {0,1,1,0,1,1}
	local labelsY = {1,0} 

	--训练构建的神经网络
	--Train()函数参数有5个，分别为 输入数据、标签数据、Adam优化器、训练次数、是否打印输出结果，注意，目前只有Adam优化器可以使用
	--Train()函数训练结果返回的就是输入数据中每个元素经神经网络处理后的激活值
	--特征数据的元素个数即为输入（刺激）神经元数量，标签数据的元素个数即为受作用（反应）神经元数量
	--若标签数据元素有几个，意为多个输入神经元对多个受作用神经元产生影响
	--但Train()函数返回结果的个数依然为输入数据（特征数据）数量
	--可理解为，某1个输入神经元，对所有受作用神经元的效用之和
	local result = MyNeuralNetwork.Train(featuresX, labelsY, LuatAIgeneralNN.optim.Adam, 20, true)
	for k,v in ipairs(result) do
        print(k,v)
	end

	log.info('generalNNTrained!')

	--使用训练后的神经网络
	--run()函数参数有2个，第1个参数为输入的特征数据，第2个参数有两种用法，返回结果与Train()函数一致
	--一种为构建的神经网络骨干名称，另一种为神经网络骨干中的某一神经元层，可以查看某特定神经元层的输出情况


	--local guess = MyNeuralNetwork.run(featuresX,"generalnn")
	local guess = MyNeuralNetwork.run(featuresX,MyNeuralNetwork.layers[2])
	for k,v in ipairs(guess) do
        print(k,v)
	end


	log.info('generalNNtested!')


end


sys.taskInit(function()
	log.info('Hello,LuatAI!')
	generalNNTest()
end)

sys.init(0, 0)
sys.run()