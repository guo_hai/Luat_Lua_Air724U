---@diagnostic disable: lowercase-global, undefined-global
module(..., package.seeall)

-- require "LCD"
require "mipi_lcd_GC9503"
-- 触摸屏
require "tp"

module(..., package.seeall)

local aligntype = {lvgl.ALIGN_CENTER,lvgl.ALIGN_IN_LEFT_MID,lvgl.ALIGN_IN_RIGHT_MID,lvgl.ALIGN_IN_TOP_MID,lvgl.ALIGN_IN_TOP_LEFT,lvgl.ALIGN_IN_TOP_RIGHT,lvgl.ALIGN_IN_BOTTOM_MID,lvgl.ALIGN_IN_BOTTOM_LEFT,lvgl.ALIGN_IN_BOTTOM_RIGHT}
local Nanum = {0,1,2,3,4,5,6,7,8,9,10}
local sign = false
local delpara = 0
local orien = false
local sym = {
    "\xef\x80\x81",
    "\xef\x80\x88",
    "\xef\x80\x8b",
    "\xef\x80\x8c",
    "\xef\x80\x8d",
    "\xef\x80\x91",
    "\xef\x80\x93",
    "\xef\x80\x95",
    "\xef\x80\x99",
    "\xef\x80\x9c",
    "\xef\x80\xa1",
    "\xef\x80\xa6",
    "\xef\x80\xa7",
    "\xef\x80\xa8",
}

local longmode ={
    lvgl.LABEL_LONG_EXPAND,
    lvgl.LABEL_LONG_BREAK,
    lvgl.LABEL_LONG_CROP,
    lvgl.LABEL_LONG_DOT,
    lvgl.LABEL_LONG_SROLL,
    lvgl.LABEL_LONG_SROLL_CIRC
    }

-- 图表
function demo_chart()
    if (not lvgl.chart_create) then
        log.info("提示", "不支持 图表 控件")
        return
    end
    -- 创建图表
    chart = lvgl.chart_create(lvgl.scr_act(), nil)
    lvgl.obj_set_size(chart, 256, 256)
    lvgl.obj_align(chart, nil, lvgl.ALIGN_CENTER, 0, 0)

    -- 设置 Chart 的显示模式 (折线图)
    lvgl.chart_set_type(chart, lvgl.CHART_TYPE_LINE)

    ser1 = lvgl.chart_add_series(chart, lvgl.color_hex(0xFF0000))
    ser2 = lvgl.chart_add_series(chart, lvgl.color_hex(0x008000))

    -- 添加点
    for i = 0, 15 do lvgl.chart_set_next(chart, ser1, i * 10) end
    for i = 15, 0, -1 do lvgl.chart_set_next(chart, ser2, i * 10) end

    -- 刷新图表
    lvgl.chart_refresh(chart)

    -- sys.wait(3000)

    sys.taskInit(function() 
        sys.wait(3000)
        if delpara == Nanum[1]+1 then
            lvgl.chart_set_point_id(chart, ser1, 0, 5)
        end
        sys.wait(3000)
        if delpara == Nanum[1]+1 then
            lvgl.chart_init_points(chart, ser1, 50)
            lvgl.chart_refresh(chart)
        end
        sys.wait(3000)
        if delpara == Nanum[1]+1 then
            lvgl.chart_set_type(chart, lvgl.CHART_TYPE_COLUMN)
            lvgl.chart_refresh(chart)
        end
    end)
    -- 清除对象
    -- lvgl.obj_del(chart)
end

--初始化
function demo_init()
        if (not lvgl.scr_act) then
            log.info("提示", "不支持 下拉框 控件")
            return
        end
        event_dd = function(obj, event)
            if (event == lvgl.EVENT_VALUE_CHANGED) then
                if delpara ~=0 then
                    if delpara == Nanum[1]+1 then
                        lvgl.obj_del(chart)
                        log.info("删除图表")
                    elseif delpara == Nanum[2]+1 then
                        if orien then
                            lvgl.obj_del(opendire)
                        else
                            lvgl.obj_del(dd)
                        end

                        log.info("删除下拉框")
                    elseif delpara == Nanum[3]+1 then
                        if keyBoard then
                            lvgl.obj_del(keyBoard)
                            keyBoard = nil
                        end
                        lvgl.obj_del(textarea)
                        log.info("删除输入框")
                    elseif delpara == Nanum[4]+1 then
                        lvgl.obj_del(longmodea)
                        lvgl.obj_del(label)
                        log.info("删除标签")
                    elseif delpara == Nanum[5]+1 then
                        lvgl.obj_del(img)
                        log.info("删除图片")
                    else
                        lvgl.obj_del(page)
                        log.info("删除页面")
                    end
                end

                if sign then
                    lvgl.obj_align(abc, nil, aligntype[5], 10, 20)
                    sign = false
                end
            end

            if (event == lvgl.EVENT_VALUE_CHANGED) then
                -- print("Option:", lvgl.dropdown_get_symbol(obj))
                print("测试",lvgl.dropdown_get_selected(obj))
                if lvgl.dropdown_get_selected(obj) == Nanum[1] then
                    -- demo_dd()
                    demo_chart()
                    delpara = Nanum[1]+1
                elseif lvgl.dropdown_get_selected(obj) == Nanum[2] then
                    demo_dd()
                    delpara = Nanum[2]+1
                elseif lvgl.dropdown_get_selected(obj) == Nanum[3] then
                    demo_textarea()
                    delpara = Nanum[3]+1
                elseif lvgl.dropdown_get_selected(obj) == Nanum[4] then
                    demo_label()
                    delpara = Nanum[4]+1
                elseif lvgl.dropdown_get_selected(obj) == Nanum[5] then
                    demo_img()
                    delpara = Nanum[5]+1
                else
                    demo_page()
                    delpara = Nanum[6]+1
                end
                -- sign = 1
                -- log.info("标志")
            end

        end
    
        -- 创建下拉框
        abc = lvgl.dropdown_create(lvgl.scr_act(), nil)
        lvgl.dropdown_set_options(abc, [[图表
下拉框
键盘
标签
图片
页面
]])
        -- 设置对齐
        -- lvgl.obj_align(dd, nil, lvgl.ALIGN_IN_TOP_MID, 10, 20)
        lvgl.obj_align(abc, nil, aligntype[5], 10, 20)
        lvgl.obj_set_event_cb(abc, event_dd)
    
        -- sys.wait(3000)
        -- 清除对象
        -- lvgl.obj_del(dd)
end

-- 下拉框
function demo_dd()
    if (not lvgl.scr_act) then
        log.info("提示", "不支持 下拉框 控件")
        return
    end
    event_dd = function(obj, event)
        if (event == lvgl.EVENT_VALUE_CHANGED) then
            if sign and not (lvgl.dropdown_get_selected(obj) == Nanum[5]) then 
                lvgl.obj_align(abc, nil, aligntype[5], 10, 20)
                sign =false
            end
            -- print("Option:", lvgl.dropdown_get_symbol(obj))
            print("测试",lvgl.dropdown_get_selected(obj))
            if lvgl.dropdown_get_selected(obj) == Nanum[1] then
                lvgl.obj_align(dd, nil, aligntype[1], 10, 20)
            elseif lvgl.dropdown_get_selected(obj) == Nanum[2] then
                lvgl.obj_align(dd, nil, aligntype[2], 10, 20)
            elseif lvgl.dropdown_get_selected(obj) == Nanum[3] then
                lvgl.obj_align(dd, nil, aligntype[3], 10, 20)
            elseif lvgl.dropdown_get_selected(obj) == Nanum[4] then
                lvgl.obj_align(dd, nil, aligntype[4], 10, 20)
            elseif lvgl.dropdown_get_selected(obj) == Nanum[5] then
                lvgl.obj_align(dd, nil, aligntype[5], 10, 20)
                sign = true
                lvgl.obj_align(abc, nil, aligntype[2], 10, 20)
            elseif lvgl.dropdown_get_selected(obj) == Nanum[6] then
                lvgl.obj_align(dd, nil, aligntype[6], 10, 20)
            elseif lvgl.dropdown_get_selected(obj) == Nanum[7] then
                lvgl.obj_align(dd, nil, aligntype[7], 0, 0)
            elseif lvgl.dropdown_get_selected(obj) == Nanum[8] then
                lvgl.obj_align(dd, nil, aligntype[8], 0, 0)
            elseif lvgl.dropdown_get_selected(obj) == Nanum[9] then
                lvgl.obj_align(dd, nil, aligntype[9], 0, 0)
            elseif lvgl.dropdown_get_selected(obj) == Nanum[10] then
                lvgl.obj_del(dd)
                orien = true
                demo_opendire()
            else
                sys.taskInit(function() 
                    for i=1, #sym do
                        lvgl.dropdown_set_symbol(dd, sym[i])
                        sys.wait(1000)
                        if delpara ~= Nanum[2]+1 then
                            break
                        end
                        if lvgl.dropdown_get_selected(obj) ~= Nanum[11] then
                            break
                        end
                    end
                end)
            end
        end
    end

    -- 创建下拉框
    dd = lvgl.dropdown_create(lvgl.scr_act(), nil)
    lvgl.dropdown_set_options(dd, [[正中间
左边
右边
顶部中间
左上
右上
底部中间
左下
右下
展开方向
小图标循环]])
    -- 设置对齐
    -- lvgl.obj_align(dd, nil, lvgl.ALIGN_IN_TOP_MID, 10, 20)
    lvgl.obj_align(dd, nil, aligntype[1], 10, 20)
    lvgl.obj_set_event_cb(dd, event_dd)

    -- sys.wait(3000)
    -- 清除对象
    -- lvgl.obj_del(dd)
end

function demo_opendire()

    event_opendire = function(obj, event)
        if (event == lvgl.EVENT_VALUE_CHANGED) then
            -- print("Option:", lvgl.dropdown_get_symbol(obj))
            print("测试",lvgl.dropdown_get_selected(obj))
            if lvgl.dropdown_get_selected(obj) == Nanum[1] then
                lvgl.dropdown_set_dir(opendire, lvgl.DROPDOWN_DIR_DOWN)
            elseif lvgl.dropdown_get_selected(obj) == Nanum[2] then
                lvgl.dropdown_set_dir(opendire, lvgl.DROPDOWN_DIR_UP)
            elseif lvgl.dropdown_get_selected(obj) == Nanum[3] then
                lvgl.dropdown_set_dir(opendire, lvgl.DROPDOWN_DIR_LEFT)
            elseif lvgl.dropdown_get_selected(obj) == Nanum[4] then
                lvgl.dropdown_set_dir(opendire, lvgl.DROPDOWN_DIR_RIGHT)
            end
        end
    end

    opendire = lvgl.dropdown_create(lvgl.scr_act(), nil)
    lvgl.dropdown_set_options(opendire, [[下边展开
上边展开
左边展开
右边展开]])
    lvgl.obj_align(opendire, nil, aligntype[1], 10, 20)
    lvgl.obj_set_event_cb(opendire, event_opendire)
    
end

-- 输入框
function demo_textarea()
    if (not lvgl.textarea_create) then
        log.info("提示", "不支持 输入框 控件")
        return
    end
    -- 按键回调
    event_key = function(obj, e)
        -- 默认处理事件
        lvgl.keyboard_def_event_cb(keyBoard, e)
        if (e == lvgl.EVENT_CANCEL) then
            lvgl.keyboard_set_textarea(keyBoard, nil)
            -- 删除 KeyBoard
            lvgl.obj_del(keyBoard)
            keyBoard = nil

        end
    end

    -- 输入框回调
    event_txt = function(obj, e)
        if (e == lvgl.EVENT_CLICKED) and not keyBoard then
            -- 创建一个 KeyBoard
            keyBoard = lvgl.keyboard_create(lvgl.scr_act(), nil)
            -- 设置 KeyBoard 的光标是否显示
            lvgl.keyboard_set_cursor_manage(keyBoard, true)
            -- 为 KeyBoard 设置一个文本区域
            lvgl.keyboard_set_textarea(keyBoard, textarea)
            lvgl.obj_set_event_cb(keyBoard, event_key)
        end
    end

    textarea = lvgl.textarea_create(lvgl.scr_act(), nil)
    lvgl.obj_set_size(textarea, 200, 50)
    lvgl.textarea_set_text(textarea, "please input:")
    lvgl.obj_align(textarea, DEMO_BASE_CONT, lvgl.ALIGN_CENTER, 0, -200)
    lvgl.obj_set_event_cb(textarea, event_txt)

end

-- 标签
function demo_label()
    if (not lvgl.label_create) then
        log.info("提示", "不支持 标签 控件")
        return
    end
    -- 创建标签
    label = lvgl.label_create(lvgl.scr_act(), nil)
    lvgl.label_set_text(label, "Hello World!")
    lvgl.label_set_align(label, lvgl.LABEL_ALIGN_CENTER)
    lvgl.obj_align(label, nil, lvgl.ALIGN_CENTER, 0, -40)
    demo_longmodea()


end

function testlabel()
    lvgl.label_set_recolor(label, true)
    lvgl.label_set_text(label,"#0000ff Re-color# #ff00ff words# #ff0000 of\n# align the lines to\n the center and wrap\n long text automatically.")
end

function demo_longmodea()

    event_longmodea = function(obj, event)
        if (event == lvgl.EVENT_VALUE_CHANGED) then
            -- print("Option:", lvgl.dropdown_get_symbol(obj))
            print("测试",lvgl.dropdown_get_selected(obj))
            if lvgl.dropdown_get_selected(obj) == Nanum[1] then
                lvgl.label_set_long_mode(label, lvgl.LABEL_LONG_BREAK)
            elseif lvgl.dropdown_get_selected(obj) == Nanum[2] then
                lvgl.label_set_long_mode(label, lvgl.LABEL_LONG_CROP)
            elseif lvgl.dropdown_get_selected(obj) == Nanum[3] then
                lvgl.label_set_long_mode(label, lvgl.LABEL_LONG_DOT)
            elseif lvgl.dropdown_get_selected(obj) == Nanum[4] then
                lvgl.label_set_long_mode(label, lvgl.LABEL_LONG_SROLL)
            else
                lvgl.label_set_long_mode(label, lvgl.LABEL_LONG_SROLL_CIRC)
            end
            lvgl.obj_set_width(label, 60)
            lvgl.label_set_text(label, "Hello World!")
        end
    end

    longmodea = lvgl.dropdown_create(lvgl.scr_act(), nil)
    lvgl.dropdown_set_options(longmodea, [[正常显示
超出截断
超出省略
左右滚动
循环滚动]])
    lvgl.obj_align(longmodea, nil, aligntype[4], 10, 20)
    lvgl.obj_set_event_cb(longmodea, event_longmodea)
    
end


function demo_page()
    if (not lvgl.page_create) then
        log.info("提示", "不支持 页面 控件")
        return
    end
    page = lvgl.page_create(lvgl.scr_act(), nil)
    lvgl.obj_set_size(page, 150, 200)
    lvgl.obj_align(page, nil, lvgl.ALIGN_CENTER, 0, 0)
    
    label = lvgl.label_create(page, nil)
    lvgl.label_set_long_mode(label, lvgl.LABEL_LONG_BREAK)            
    lvgl.obj_set_width(label, lvgl.page_get_width_fit(page)) 
            
    lvgl.label_set_text(label, 
    [[Lorem ipsum dolor sit amet, consectetur adipiscing elit,
    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
    Ut enim ad minim veniam, quis nostrud exercitation ullamco
    laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure
    dolor in reprehenderit in voluptate velit esse cillum dolore
    eu fugiat nila pariatur.
    Excepteur sint occaecat cupidatat non proident, sunt in culpa
    qui officia deserunt mollit anim id est laborum.]])
end

function demo_img()
    if (not lvgl.img_create ) then
        log.info("提示","不支持 图片 控件")
    end

    img = lvgl.img_create(lvgl.scr_act(), nil)
    -- 设置图片显示的图像
    lvgl.img_set_src(img, "/lua/Luat.png")
    -- 图片居中
    lvgl.obj_align(img, nil, lvgl.ALIGN_CENTER, 0, 0)
end

lvgl.init(function() end, tp.input)

sys.taskInit(function()
    sys.wait(3000)
        demo_init()
end)
