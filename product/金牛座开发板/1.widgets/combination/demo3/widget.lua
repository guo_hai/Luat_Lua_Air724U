---@diagnostic disable: lowercase-global, undefined-global

require "mipi_lcd_GC9503"
require "tp"

module(..., package.seeall)

lcd={
    width=480,
    height=854
}


-- 容器
function demo_cont()
    
    -- 创建容器
    cont = lvgl.cont_create(lvgl.scr_act(), nil)
    lvgl.obj_set_auto_realign(cont, true)
    lvgl.obj_align(cont, nil, lvgl.ALIGN_CENTER, 0, 0)
    lvgl.cont_set_fit(cont, lvgl.FIT_NONE)
    lvgl.cont_set_layout(cont, lvgl.LAYOUT_COLUMN_MID )
    lvgl.obj_set_size(cont,lcd.width,lcd.height)
end

function arc()
    -- 创建曲线
    arc = lvgl.arc_create(cont, nil)
    -- 设置尺寸
    lvgl.obj_set_size(arc, g_w or 200, 200)
    -- 设置位置居中
    lvgl.obj_align(arc, nil, lvgl.ALIGN_CENTER, 0, 0)
    -- 绘制弧度
    lvgl.arc_set_range(arc,0,100)
    lvgl.arc_set_end_angle(arc, 0)
    lvgl.arc_set_adjustable(arc, true)

    label = lvgl.label_create(arc, nil)
    lvgl.label_set_text(label, "曲线")
    event_handler = function(obj, event)
        if event == lvgl.EVENT_VALUE_CHANGED then
            
            local value=lvgl.arc_get_value(obj)/100
            lvgl.label_set_text(label, value)
            print(value)
            lvgl.obj_set_size(calendar, lcd.width*value/2, lcd.width*value/2)
            print(lcd_width)
        end
    end

lvgl.obj_set_event_cb(arc, event_handler)
end
function btn()
    -- 按键回调函数
    event_btn = function(obj, event)
        if event == lvgl.EVENT_CLICKED then
            print("Clicked\n")
        elseif event == lvgl.EVENT_VALUE_CHANGED then
            print("Toggled\n")
        end
    end

    -- 按键1
    btn1 = lvgl.btn_create(cont, nil)
    lvgl.obj_set_event_cb(btn1, event_btn)
    lvgl.obj_align(btn1, nil, lvgl.ALIGN_CENTER, 0, -40)
    -- 按键1 的文字
    label = lvgl.label_create(btn1, nil)
    lvgl.label_set_text(label, "Button")
    -- 按键2
    btn2 = lvgl.btn_create(cont, nil)
    lvgl.obj_set_event_cb(btn2, event_btn)
    lvgl.obj_align(btn2, nil, lvgl.ALIGN_CENTER, 0, 40)
    lvgl.btn_set_checkable(btn2, true)
    lvgl.btn_toggle(btn2)
    lvgl.btn_set_fit2(btn2, lvgl.FIT_NONE, lvgl.FIT_TIGHT)
    -- 按键2 的文字
    label = lvgl.label_create(btn2, nil)
    lvgl.label_set_text(label, "Toggled")
end
function calendar()
    -- 添加标签
    label = lvgl.label_create(calendar_, nil)
    lvgl.label_set_text(label, "日历")
    -- 高亮显示的日期
    highlightDate = lvgl.calendar_date_t()

    -- 日历点击的回调函数
    -- 将点击日期设置高亮
    event_calendar = function(obj, event)
        if event == lvgl.EVENT_VALUE_CHANGED then
            date = lvgl.calendar_get_pressed_date(obj)
            if date then
                print(string.format("Clicked date: %02d.%02d.%d\n", date.day,
                                    date.month, date.year))
                highlightDate.year = date.year
                highlightDate.month = date.month
                highlightDate.day = date.day
                lvgl.calendar_set_highlighted_dates(obj, highlightDate, 1)
            end
        end
    end

    -- 创建日历
    calendar = lvgl.calendar_create(cont, nil)
    
    lvgl.obj_align(calendar, nil, lvgl.ALIGN_CENTER, 0, 0)
    lvgl.obj_set_event_cb(calendar, event_calendar)

    -- 设置今天日期
    today = lvgl.calendar_date_t()
    today.year = 2021
    today.month = 10
    today.day = 28

    lvgl.calendar_set_today_date(calendar, today)
    lvgl.calendar_set_showed_date(calendar, today)

end
function bar()
    -- 创建进度条
    bar = lvgl.bar_create(cont, nil)
    -- 设置尺寸
    lvgl.obj_set_size(bar, 200, 20);
    -- 设置位置居中
    lvgl.obj_align(bar, NULL, lvgl.ALIGN_CENTER, 0, 0)
    -- 设置加载完成时间
    lvgl.bar_set_anim_time(bar, 2000)
    -- 设置加载到的值
    lvgl.bar_set_value(bar, 100, lvgl.ANIM_ON)
end
function checkbox()
    -- 复选框回调函数
    event_cb = function(obj, event)
        if event == lvgl.EVENT_VALUE_CHANGED then
            print("State", lvgl.checkbox_is_checked(obj))
        end
    end
    -- 创建复选框
    cb = lvgl.checkbox_create(cont, nil)
    -- 设置标签
    lvgl.checkbox_set_text(cb, "hello")
    -- 设置居中位置
    lvgl.obj_align(cb, nil, lvgl.ALIGN_CENTER, 0, 0)
    -- 设置回调函数
    lvgl.obj_set_event_cb(cb, event_cb)
end


lvgl.init(demo_cont, tp.input)
checkbox()
arc()
btn()
bar()
calendar()
