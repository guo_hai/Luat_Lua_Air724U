---@diagnostic disable: lowercase-global, undefined-global
module(...,package.seeall)

require "mipi_lcd_GC9503"
--触摸屏
require "tp"

seq=0
lock=false
MAXTIME=30000
MINTIME=1000
XP=20000

function getY(ix)
	if ix==0 then return 0 end
	return MAXTIME-(((MAXTIME-MINTIME)/XP)*ix)
end

function changeSpeed()
	local timesed =getY(seq)
	local val = (seq==0 and "0.00" or string.format("%.2f", 60000/timesed)).." r/min"
	lvgl.label_set_text(slider_label, val)
	lvgl.slider_set_value(slider, seq, lvgl.ANIM_OFF)
	lvgl.spinbox_set_value(spinbox,seq)
	if lock then 
		lvgl.spinner_set_spin_time(spinner,timesed)
	else
		lvgl.spinner_set_spin_time(spinner,0)
	 end 
end

----------------------------------------------------------------- 滑动条
function sliderInit()
-- 回调函数
slider_event_cb = function(obj, event)
    if event == lvgl.EVENT_VALUE_CHANGED then 
		seq=lvgl.slider_get_value(obj)
		changeSpeed()
    end
end

-- 创建滑动条
slider = lvgl.slider_create(win, nil)
lvgl.obj_set_size(slider, 350, 20)
lvgl.slider_set_range(slider, 0, XP)
lvgl.obj_align(slider, spinner, lvgl.ALIGN_CENTER, 0, 0)
lvgl.obj_set_event_cb(slider, slider_event_cb)

-- 创建标签
slider_label = lvgl.label_create(win, nil)
lvgl.label_set_text(slider_label, "0.00 r/min")
lvgl.obj_align(slider_label, slider, lvgl.ALIGN_OUT_BOTTOM_MID, 0, 10)
end
-----------------------------------------------------------------------  加载框

function spinnerInit()
	spinner = lvgl.spinner_create(win, nil)
	lvgl.spinner_set_type(spinner, lvgl.SPINNER_TYPE_CONSTANT_ARC)
	lvgl.obj_set_size(spinner, 450, 450)
	lvgl.spinner_set_arc_length(spinner, 60)
	lvgl.spinner_set_spin_time(spinner, 0)
	lvgl.obj_align(spinner,  nil, lvgl.ALIGN_IN_TOP_MID, 0, 20) 
end
--------------------------------------------------------------------------  switch

function event_handler(obj, event)
    if event == lvgl.EVENT_VALUE_CHANGED then
		if lvgl.switch_get_state(obj) then
			lock=true
		else
			lock=false
		end
		changeSpeed()
    end
end

function switchInit()
sw1 = lvgl.switch_create(win, nil)
lvgl.obj_set_size(sw1, 150, 40)
lvgl.obj_align(sw1,spinner, lvgl.ALIGN_CENTER, 0, 100)
lvgl.obj_set_event_cb(sw1, event_handler)
end
------------------------------------------------------------------------- 微调框

-- 回调函数
function spinbox_increment_event_cb(obj, event)
    if event == lvgl.EVENT_SHORT_CLICKED then
        lvgl.spinbox_increment(spinbox)
		seq=lvgl.spinbox_get_value(spinbox)
		changeSpeed()
    end
end

function spinbox_decrement_event_cb(obj, event)
    if event == lvgl.EVENT_SHORT_CLICKED then
        lvgl.spinbox_decrement(spinbox)
		seq=lvgl.spinbox_get_value(spinbox)
		changeSpeed()
    end
end

-- 创建按钮
function cBt(cont, txt, cb)
    local btn = lvgl.btn_create(cont, nil)
	if txt=="speed+" then 
		lvgl.obj_align(btn, cont, lvgl.ALIGN_IN_BOTTOM_RIGHT, 0, 0)
	else
		lvgl.obj_align(btn, cont, lvgl.ALIGN_IN_BOTTOM_LEFT, 0, 0)
	end
	lvgl.obj_set_size(btn, 90, 40)
    lvgl.obj_set_event_cb(btn, cb) 
    local label = lvgl.label_create(btn, nil)
    lvgl.label_set_text(label, txt)
end

function spinboxInit()
-- 容器
cont = lvgl.cont_create(win, nil)
lvgl.obj_set_size(cont, 400, 80)
lvgl.obj_align(cont, win, lvgl.ALIGN_CENTER, 0, 340)
lvgl.cont_set_layout(cont, lvgl.LAYOUT_PRETTY_MID)

-- -- 按钮一
cBt(cont, "speed-", spinbox_decrement_event_cb)

-- 微调框
spinbox = lvgl.spinbox_create(cont, nil)
lvgl.obj_align(spinbox, cont, lvgl.ALIGN_CENTER, 0, 100)
lvgl.obj_set_size(spinbox, 100, 30)
lvgl.spinbox_set_range(spinbox, 0, XP)
local f=1
local xpt=XP
while true do 
	xpt=xpt/10
	if xpt<1 then break end
	f=f+1
end
lvgl.spinbox_set_digit_format(spinbox, f, 0)

-- 按钮二
cBt(cont, "speed+", spinbox_increment_event_cb)
end
------------------------------------------------------------------------- window

function winInit()
	win = lvgl.win_create(lvgl.scr_act(), nil)
	lvgl.win_set_title(win, "Window")                     
	lvgl.win_set_drag(win, true)
end

-------------------------------------------------------------------------

lvgl.init(winInit, tp.input)
spinnerInit()
sliderInit()
switchInit()
spinboxInit()