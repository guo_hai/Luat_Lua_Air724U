---@diagnostic disable: lowercase-global, undefined-global
module(...,package.seeall)

--LCD
require "mipi_lcd_GC9503"
--触摸屏
require "tp"

local data = {type = lvgl.INDEV_TYPE_POINTER}
local function input()
	pmd.sleep(100)
	local ret,ispress,px,py = tp.get()
	if ret then
		if lastispress == ispress and lastpx == px and lastpy == py then
			return data
		end
		lastispress = ispress
		lastpx = px
		lastpy = py
		if ispress then
			tpstate = lvgl.INDEV_STATE_PR
		else
			tpstate = lvgl.INDEV_STATE_REL
		end
	else
		return data
	end

	local topoint = {x = px,y = py}
	data.state = tpstate
	data.point = topoint

	return data
end

function demo_WindowInit()
	win = lvgl.win_create(lvgl.scr_act(), nil)
	lvgl.win_set_title(win, "Window title")                     
	txt = lvgl.label_create(win, nil)
	lvgl.label_set_text(txt, "This is the content of the window\n\n")
	
	lvgl.win_set_drag(win, true)
end


local function init()
	lvgl.init(demo_WindowInit, input)
	pmd.ldoset(8,pmd.LDO_VIBR)
end

sys.taskInit(init, nil)