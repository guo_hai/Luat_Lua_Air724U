---@diagnostic disable: lowercase-global, undefined-global
module(...,package.seeall)

local function lessLife()
	if not status.protect and not status.god then 
		status.life=status.life-1
	end
end
_G.status={
	NOMAL={"me.bin","me2.bin"},
    BREAK={"mebk1.bin",
        "mebk2.bin",
        "mebk3.bin",
        "mebk4.bin",
    },
	width=102,
	height=126,
	life=1,
	protect=true,
	less=lessLife,
	score=0,
	scoreMax=0,
	god=false
}
local lock=false
function keyMsg(msg)
	if msg.key_matrix_row..msg.key_matrix_col=="255255" and lock then
		if msg.pressed then
			status.god = not status.god
			if status.god then 
				lvgl.label_set_text(god, "上帝模式")
			else
				lvgl.label_set_text(god, "")
			 end
		end
	elseif msg.key_matrix_row..msg.key_matrix_col=="40" then
		lock=msg.pressed
	elseif msg.key_matrix_row .. msg.key_matrix_col == "00" then
			if msg.pressed then
				if ProjectSwitch.ProjectSwitchBox then
					sys.publish("projectSwitchUnInit")
					return
				end
				t = sys.timerStart(function()
					sys.publish("projectSwitchInit")
				end, 1000)
			else
				print("放开")
				if sys.timerIsActive(t) then sys.timerStop(t) end
			end
	end
end
sys.taskInit(function()
	rtos.on(rtos.MSG_KEYPAD,keyMsg)
	rtos.init_module(rtos.MOD_KEYPAD,0,0xff,0xff)
end)

function init()
	_G.plane=lvgl.img_create(lvgl.scr_act(), nil)
	status.protect=true
	status.life=1
	lvgl.label_set_text(label, "当前分数："..status.score.."\n最高分数："..status.scoreMax)
	lvgl.img_set_src(plane,"/lua/"..status.NOMAL[1] )
	lvgl.obj_align(plane, backImg, lvgl.ALIGN_IN_BOTTOM_MID, 0, 0)
	sys.timerStart(function() status.protect=false end,2000)
	-- lvgl.obj_set_drag(plane, true)
end

function touchMove()
	sys.taskInit(function()
		local index=0
		local count=0
		while true do
			if not ProjectSwitch.ProjectSwitchBox then
				if status.life >0 then
					if count==0 then 
						lvgl.img_set_src(plane,"/lua/"..status.NOMAL[index+1] )
						index=index+1
						index=index%2
					end
					count=count+1
					count=count%8
					local ret,ispress,px,py = tp.get()
					if ret and ispress then
						lvgl.obj_set_pos(plane, px-(status.width/2),py-status.height+20)
					end
				
				end
			end
			sys.wait(5)
		end
	end)
end



local function makeBullet()
	sys.taskInit(function()
	local bb=lvgl.img_create(lvgl.scr_act(), nil)
	local life=true
	lvgl.img_set_src(bb,"/lua/bb.bin" )
	lvgl.obj_set_pos(bb, lvgl.obj_get_x(plane)+51,lvgl.obj_get_y(plane))
	while lvgl.obj_get_y(bb)>0 do
		if not ProjectSwitch.ProjectSwitchBox then 
			lvgl.obj_set_pos(bb, lvgl.obj_get_x(bb),lvgl.obj_get_y(bb)-20)
			for k, v in ipairs(enemyTable) do
				local c=G_util.coverTest(bb,v[2])
				if c and status.life>0 then
					 v[1]=v[1]-1 
					 life=false
					 break
				end
			end
		end

		if life then 
			sys.wait(20)
		else
			break
		end

	end
	lvgl.obj_del(bb)
	end)
end

function breakMe() 
	-- if status.score>status.scoreMax then status.scoreMax=status.score end
	status.score=0
	-- lvgl.label_set_text(label, "分数："..status.score)
	for k, v in ipairs(status.BREAK) do
		lvgl.img_set_src(plane, "/lua/"..v)
		sys.wait(150)
	end
	lvgl.obj_del(plane)
	sys.wait(2000)
	init()
end

function shoot()
	sys.taskInit(function()
		while true do
			if not ProjectSwitch.ProjectSwitchBox then
				if status.life>0 then
					makeBullet()
				else
					breakMe()
				end
			end
			sys.wait(180)
		end
	end)
end