module(..., package.seeall)

function event_handler(obj, event)
    local btnmatrix = lvgl.msgbox_get_btnmatrix(obj)
    if (event == lvgl.EVENT_VALUE_CHANGED) then
        -- 按键文字内容
        local txt = lvgl.btnmatrix_get_active_btn_text(btnmatrix)
        -- 按键id，矩阵键盘数组的下标
        local temp = lvgl.btnmatrix_get_active_btn(btnmatrix)
        print("矩阵键盘默认回调")
        print(txt)
        print(temp)
        -- 把自动关闭时间设置为0，0秒后关闭消息框
        -- lvgl.msgbox_start_auto_close(obj,0)
    end
end

function create(p, v)
    local obj = lvgl.msgbox_create(p, nil)
    lvgl.obj_set_size(obj, v.W or 400, v.H or 800)
    -- 消息框文字内容
    if v.msg_text then lvgl.msgbox_set_text(obj, v.msg_text) end
    -- 消息框矩阵键盘，数组
    if v.btns then lvgl.msgbox_add_btns(obj, v.btns) end
    -- 设置动画时间
    lvgl.msgbox_set_anim_time(obj, v.anim_time or 1000)
    -- 设置回调事件
    if v.event then lvgl.obj_set_event_cb(obj, v.event) end
    -- 自动关闭时间
    if v.auto_close then lvgl.msgbox_start_auto_close(obj, v.auto_close) end

    -- 消息框的主要部分称为 lvgl.MSGBOX_PART_MAIN ，它使用所有典型的背景样式属性。
    -- 使用填充会增加侧面的空间。pad_inner将在文本和按钮之间添加空格。标签样式属性会影响文本样式。
    -- 按钮部分与 按钮矩阵(lvgl.imgbtn) 的情况相同：
    -- lvgl.MSGBOX_PART_BTN_BG 按钮的背景
    -- lvgl.MSGBOX_PART_BTN 按钮，单个按钮样式
    local style = {}
    if v.style then style = v.style end
    if style.main then
        lvgl.obj_add_style(obj, lvgl.MSGBOX_PART_BG,
                           create_style(style.main))
    end

    if style.btn then
        lvgl.obj_add_style(obj, lvgl.MSGBOX_PART_BTN,
                           create_style(style.btn))
    end
    -- 矩阵键盘容器
    -- 透明度默认为0
    if style.main_btn then
        lvgl.obj_add_style(obj, lvgl.MSGBOX_PART_BTN_BG,
                           create_style(style.main_btn))
    end

    lvgl.obj_align(obj, v.align_to, v.align or lvgl.ALIGN_CENTER,
                   v.align_x or 0, v.align_y or 0)

    return obj
end
msg_box_data = {
    btns = {"Apple", "Close", "Close", "Close", "Close", ""},
    msg_text = "密码错误\n",
    align = lvgl.ALIGN_CENTER,
    anim_time = 1000,
    style = {
        main = {
            border = {color = 0x0f0f0f, width = 2, opa = 200},
            shadow = {spread = 10, color = 0xff00f0},
            text = {font = style.font48, color = 0xff0000, opa = 80},
            bg = {
                radius = 10,
                color = 0x0f0ff0,
                opa = 150,
                grad = {color = 0x0f0f0f}
            }
        },
        btn = {
            border = {color = 0x0f0f0f, width = 2, opa = 200},
            shadow = {spread = 10, color = 0xff00f0},
            text = {font = style.font24, color = 0xff0000, opa = 80},
            bg = {
                radius = 10,
                color = 0xffcccc,
                opa = 150,
                grad = {color = 0xff0f0f}
            }
        },
        main_btn = {
            border = {color = 0x0f0f0f, width = 2, opa = 200},
            shadow = {spread = 10, color = 0xff00f0},
            bg = {
                radius = 10,
                color = 0xccccff,
                opa = 150,
                grad = {color = 0xccccff}
            }
        }
    }
}
-- create(lvgl.scr_act(), msg_box_data)

