-- 定位里的 RTK 页面
module(..., package.seeall)

-- RTK 定位方式
function printGps()

    if tools.testMode then 
        page.loc1 = "RTK状态:".."18002"
        page.loc2 = "RTK类型:".."5"
        page.loc3 = "差分时间:".."0"
        page.loc4 = "参考站:".."0"
        page.loc5 = "RTK经度:".."121.6009941"
        page.loc6 = "RTK纬度:".."31.1755902"
        return 
    end

    -- 1.定位时间,2.纬度,3.经度,4.定位类型,5.卫星个数,6.水平精度因子,7.海拔高度,8.异常差值,9.差分时间,10.参考站
    local status = tools.rtkStatus or 0
    local t = tools.rtkTab or {"无","无","无","无","无","无","无","无","无","无",}
    page.loc1 = "RTK状态:"..(tools.isRtk and status or "打开失败")
    page.loc2 = "RTK类型:"..(t[4] or "")
    page.loc3 = "差分时间:"..(t[9] or "")
    page.loc4 = "参考站:"..(t[10] or "")
    page.loc5 = "RTK经度:"..(t[3] or "")
    page.loc6 = "RTK纬度:"..(t[2] or "")
end

function enter()
    printGps()
    sys.timerLoopStart(printGps, 2000)
end

function exit()
    sys.timerStop(printGps)
end


-- 上一页
function keyUpShort() 
    airui.show("locMsg")
end

-- 下一页
function keyDownShort() 
    airui.show("locGps")
end
