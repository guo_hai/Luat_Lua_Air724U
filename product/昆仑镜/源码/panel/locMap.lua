-- 地图页面
module(..., package.seeall)

map0 = "/map0.jpg"
map1 = "/map1.jpg"
blank = "/lua/blank.jpg"

local step = 1
local mapType = 0
local lastX = 0
local lastY = 0
local ent = false
local lastTime = 0

function showMap()
    if tools.testMode then
        page.mapOpa = 255
        page.mapTxt = " "
        return
    end

    -- 网络检查
    if not socket.isReady() then
        page.mapOpa = 30
        page.mapTxt = "无网络连接"
        return 
    end
    -- 开启检查
    if not (gps and gps.isOpen()) then
        page.mapOpa = 30
        page.mapTxt = "GPS 未打开"
        return 
    end
    -- 定位成功检查
    if not gps.isFix() then
        page.mapOpa = 30
        page.mapTxt = "GPS 未定位"
        return 
    end
    -- 距离上次成功时间要大于 60s
    local ntime = os.time() or 0
    if ntime-lastTime<60 then
        return
    end
    -- 请求图片数据
    local t = gps.getLocation() or {}
    if t.lng and t.lat and #t.lng>0 and #t.lat>0 then
        sys.publish("LOC_CHANGE", t.lng, t.lat)
    end
end

function enter()
    ent = true 
    mapType = 0
    lastX = 0
    lastY = 0
    page.mapOpa = 255
    page.mapTxt = " "
    if gps then
        gps.open(gps.DEFAULT, {
            tag = "DEF"
        })   
    end
    showMap()
    sys.timerLoopStart(showMap, 1000)
    setSrc()
end

function exit()
    ent = false
    sys.timerStop(showMap)
end

function keyUpShort()
    mapType = mapType==0 and 1 or 0
    setSrc()
end

function keyDownShort()
    mapType = mapType==0 and 1 or 0
    setSrc()
end

-- 简单校验是否为 jpg
function chkJpg(f)
    if not io.exists(f) then
        return false
    end
    local size = io.fileSize(f)
    if not size or size < 10 then
        return false
    end
    local dat = io.readStream(f, 0, 2)
    if dat ~= "\xFF\xD8" then
        return false
    end
    return true
end

function setSrc()
    local val = (mapType==0) and map0 or map1
    if not io.exists(map0) or not io.exists(map1) then
        if mapType==0 then
            page.mapSrc = page.src.map0
        else
            page.mapSrc = page.src.map1
        end
        return 
    end
    -- 简单校验下载的 jpg 图片是否正确
    if not chkJpg(val) then
        page.mapSrc = blank
        return
    end
    -- lvgl 特性, 图片内容发生改变, 名称不变, 不会重置数据
    -- 所以先显示一张 白图, 再显示 目标
    if val == page.mapSrc then
        page.mapSrc = blank
    end
    page.mapSrc = val
end

local lock = false
sys.subscribe("LOC_CHANGE", function(x, y)
    if not ent then return end
    -- 坐标点一样就不要请求了
    if x==lastX and y==lastY then
        return
    end
    -- 锁起task
    if lock then return end
    lock = true
    sys.taskInit(function()
        -- 请求未完毕退出页面就会被销毁
        -- 需要查看页面是否退出, 决定是否继续
        if not ent then return end
        page.mapOpa = 30
        page.mapTxt = "图片信息请求中"
        local f = tools.getMap(x, y, map0, map1)
        -- 请求完后看看是否还在当前页面
        if not ent then return end
        if f then
            setSrc()
            page.mapOpa = 255
            page.mapTxt = " "
            lastX = x
            lastY = y
            lastTime = os.time() or 0
        else
            page.mapOpa = 30
            page.mapTxt = "图片请求失败"
        end
        lock = false
    end)
end)
