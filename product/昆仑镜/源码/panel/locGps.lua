-- 定位里的 星图 页面
module(..., package.seeall)

-- 文字闪烁
txtOpa = 255

-- 计算坐标
function gsvP(elv, az, R)
    R = R or 180
    r = 120
    elv = tonumber(elv)
    az = tonumber(az)
    if not (elv and az) then
        return
    end
    local A = math.rad(450 - az)
    local B = math.cos(math.rad(elv)) * R / 2
    return r + math.cos(A) * B, r - math.sin(A) * B
end

-- 显示文字标签
local function rotate()
    local tGP
    local tGB
    if tools.testMode then
        tGP = tools.tGP_
        tGB = tools.tGB_
    else
        tGP = tools.tGP
        tGB = tools.tGB
    end

    if not (gps and gps.isOpen()) and not tools.testMode then
        return
    end

    for i = 1, page.gpN do
        page["locGPTxt" .. i] = ""
    end
    for i = 1, page.gbN do
        page["locGBTxt" .. i] = ""
    end
    for i = 1, #tGP do
        if i > page.gpN then
            break
        end
        local x, y = gsvP(tGP[i][2], tGP[i][3])
        if x and y then
            page["locGPTxt" .. i] = tGP[i][1]
            page["locGPX" .. i], page["locGPY" .. i] = x, y
        end
    end
    for i = 1, #tGB do
        if i > page.gbN then
            break
        end
        local x, y = gsvP(tGB[i][2], tGB[i][3])
        if x and y then
            page["locGBTxt" .. i] = tGB[i][1]
            page["locGBX" .. i], page["locGBY" .. i] = x, y
        end
    end
end

-- 设置文字透明度
function setTxtOpa(n)
    for i = 1, page.gpN do
        page["locGPOpa" .. i] = n
    end
    for i = 1, page.gbN do
        page["locGBOpa" .. i] = n
    end
end

-- 动画
function anim()
    -- 文字闪烁
    setTxtOpa(txtOpa)
    txtOpa = txtOpa - 20
    if txtOpa < 75 then
        txtOpa = 255
    end
    -- 旋转扫描动画
    page.locAngle = page.locAngle + 100
    if page.locAngle >= 3600 then
        page.locAngle = 0
    end
end

function enter()
    ent = true
    txtOpa = 255
    page.locAngle = 0
    -- GPS 打开就不关了
    if gps then
        gps.open(gps.DEFAULT, {
            tag = "DEF"
        })   
    end

    rotate()
    sys.timerLoopStart(rotate, 4000)
    sys.timerLoopStart(anim, 100)
end

function exit()
    ent = false
    txtOpa = 255
    sys.timerStop(rotate)
    sys.timerStop(anim)
end

-- 上一页
function keyUpShort()
    airui.show("locRtk", "locMsg")
end

-- 下一页
function keyDownShort()
    airui.show("locSig")
end

