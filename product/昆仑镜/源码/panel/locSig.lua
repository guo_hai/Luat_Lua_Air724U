-- 定位里的 信噪比 页面
module(..., package.seeall)

local function getS(v)
    local strength = (tonumber(v) or 0)/11*7 + 1
    strength = strength > 35 and 35 or strength
    return strength
end

function setLine(q, i)
    local k = "tG"..q
    if tools.testMode then
        k = k.."_"
    end
    local attr = "sigG"..q
    local t = tools[k]
    if t and t[i] then
        page[attr.."val"..i] = getS(t[i][4])
        page[attr.."sig"..i] = tonumber(t[i][4]) or 0
        if t[i][2] and t[i][3] then
            page[attr.."opa"..i] = 50
        else
            page[attr.."opa"..i] = 0
        end
        if t[i][1] then
            page[attr.."txt"..i] = t[i][1]
        else
            page[attr.."txt"..i] = ""
        end
    else
        page[attr.."val"..i] = 1
        page[attr.."sig"..i] = 0
        page[attr.."txt"..i] = ""
        page[attr.."opa"..i] = 0
    end
end

function flushSig()
    for i=1, page.sigN do
        setLine("P", i)
        setLine("B", i)
    end
end

function enter()
    flushSig()
    sys.timerLoopStart(flushSig, 2000)
end

function exit()
    sys.timerStop(flushSig)
end

-- 上一页
function keyUpShort() 
    airui.show("locGps")
end

-- 下一页
function keyDownShort() 
    airui.show("locMsg")
end
