PROJECT = "KLJ"
VERSION = "1.0.1"

-- 系统加载库
require "log"
require "sys"

require "misc"
require "audio"
require "socket"
require "config"
require "nvm"
nvm.init("config.lua")

-- 电压域
pmd.ldoset(15, pmd.LDO_VLCD)  -- 屏幕
pmd.ldoset(12, pmd.LDO_VCAMA) -- 九轴

pmd.ldoset(15, pmd.LDO_VIBR)
pmd.ldoset(15, pmd.LDO_VMMC)

-- 驱动
require "lcd" -- 屏幕
require "myiic" -- iic

-- 界面框架
require "page" -- 布局
require "airui" -- 框架

-- 工具
require "tools"

require "autoGPS"
sys.subscribe("AUTOGPS_READY", function(gpsLib, agpsLib, kind, baudrate)
    log.info("opengps", gpsLib, agpsLib, kind, baudrate)
    _G.gps = gpsLib
    _G.agps = agpsLib
    tools.setGps(3, baudrate)
end)

-- 页面
require "base"   -- 基础
require "menu"   -- 菜单
require "msg"    -- 信息
require "locGps" -- 定位 星图
require "locMap" -- 定位 地图
require "locSig" -- 定位 信噪比 
require "locMsg" -- 定位 信息
require "locRtk" -- 定位 信息
require "oracle" -- 占卜
require "cld"    -- 农历
require "compass"-- 罗盘
require "wsled"  -- 彩灯
require "bird"   -- 游戏
require "balance"-- 平衡
require "bagua"  -- 八卦

-- 初始化
sys.taskInit(function()
    ril.request("AT^TRACECTRL=0,1,1\r\n")
    -- 播放可以被打断
    audio.setStrategy(1)
    myiic.init()
    sys.waitUntil("IP_READY_IND")
    sys.wait(4000)
    local key = nvm.get("key")
    if not (type(key) == "string" and #key > 5) then
        log.warn("rtk", "请设置 RTK 的 appKey")
        return
    end
    -- 打开 RTK
    log.info("rtk", tools.openRtk {
        appKey = key,
        appSecret = "",
        solMode = rtk.SOLMODE_RTK,
        solSec = rtk.SEC_5S,
        reqSec = rtk.SEC_5S
    })
end)

sys.taskInit(function()
    sys.waitUntil("LVGL_INIT")
    sys.wait(100)
    -- 按键初始化
    airui.keyInit()
    -- 数据绑定
    airui.bind(page)
    airui.setBase("base")
    -- 显示开机界面
    airui.show("klj")
    -- 背光
    pmd.ldoset(8, pmd.LDO_VBACKLIGHT_R) 
    -- 亮灯
    wsled.begin()
    sys.wait(2000)
    -- 显示菜单
    airui.show("menu")
    -- 灭灯
    wsled.unlight()
end)

-- lvgl 初始化
lvgl.init(function()
    sys.publish("LVGL_INIT")
end, nil)

sys.init(0, 0)
sys.run()
