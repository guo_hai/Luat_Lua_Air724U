require "desktop"
require "power"
require "volume"
require "mine"
require "group"
require "msgBox"
require "setting"
require "online"
require "netset"
require "pttset"
require "heartset"
require "lcdback"
require "gpsset"
require "upo"
--require "simset"
require "lcdLight"
require "setonline"
require "upset"
require "upnew"
require "upok"
require "lockset"
module(..., package.seeall)

--当前的页面
local now = ""
--弹出层
local popNow
local strsim = "请插卡"
sys.taskInit(
    function()
        while true do
            if sim.getStatus() then
                strsim = "登陆中"
                return
            else
                strsim = "请插卡"
            end
            sys.wait(1000)
        end
    end
)
--屏幕显示参数
width, height = disp.getlcdinfo()

--页面对象
local plib = require("page")
local p =
    plib(
    width,
    height,
    nil,
    function()
        if poc.GroupNow ~= nil and #poc.GroupNow.name > 26 then
            poc.GroupNow.name = poc.GroupNow.name:sub(1, 26)
        end
        if poc.ptting then
            return "本机正在讲话"
        end
        if poc.NERROR ~= nil then
            return poc.NERROR
        end
        --if poc.GroupNow ~= nil and #poc.GroupNow.name > 10 then
        --     poc.GroupNow.name = poc.GroupNow.name:sub(1, 10)
        --  end
        return (poc.SayNow and poc.SayNow or ((poc.GroupNow and poc.GroupNow ~= "") and poc.GroupNow.name or strsim))
    end
)

--对外使用的高度偏移
function h(n)
    if not n then
        n = 0
    end
    return n + 21
end

local open
--刷新屏幕内容
--区域大小160*85
function update(first)
    if not lcd.isWake() or not open then
        return
    end
     --没亮屏不刷新
    --disp.clear()
    p:show()
    if _G[now] and _G[now].update then
        _G[now].update(first)
    end
    if popNow and _G[popNow] and _G[popNow].update then
        _G[popNow].update()
    end
    if pincfg.backled ~= true then
        log.info("-点亮背光-")
        pmd.ldoset(4, pmd.LDO_VLCD)
         -- 4 = 2.05v --2g为7
        pmd.ldoset(13, pmd.LDO_VMMC)
         -- 4 = 2.05v --2g为7 6=3.0V 13=3.0V
        sys.publish("lcdback_on", nvm.get("light"), 0)
    -- pio.pin.plus(5, 1, 6000, 1, 0)
    -- pio.pin.plus(5, 40, 10, nvm.get("light"), 1)
    end
    disp.update()
end
disp.drawrect(0, window.h(106), window.width - 1, window.h(106), lcd.rgb(255, 255, 255))
--设置/切换页面
function set(page)
    if _G[now] and _G[now].close then
        _G[now].close()
    end
    now = page
    update(true)
end

--顶层弹出窗口
--弹出层大小130*60
function pop(page)
    popNow = page
    update()
end
--取消弹出层
function unpop()
    if _G[popNow] and _G[popNow].close then
        _G[popNow].close()
    end
    popNow = nil
    update()
end
--对外使用的弹出层高度偏移
function ph(h)
    if not h then
        h = 0
    end
    return h + 33
end
--对外使用的弹出层宽度偏移
function pw(w)
    if not w then
        w = 0
    end
    return w + 15
end

--分发按键事件
sys.subscribe(
    "WINDOW_KEY_IND",
    function(key, pressed)
        local p = popNow or now
        log.info("WINDOW_KEY_IND6",  key, msg)
        if _G[p] and _G[p].onKey then
            _G[p].onKey(key, pressed)
        end
    end
)

sys.taskInit(
    function()
        sys.waitUntil("SIM_IND")
        poc.tts("欢迎使用公网对讲机")
        sys.wait(2000)
        open = true

        set("desktop")
    end
)

sys.subscribe("IP_READY_IND", update)
 --网络状态变化刷新
sys.timerLoopStart(update, 5000)
 --一分钟刷新一次
