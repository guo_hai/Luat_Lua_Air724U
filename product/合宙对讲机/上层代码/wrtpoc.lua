module(..., package.seeall)

require "utils"
require "pm"
require "common"
require "config"
require "nvm"
nvm.init("config.lua")


local UART_ID = uart.USB

local USE_FILE = "/fota/poc"
local TMP_FILE = "/fota/poc"
rtos.make_dir("/fota")
--[[ 
    --发送方
    1:开始发送文件
    2:正常传输数据
    3:数据传输完毕
    4:停止传输


    7:设置参数
    8:获取参数
    9:重启设备

    -- 接收方
    1:
    2:
    3:发送完毕
    4:停止发送
    5:继续发送
    6:重新发送

    7:设置应答
    8:获取应答
]]

-- 反序列化
function unserialize(str)
    if not str or type(str) ~= "string" then return end
    local f = loadstring("return " .. str)
    if not f or type(f) ~= "function" then return nil end
    setfenv(f, {})
    if pcall(f) then return f() end
    return nil
end

local getDirContent

getDirContent = function(dirPath, level)
    local ftb = {}
    local dtb = {}
    level = level or "    "
    local tag = " "
    if not io.opendir(dirPath) then return end
    while true do
        local fType, fName, fSize = io.readdir()
        if fType == 32 then
            local size
            if fSize>99 then
                size = string.format("%dB", fSize)--string.format("%.2f KB", fSize/1024)
            else
                size = fSize.." B"
            end
            table.insert(ftb, {name = fName, size = size})
        elseif fType == 16 then
            table.insert(dtb, {name = fName, path = dirPath.."/"..fName})
        else
            break
        end
    end
    io.closedir(dirPath)
    for i = 1, #ftb do 
        if i==#ftb then 
            log.info(tag, level.."└─", ftb[i].name, "["..ftb[i].size.."]")
        else
            log.info(tag, level.."├─", ftb[i].name, "["..ftb[i].size.."]")
        end
    end
    for i = 1, #dtb do 
        if i==#dtb then
            log.info(tag, level.."└─", dtb[i].name)
            getDirContent(dtb[i].path, level.."  ")
        else
            log.info(tag, level.."├─", dtb[i].name)
            getDirContent(dtb[i].path, level.."│ ")
        end
    end
end

dir = getDirContent

function fileNew()
    os.remove(USE_FILE)
    filehandle = io.open(USE_FILE, "w+")
    if filehandle then
        log.info("文件创建成功")
        return true
    else
        log.info("文件创建失败")
    end
end

function fileWrt(data)
    -- log.info("写入数据")
    return filehandle:write(data)
end

function fileClose()
    if filehandle then
        log.info("关闭文件")
        filehandle:close()
        filehandle = false
    end
end

local rdTab = {"User", "Password", "PARAM1", "PARAM2", "PARAM3", "Platform"}

function iccid2(str)
	local z19 = string.rep("0", 19)
	local iccid = str
	if type(str)~="string" or #str<19 or #str>20 or string.match(str, z19) then
		iccid = sim.getIccid() or z19
	end
	local id = string.match(iccid, "^8986(..).*")
	if not ({["00"]=true, ["02"]=true, ["04"]=true, ["07"]=true})[id] then
		iccid = string.sub(iccid, 1, 19)
	end
	return iccid
end

function rd()
    local user = iccid2(nvm.get("User"))
    log.info("user", user)
	local lt =	{
		(user or ""),
		(nvm.get("Password") or ""),
		(nvm.get("PARAM1") or ""),
		(nvm.get("PARAM2") or ""),
		(nvm.get("PARAM3") or ""),
		(nvm.get("Platform") or "")	
	}				 
    str = "{'"..table.concat(lt, "','").."'}"
    log.info("读取", str)
    return str
end

local tab = {
    "版本号", "认证", "User", "Password", "DevicePasswd", "Platform", "PARAM1", "PARAM2", "PARAM3", 
}

function setCfg(t)
	-- 版本校验
	if t[1]~="1.0.0" then
		poc.tts("版本校验失败")
		return 
	end

	-- 认证
	local Auth = nvm.get("DevicePasswd")
	if not (t[2] and Auth and t[2]==Auth) then
		poc.tts("认证失败")
		return 
	end

	local errTab = {}

	-- 设置成功标志
	for i=3, #tab do
        log.info("键值对", tab[i], t[i])
        if t[i] and type(t[i])=="string" then 
            local f = nvm.set(tab[i], t[i])
            if not f then
                errTab[#errTab+1] = tab[i]
            end
		end
	end

    if #errTab>0 then
        poc.tts("数据写入失败")
	else
        sys.taskInit(function()
            wrtD(7, "ok")
            poc.tts("数据写入成功, 自动重启")
			sys.wait(4000)
			rtos.restart()
		end)
	end
end

filehandle = false

-- 获取 table 
local getT = {
    [1] = function(data)
        -- 删除临时文件
        -- 创建临时文件
        -- 返回 5 or 4
        if fileNew() then
            wrtD(5, "ok")
        else
            wrtD(4, "create err")
        end
    end,
    [2] = function(data)
        -- 写入临时文件
        -- 返回 5 + 包号
        fileWrt(data)    
        wrtD(5, "ok")
    end,
    [3] = function()
        -- 写入临时文件
        -- 文件重命名, 返回 3  
        fileClose()
        dir("/")
        wrtD(3, "ok")
        rtos.poc_ota()
        -- local userAudioParam, len = io.readFile(USE_FILE)
        -- log.info("md5", crypto.md5(userAudioParam, len))
    end,
    [4] = function(data)
        -- 删除临时文件
        -- 返回 4
        os.remove(USE_FILE)
    end,
    [7] = function(data)
        log.info(data)
        -- 设置参数
        -- 返回 7
        local t = unserialize(data)
        if t then
            for k, v in ipairs(t) do
                log.info(k, v)
            end
        end
        setCfg(t)
    end,
    [8] = function(data)
        log.info(data)
        -- 返回参数
        -- 返回 8
        wrtD(8, rd() or "err")
    end,
    [9] = function(data)
        log.info(data)
        -- 重启设备
        -- 返回 9

    end,
}

-- crc (与C保持一致)
if not crc then
    crc = function(p)
        return string.format("%08x", crypto.crc32(p, #p))
    end
end

local key = "Q"

-- 数据发送
function wrtD(cmd, data)
	send = string.format(key.."%d%04d%s%s", cmd, string.len(data), crc(data), data)
	write(send)
end

-- 再次校验解析
function rdD(str)
	if not(str and str~="" and #str>12) then
		print("长度不够") 
		-- 需要 nil 填充返回与C保持一致
		return nil 
	end
	-- 指令 长度 校验 数据
	local t = {string.match(str, key.."(.)(%d%d%d%d)(%x%x%x%x%x%x%x%x)(.*)")}
	if #t~= 4 then
		print("格式异常", str, #t)
		return nil
	end
    t[1] = tonumber(t[1])
    t[2] = tonumber(t[2])
	--t[3] = tonumber(t[3], 16)
	if not (t[1] and t[2]) then
		print("数据解析异常")
		return nil
	end
	if t[2]~=string.len(t[4]) then
		print("数据长度异常")
		return nil
	end
    
    -- crc 校验
	crcdata = crc(t[4])
	if crcdata ~= t[3] then
		print("crc 校验失败", crcdata, t[3]) 
		return nil
	end

	return t[1], t[4]
end

-- 数据缓冲
local cache = ""

-- 分包解析
function uartRead(str)
    if not str or type(str)~="string" or #str==0 then 
        return 
    end
	if #cache > 0xFFFF then 
		log.info("缓冲过大", "清空数据")
		cache = "" 
    end
    cache = cache .. str
    local i = cache:find(key)
    while i do
        if #cache<i+14 then 
			log.info("分包解析", "报文长度太短", #cache, cache:toHex())
			break 
		end
        local len = tonumber(string.sub(cache, i+2, i+5))
        if not len or len>1024 or len<1 then
            log.info("长度异常", #cache, len or 0, i)
            cache = ""
            break
        elseif #cache>=len+i-1+14 then
            local crc1 = string.sub(cache, i+6, i+13)
            local line =string.sub(cache, i+14, i+14+len)
            local crc2 = crc(line)
            if crc1~=crc2 then
                cache = string.sub(cache, i+1)
                log.info("CRC校验未通过", crc1, crc2)
            else
                local cmd, data = rdD(cache)
                -- log.info(cmd.."", data)
                if cmd and data then
                    if getT[cmd] then
                        getT[cmd](data)
                    end
                else
                    print("参数解析失败", #cache, cache:toHex())
                end
                cache = string.sub(cache, i+len+14)
            end
        else
            log.info("分包解析", "长度不够", #cache, len, i)
            break
        end

        i = string.find(cache or "", key)
    end
end

-- 数据读取

local function taskRead()
    local cacheData = ""
   
    while true do
        local s = uart.read(UART_ID, "*l")
        if s == "" then
            if not sys.waitUntil("UART_RECEIVE", 20) then
                sys.publish("UART_RECV_DATA", cacheData:sub(1, 1024))
                cacheData = cacheData:sub(1024, -1)
            end
            uart.on(UART_ID, "receive")
        else
            cacheData = cacheData .. s
            if cacheData:len() >= 1024 then
                sys.publish("UART_RECV_DATA", cacheData:sub(1, 1024))
                cacheData = cacheData:sub(1024, -1)
            end
        end
    end
end

-- com 发送数据
function write(data)
    -- log.info("com 发送", string.toHex(data)) 
    uart.write(UART_ID, data) 
end

pm.wake("mcuUart.lua")
uart.setup(UART_ID, 115200, 8, uart.PAR_NONE, uart.STOP_1)
sys.taskInit(taskRead)

sys.subscribe("UART_RECV_DATA", uartRead) -- 接收串口数据



uart.on(UART_ID, "receive", function() sys.publish("UART_RECEIVE") end)
