require"common"
module(..., package.seeall)

--LCD分辨率的宽度和高度(单位是像素)
WIDTH, HEIGHT, BPP = disp.getlcdinfo()
--WIDTH, HEIGHT = 100,128
log.info("getlcdinfo",WIDTH, HEIGHT, BPP)
--1个ASCII字符宽度为8像素，高度为16像素；汉字宽度和高度都为16像素
CHAR_WIDTH = 8

--rgb值转rgb565
function rgb(r,g,b)
    return ((r-r%8)/8)*2048 + ((g-g%4)/4)*32 + ((b-b%8)/8)
end

--放置文字，自动转码
function puttext(s,x,y,r,g,b)
    if r and g and b then
        disp.setcolor(rgb(r,g,b))
    else
        disp.setcolor(0)
    end
    disp.puttext(common.utf8ToGb2312(s),x,y)
end

--放置第一个字，自动转码
function putatext(s,x,y)
    disp.puttext(common.utf8ToGb2312(s):sub(1,2),x,y)
end

--居中显示文字
function putStringCenter(s,x,y,r,g,b)
    local str = common.utf8ToGb2312(s)
    puttext(s,x-str:len()*((CHAR_WIDTH-CHAR_WIDTH%2)/2),y,r,g,b)
end

--靠右显示文字
function putStringRight(s,x,y,r,g,b)
    local str = common.utf8ToGb2312(s)
    puttext(s,x-str:len()*CHAR_WIDTH,y,r,g,b)
end

