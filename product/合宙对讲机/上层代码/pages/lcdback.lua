require "poc"
require "lcd"
module(..., package.seeall)

--此窗口被开启？
local opened

local last, now = 1, 1
local list = {"一级", "二级", "三级", "四级","五级"}
local pages = {9,7,5,3,1}

function update(first)
    if first then --第一次开
        opened = true
        t=nvm.get("light")
        for i, v in pairs(pages) do
            if v == t then
                now = i
                if i==#list then
                    last=2
                end
                break
            end
        end
    end
    lcd.putStringCenter("屏幕亮度", window.width / 2, window.h(2), 255, 255, 255)
   
    last = lcd.list(list, now, last)
    if not volume.volopened then
        disp.update()
    end
end

--关闭此页面
function close()
    opened = nil
end

function onKey(key, pressed)
    if not pressed then
        return
    end
    log.info("menu", "key", key)
    if key == "EXIT" then
        sys.timerStart(pincfg.lcdBack, 50, 1 ,true,nvm.get("light"))
        window.set("setting")
        --切换窗口
        return
    elseif key == "PLUS" then
        now = now - 1
        if now < 1 then
            now = #list
        end
        pincfg.lcdBack(1,true,pages[now])
    elseif key == "MIN" then
        now = now + 1
        if now > #list then
            now = 1
        end
        pincfg.lcdBack(1,true,pages[now])
    elseif key == "OK" then
        log.info("---------------",now)
        nvm.set("light",pages[now])
        pincfg.lcdBack(1,true)
        window.pop("upo")
        --window.set("setting")
        --切换窗口
        return
    end
    if opened then
        window.update()
    end
    --刷新
end
