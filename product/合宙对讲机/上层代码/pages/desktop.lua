require "lcd"
module(..., package.seeall)

--此窗口被开启？
local opened

--被选中的
local selected = 1

local desktopList = {"成 员", "群 组", "设 置", "我 的"}
local desktopIcon = {"online", "group", "setting", "mine"}
--local desktopIcon ={"a","b","c","d"}
local desktopHeight = {{0, 20}, {22, 42}, {44, 64}, {66, 84}}

function update(first)
    if first then
        opened = true
    end
     --第一次开启
     opened = true
    disp.putimage("/lua/" .. desktopIcon[selected] .. ".png", 54, 29)

    lcd.putStringCenter(desktopList[selected], window.width / 2, window.h(65), 255, 255, 255)

end

function close()
    opened = nil
end

function onKey(key, pressed)
    if not pressed then
        return
    end
    log.info("desktop", "key", key)
    if key == "MIN" then
        selected = selected + 1
        if selected > 4 then
            selected = 1
        end
    elseif key == "PLUS" then
        selected = selected - 1
        if selected < 1 then
            selected = 4
        end
    elseif key == "OK" then
        --重置全局变量，确保从桌面进入菜单是第一行
        poc.gm_last, poc.gm_now = 1, 1
        --切换窗口
        window.set(desktopIcon[selected])
        --直接退出不然TTS会多报一声
        return
    elseif key == "MENU" then
        --按菜单键切回第一个窗口
        selected=1
    else
        return
    end
    poc.tts(desktopList[selected]:gsub(" ", ""))
    if opened then
        window.update()
    end
 --刷新
end

--window.set("desktop")

--测试
-- sys.timerLoopStart(function ()
--     selected = selected + 1
--     if selected > 4 then selected = 1 end
--     window.update()
-- end,1000)
