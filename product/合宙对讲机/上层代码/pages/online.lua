require "lcd"
module(..., package.seeall)

--此窗口被开启？
local opened

--被选中的
local selected = 1
local close_oline
--成员获取完毕？
local memberOK = nil

--正在呼叫的用户
local calling
local memberList = {}
local memberHeight = {{23, 42}, {44, 63}, {65, 84}}

function update(first)
    if first then --第一次开启
        selected, opened, memberList, memberOK, calling = 1, true, {}
        sys.taskInit(
            function()
                if not poc.GroupNow or poc.GroupNow.temp ~= 0 then
                    while not msgBox.show("当前群组无效") do
                        sys.wait(1000)
                    end
                    poc.tts("当前群组无效")
                    sys.waitUntil("MSGBOX_DONE", 5000)
                    window.set("desktop")
                    return
                end
                sys.wait(300)
                poc.req("members", poc.GroupNow.id, 0, -1)
                --获取群组信息
                while true do
                    local r, d = sys.waitUntil("POC_MEMBERS", 1000)
                    if not r or not d then
                        break
                    end
                    if d[2] == "0" then
                        break
                    end
                    local n = d[6]:fromHex()
                    n = common.ucs2ToGb2312(n)
                    log.info("member name", n)
                    n = common.gb2312ToUtf8(n):gsub(string.char(0), "")
                    table.insert(
                        memberList,
                        {
                            online = d[4],
                            id = d[5],
                            name = n
                        }
                    )
                    if tonumber(d[2]) == #memberList then
                        break
                    end
                end
                if not opened then
                    return
                end
                --窗口关闭了
                log.info("get member", #memberList)
                if #memberList == 0 then
                    while not msgBox.show("成员获取失败") do
                        sys.wait(1000)
                    end
                    poc.tts("成员获取失败")
                    print("成员获取失败")
                    sys.waitUntil("MSGBOX_DONE", 5000)
                    window.set("desktop")
                    return
                end
                memberOK = true
                --poc.tts(memberList[1].name)
                --念一下第一个
                window.update()
                sys.subscribe("POC_NUSERCHANGED", callStop)
            end
        )
    end
    if not memberOK then--没获取完
        lcd.puttext("获取中……",45,window.h(memberHeight[2][1])+2,0,0,0)
        return
    end
    lcd.putStringCenter("临时呼叫",window.width/2,window.h(2),255,255,255)
    --分割线
    local f = true
    --第一根线
    if #memberList == 1 then
        num = 44
    elseif #memberList == 2 then
        num = 65
    else
        num = 86
    end
    --第一根线
    for i = 22, num, 21 do
        disp.drawrect(f and 0 or 37, window.h(i), window.width - 1, window.h(i), lcd.rgb(255, 255, 255))
        f = nil
    end


    --当前页面开始显示的群组
    local start = selected
    --开始从第几个显示
    local selectTemp = 1
    --显示的是页面上的第几个
    if #memberList > 3 then --大于三个的情况下才考虑按页显示
        if #memberList - 2 < start then
            start = #memberList - 2
            selectTemp = 3 - #memberList + selected
        end
    else--小于三个
        start = 1
        selectTemp = selected
    end
    --背景
    disp.drawrect(27,window.h(memberHeight[selectTemp][1]),
                window.width-1,window.h(memberHeight[selectTemp][2]),lcd.rgb(255,255,255))
    --摆字和图标
    for i=1,3 do
        if start+i-1 > #memberList then return end--超了
        if #memberList[start + i - 1].name > 15 then
            memberList[start + i - 1].name = memberList[start + i - 1].name:sub(1, 15)
        end
        local color = (start+i-1)==selected and 0 or 255
        lcd.puttext(memberList[start+i-1].name,30,window.h(memberHeight[i][1])+2,color,color,color)
        --if color then disp.putimage("/lua/icon_arrow.png",27,window.h(memberHeight[i][1])+7) end
        disp.putimage(
            "/lua/icon_"..(memberList[start+i-1].online ~= "1" and "on" or "off").."line.png",
            8,window.h(memberHeight[i][1])+1)
        disp.putimage(
            "/lua/checked_box_"..(memberList[start+i-1].id == calling and "on" or "off")..".png",
            window.width - 19,window.h(memberHeight[i][1])+6)
    end
end

function close()
    opened = nil
    selected,memberList,memberOK = 1,{}
    sys.unsubscribe("POC_NUSERCHANGED", callStop)
end

--确定锁
local okLock
function onKey(key,pressed)
    if not pressed or not memberOK then return end
    log.info("member","key",key)
    if key == "MIN" and not calling then
        selected = selected + 1
        if selected > #memberList then selected = #memberList end
    elseif key == "PLUS" and not calling then
        selected = selected - 1
        if selected < 1 then selected = 1 end
    elseif key == "EXIT" and not okLock then
        -- if calling then
        --     poc.req("singlecall", calling)
        -- end
        --退出取消单呼
        window.set("desktop")
        --回首页面
        return
    elseif key == "OK" and not okLock then
        if memberList[selected].online == "1" then--不在线
            poc.tts("用户不在线")
            return
        end
        sys.taskInit(
            function()
                --单独起个逻辑处理，防止单呼失败逻辑错误
                okLock = true
                poc.req("singlecall", memberList[selected].id)
                local r, d = sys.waitUntil("POC_SINGLECALL", calling and 1 or 3000)
                if calling or (r and (d[2] == "1" or d[3] == "1")) then --单呼中不考虑返回，直接退出
                    if calling then
                        calling = nil
                    else
                        calling = memberList[selected].id
                    end
                end
                okLock = nil
                if opened then
                    window.update()
                end
                --刷新
            end
        )
        return
    end
    --poc.tts(memberList[selected].name:gsub(" ", ""))
    if opened then
        window.update()
    end
    --刷新
end

--单呼结束
function callStop(data)
    if calling and data and data[4] == "3" then
        calling = nil
        window.update()
    end
end
