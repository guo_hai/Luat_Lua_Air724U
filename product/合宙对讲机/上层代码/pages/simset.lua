require "poc"
require "lcd"
module(..., package.seeall)

--此窗口被开启？
local opened

local last, now = 1, 1
local list = {"自动(暂不支持)", "卡1(设置后重启)", "卡2(设置后重启)"}
local pages = {2, 0, 1}

function update(first)
    if first then --第一次开
        t = nvm.get("sim")
        for i, v in pairs(pages) do
            if v == t then
                now = i
                break
            end
        end
        opened = true
    end
    lcd.putStringCenter("SIM设置", window.width / 2, window.h(2), 255, 255, 255)
  
    --disp.clear()
    last = lcd.list(list, now, last)
    if not volume.volopened then
        disp.update()
    end
end

--关闭此页面
function close()
    opened = nil
end

function onKey(key, pressed)
    if not pressed then
        return
    end
    log.info("menu", "key", key)
    if key == "EXIT" then
        window.set("setting")
        --切换窗口
        return
    elseif key == "PLUS" then
        now = now - 1
        if now < 1 then
            now = #list
        end
    elseif key == "MIN" then
        now = now + 1
        if now > #list then
            now = 1
        end
    elseif key == "OK" then
        log.info("---------------", now)
        if pages[now] == 2 then
            return
        end
        nvm.set("sim", pages[now])
        ril.request("AT+SIMCROSS=" .. pages[now])
        window.pop("upo")
        sys.taskInit(
            function()
                poc.tts("正在重启，请勿按任意按键")
                sys.wait(3500)
                sys.restart("SIM")
            end
        )
    end
    if opened then
        window.update()
    end
    --刷新
end
