module(..., package.seeall)

--此窗口被开启？
local opened

--被选中的
local selected = 1

local powerList = {"关 机", "重 启"}
local listHeight = {{22, 42}, {44, 64}, {66, 84}}

function update(first)
    if first then
        selected, opened = 1, true
        if sim.getStatus() then
            poc.tts("电源选项")
        else
            --无sim卡
            poc.tts("未检测到卡")
        end
    end
    --第一次开启
    lcd.putStringCenter(sim.getStatus() and "电源选项" or "未检测到卡", window.width / 2, window.h(2), 255, 255, 255)
    --分割线
    local f = true
    --第一根线
    for i = 21, 80, 22 do
        disp.drawrect(f and 0 or 10, window.h(i), window.width - 1, window.h(i), lcd.rgb(222, 222, 222))
        f = nil
    end
    --背景
    disp.drawrect(
        10,
        window.h(listHeight[selected][1]),
        window.width - 1,
        window.h(listHeight[selected][2]),
        lcd.rgb(255, 255, 255)
    )
    --摆字和图标
    for i = 1, 2 do
        local color = i == selected and 0 or 255
        lcd.puttext(powerList[i], 13, window.h(listHeight[i][1]) + 2, color, color, color)
        if color then
            disp.putimage("/lua/icon_arrow.png", 5, window.h(listHeight[i][1]) + 7)
        end
        --disp.putimage("/lua/icon_"..desktopIcon[i]..".png",8,window.h(desktopHeight[i][1])+1)
    end
end

function close()
    opened = nil
end

function onKey(key, pressed)
    if not pressed then
        return
    end
    log.info("power", "key", key, sim.getStatus())
    if key == "MIN" or key == "PLUS" then
        selected = selected == 1 and 2 or 1
    elseif key == "EXIT" and sim.getStatus() then
        window.set("desktop")
        --切换窗口
        return
    elseif key == "OK" then
        pincfg.lcdBack(0)
        if selected == 1 then
            rtos.poweroff()
        else
            sys.taskInit(
                function()
                    poc.tts("正在重启，请勿按任意按键")
                    sys.wait(3500)
                    sys.restart("user_key")
                end
            )
        end
        return
    end
    --poc.tts(powerList[selected]:gsub(" ",""))
    if opened then
        window.update()
    end
    --刷新
end
