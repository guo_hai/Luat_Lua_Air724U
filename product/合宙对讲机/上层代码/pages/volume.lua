--音量弹出层
module(..., package.seeall)

--此窗口被开启？
volopened = nil

local closeTimer

--弹出层大小130*30
function update()
    volopened = true
    sys.timerStop(closeTimer)
    closeTimer = sys.timerStart(window.unpop, 1000)
    --音量弹出层的四个顶点坐标
    disp.drawrect(window.pw(0), window.ph(0), window.pw(120), window.ph(29), lcd.rgb(56, 65, 74))
    disp.drawrect(window.pw(34), window.ph(14), window.pw(97), window.ph(15), lcd.rgb(135, 208, 197))
    disp.putimage("/lua/volume.png", window.pw(5), window.ph(5))
    disp.putimage("/lua/dot.png", window.pw(29 + nvm.get("volume") * 9), window.ph(10))
end

function close()
    volopened = nil
end

seting = nil
local v
function onKey(key, pressed)
    if not pressed then
        return
    end

    v = nvm.get("volume")
    if key == "VOLM" then
        v = v - 1
    elseif key == "VOLP" then
        v = v + 1
    elseif key=="OK" then
        log.info("完成")
    else
        sys.timerStop(closeTimer)
        window.unpop()
        return
    end
    if v > 7 then
        v = 7
    end
    if v < 0 then
        v = 0
    end
    nvm.set("volume", v)
    log.info("setVolume", v)
    audio.setVolume(v)
    audio.setCallVolume(v)
    if pincfg.volseting then return end
    poc.tts("音量" .. tostring(v) .. "级")

    if volopened then
        window.update()
    end
 --刷新
end
