--音量弹出层
module(..., package.seeall)

--此窗口被开启？
volopened = nil

local closeTimer
function upp()
    window.unpop()
    --window.set("setting")
end
--弹出层大小130*30
function update()
    volopened = true
    sys.timerStop(closeTimer)
    closeTimer = sys.timerStart(upp, 800)
    disp.drawrect(window.pw(10), window.ph(-5), window.pw(80), window.ph(22), lcd.rgb(255, 255, 255))
    lcd.puttext("升级成功", window.pw(13), window.ph(0), 0, 0, 0)
end

function close()
    volopened = nil
end

seting = nil
local v
function onKey(key, pressed)
    if not pressed then
        return
    end

    sys.timerStop(closeTimer)
    upp()

    --刷新
end
