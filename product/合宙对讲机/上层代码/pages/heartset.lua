require "poc"
require "lcd"
module(..., package.seeall)

--此窗口被开启？
local opened

local last, now = 1, 1
local list = {"10秒", "20秒", "30秒", "40秒", "50秒"}
local pages = {10, 20, 30, 40, 50}

function update(first)
    if first then --第一次开
        t = nvm.get("poctime")
        for i, v in pairs(pages) do
            if v == t then
                now = i
                if i == #list then
                    last = 2
                end
                break
            end
        end
        opened = true
    end
    lcd.putStringCenter("心跳时间", window.width / 2, window.h(2), 255, 255, 255)
    --disp.clear()
    last = lcd.list(list, now, last)
    if not volume.volopened then
        disp.update()
    end
end

--关闭此页面
function close()
    opened = nil
end

function onKey(key, pressed)
    if not pressed then
        return
    end
    log.info("menu", "key", key)
    if key == "EXIT" then
        window.set("setting")
        --切换窗口
        return
    elseif key == "PLUS" then
        now = now - 1
        if now < 1 then
            now = #list
        end
    elseif key == "MIN" then
        now = now + 1
        if now > #list then
            now = 1
        end
    elseif key == "OK" then
        log.info("---------------", now)
        nvm.set("poctime", pages[now])
        poc.req("setping", pages[now])
        window.pop("upo")
    end
    if opened then
        window.update()
    end
    --刷新
end
