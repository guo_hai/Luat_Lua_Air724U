--音量弹出层
module(..., package.seeall)

--此窗口被开启？
local opened

local closeTimer

local textShow

--弹出层大小130*30
function update()
    opened = true
    sys.timerStop(closeTimer)
    closeTimer = sys.timerStart(function ()
        window.unpop()
        sys.publish("MSGBOX_DONE")
    end, 1000)
    disp.drawrect(window.pw(0),window.ph(20),window.pw(129),window.ph(49),lcd.rgb(56,65,74))
    lcd.putStringCenter(textShow,window.width/2,window.ph(27),255,255,255)
end

function close()
    opened = nil
end


function onKey(key,pressed)
    --不响应
end

--显示遮罩
function show(text)
    if opened then return end
    textShow = text
    window.pop("msgBox")
    return true
end
