require "poc"
require "lcd"
module(..., package.seeall)

--此窗口被开启？
local opened

local last, now = 1, 1
local list = {"网络模式", "PTT提示音", "心跳时间", "背光亮度", "亮屏时间", "键盘锁时间","升级设置", "GPS定位", "退出单呼时间"}
local pages = {"netset", "pttset", "heartset", "lcdback", "lcdLight","lockset", "upset", "gpsset", "setonline"}

function update(first)
    if first then --第一次开
        if poc.gm_now == nil or poc.gm_last == nil then
            --归位
            last, now = 1, 1
        else
            now = poc.gm_now
            last = poc.gm_last
        end
        opened = true
    end
    lcd.putStringCenter("设  置", window.width / 2, window.h(2), 255, 255, 255)
 
    --disp.clear()
    last = lcd.list(list, now, last)
    log.info("menu last", last, now)
    if not volume.volopened then
        disp.update()
    end
end

--关闭此页面
function close()
    opened = nil
end

function onKey(key, pressed)
    if not pressed then
        return
    end
    poc.gm_now = now
    poc.gm_last = last
    log.info("menu", "key", key)
    if key == "EXIT" then
        window.set("desktop")
        --切换窗口
        return
    elseif key == "PLUS" then
        now = now - 1
        if now < 1 then
            now = #list
        end
    elseif key == "MIN" then
        now = now + 1
        if now > #list then
            now = 1
        end
    elseif key == "OK" then
        window.set(pages[now])
    end
    if opened then
        window.update()
    end
    --刷新
end
