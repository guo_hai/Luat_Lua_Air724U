require "lcd"
module(..., package.seeall)

--此窗口被开启？
local opened

--被选中的
local selected = 1

--群组获取完毕？
local groupOK = nil

local groupList = {}
local groupHeight = {{23, 42}, {44, 63}, {65, 84}}

function update(first)
    if first then --第一次开启
        selected, opened, groupList, groupOK = 1, true, {}
        sys.taskInit(
            function()
                poc.req("groups", 0, -1)
                --获取群组信息
                while true do
                    local r, d = sys.waitUntil("POC_GROUPS", 1000)
                    if not r or not d then
                        break
                    end
                    if d[2] == "0" then
                        break
                    end
                    local n = d[6]:fromHex()
                    n = common.ucs2ToGb2312(n)
                    n = common.gb2312ToUtf8(n):gsub(string.char(0), "")
                    table.insert(
                        groupList,
                        {
                            t = d[4],
                            id = d[5],
                            name = n
                        }
                    )
                    log.info("group info", n, d[4], d[5])
                    if tonumber(d[2]) == #groupList then
                        break
                    end
                end
                if not opened then
                    return
                end
                --窗口关闭了
                log.info("get group", #groupList)
                if #groupList == 0 then
                    while not msgBox.show("群组获取失败") do
                        sys.wait(1000)
                    end
                    poc.tts("群组获取失败")
                    print("群组获取失败")
                    sys.waitUntil("MSGBOX_DONE", 5000)
                    window.set("desktop")
                    return
                end
                groupOK = true
                --poc.tts(groupList[1].name)
                --念一下第一个
                window.update()
            end
        )
    end
    if not groupOK then --没获取完
        lcd.puttext("获取中……", 45, window.h(groupHeight[2][1]) + 2, 0, 0, 0)
        return
    end
    lcd.putStringCenter("群组列表", window.width / 2, window.h(2), 255, 255, 255)
    --分割线
    local f = true
    local num
    if #groupList == 1 then
        num = 44
    elseif #groupList == 2 then
        num = 65
    else
        num = 86
    end
    --第一根线
    for i = 22, num, 21 do
        disp.drawrect(f and 0 or 37, window.h(i), window.width - 1, window.h(i), lcd.rgb(255, 255, 255))
        f = nil
    end
   
    --当前页面开始显示的群组
    local start = selected
    --开始从第几个显示
    local selectTemp = 1
    --显示的是页面上的第几个
    if #groupList > 3 then --大于三个的情况下才考虑按页显示
        if #groupList - 2 < start then
            start = #groupList - 2
            selectTemp = 3 - #groupList + selected
        end
    else --小于三个
        start = 1
        selectTemp = selected
    end
    --背景
    disp.drawrect(27,window.h(groupHeight[selectTemp][1]),
                window.width-1,window.h(groupHeight[selectTemp][2]),lcd.rgb(255,255,255))
    --摆字和图标
    for i=1,3 do
        if start+i-1 > #groupList then return end--超了

        local color = (start+i-1)==selected and 0 or 255
        local name = groupList[start+i-1].name:gsub(string.char(0),"")
        if groupList[start+i-1].t == "2" then name = "临时"..name end
        if #name > 18 then
            name = name:sub(1, 18)
        end
        lcd.puttext(name,30,window.h(groupHeight[i][1])+2,color,color,color)
        --if color then disp.putimage("/lua/icon_arrow.png",27,window.h(groupHeight[i][1])+7) end
        disp.putimage("/lua/icon_group.png",2,window.h(groupHeight[i][1])+1)
    end
end

function close()
    opened = nil
    selected,groupList,groupOK = 1,{}
end

function onKey(key,pressed)
    if not pressed or not groupOK then return end
    log.info("group","key",key)
    if key == "MIN" then
        selected = selected + 1
        if selected > #groupList then selected = #groupList end
    elseif key == "PLUS" then
        selected = selected - 1
        if selected < 1 then selected = 1 end
    elseif key == "EXIT" then
        window.set("desktop")--回首页面
        return
    elseif key == "OK" then

        poc.req("entergroup", groupList[selected].id)
        log.warn("entergroup", groupList[selected].id)
        log.info("----------进入群组--------")
        poc.GroupNow = groupList[selected]
        poc.GroupNow.temp = 0
        poc.Group_up = true
        window.set("online")
        return
    end
    --poc.tts(groupList[selected].name:gsub(" ",""))
    if opened then window.update() end--刷新
end

