module(..., package.seeall)

--此窗口被开启？
local opened

--被选中的
local selected = 1
--上次按键是上
local lastUp = true

--显示的项目
local mineList = {"用户名称", "卡ICCID", "设备IMEI", "软件版本", "电池电压", "运行时间"}
--tts播报
local ttsList = {"用户名称", "卡信息", "设备编号", "软件版本", "电池电压", "运行时间"}
--每项目的高度
local listHeight = {{22, 42}, {44, 64}, {66, 84}}
--每个项目获取显示值的函数
local mineGet = {
    function()
        return poc.Name or "无用户名"
    end,
    function()
        return sim.getIccid() or "获取失败"
    end,
    function()
        return misc.getImei() or "获取失败"
    end,
    function()
        return VERSION
    end,
    function()
        local vbat = (misc.getVbatt() - 3200) * 10 / 75
        if vbat > 100 then
            vbat = 100
        end
        if vbat<1 then
            vbat=1
        end
        return tostring(vbat) .. "% " .. tostring(misc.getVbatt()) .. "mV"
    end,
    function()
        local t = os.time() - _G.startTime
        return string.format("%d:%02d:%02d", t / 3600, t / 60 % 60, t % 60)
    end
}

function update(first)
    if first then
        selected, opened, lastUp = 1, true, true
        --poc.tts(ttsList[selected])
    end
     --第一次开启
    lcd.putStringCenter("个人中心", window.width / 2, window.h(2), 255, 255, 255)
  

    if lastUp then --向上
        --分割线
        disp.drawrect(0, window.h(65), window.width - 1, window.h(65), lcd.rgb(222, 222, 222))
        --背景
        disp.drawrect(
            0,
            window.h(listHeight[1][1]),
            window.width - 1,
            window.h(listHeight[2][2]),
            lcd.rgb(255, 255, 255)
        )
        lcd.putStringCenter(mineList[selected], window.width / 2, window.h(listHeight[1][1] + 2), 0, 0, 0)
        lcd.putStringCenter(mineGet[selected](), window.width / 2, window.h(listHeight[2][1] + 2), 0, 0, 0)
        lcd.putStringCenter(mineList[selected + 1], window.width / 2, window.h(listHeight[3][1] + 2), 255, 255, 255)
    else --向下
        --分割线
        disp.drawrect(0, window.h(43), window.width - 1, window.h(43), lcd.rgb(222, 222, 222))
        --背景
        disp.drawrect(
            0,
            window.h(listHeight[2][1]),
            window.width - 1,
            window.h(listHeight[3][2]),
            lcd.rgb(255, 255, 255)
        )
        lcd.putStringCenter(mineGet[selected - 1](), window.width / 2, window.h(listHeight[1][1] + 2), 255, 255, 255)
        lcd.putStringCenter(mineList[selected], window.width / 2, window.h(listHeight[2][1] + 2), 0, 0, 0)
        lcd.putStringCenter(mineGet[selected](), window.width / 2, window.h(listHeight[3][1] + 2), 0, 0, 0)
    end
end

function close()
    opened = nil
end

function onKey(key, pressed)
    if not pressed then
        return
    end
    log.info("power", "key", key)
    if key == "PLUS" then
        lastUp = true
        selected = selected - 1
        if selected < 1 then
            selected = 1
        end
    elseif key == "MIN" then
        lastUp = false
        selected = selected + 1
        if selected > #mineList then
            selected = #mineList
        end
    elseif key == "EXIT" then
        window.set("desktop")
         --切换窗口
        return
    end
    --poc.tts(ttsList[selected])
    if opened then
        window.update()
    end
 --刷新
end
