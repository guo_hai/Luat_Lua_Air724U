require "poc"
require "lcd"
module(..., package.seeall)

--此窗口被开启？
local opened

local last, now = 1, 1
local list = {"自动升级", "手动检测", "不升级"}
local pages = {0, 1, 2}

function update(first)
    if first then --第一次开
        t = nvm.get("upset")
        for i, v in pairs(pages) do
            if v == t then
                now = i
                break
            end
        end
        opened = true
    end
    lcd.putStringCenter("升级设置", window.width / 2, window.h(2), 255, 255, 255)
    disp.drawrect(0, window.h(106), window.width - 1, window.h(106), lcd.rgb(255, 255, 255))
    --disp.clear()
    last = lcd.list(list, now, last)
    if not volume.volopened then
        disp.update()
    end
end

--关闭此页面
function close()
    opened = nil
end

function onKey(key, pressed)
    if not pressed then
        return
    end
    log.info("menu", "key", key)
    if key == "EXIT" then
        window.set("setting")
        --切换窗口
        return
    elseif key == "PLUS" then
        now = now - 1
        if now < 1 then
            now = #list
        end
    elseif key == "MIN" then
        now = now + 1
        if now > #list then
            now = 1
        end
    elseif key == "OK" then
        log.warn("setupdata", now)
        if now == 1 then
            window.pop("upo")
        end
        if pages[now] == 1 then
            local s22 = "AT+POC=upgrade"
            log.info("POC SEND", s22)
            uart.write(uart.ATC, s22)
        end
        nvm.set("upset", pages[now])
    end
    if opened then
        window.update()
    end
    --刷新
end

sys.subscribe(
    "POC_NUPDATE",
    function(msg)
        if msg[2] == "0" then
            -- poc.tts("已是最新版本")
            if opened then
                window.pop("upnew")
            end
        elseif msg[2] == "1" then
            poc.tts("升级新版本成功 十秒后重启 请勿断电")
            window.pop("upok")
            nvm.remove()
            if io.exists("/usernvm/user_audio_calib.bin") then
                log.warn("-移除文件-")
                os.remove("/usernvm/user_audio_calib.bin")
            end
            if io.exists("/usernvm/user_audio_calib_flag.bin") then
                log.warn("-移除文件-")
                os.remove("/usernvm/user_audio_calib_flag.bin")
            end
            sys.timerStart(sys.restart, 10000, "updata")
        end
    end
)
