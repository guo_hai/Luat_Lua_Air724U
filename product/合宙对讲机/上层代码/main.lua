--必须在这个位置定义PROJECT和VERSION变量
--PROJECT：ascii string类型，可以随便定义，只要不使用,就行
--VERSION：ascii string类型，如果使用Luat物联云平台固件升级的功能，必须按照"X.X.X"定义，X表示1位数字；否则可随便定义
PROJECT = "HZPOC"
VERSION = "1.0.11"
--模组商
setsolution = "airm2m"
--版本号
setsolutionversion = "1000"
--厂商
setmanufacturer = "HEZHOU"
--型号
setproductInfo = "AIR-724UG-001"

--根据固件判断模块类型
moduleType = string.find(rtos.get_version(), "8955") and 2 or 4

require "log"
--LOG_LEVEL = log.LOG_SILENT
--LOG_LEVEL = log.LOGLEVEL_WARN
--LOG_LEVEL = log.LOGLEVEL_TRACE
require "color_lcd_spi_st7735S"
require "nvm"
require "config"
nvm.init("config.lua")

require "sys"
sys.taskInit(
    function()
        while true do
            local _, light, reset = sys.waitUntil("lcdback_on")
            if reset == 1 then
                pio.pin.plus(5, 1, 200, 1, 0)
            end
            pio.pin.plus(5, 35, 1, 1, 1)
            pio.pin.plus(5, 2, 2, light, 1)
            pmd.ldoset(4, pmd.LDO_VLCD)
             -- 4 = 2.05v --2g为7
            pmd.ldoset(13, pmd.LDO_VMMC)
             -- 4 = 2.05v --2g为7 6=3.0V 13=3.0V

            sys.wait(300)
        end
    end
)

require "misc"
require "net"
misc.setClock({year = 2017, month = 2, day = 14, hour = 14, min = 2, sec = 58})

require "pm"
--pm.wake("TEST_PM")
require "wrtpoc"
require "audio"
--默认切到外放通道，如果检测到耳机会自动切换通道
audio.setChannel(0)
audio.setMicVolume(0)
require "errDump"
errDump.request("udp://ota.airm2m.com:9072")
require "pincfg"
require "poc"
ril.request("AT^TRACECTRL=0,1,1")
require "lcd"
require "window"
net.startQueryAll(60000, 60000)
startTime = os.time()
sys.taskInit(
    function()
        while os.time() < 1593751354 do
            sys.wait(1000)
            log.info("time", os.time())
        end
        log.info("settime")
        startTime = os.time()
        --开机时间戳
    end
)

--启动系统框架
sys.init(0, 0)
sys.run()

