require "common"
require "utils"
require "gps"
require "lbsLoc"
require "usbdebug"
module(..., package.seeall)
gm_now, gm_last = nil, nil
local Group_up = nil
-- 获取经纬度
local lat, lng = 0, 0
--播放tts
function tts(text)
    if not text or SayNow then
        return
    end
    sys.timerStart(
        function()
            local s = "AT+POC=LUA,TTS," .. common.utf8ToUcs2(text):toHex()
            log.info("POC SEND", s)
            uart.write(uart.ATC, s .. "\r")
        end,
        300
    )
end

--发送poc命令
function req(...)
    local data = {...}
    local s = table.concat(data, ",")
    log.warn("POC SEND", s)
    --usbdebug.write("send:"..s)
    uart.write(uart.ATC, "AT+POC=" .. s .. "\r\n")
end
function req1(...)
    local data = {...}
    local s = table.concat(data, ",")
    log.warn("POC SEND", s)
    --usbdebug.write("send:"..s)
    uart.write(uart.ATC, "AT+POC=" .. s)
end
--接收数据分发
ril.regUrc(
    "+POC",
    function(data, p)
        --if data:find("%+POC: *LUA") == 1 then return end--先不处理lua事件
        data = data:match("%+POC: *(.+)")
        log.warn("POC URC", data)
        --usbdebug.write("pocurc:"..data)
        if not data then
            return
        end --没匹配到
        local dt = data:split(",")
        sys.publish("POC_" .. dt[1]:upper(), dt)
    end
)

--心跳结果处理
local noack = 0
sys.subscribe(
    "POC_NPONG",
    function(msg)
        log.info("poc", "ping result", msg[2], noack)
        if msg[2] == "1" then
            noack = 0
        else
            noack = noack + 1
            if noack > 20 then --超过20次心跳没返回
                log.info("no ack > 20", "shut")
                noack = 0
                --link.shut()
                net.switchFly(true)
                sys.timerStart(net.switchFly, 5000, false)
            --sys.restart("no ack > 20")
            end
        end
    end
)

--用户登陆状态通知的指令
Name = nil
sys.subscribe(
    "POC_NLOGINSTAT",
    function(msg)
        log.info("POC_NLOGINSTAT", msg[1], msg[2], msg[3])
        if msg[2] ~= "2" then
            Name = nil
        else
            local n = msg[4]:fromHex()
            n = common.ucs2ToGb2312(n)
            log.info("poc name", n)
            n = common.gb2312ToUtf8(n):gsub(string.char(0), "")
            Name = n
            sys.publish("loginok")
        end
        lcd.wake()
        window.update()
        --刷新显示
    end
)
--账号错误信息提示
NERROR = nil
sys.subscribe(
    "POC_NERROR",
    function(msg)
        n = msg[2]:fromHex()
        n = common.ucs2ToGb2312(n)
        log.error("error", n)
        NERROR = common.gb2312ToUtf8(n):gsub(string.char(0), "")
        lcd.wake()
        window.update()
        --刷新显示
    end
)
--通知用户当前群组变化的信息
GroupNow = nil
GroupNow_old = nil
-- GroupNow = {
--     temp = 0,
--     id = 0,
--     name = "",
-- }
sys.subscribe(
    "POC_NCURRGROUP",
    function(msg)
        --NERROR = nil
        if msg[2] == "2" then
            log.warn("进入单呼", "记录之前群组")
            grouptmp = true
            GroupNow_old = GroupNow
        else
            grouptmp = nil
        end
        local n = msg[4]:fromHex()
        n = common.ucs2ToGb2312(n)
        log.info("切换群组消息：", n)
        Group_up = true
        sys.timerStart(
            function()
                Group_up = nil
            end,
            2000
        )
        if GroupNow ~= nil then
            --log.warn("单呼测试", GroupNow.temp)
            if GroupNow.temp == 2 then
                lcd.wake()
                window.set("desktop")
            end
        end
        n = common.gb2312ToUtf8(n):gsub(string.char(0), "")
        GroupNow = {
            temp = tonumber(msg[2]),
            id = tonumber(msg[3]),
            name = n
        }
        lcd.wake()
        window.update()
        --刷新显示
    end
)
sys.subscribe(
    "WINDOW_KEY_IND",
    function(key, pressed)
        if not GroupNow then
            return
        end
        log.info("WINDOW_KEY_IND5", key, msg)
        if pressed == true and GroupNow.temp == 2 then
            if key == "EXIT" then
                log.warn("退出单呼")
                poc.req("entergroup", GroupNow_old.id)
                return
            end
        end
    end
)

sys.subscribe(
    "POC_LUA",
    function(msg)
        if msg[2] == "TTS" then
            if msg[3] == "0" then
                log.warn("----------", SayNow)
                if not SayNow then
                    pins.setup(15, 0)
                end
            else
                pins.setup(15, 1)
            end
        end
    end
)
ptting = nil
sys.subscribe(
    "POC_PTT",
    function(msg)
        if msg[2] == "1" then
            if msg[3] == "1" then
                log.warn("呼叫成功")
                ptting = true
                pincfg.blueLed(1)
                pincfg.greenLed(0)
            else
                log.warn("关闭")
                ptting = nil
                pincfg.blueLed(0)
                pincfg.greenLed(0)
            end
        else
            pincfg.greenLed(0)
            pincfg.blueLed(0)
        end
    end
)
sys.subscribe(
    "POC_NAUD",
    function(msg)
        if msg[2] == "0" then
            pins.setup(15, 0)
            --log.warn("关闭PA")
            lcd.wake(nvm.get("lcdLight") * 1000)
            window.update()
        else
            --log.warn("打开PA")
            pins.setup(15, 1)
        end
    end
)
--讲话用户信息通知的指令
SayNow = nil
saynow_info = nil
sys.subscribe(
    "POC_NSPK",
    function(msg)
        if msg[2] == "0" and msg[3] == "0" then
            --这是本机通话
        elseif msg[3] == "-1" then
            --被叫结束
            SayNow = nil
            saynow_info = nil
            pincfg.greenLed(0)
            pincfg.blueLed(0)
        elseif msg[2] == "1" and msg[3] ~= "-1" then
            --被呼叫，但是本机可以打断
            saynow_info = nil
            ptting = nil
            pincfg.greenLed(1)
            pincfg.blueLed(0)
            local n = msg[4]:fromHex()
            n = common.ucs2ToGb2312(n)
            n = common.gb2312ToUtf8(n):gsub(string.char(0), "")
            SayNow = n
        elseif msg[2] == "0" and msg[3] ~= "-1" then
            --被叫不能打断
            saynow_info = true
            ptting = nil
            pincfg.greenLed(1)
            pincfg.blueLed(0)
            local n = msg[4]:fromHex()
            n = common.ucs2ToGb2312(n)
            n = common.gb2312ToUtf8(n):gsub(string.char(0), "")
            SayNow = n
        end

        lcd.wake()
        window.update()
        --刷新显示
    end
)
--获取群组更新后的成员信息
sys.subscribe(
    "POC_ncurrgroup",
    function ()
        req("members", GroupNow.id, 0, -1)
    end

)

--写号
function loadaccount()
    local account = nvm.get("User")         
    local f, f2 = string.match(account, "(%w%w%w%w%w%w%w%w%w%w%w%w%w%w%w%w%w%w%w)(%w?)")
    log.info("account", account)
    if f then
        f = f..(f2 or "")
        f = wrtpoc.iccid2(f)
        req("accountfirst=1")
        sys.wait(500)
        req("setaccount",f,"1")
        log.info("写入成功")
    else
        log.info("格式异常")
    end
end

--主逻辑
sys.taskInit(
    function()
        local r = sys.waitUntil("POC_RDY", 30000)
        if not r then
            poc.tts("正在重启，请勿按任意按键")
            sys.wait(3500)
            sys.restart("poc not rdy")
        end
        log.warn("poc ready")

        loadaccount()

        req("open", "1", "1")
        sys.waitUntil("POC_OPEN", 5000)
        ---------------------------------------------------------------
        local s22 = "AT+POC=debug1"
        uart.write(uart.ATC, s22)
        sys.wait(50)
        log.warn("setsolution", _G.setsolution)
        uart.write(uart.ATC, "AT+POC=setsolution," .. _G.setsolution)
        sys.wait(50)
        uart.write(uart.ATC, "AT+POC=setsolutionversion," .. _G.setsolutionversion)
        sys.wait(50)
        uart.write(uart.ATC, "AT+POC=setmanufacturer," .. _G.setmanufacturer)
        sys.wait(50)
        uart.write(uart.ATC, "AT+POC=setproductInfo," .. _G.setproductInfo)
        sys.wait(50)
        uart.write(uart.ATC, "AT+POC=writecustom,record,suf_stop_probation,200")
        sys.wait(50)
        ----------------------------------------------------------------
        req("leavetempcall", 1)
        sys.wait(100)
        poc.req("autoleavetime", nvm.get("onlinetime"))
        --poc按键
        sys.subscribe(
            "KEY_POC",
            function(msg)
                if Group_up or saynow_info then
                    return
                end
                log.warn("发送指令")
                req("ptt", msg and 1 or 0)

                lcd.wake()
                window.update()
            end
        )
    end
)

sys.taskInit(
    function()
        -- sys.wait(10000)
        local USERNVM_DIR = "/usernvm"
        local USERNVM_AUDIOCALIB_FILE_PATH = USERNVM_DIR .. "/user_audio_calib.bin"
        local USERNVM_AUDIOCALIB_SET_FILE_PATH = USERNVM_DIR .. "/user_audio_calib_flag.bin"
        if nvm.get("user_audio") == 0 then
            if io.exists("/usernvm/user_audio_calib.bin") then
                log.warn("-移除文件-")
                os.remove("/usernvm/user_audio_calib.bin")
            end
            if io.exists("/usernvm/user_audio_calib_flag.bin") then
                log.warn("-移除文件-")
                os.remove("/usernvm/user_audio_calib_flag.bin")
            end
            nvm.set("user_audio", 1)
        else
            log.warn("-nvm-", nvm.get("user_audio"))
        end

        if rtos.make_dir(USERNVM_DIR) then
            if io.exists(USERNVM_AUDIOCALIB_SET_FILE_PATH) then
                if io.exists(USERNVM_AUDIOCALIB_FILE_PATH) then
                    log.error("audioParam USERNVM_AUDIOCALIB_FILE_PATH error", USERNVM_AUDIOCALIB_FILE_PATH)
                else
                    log.error("audioParam success")
                end
            else
                os.remove(USERNVM_AUDIOCALIB_FILE_PATH)
                local userAudioParam = io.readFile("/lua/audio_calib.bin")
                io.writeFile(USERNVM_AUDIOCALIB_FILE_PATH, pack.pack("<i", userAudioParam:len()))
                io.writeFile(USERNVM_AUDIOCALIB_FILE_PATH, userAudioParam, "ab")
                io.writeFile(USERNVM_AUDIOCALIB_SET_FILE_PATH, "1")

                log.error("audioParam write, restart later...")
                -- poc.tts("正在重启，请勿按任意按键")
                -- sys.wait(3500)
                sys.restart("audioParam")
            end
        else
            log.error("audioParam make_dir error", USERNVM_DIR)
        end
    end
)
function setLocation(la, ln)
    lat, lng = la, ln
    log.info("基站定位请求的结果:", lat, lng)
end
-- 获取实时经纬度
function getRealLocation()
    lbsLoc.request(
        function(result, lat, lng, addr)
            log.info("基站定位请求结果", result)
            if result then
                setLocation(lat, lng)
                sys.publish("LOCOK")
            end
        end
    )
    return lat, lng
end
--getRealLocation()
sys.taskInit(
    function()
        while true do
            if nvm.get("gps") == 1 then
                getRealLocation()
                if sys.waitUntil("LOCOK", 10000) then
                    east8Time = misc.getClock()
                    east8Time =
                        string.format(
                        "%04d/%02d/%02d-%02d:%02d:%02d",
                        east8Time.year,
                        east8Time.month,
                        east8Time.day,
                        east8Time.hour,
                        east8Time.min,
                        east8Time.sec
                    )
                    log.info("---------------", east8Time)
                    if lat:sub(1, 1) == "0" then
                        lat = lat:sub(2, -3)
                    else
                        lat = lat:sub(1, -3)
                    end
                    req("loc", lat, lng, "1", "0", "0", "4", east8Time)
                end
            end
            sys.wait(5000)
            gps.closeAll()

            sys.wait(30000)
        end
    end
)

sys.taskInit(
    function()
        local r = sys.waitUntil("loginok", 60000)
        poc.req("setping", nvm.get("poctime"))
        while true do
            sys.wait(5000)
            if nvm.get("upset") == 0 then
                uart.write(uart.ATC, "AT+POC=upgrade")
            end
            sys.wait(24 * 60 * 60 * 1000)
        end
    end
)
sys.timerStart(
    function()
        audio.setVolume(nvm.get("volume"))
        audio.setCallVolume(nvm.get("volume"))
    end,
    500
)
