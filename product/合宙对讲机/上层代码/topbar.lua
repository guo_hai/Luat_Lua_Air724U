require("lcd")
require("misc")
require("socket")
module(..., package.seeall)
local TopBar
do
  local _class_0
  local _base_0 = {
    height = nil,
    width = nil,
    show = function(self)
      sim = nvm.get("sim")
      disp.drawrect(0, 0, self.width - 1, self.height - 1, lcd.rgb(0, 0, 0))
      local single = 0
      net.csqQueryPoll()
      local rssi = net.getRssi() or 0
      if rssi > 20 then
        single = 5
      elseif rssi > 18 then
        single = 4
      elseif rssi > 13 then
        single = 3
      elseif rssi > 10 then
        single = 2
      elseif rssi > 5 then
        single = 1
      else
        single = 0
      end
      if not (socket.isReady()) then
        single = 0
      end
      disp.putimage("/lua/sign_level_" .. tostring(single) .. ".png", 3, 5)
      --disp.putimage("/lua/sim" .. tostring(self.sim) .. ".png", 15, (self.height - 20) / 2)
      lcd.puttext("4G", 20, 1, 255, 255, 255)
      local battery = 0
      local vbat = misc.getVbatt()
      if vbat > 4000 then
        battery = 23
      elseif vbat > 3700 then
        battery = 19
      elseif vbat > 3600 then
        battery = 16
      elseif vbat > 3450 then
        battery = 13
      elseif vbat > 3300 then
        battery = 10
      else
        battery = 7
      end
      --disp.putimage("/lua/battery_level_" .. tostring(battery) .. ".png", self.width - 30, (self.height - 20) / 2)
      --disp.drawrect(107, 1, lcd.WIDTH - 3, 14, lcd.rgb(200, 200, 255))
      if battery == 7 then
        disp.drawrect(lcd.WIDTH - 23, 6, lcd.WIDTH - 6, 14, lcd.rgb(255, 0, 0))
        disp.drawrect(lcd.WIDTH - 24, 9, lcd.WIDTH - 23, 11, lcd.rgb(255, 0, 0))
      else
        if pincfg.ch_info == 0 then
          disp.drawrect(lcd.WIDTH - 23, 6, lcd.WIDTH - 6, 14, lcd.rgb(255, 255, 255))
          disp.drawrect(lcd.WIDTH - 24, 9, lcd.WIDTH - 23, 11, lcd.rgb(255, 255, 255))
        else
          disp.drawrect(lcd.WIDTH - 23, 6, lcd.WIDTH - 6, 14, lcd.rgb(18, 255, 70))
          disp.drawrect(lcd.WIDTH - 24, 9, lcd.WIDTH - 23, 11, lcd.rgb(18, 255, 70))
        end
      end
      disp.drawrect(lcd.WIDTH - 22, 7, lcd.WIDTH - battery, 13, lcd.rgb(0, 0, 0))
      --disp.putimage("/lua/gps.png", self.width - 38, (self.height - 10) / 2)
      if pincfg.lock == 1 then
        disp.putimage("/lua/lock.png", self.width - 82, 2)
      end
      local time = os.date("*t")
      time = string.format("%02d:%02d", time.hour, time.min)
      return lcd.puttext(time, self.width - 64, 2, 255, 255, 255)
    end
  }
  _base_0.__index = _base_0
  _class_0 =
    setmetatable(
    {
      __init = function(self, w, h)
        self.height = h
        self.width = w
      end,
      __base = _base_0,
      __name = "TopBar"
    },
    {
      __index = _base_0,
      __call = function(cls, ...)
        local _self_0 = setmetatable({}, _base_0)
        cls.__init(_self_0, ...)
        return _self_0
      end
    }
  )
  _base_0.__class = _class_0
  TopBar = _class_0
end
return TopBar
