require "pins"
module(..., package.seeall)

local keyName = {
    [0] = {},
    [1] = {
        [0] = "OK",
        [1] = "MIN",
        [2] = "MIN"
    },
    [2] = {
        [0] = "VOLM",
        [1] = "VOLP",
        [2] = "POC"
    },
    [3] = {},
    [4] = {},
    [255] = {
        [255] = "EXIT"
    }
}
lock = 0
local closeTimer
-- 按键消息处理函数
-- @table msg，按键消息体
--               msg.key_matrix_row：number类型，行信息
--               msg.key_matrix_col：number类型，列信息
--               msg.pressed：bool类型，true表示按下，false表示弹起
local function keyMsg(msg)
    log.info(
        "keyMsg",
        msg.key_matrix_row,
        msg.key_matrix_col,
        keyName[msg.key_matrix_row][msg.key_matrix_col],
        msg.pressed
    )

    if lcd.isWake() or keyName[msg.key_matrix_row][msg.key_matrix_col] == "POC" then --亮屏才触发
        sys.timerStop(closeTimer)
        if nvm.get("lockset") ~= 0 then
            --log.warn("键盘定时器", nvm.get("lockset"))
            closeTimer =
                sys.timerStart(
                function()
                    --log.warn("键盘定时器", nvm.get("lockset"))
                    lock = 1
                    --poc.tts("键盘已锁")
                    window.update()
                end,
                nvm.get("lockset") * 1000
            )
        end
        if keyName[msg.key_matrix_row][msg.key_matrix_col] == "KEY_POWER_ROW" then
            sys.publish("KEY_POWER", msg.pressed)
        elseif keyName[msg.key_matrix_row][msg.key_matrix_col] == "POC" then
            sys.publish("KEY_POC", msg.pressed)
        elseif keyName[msg.key_matrix_row][msg.key_matrix_col] == "OK" then
            sys.publish("KEY_OK", msg.pressed)
        elseif keyName[msg.key_matrix_row][msg.key_matrix_col] then
            if lock == 0 then
                sys.publish("WINDOW_KEY_IND", keyName[msg.key_matrix_row][msg.key_matrix_col], msg.pressed)
            elseif msg.pressed then
                poc.tts("请洁锁键盘")
            end
        end
    end
    if msg.pressed then --按下亮屏
        lcd.wake()
    --延长亮屏时间
    end
end

--注册按键消息的处理函数
rtos.on(rtos.MSG_KEYPAD, keyMsg)
--初始化键盘阵列
rtos.init_module(rtos.MOD_KEYPAD, 0, 0x1F, 0x1F)

--关机键长按关机

--长按音量键
local powerKeyTimer = nil
sys.subscribe(
    "WINDOW_KEY_IND",
    function(key, msg)
        if key ~= "EXIT" then
            return
        end
        log.info("WINDOW_KEY_IND2", key, msg)
        if msg then
        
            powerKeyTimer =
                sys.timerStart(
                function()
                    log.info("powerKey", "long press")
                    window.set("power")
                end,
                2000
            )
        else
            if powerKeyTimer then
                sys.timerStop(powerKeyTimer)
                log.info("进入菜单")
                window.set("desktop")
            end
        end
    end
)


--长按音量键
local volumeKeyTimer = nil
sys.subscribe(
    "WINDOW_KEY_IND",
    function(key, msg)
        if key ~= "VOLM" and key ~= "VOLP" then
            return
        end
        log.info("WINDOW_KEY_IND3", key, msg)
        if msg then
            window.pop("volume")
        else
            if volumeKeyTimer then
                sys.timerStop(volumeKeyTimer)
            end
        end
    end
)

local lockTimer = nil
local lockinfo = 0
sys.subscribe(
    "KEY_OK",
    function(msg)
        if msg then
            lockTimer =
                sys.timerStart(
                function()
                    lock = lock == 0 and 1 or 0
                    if lock == 1 and lcd.isWake() then
                        poc.tts("键盘已锁")
                        lockinfo = 1
                    else
                        poc.tts("键盘已洁锁")
                        lockinfo = 1
                    end
                    window.update()
                    log.warn("lock", lock)
                end,
                2000
            )
        else
            if lockTimer then
                sys.timerStop(lockTimer)
                if lock == 0 and lockinfo == 0 then
                    sys.publish("WINDOW_KEY_IND", "OK", true)
                elseif lock == 0 and lockinfo == 1 then
                    lockinfo = 0
                end
            end
        end
    end
)


--背光,1开，0关
--light参数用于设置时的亮度预览
--pwm.set(0, 100, nvm.get("light"))
backled = false
lcdBack = function(open, set, light)
    if open == 1 then
        back = lcd.isWake()
        log.info("lcd back = ", back)
        log.info("lcd light = ", light)
        if back and not light then
            log.info("lcd back is ture and light is nil")
     
            pins.setup(5, 1)
            light = true
            return
        end
        if not back then
            log.info("-点亮背光1-")
            pmd.ldoset(4, pmd.LDO_VLCD)

            pmd.ldoset(13, pmd.LDO_VMMC)

            sys.publish("lcdback_on", nvm.get("light"), 1)
            pins.setup(5, 1)

            -- pmd.ldoset(4, pmd.LDO_VLCD)
             -- 4 = 2.05v --2g为7
            -- pmd.ldoset(13, pmd.LDO_VMMC)
             -- 4 = 2.05v --2g为7 6=3.0V 13=3.0V
            backled = true
        else
            log.info("设置屏幕背光")
            sys.publish("lcdback_on", light, 1)
        end
    else
        pins.setup(5, 0)
        log.warn("-熄灭背光-")
        pins.setup(9, 0)
        pins.setup(12, 0)
        backled = false
    end
end
-- pwm.open(0)
-- sys.taskInit(function ()
--     while true do
--         for i=30,50 do
--             pwm.set(0,2000,i*8)
--             log.info("=====",i*8)
--             sys.wait(5000)
--         end
--     end
-- end)

--声音控制
outSpk = pins.setup(15, 1)
-- outSpk(0)
--outSpk(1)
-- outSpk(0)
outSpk = pins.setup(15, 1)
--pio.pin.plus(15,1,3)
--pio.pin.plus(15, 1, 1, 2, 1)
ttsply.setParm(1, 5)
--初始化两个灯
blueLed = pins.setup(11, 0)
greenLed = pins.setup(10, 0)
pio.pin.setdebounce(20)
faceled1 = pins.setup(12, 0)
faceled1 = pins.setup(9, 0)
HPTT =
    pins.setup(
    pio.P0_7,
    function(msg)
        if msg == cpu.INT_GPIO_NEGEDGE then
            log.info("HPTT弹起")
            sys.publish("KEY_POC", true)
        else
            log.info("按下")
            sys.publish("KEY_POC", false)
        end
    end,
    pio.NOPULL
)
HPTTD =
    pins.setup(
    pio.P0_23,
    function(msg)
        if msg == cpu.INT_GPIO_NEGEDGE then
            log.info("耳机插入")
            audio.setChannel(1)
        else
            log.info("耳机拔出")
            audio.setChannel(0)
        end
    end,
    pio.PULLUP
)
local level = 0

sys.taskInit(
    function()
        sys.wait(10000)
        while true do
            if misc.getVbatt() < 3400 and not poc.ptting then
                level = level == 0 and 1 or 0
                pincfg.blueLed(level)
            end
            if misc.getVbatt() > 3400 and not poc.ptting then
                pincfg.blueLed(0)
            end
            sys.wait(1000)
        end
    end
)

sys.taskInit(
    function()
        sys.wait(10000)
        while true do
            if not poc.SayNow and not poc.ptting then
                pincfg.greenLed(0)
                sys.wait(3000)
            end
            if not poc.SayNow and not poc.ptting then
                pincfg.greenLed(1)
                sys.wait(80)
            end
            if poc.SayNow or poc.ptting then
                sys.wait(30)
            end
        end
    end
)
ch_info = 0
rtos.on(
    rtos.MSG_PMD,
    function(msg)
        if msg then
            log.info("charge.proc", msg.level, msg.voltage, msg.charger, msg.state)
            if msg.charger then
                --插入vbus
                if ch_info == 0 then
                    poc.tts("正在充电")
                    ch_info = 1
                    --lcd.wake()
                    window.update()
                end
            else
                if ch_info == 1 then
                    poc.tts("充电结束")
                    ch_info = 0
                    --lcd.wake()
                    window.update()
                end
            end
        end
    end
)

ch_en = pins.setup(14, 0)
ch_st = pins.setup(18)
sys.taskInit(
    function()
        sys.wait(5000)
        pmd.init({})
        while true do
            --log.warn("-充电状态-", ch_st())
            sys.wait(5000)
        end
    end
)
