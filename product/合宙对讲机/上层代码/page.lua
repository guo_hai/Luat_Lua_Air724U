require("lcd")
require("misc")
local bar = require("topbar")
module(..., package.seeall)
local page
do
  local _class_0
  local _base_0 = {
    close = function(self, p)
      self.topBar = nil
      if self.closeF then
        return self:closeF(p)
      end
    end,
    show = function(self)
      self.topBar:show()
      --disp.drawrect(0, 20, self.w - 1, 20, lcd.rgb(0, 0, 0))
      disp.drawrect(0, self.h - 21, self.w - 1, self.h - 21, lcd.rgb(0, 0, 0))
      for i = 0, self.w - 1 do
        disp.putimage("/lua/ground.png", i, self.h - 20)
      end
      disp.setbkcolor(lcd.rgb(0, 0, 0))
      disp.drawrect(0, 20, self.w - 1, self.h - 22, lcd.rgb(0, 0, 0))
      disp.drawrect(0, 105, self.w - 1, self.h , lcd.rgb(0, 0, 0))
      if self.group then
        return lcd.puttext(self:group(), 10, 108, 255, 255, 255)
      end
    end
  }
  _base_0.__index = _base_0
  _class_0 = setmetatable({
    __init = function(self, w, h, close, getGroup)
      self.w, self.h, self.closeF, self.group = w, h, close, getGroup
      self.topBar = bar(w, 20)
    end,
    __base = _base_0,
    __name = "page"
  }, {
    __index = _base_0,
    __call = function(cls, ...)
      local _self_0 = setmetatable({}, _base_0)
      cls.__init(_self_0, ...)
      return _self_0
    end
  })
  _base_0.__class = _class_0
  page = _class_0
end
return page
