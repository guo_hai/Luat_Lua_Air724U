module(..., package.seeall)
--[[
函数名：usbreader
功能  ：向USB AT 口发送数据
参数  ：无
返回值：无
]]
-- local function write(s)
--     log.info("usb send", s)
--     uart.write(uart.USB, s)
-- end
--[[
函数名：usbreader
功能  ：从USB AT 口接收数据
参数  ：无
返回值：无
]]
local function usbreader()
    local s
    while true do
        --每次读取一行
        s = uart.read(uart.USB, "*l", 0)
        if string.len(s) ~= 0 then
            --log.info("usb rcv", s)
            poc.req1(s)
        else
            break
        end
    end
end
--  uart.setup(uart.USB, 0, 0, uart.PAR_NONE, uart.STOP_1)
--  uart.on(uart.USB, "receive", usbreader)
