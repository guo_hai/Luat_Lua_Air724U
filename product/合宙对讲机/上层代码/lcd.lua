require "common"
module(..., package.seeall)
local UIhead, UIbottom = 15, 44 --最顶部高度和底部高度
--LCD分辨率的宽度和高度(单位是像素)
WIDTH, HEIGHT, BPP = disp.getlcdinfo()
log.info("getlcdinfo", WIDTH, HEIGHT, BPP)
--1个ASCII字符宽度为8像素，高度为16像素；汉字宽度和高度都为16像素
local CHAR_WIDTH = 8

--rgb值转rgb565
function rgb(r, g, b)
    return ((r - r % 8) / 8) * 2048 + ((g - g % 4) / 4) * 32 + ((b - b % 8) / 8)
end

--放置文字，自动转码
function puttext(s, x, y, r, g, b)
    if r and g and b then
        disp.setcolor(rgb(r, g, b))
    else
        disp.setcolor(0)
    end
    disp.puttext(common.utf8ToGb2312(s), x, y)
end

--放置第一个字，自动转码
function putatext(s, x, y)
    disp.puttext(common.utf8ToGb2312(s):sub(1, 2), x, y)
end

--居中显示文字
function putStringCenter(s, x, y, r, g, b)
    local str = common.utf8ToGb2312(s)
    puttext(s, x - str:len() * ((CHAR_WIDTH - CHAR_WIDTH % 2) / 2), y, r, g, b)
end

--靠右显示文字
function putStringRight(s, x, y, r, g, b)
    local str = common.utf8ToGb2312(s)
    puttext(s, x - str:len() * CHAR_WIDTH, y, r, g, b)
end
--菜单列表显示
function list(data, item, lastHead, onin)
    if item > #data or item <= 0 or lastHead > #data or lastHead <= 0 then
        log.error("lcd.list", "error")
        return lastHead
    end

    --最多显示的项目数
    local max = HEIGHT - UIbottom
    max = (max - max % (CHAR_WIDTH * 2 + 5)) / (CHAR_WIDTH * 2 + 5) - 1

    --确定开头的项目编号
    if item < lastHead then
        lastHead = item
    elseif item > lastHead + max - 1 then
        lastHead = item - max + 1
    end
    disp.drawrect(0, 42, WIDTH, 100, rgb(0, 0, 0))
    --画分割线
    for i = 21, 80, 22 do
        disp.drawrect(0, window.h(i), window.width - 1, window.h(i), rgb(255, 255, 255))
        f = nil
    end
    local last = 2
    for i = lastHead, lastHead + max - 1 do
        if not data[i] then
            break
        end
        --防止项目数，比要显示的项目数少，导致报错
        if i == item then --如果是被选中的项目
            --画背景
            disp.drawrect(0, 22 * last - 2, WIDTH - 1, 22 * (last + 1) - 3, rgb(255, 255, 255))
            --反色显示字
            puttext(i .. "." .. data[i], 2, last * 22, 0, 0, 0)
        else
            puttext(i .. "." .. data[i], 2, last * 22, 255, 255, 255)
        end
        last = last + 1
    end
    return lastHead
end
--亮屏状态
local waked
function isWake()
    return waked
end

--上层亮屏时间为默认
local lastWake = nvm.get("lcdLight") * 1000

--休眠lcd
function sleep()
    log.info("lcd.sleep")
    pincfg.lcdBack(0)
    disp.sleep(1)
    pm.sleep("lcd.lua")
    sys.timerStopAll(sleep)
    waked = nil
    lastWake = nvm.get("lcdLight") * 1000
    log.info("lcd.sleep end")
end

local firstUpdate = true
--唤醒
--传入值为0，表示常亮
--传入值为nil表示和上次亮屏时间相同
function wake(t)
    log.info("lcd.wake", t)
    --唤醒以后首先查一次信号强度
    net.csqQueryPoll()
    if not t then
        t = lastWake
    else
        lastWake = t
    end
    pm.wake("lcd.lua")
    sys.publish("LCD_WAKE")
    disp.sleep(0)
    if t ~= 0 then
        sys.timerStopAll(sleep)
        sys.timerStart(sleep, t)
    else
        sys.timerStop(sleep)
    end
    if not firstUpdate and not wake then
        window.update()
    else
        sys.timerStart(
            function()
                waked = true
            end,
            155
        )
        log.info("lcd light", t)
        firstUpdate = nil
    end
    --打开背光
    sys.timerStart(pincfg.lcdBack, 150, 1)
    
    log.info("lcd.wake end")
end

wake()
